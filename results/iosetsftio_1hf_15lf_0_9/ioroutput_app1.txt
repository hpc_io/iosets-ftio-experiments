############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 19:57:57 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705172277.881249
TestID              : 0
StartTime           : Sat Jan 13 19:57:57 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705172637.892925
Starting IO phase at 1705172637.892946
Ending IO phase at 1705172779.573770
write     18.07      24.61      0.139165    327680     1024.00    0.090000   104.02     97.13      141.67     0   
Ending compute phase at 1705173163.364095
Starting IO phase at 1705173163.364116
Ending IO phase at 1705173210.156931
write     55.23      74.68      0.106635    327680     1024.00    0.341349   34.28      32.10      46.36      1   
Ending compute phase at 1705173570.343515
Starting IO phase at 1705173570.343534
Ending IO phase at 1705173600.248842
write     86.77      86.79      0.067479    327680     1024.00    0.066662   29.50      7.91       29.50      2   
Ending compute phase at 1705173963.409510
Starting IO phase at 1705173963.409534
Ending IO phase at 1705174018.700381
write     44.30      46.68      0.164490    327680     1024.00    3.32       54.84      3.06       57.79      3   
Ending compute phase at 1705174378.920109
Starting IO phase at 1705174378.920132
Ending IO phase at 1705174440.708496
write     41.65      41.68      0.191922    327680     1024.00    1.68       61.42      1.87       61.46      4   
Ending compute phase at 1705174807.864065
Starting IO phase at 1705174807.864083
Ending IO phase at 1705174868.328638
write     38.15      42.57      0.182206    327680     1024.00    7.57       60.14      9.24       67.10      5   
Ending compute phase at 1705175234.098164
Starting IO phase at 1705175234.098188
Ending IO phase at 1705175300.036159
write     36.00      39.07      0.202585    327680     1024.00    8.07       65.53      7.34       71.12      6   
Ending compute phase at 1705175663.451250
Starting IO phase at 1705175663.451275
Ending IO phase at 1705175729.792125
write     37.04      38.84      0.204603    327680     1024.00    4.11       65.91      3.88       69.11      7   
