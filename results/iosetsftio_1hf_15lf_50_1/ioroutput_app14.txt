############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Dec 22 01:42:16 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703205736.831752
TestID              : 0
StartTime           : Fri Dec 22 01:42:17 2023
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703206097.868952
Starting IO phase at 1703206097.868977
Ending IO phase at 1703206376.637801
write     9.16       10.07      0.629074    327680     1024.00    28.71      254.13     77.44      279.56     0   
Ending compute phase at 1703206905.245578
Starting IO phase at 1703206905.245610
Ending IO phase at 1703206996.002124
write     10.67      15.13      0.283088    327680     1024.00    149.35     169.24     61.68      239.97     1   
Ending compute phase at 1703207382.619312
Starting IO phase at 1703207382.619332
Ending IO phase at 1703207422.224644
write     65.36      65.37      0.121160    327680     1024.00    0.209655   39.16      5.69       39.17      2   
Ending compute phase at 1703207783.700211
Starting IO phase at 1703207783.700242
Ending IO phase at 1703207807.744465
write     102.72     108.30     0.070288    327680     1024.00    1.30       23.64      4.51       24.92      3   
Ending compute phase at 1703208170.690393
Starting IO phase at 1703208170.690415
Ending IO phase at 1703208196.505280
write     91.17      101.09     0.078883    327680     1024.00    3.23       25.32      3.79       28.08      4   
Ending compute phase at 1703208556.762956
Starting IO phase at 1703208556.762976
Ending IO phase at 1703208585.748208
write     90.00      90.02      0.083173    327680     1024.00    0.872053   28.44      5.73       28.44      5   
Ending compute phase at 1703208946.053081
Starting IO phase at 1703208946.053104
Ending IO phase at 1703209001.772397
write     46.10      46.18      0.170978    327680     1024.00    1.17       55.43      6.40       55.53      6   
Ending compute phase at 1703209362.590527
Starting IO phase at 1703209362.590548
Ending IO phase at 1703209422.678904
write     42.39      42.82      0.184858    327680     1024.00    0.604852   59.79      7.31       60.39      7   
