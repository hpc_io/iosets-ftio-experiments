############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Dec 22 01:42:16 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703205736.635981
TestID              : 0
StartTime           : Fri Dec 22 01:42:16 2023
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703206096.993217
Starting IO phase at 1703206096.993245
Ending IO phase at 1703206303.842113
write     12.37      14.10      0.548844    327680     1024.00    0.196038   181.51     77.59      206.98     0   
Ending compute phase at 1703206740.491435
Starting IO phase at 1703206740.491460
Ending IO phase at 1703206870.633525
write     17.12      29.61      0.250900    327680     1024.00    39.98      86.46      49.67      149.57     1   
Ending compute phase at 1703207240.418988
Starting IO phase at 1703207240.419009
Ending IO phase at 1703207262.732939
write     116.46     116.49     0.059940    327680     1024.00    0.034430   21.98      2.80       21.98      2   
Ending compute phase at 1703207622.897325
Starting IO phase at 1703207622.897345
Ending IO phase at 1703207657.352930
write     75.22      75.24      0.079487    327680     1024.00    0.186638   34.03      8.59       34.03      3   
Ending compute phase at 1703208017.535232
Starting IO phase at 1703208017.535253
Ending IO phase at 1703208054.588436
write     69.62      70.60      0.109581    327680     1024.00    0.095845   36.26      2.54       36.77      4   
Ending compute phase at 1703208414.761903
Starting IO phase at 1703208414.761924
Ending IO phase at 1703208460.740220
write     55.98      55.99      0.134956    327680     1024.00    1.38       45.72      2.54       45.73      5   
Ending compute phase at 1703208820.955836
Starting IO phase at 1703208820.955859
Ending IO phase at 1703208864.240218
write     59.54      59.56      0.128495    327680     1024.00    0.058271   42.98      3.84       42.99      6   
Ending compute phase at 1703209224.813822
Starting IO phase at 1703209224.813851
Ending IO phase at 1703209277.820945
write     48.15      48.52      0.164868    327680     1024.00    0.410418   52.76      5.49       53.17      7   
