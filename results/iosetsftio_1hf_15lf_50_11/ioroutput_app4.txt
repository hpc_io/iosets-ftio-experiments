############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 11:26:56 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705141616.560786
TestID              : 0
StartTime           : Sat Jan 13 11:26:56 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705141976.572589
Starting IO phase at 1705141976.572609
Ending IO phase at 1705142123.608728
write     17.41      19.90      0.297273    327680     1024.00    0.054032   128.64     55.05      147.02     0   
Ending compute phase at 1705142483.628429
Starting IO phase at 1705142483.628450
Ending IO phase at 1705142521.377202
write     68.66      68.67      0.107509    327680     1024.00    0.058791   37.28      5.91       37.29      1   
Ending compute phase at 1705142940.545004
Starting IO phase at 1705142940.545026
Ending IO phase at 1705143082.588357
write     14.22      18.66      0.423915    327680     1024.00    38.29      137.18     35.33      180.04     2   
Ending compute phase at 1705143483.939408
Starting IO phase at 1705143483.939433
Ending IO phase at 1705143568.384074
write     20.44      32.75      0.244247    327680     1024.00    45.02      78.16      46.46      125.25     3   
Ending compute phase at 1705143932.816937
Starting IO phase at 1705143932.816959
Ending IO phase at 1705144019.575417
write     28.21      29.59      0.264194    327680     1024.00    4.63       86.51      6.36       90.75      4   
Ending compute phase at 1705144387.197351
Starting IO phase at 1705144387.197374
Ending IO phase at 1705144430.820894
write     50.40      59.12      0.135326    327680     1024.00    8.57       43.30      6.68       50.79      5   
Ending compute phase at 1705144793.974104
Starting IO phase at 1705144793.974127
Ending IO phase at 1705144887.645966
write     26.53      27.75      0.288045    327680     1024.00    3.54       92.24      3.90       96.49      6   
Ending compute phase at 1705145251.853464
Starting IO phase at 1705145251.853485
Ending IO phase at 1705145306.153460
write     47.54      66.24      0.109556    327680     1024.00    0.061727   38.64      20.91      53.85      7   
