############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 11:26:56 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705141616.851409
TestID              : 0
StartTime           : Sat Jan 13 11:26:56 2024
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705141977.191246
Starting IO phase at 1705141977.191270
Ending IO phase at 1705142160.578469
write     13.94      16.20      0.468291    327680     1024.00    0.556010   158.07     40.44      183.65     0   
Ending compute phase at 1705142527.628797
Starting IO phase at 1705142527.628820
Ending IO phase at 1705142605.287874
write     33.10      33.10      0.173675    327680     1024.00    0.105271   77.33      24.13      77.34      1   
Ending compute phase at 1705142971.178025
Starting IO phase at 1705142971.178049
Ending IO phase at 1705144688.867947
write     1.49       1.49       5.33        327680     1024.00    6.46       1717.37    26.13      1723.06    2   
Ending compute phase at 1705145064.006879
Starting IO phase at 1705145064.006902
Ending IO phase at 1705145216.388729
write     15.34      16.85      0.462062    327680     1024.00    46.14      151.94     4.08       166.86     3   
