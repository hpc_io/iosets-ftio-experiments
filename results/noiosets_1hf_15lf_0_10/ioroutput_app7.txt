############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:14 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202954.276610
TestID              : 0
StartTime           : Sun Jan 14 04:29:14 2024
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203314.999804
Starting IO phase at 1705203314.999827
Ending IO phase at 1705203633.946711
write     8.02       8.12       0.968653    327680     1024.00    0.647274   315.16     11.41      319.33     0   
Ending compute phase at 1705203995.245935
Starting IO phase at 1705203995.245963
Ending IO phase at 1705204263.026955
write     9.53       9.57       0.836130    327680     1024.00    1.10       267.56     2.61       268.76     1   
Ending compute phase at 1705204623.924044
Starting IO phase at 1705204623.924083
Ending IO phase at 1705204821.341704
write     12.95      13.02      0.611106    327680     1024.00    0.569397   196.60     4.58       197.69     2   
Ending compute phase at 1705205181.647051
Starting IO phase at 1705205181.647065
Ending IO phase at 1705205237.006317
write     46.42      46.43      0.167293    327680     1024.00    0.098867   55.14      2.34       55.15      3   
Ending compute phase at 1705205597.231219
Starting IO phase at 1705205597.231234
Ending IO phase at 1705205652.849176
write     46.28      46.28      0.169633    327680     1024.00    0.171103   55.31      1.03       55.32      4   
Ending compute phase at 1705206014.865758
Starting IO phase at 1705206014.865791
Ending IO phase at 1705206128.925972
write     22.17      22.53      0.355035    327680     1024.00    1.85       113.61     2.33       115.47     5   
Ending compute phase at 1705206489.655756
Starting IO phase at 1705206489.655781
Ending IO phase at 1705206642.049923
write     16.77      16.83      0.474447    327680     1024.00    0.589472   152.11     1.11       152.67     6   
