############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:14 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202954.448422
TestID              : 0
StartTime           : Sun Jan 14 04:29:14 2024
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203315.385191
Starting IO phase at 1705203315.385212
Ending IO phase at 1705203644.089178
write     7.79       7.80       1.03        327680     1024.00    0.357226   328.33     6.78       328.64     0   
Ending compute phase at 1705204006.974065
Starting IO phase at 1705204006.974094
Ending IO phase at 1705204295.629497
write     8.80       8.88       0.898780    327680     1024.00    2.65       288.19     2.95       290.82     1   
Ending compute phase at 1705204657.215536
Starting IO phase at 1705204657.215561
Ending IO phase at 1705204876.596973
write     11.60      11.68      0.681700    327680     1024.00    1.43       219.26     6.31       220.65     2   
Ending compute phase at 1705205237.248527
Starting IO phase at 1705205237.248550
Ending IO phase at 1705205426.205440
write     13.54      13.59      0.588705    327680     1024.00    0.425831   188.41     3.50       189.05     3   
Ending compute phase at 1705205786.922730
Starting IO phase at 1705205786.922761
Ending IO phase at 1705205957.857892
write     14.95      15.01      0.525192    327680     1024.00    0.607835   170.51     3.95       171.28     4   
Ending compute phase at 1705206317.967032
Starting IO phase at 1705206317.967046
Ending IO phase at 1705206385.419071
write     38.15      38.16      0.172481    327680     1024.00    0.144756   67.09      11.91      67.10      5   
Ending compute phase at 1705206749.120263
Starting IO phase at 1705206749.120283
