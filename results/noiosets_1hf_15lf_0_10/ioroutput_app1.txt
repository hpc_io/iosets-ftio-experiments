############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:13 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202953.008260
TestID              : 0
StartTime           : Sun Jan 14 04:29:13 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203313.023814
Starting IO phase at 1705203313.023827
Ending IO phase at 1705203583.281998
write     9.47       9.49       0.704527    327680     1024.00    0.094586   269.86     44.79      270.25     0   
Ending compute phase at 1705203943.699299
Starting IO phase at 1705203943.699313
Ending IO phase at 1705203973.585650
write     86.15      86.19      0.090338    327680     1024.00    0.060870   29.70      3.06       29.71      1   
Ending compute phase at 1705204333.782278
Starting IO phase at 1705204333.782292
Ending IO phase at 1705204363.509165
write     87.42      87.43      0.086837    327680     1024.00    0.059457   29.28      8.21       29.28      2   
Ending compute phase at 1705204725.316300
Starting IO phase at 1705204725.316365
Ending IO phase at 1705204892.950194
write     15.16      15.30      0.522776    327680     1024.00    1.60       167.30     2.50       168.89     3   
Ending compute phase at 1705205254.193013
Starting IO phase at 1705205254.193037
Ending IO phase at 1705205439.869311
write     13.72      13.80      0.576553    327680     1024.00    1.13       185.47     2.63       186.56     4   
Ending compute phase at 1705205800.513926
Starting IO phase at 1705205800.513951
Ending IO phase at 1705205976.919804
write     14.49      14.52      0.545808    327680     1024.00    0.467073   176.26     1.62       176.73     5   
Ending compute phase at 1705206338.371510
Starting IO phase at 1705206338.371537
Ending IO phase at 1705206413.282650
write     33.74      34.26      0.232884    327680     1024.00    1.15       74.73      14.87      75.88      6   
