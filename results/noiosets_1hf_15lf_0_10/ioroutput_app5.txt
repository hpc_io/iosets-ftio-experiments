############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:14 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202954.332757
TestID              : 0
StartTime           : Sun Jan 14 04:29:14 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203315.053237
Starting IO phase at 1705203315.053257
Ending IO phase at 1705203634.289577
write     8.01       8.04       0.972347    327680     1024.00    0.650652   318.35     11.28      319.48     0   
Ending compute phase at 1705204002.865750
Starting IO phase at 1705204002.865787
Ending IO phase at 1705204281.559720
write     9.15       9.25       0.864409    327680     1024.00    1.43       276.62     10.69      279.84     1   
Ending compute phase at 1705204642.663664
Starting IO phase at 1705204642.663689
Ending IO phase at 1705204850.017354
write     12.30      12.36      0.647002    327680     1024.00    0.992994   207.04     2.08       208.11     2   
Ending compute phase at 1705205210.198886
Starting IO phase at 1705205210.198899
Ending IO phase at 1705205323.134845
write     22.68      22.72      0.323696    327680     1024.00    0.202598   112.65     9.27       112.86     3   
Ending compute phase at 1705205683.539814
Starting IO phase at 1705205683.539828
Ending IO phase at 1705205714.221561
write     84.22      84.23      0.091152    327680     1024.00    0.039709   30.39      4.17       30.40      4   
Ending compute phase at 1705206074.786760
Starting IO phase at 1705206074.786782
Ending IO phase at 1705206153.011425
write     32.70      32.86      0.243491    327680     1024.00    0.389090   77.92      2.14       78.30      5   
Ending compute phase at 1705206515.044352
Starting IO phase at 1705206515.044376
Ending IO phase at 1705206663.490048
write     17.08      17.29      0.462679    327680     1024.00    1.97       148.06     5.85       149.88     6   
