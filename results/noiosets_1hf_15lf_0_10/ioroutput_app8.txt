############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:14 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202954.076765
TestID              : 0
StartTime           : Sun Jan 14 04:29:14 2024
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203314.494887
Starting IO phase at 1705203314.494910
Ending IO phase at 1705203619.822970
write     8.38       8.39       0.935543    327680     1024.00    0.141436   305.02     7.21       305.34     0   
Ending compute phase at 1705203980.482422
Starting IO phase at 1705203980.482441
Ending IO phase at 1705204248.855047
write     9.53       9.54       0.835843    327680     1024.00    0.412059   268.25     6.70       268.66     1   
Ending compute phase at 1705204609.114559
Starting IO phase at 1705204609.114572
Ending IO phase at 1705204735.442696
write     20.27      20.31      0.331449    327680     1024.00    0.129657   126.05     57.08      126.28     2   
Ending compute phase at 1705205095.855478
Starting IO phase at 1705205095.855491
Ending IO phase at 1705205127.049764
write     82.51      82.58      0.061990    327680     1024.00    0.064776   31.00      11.18      31.03      3   
Ending compute phase at 1705205487.246433
Starting IO phase at 1705205487.246446
Ending IO phase at 1705205518.237314
write     83.67      83.68      0.089941    327680     1024.00    0.047634   30.59      7.05       30.60      4   
Ending compute phase at 1705205880.302532
Starting IO phase at 1705205880.302557
Ending IO phase at 1705206054.034608
write     14.60      14.76      0.541114    327680     1024.00    1.85       173.45     5.12       175.32     5   
Ending compute phase at 1705206414.703692
Starting IO phase at 1705206414.703713
Ending IO phase at 1705206565.338878
write     16.95      17.01      0.455806    327680     1024.00    0.545121   150.52     4.73       151.07     6   
