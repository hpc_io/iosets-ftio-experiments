############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:13 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202953.024533
TestID              : 0
StartTime           : Sun Jan 14 04:29:13 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203313.289033
Starting IO phase at 1705203313.289055
Ending IO phase at 1705203596.351190
write     9.04       9.05       0.883719    327680     1024.00    0.224943   282.86     34.64      283.23     0   
Ending compute phase at 1705203963.486754
Starting IO phase at 1705203963.486775
Ending IO phase at 1705204168.243955
write     12.10      12.52      0.630125    327680     1024.00    6.78       204.40     4.48       211.51     1   
Ending compute phase at 1705204530.480704
Starting IO phase at 1705204530.480720
Ending IO phase at 1705204570.745450
write     61.45      64.35      0.124312    327680     1024.00    1.88       39.78      5.38       41.66      2   
Ending compute phase at 1705204930.947902
Starting IO phase at 1705204930.947917
Ending IO phase at 1705204976.057052
write     57.19      57.20      0.137330    327680     1024.00    0.287258   44.76      2.16       44.76      3   
Ending compute phase at 1705205337.789496
Starting IO phase at 1705205337.789547
Ending IO phase at 1705205484.562045
write     17.30      17.48      0.444277    327680     1024.00    1.53       146.47     4.70       147.98     4   
Ending compute phase at 1705205846.222102
Starting IO phase at 1705205846.222130
Ending IO phase at 1705206028.040537
write     13.99      14.11      0.566972    327680     1024.00    1.47       181.43     2.63       182.95     5   
Ending compute phase at 1705206388.771643
Starting IO phase at 1705206388.771665
Ending IO phase at 1705206515.785188
write     20.10      20.22      0.394714    327680     1024.00    0.621515   126.61     3.70       127.38     6   
