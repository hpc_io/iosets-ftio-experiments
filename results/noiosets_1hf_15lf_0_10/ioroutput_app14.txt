############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 04:29:14 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705202954.300453
TestID              : 0
StartTime           : Sun Jan 14 04:29:14 2024
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705203314.939849
Starting IO phase at 1705203314.939872
Ending IO phase at 1705203634.364368
write     8.01       8.06       0.961068    327680     1024.00    0.649394   317.70     11.57      319.49     0   
Ending compute phase at 1705204003.018041
Starting IO phase at 1705204003.018084
Ending IO phase at 1705204288.289834
write     8.94       9.10       0.857325    327680     1024.00    1.59       281.39     12.27      286.49     1   
Ending compute phase at 1705204653.435297
Starting IO phase at 1705204653.435329
Ending IO phase at 1705204866.573901
write     11.98      12.03      0.664867    327680     1024.00    0.893559   212.76     2.96       213.66     2   
Ending compute phase at 1705205234.350984
Starting IO phase at 1705205234.351020
Ending IO phase at 1705205408.479404
write     14.09      14.73      0.540538    327680     1024.00    7.64       173.85     5.70       181.64     3   
Ending compute phase at 1705205768.714765
Starting IO phase at 1705205768.714778
Ending IO phase at 1705205894.666495
write     20.33      20.34      0.359248    327680     1024.00    0.171527   125.88     18.76      125.91     4   
Ending compute phase at 1705206254.758170
Starting IO phase at 1705206254.758183
Ending IO phase at 1705206305.948421
write     50.17      50.17      0.087392    327680     1024.00    0.055252   51.02      32.06      51.03      5   
Ending compute phase at 1705206666.095605
Starting IO phase at 1705206666.095619
Ending IO phase at 1705206719.680858
write     48.04      48.07      0.103916    327680     1024.00    0.076476   53.25      20.03      53.28      6   
