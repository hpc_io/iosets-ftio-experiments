############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 21:40:05 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705524005.332775
TestID              : 0
StartTime           : Wed Jan 17 21:40:05 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705524023.351768
Starting IO phase at 1705524023.351787
Ending IO phase at 1705524025.199715
write     71.76      72.22      0.110760    16384      1024.00    0.057774   1.77       0.017709   1.78       0   
Ending compute phase at 1705524043.429169
Starting IO phase at 1705524043.429180
Ending IO phase at 1705524045.183545
write     75.98      76.25      0.104190    16384      1024.00    0.042293   1.68       0.012416   1.68       1   
Ending compute phase at 1705524063.386901
Starting IO phase at 1705524063.386914
Ending IO phase at 1705524065.047530
write     78.70      79.00      0.098681    16384      1024.00    0.050545   1.62       0.023822   1.63       2   
Ending compute phase at 1705524083.280739
Starting IO phase at 1705524083.280748
Ending IO phase at 1705524084.983615
write     75.37      75.92      0.104650    16384      1024.00    0.041895   1.69       0.019022   1.70       3   
Ending compute phase at 1705524103.248563
Starting IO phase at 1705524103.248573
Ending IO phase at 1705524105.107523
write     72.30      72.77      0.109935    16384      1024.00    0.066816   1.76       0.018182   1.77       4   
Ending compute phase at 1705524123.329036
Starting IO phase at 1705524123.329047
Ending IO phase at 1705524125.211580
write     71.50      71.82      0.110671    16384      1024.00    0.056857   1.78       0.025889   1.79       5   
Ending compute phase at 1705524143.389932
Starting IO phase at 1705524143.389942
Ending IO phase at 1705524145.251572
write     72.34      73.10      0.108716    16384      1024.00    0.038238   1.75       0.024768   1.77       6   
Ending compute phase at 1705524163.489853
Starting IO phase at 1705524163.489863
Ending IO phase at 1705524165.271564
write     74.50      75.14      0.105738    16384      1024.00    0.046851   1.70       0.020507   1.72       7   
Ending compute phase at 1705524183.612969
Starting IO phase at 1705524183.612980
Ending IO phase at 1705524185.471565
write     71.35      72.33      0.110609    16384      1024.00    0.048463   1.77       0.019096   1.79       8   
Ending compute phase at 1705524203.740528
Starting IO phase at 1705524203.740539
Ending IO phase at 1705524205.595592
write     71.18      72.15      0.109833    16384      1024.00    0.049519   1.77       0.027923   1.80       9   
Ending compute phase at 1705524223.805652
Starting IO phase at 1705524223.805662
Ending IO phase at 1705524225.663564
write     72.19      72.43      0.107246    16384      1024.00    0.059693   1.77       0.052073   1.77       10  
Ending compute phase at 1705524243.885080
Starting IO phase at 1705524243.885091
Ending IO phase at 1705524245.798663
write     69.34      69.88      0.114479    16384      1024.00    0.055721   1.83       0.021003   1.85       11  
Ending compute phase at 1705524264.072914
Starting IO phase at 1705524264.072924
Ending IO phase at 1705524266.006955
write     70.48      70.70      0.111712    16384      1024.00    0.068634   1.81       0.023849   1.82       12  
Ending compute phase at 1705524284.252718
Starting IO phase at 1705524284.252729
Ending IO phase at 1705524286.131562
write     70.72      71.96      0.109822    16384      1024.00    0.054158   1.78       0.027590   1.81       13  
Ending compute phase at 1705524304.349093
Starting IO phase at 1705524304.349103
Ending IO phase at 1705524306.339545
write     68.58      68.96      0.115286    16384      1024.00    0.049810   1.86       0.016507   1.87       14  
Ending compute phase at 1705524324.553032
Starting IO phase at 1705524324.553043
Ending IO phase at 1705524326.247567
write     79.07      79.35      0.100093    16384      1024.00    0.056985   1.61       0.012582   1.62       15  
Ending compute phase at 1705524344.457404
Starting IO phase at 1705524344.457413
Ending IO phase at 1705524346.303559
write     74.16      74.87      0.106120    16384      1024.00    0.048718   1.71       0.022834   1.73       16  
Ending compute phase at 1705524364.579884
Starting IO phase at 1705524364.579894
Ending IO phase at 1705524366.676605
write     62.12      69.81      0.114602    16384      1024.00    0.060978   1.83       0.190911   2.06       17  
Ending compute phase at 1705524388.665051
Starting IO phase at 1705524388.665065
Ending IO phase at 1705524396.544947
write     11.41      16.68      0.361301    16384      1024.00    3.95       7.67       2.42       11.22      18  
Ending compute phase at 1705524418.580915
Starting IO phase at 1705524418.580937
Ending IO phase at 1705524434.165907
write     6.56       8.23       0.289309    16384      1024.00    15.34      15.55      10.95      19.52      19  
Ending compute phase at 1705524454.841163
Starting IO phase at 1705524454.841177
Ending IO phase at 1705524465.986634
write     9.28       11.49      0.423596    16384      1024.00    3.20       11.14      4.37       13.79      20  
Ending compute phase at 1705524488.053687
Starting IO phase at 1705524488.053704
Ending IO phase at 1705524499.924812
write     8.06       10.78      0.410732    16384      1024.00    10.59      11.87      5.30       15.89      21  
Ending compute phase at 1705524523.101562
Starting IO phase at 1705524523.101584
Ending IO phase at 1705524534.083784
write     7.96       11.67      0.416338    16384      1024.00    11.16      10.97      5.29       16.08      22  
Ending compute phase at 1705524558.055787
Starting IO phase at 1705524558.055807
Ending IO phase at 1705524569.286261
write     7.52       11.36      0.355323    16384      1024.00    12.55      11.27      5.51       17.02      23  
Ending compute phase at 1705524593.473429
Starting IO phase at 1705524593.473448
Ending IO phase at 1705524607.667580
write     6.33       9.04       0.400768    16384      1024.00    12.46      14.16      7.75       20.21      24  
Ending compute phase at 1705524631.074013
Starting IO phase at 1705524631.074033
Ending IO phase at 1705524647.538444
write     5.88       8.18       0.618657    16384      1024.00    6.74       15.65      8.09       21.75      25  
Ending compute phase at 1705524670.605424
Starting IO phase at 1705524670.605442
Ending IO phase at 1705524682.675597
write     7.58       10.63      0.459036    16384      1024.00    6.62       12.04      4.70       16.88      26  
Ending compute phase at 1705524706.233756
Starting IO phase at 1705524706.233778
Ending IO phase at 1705524716.863464
write     8.04       11.94      0.583777    16384      1024.00    6.38       10.72      5.71       15.91      27  
Ending compute phase at 1705524741.085576
Starting IO phase at 1705524741.085596
Ending IO phase at 1705524754.907518
write     6.48       9.17       0.482649    16384      1024.00    7.76       13.96      6.03       19.74      28  
Ending compute phase at 1705524779.513837
Starting IO phase at 1705524779.513858
Ending IO phase at 1705524798.235425
write     5.10       7.85       0.493389    16384      1024.00    9.87       16.30      10.78      25.11      29  
Ending compute phase at 1705524819.637804
Starting IO phase at 1705524819.637822
Ending IO phase at 1705524825.535380
write     14.25      22.27      0.339633    16384      1024.00    3.65       5.75       0.788388   8.98       30  
Ending compute phase at 1705524848.738439
Starting IO phase at 1705524848.738460
Ending IO phase at 1705524859.507409
write     8.12       11.98      0.413281    16384      1024.00    6.12       10.68      4.08       15.76      31  
Ending compute phase at 1705524885.994549
Starting IO phase at 1705524885.994569
Ending IO phase at 1705524890.047543
write     15.86      33.57      0.235294    16384      1024.00    4.31       3.81       0.490152   8.07       32  
Ending compute phase at 1705524911.909984
Starting IO phase at 1705524911.910005
Ending IO phase at 1705524926.115554
write     7.16       9.10       0.401890    16384      1024.00    10.69      14.07      7.71       17.87      33  
Ending compute phase at 1705524947.634544
Starting IO phase at 1705524947.634569
Ending IO phase at 1705524963.259488
write     6.76       8.21       0.380192    16384      1024.00    4.59       15.59      9.51       18.95      34  
Ending compute phase at 1705524988.210162
Starting IO phase at 1705524988.210176
Ending IO phase at 1705525000.767495
write     6.94       10.27      0.423305    16384      1024.00    7.28       12.47      6.91       18.44      35  
Ending compute phase at 1705525026.426496
Starting IO phase at 1705525026.426510
Ending IO phase at 1705525039.395612
write     6.28       9.93       0.361604    16384      1024.00    8.91       12.90      7.12       20.38      36  
Ending compute phase at 1705525062.550154
Starting IO phase at 1705525062.550173
Ending IO phase at 1705525075.663473
write     7.09       9.66       0.478960    16384      1024.00    6.25       13.25      5.43       18.06      37  
Ending compute phase at 1705525100.530304
Starting IO phase at 1705525100.530341
Ending IO phase at 1705525115.204135
write     6.01       8.32       0.913445    16384      1024.00    8.77       15.38      6.78       21.28      38  
Ending compute phase at 1705525138.538537
Starting IO phase at 1705525138.538558
Ending IO phase at 1705525155.007451
write     5.94       7.80       0.700474    16384      1024.00    6.21       16.41      6.07       21.56      39  
Ending compute phase at 1705525177.346377
Starting IO phase at 1705525177.346398
Ending IO phase at 1705525185.673243
write     10.29      16.14      0.495696    16384      1024.00    4.86       7.93       1.05       12.44      40  
Ending compute phase at 1705525208.634985
Starting IO phase at 1705525208.635001
Ending IO phase at 1705525218.491532
write     8.81       13.07      0.486115    16384      1024.00    5.31       9.80       2.91       14.53      41  
Ending compute phase at 1705525243.906904
Starting IO phase at 1705525243.906921
Ending IO phase at 1705525258.091471
write     6.00       9.04       0.507343    16384      1024.00    8.74       14.16      5.99       21.35      42  
Ending compute phase at 1705525279.826658
Starting IO phase at 1705525279.826681
Ending IO phase at 1705525286.500270
write     12.59      19.72      0.391950    16384      1024.00    3.93       6.49       0.364657   10.17      43  
Ending compute phase at 1705525308.226198
Starting IO phase at 1705525308.226215
Ending IO phase at 1705525321.488146
write     7.65       9.70       0.439706    16384      1024.00    5.64       13.20      6.17       16.72      44  
Ending compute phase at 1705525340.921432
Starting IO phase at 1705525340.921454
Ending IO phase at 1705525343.263878
write     35.70      56.27      0.141781    16384      1024.00    1.37       2.27       0.206787   3.59       45  
Ending compute phase at 1705525366.643118
Starting IO phase at 1705525366.643139
Ending IO phase at 1705525376.963451
write     8.29       12.48      0.439032    16384      1024.00    6.83       10.25      3.23       15.44      46  
Ending compute phase at 1705525397.352093
Starting IO phase at 1705525397.352113
Ending IO phase at 1705525401.931618
write     18.87      28.15      0.158396    16384      1024.00    2.45       4.55       1.97       6.78       47  
Ending compute phase at 1705525422.749508
Starting IO phase at 1705525422.749525
Ending IO phase at 1705525430.779641
write     12.10      16.12      0.157545    16384      1024.00    8.38       7.94       5.42       10.58      48  
Ending compute phase at 1705525478.685373
Starting IO phase at 1705525478.685395
Ending IO phase at 1705525490.299726
write     3.10       11.08      0.258734    16384      1024.00    34.19      11.55      7.41       41.25      49  
Ending compute phase at 1705525514.530958
Starting IO phase at 1705525514.530981
Ending IO phase at 1705525525.855400
write     7.41       11.44      0.469640    16384      1024.00    7.90       11.19      5.17       17.27      50  
Ending compute phase at 1705525547.706944
Starting IO phase at 1705525547.706963
Ending IO phase at 1705525556.879435
write     10.03      14.58      0.474337    16384      1024.00    4.44       8.78       1.47       12.77      51  
Ending compute phase at 1705525578.799506
Starting IO phase at 1705525578.799520
Ending IO phase at 1705525588.147460
write     9.88       13.84      0.473116    16384      1024.00    4.51       9.25       2.50       12.96      52  
Ending compute phase at 1705525609.971008
Starting IO phase at 1705525609.971030
Ending IO phase at 1705525625.562580
write     6.66       8.46       0.559803    16384      1024.00    6.56       15.14      6.59       19.21      53  
Ending compute phase at 1705525648.755362
Starting IO phase at 1705525648.755382
Ending IO phase at 1705525661.695583
write     7.15       9.94       0.429752    16384      1024.00    6.09       12.87      6.01       17.91      54  
Ending compute phase at 1705525688.459352
Starting IO phase at 1705525688.459370
Ending IO phase at 1705525703.363556
write     5.45       8.66       0.526473    16384      1024.00    11.01      14.78      6.46       23.47      55  
Ending compute phase at 1705525725.091540
Starting IO phase at 1705525725.091558
Ending IO phase at 1705525731.531451
write     12.89      20.71      0.368748    16384      1024.00    4.18       6.18       0.487824   9.93       56  
Ending compute phase at 1705525753.323913
Starting IO phase at 1705525753.323929
Ending IO phase at 1705525770.635538
write     6.14       7.40       0.689138    16384      1024.00    4.56       17.30      10.57      20.86      57  
Ending compute phase at 1705525790.759748
Starting IO phase at 1705525790.759763
Ending IO phase at 1705525797.855746
write     14.20      18.44      0.249114    16384      1024.00    2.64       6.94       3.90       9.02       58  
Ending compute phase at 1705525817.544427
Starting IO phase at 1705525817.544449
Ending IO phase at 1705525822.055621
write     21.77      29.30      0.273015    16384      1024.00    1.53       4.37       1.76       5.88       59  
Ending compute phase at 1705525843.575667
Starting IO phase at 1705525843.575685
Ending IO phase at 1705525852.059543
write     10.81      15.38      0.311752    16384      1024.00    4.71       8.32       3.46       11.84      60  
Ending compute phase at 1705525871.827778
Starting IO phase at 1705525871.827798
Ending IO phase at 1705525877.551504
write     17.62      22.60      0.272488    16384      1024.00    1.94       5.66       1.87       7.26       61  
Ending compute phase at 1705525896.993985
Starting IO phase at 1705525896.994002
Ending IO phase at 1705525899.643003
write     33.60      49.88      0.159671    16384      1024.00    1.27       2.57       0.446260   3.81       62  
Ending compute phase at 1705525922.167618
Starting IO phase at 1705525922.167641
Ending IO phase at 1705525929.811451
write     10.75      16.97      0.458429    16384      1024.00    4.98       7.54       0.229491   11.90      63  
Ending compute phase at 1705525970.768990
Starting IO phase at 1705525970.769012
Ending IO phase at 1705525982.387059
write     3.72       11.11      0.316380    16384      1024.00    22.96      11.52      6.49       34.37      64  
Ending compute phase at 1705526003.144134
Starting IO phase at 1705526003.144155
Ending IO phase at 1705526008.859444
write     15.73      22.70      0.332061    16384      1024.00    2.92       5.64       0.350964   8.14       65  
Ending compute phase at 1705526028.863785
Starting IO phase at 1705526028.863805
Ending IO phase at 1705526035.419402
write     15.56      19.97      0.240287    16384      1024.00    2.52       6.41       2.64       8.22       66  
Ending compute phase at 1705526060.004620
Starting IO phase at 1705526060.004634
Ending IO phase at 1705526066.231550
write     10.20      20.83      0.139706    16384      1024.00    8.64       6.14       3.91       12.55      67  
Ending compute phase at 1705526084.440901
Starting IO phase at 1705526084.440913
Ending IO phase at 1705526095.207559
write     11.99      12.00      0.330675    16384      1024.00    0.971082   10.67      5.38       10.68      68  
Ending compute phase at 1705526113.474750
Starting IO phase at 1705526113.474760
Ending IO phase at 1705526115.567610
write     62.75      63.16      0.078458    16384      1024.00    0.100734   2.03       0.780086   2.04       69  
Ending compute phase at 1705526133.821213
Starting IO phase at 1705526133.821224
Ending IO phase at 1705526135.903572
write     64.85      65.27      0.122567    16384      1024.00    0.055373   1.96       0.019424   1.97       70  
Ending compute phase at 1705526154.132709
Starting IO phase at 1705526154.132723
Ending IO phase at 1705526166.203740
write     10.60      10.65      0.750815    16384      1024.00    0.061575   12.01      8.08       12.07      71  
Ending compute phase at 1705526188.800110
Starting IO phase at 1705526188.800129
Ending IO phase at 1705526203.403443
write     6.76       8.82       0.512598    16384      1024.00    5.73       14.52      6.33       18.93      72  
Ending compute phase at 1705526225.292415
Starting IO phase at 1705526225.292440
Ending IO phase at 1705526235.112134
write     9.47       12.80      0.610841    16384      1024.00    5.01       10.00      3.89       13.52      73  
Ending compute phase at 1705526259.264536
Starting IO phase at 1705526259.264557
Ending IO phase at 1705526275.511458
write     5.77       7.91       0.722587    16384      1024.00    9.45       16.19      6.47       22.16      74  
Ending compute phase at 1705526295.385545
Starting IO phase at 1705526295.385567
Ending IO phase at 1705526300.431619
write     19.07      25.76      0.176906    16384      1024.00    2.23       4.97       2.15       6.71       75  
Ending compute phase at 1705526323.584424
Starting IO phase at 1705526323.584442
Ending IO phase at 1705526336.363496
write     7.23       9.98       0.446218    16384      1024.00    6.78       12.83      5.59       17.71      76  
Ending compute phase at 1705526356.872852
Starting IO phase at 1705526356.872865
Ending IO phase at 1705526362.139629
write     16.96      24.21      0.278282    16384      1024.00    3.74       5.29       0.800118   7.55       77  
Ending compute phase at 1705526382.144865
Starting IO phase at 1705526382.144887
Ending IO phase at 1705526396.447601
write     8.02       22.46      0.182272    16384      1024.00    2.41       5.70       11.31      15.97      78  
Ending compute phase at 1705526416.320396
Starting IO phase at 1705526416.320417
Ending IO phase at 1705526422.655705
write     15.89      21.24      0.235131    16384      1024.00    2.13       6.03       3.29       8.06       79  
Ending compute phase at 1705526443.792779
Starting IO phase at 1705526443.792799
Ending IO phase at 1705526447.626412
write     19.19      34.45      0.189720    16384      1024.00    3.35       3.72       0.694170   6.67       80  
Ending compute phase at 1705526468.860042
Starting IO phase at 1705526468.860063
Ending IO phase at 1705526477.730903
write     10.77      14.64      0.236655    16384      1024.00    7.72       8.74       6.03       11.88      81  
Ending compute phase at 1705526499.641476
Starting IO phase at 1705526499.641498
Ending IO phase at 1705526504.863601
write     14.53      24.91      0.206876    16384      1024.00    6.98       5.14       1.84       8.81       82  
Ending compute phase at 1705526523.204718
Starting IO phase at 1705526523.204730
Ending IO phase at 1705526524.327559
write     125.60     126.18     0.038602    16384      1024.00    0.061078   1.01       0.397285   1.02       83  
Ending compute phase at 1705526542.584450
Starting IO phase at 1705526542.584461
Ending IO phase at 1705526544.091575
write     88.90      90.32      0.074012    16384      1024.00    0.076022   1.42       0.213518   1.44       84  
Ending compute phase at 1705526562.321625
Starting IO phase at 1705526562.321638
Ending IO phase at 1705526565.259526
write     45.53      45.63      0.152081    16384      1024.00    0.623658   2.81       0.372462   2.81       85  
Ending compute phase at 1705526583.532486
Starting IO phase at 1705526583.532497
Ending IO phase at 1705526584.635701
write     115.96     116.52     0.038637    16384      1024.00    0.053409   1.10       0.481116   1.10       86  
Ending compute phase at 1705526602.929239
Starting IO phase at 1705526602.929253
Ending IO phase at 1705526606.563208
write     36.04      36.11      0.182251    16384      1024.00    0.078265   3.55       0.629881   3.55       87  
Ending compute phase at 1705526624.767796
Starting IO phase at 1705526624.767810
Ending IO phase at 1705526628.495596
write     35.19      35.25      0.131873    16384      1024.00    0.051798   3.63       1.52       3.64       88  
Ending compute phase at 1705526646.767938
Starting IO phase at 1705526646.767952
Ending IO phase at 1705526655.839583
write     14.19      14.22      0.206735    16384      1024.00    0.809115   9.00       5.69       9.02       89  
Ending compute phase at 1705526674.032987
Starting IO phase at 1705526674.033002
Ending IO phase at 1705526679.939597
write     22.01      22.06      0.127329    16384      1024.00    0.061758   5.80       3.77       5.81       90  
Ending compute phase at 1705526698.109934
Starting IO phase at 1705526698.109945
Ending IO phase at 1705526699.311609
write     112.13     113.34     0.058088    16384      1024.00    0.043891   1.13       0.206817   1.14       91  
Ending compute phase at 1705526717.556741
Starting IO phase at 1705526717.556754
Ending IO phase at 1705526724.124944
write     19.80      19.82      0.358582    16384      1024.00    0.096979   6.46       2.98       6.46       92  
Ending compute phase at 1705526742.345263
Starting IO phase at 1705526742.345279
Ending IO phase at 1705526746.195585
write     33.80      33.85      0.223509    16384      1024.00    0.070789   3.78       0.218188   3.79       93  
Ending compute phase at 1705526766.749591
Starting IO phase at 1705526766.749604
Ending IO phase at 1705526774.283593
write     13.04      17.13      0.255936    16384      1024.00    3.19       7.47       4.29       9.82       94  
Ending compute phase at 1705526792.509266
Starting IO phase at 1705526792.509279
Ending IO phase at 1705526800.501328
write     16.05      16.07      0.221322    16384      1024.00    0.058505   7.96       5.27       7.97       95  
Ending compute phase at 1705526818.774032
Starting IO phase at 1705526818.774051
Ending IO phase at 1705526822.395554
write     35.78      36.21      0.199051    16384      1024.00    0.465411   3.54       0.351011   3.58       96  
Ending compute phase at 1705526841.721083
Starting IO phase at 1705526841.721098
Ending IO phase at 1705526848.311638
write     16.50      19.50      0.184799    16384      1024.00    2.01       6.56       4.11       7.76       97  
Ending compute phase at 1705526866.564753
Starting IO phase at 1705526866.564764
Ending IO phase at 1705526874.147452
write     17.07      17.09      0.252457    16384      1024.00    0.176763   7.49       3.45       7.50       98  
Ending compute phase at 1705526895.757301
Starting IO phase at 1705526895.757321
Ending IO phase at 1705526907.331516
write     8.59       11.11      0.471520    16384      1024.00    4.09       11.52      6.81       14.90      99  
Ending compute phase at 1705526925.488623
Starting IO phase at 1705526925.488638
Ending IO phase at 1705526931.395571
write     22.04      22.06      0.167946    16384      1024.00    0.051171   5.80       3.12       5.81       100 
Ending compute phase at 1705526949.645430
Starting IO phase at 1705526949.645445
Ending IO phase at 1705526953.842919
write     31.04      31.08      0.232718    16384      1024.00    0.039493   4.12       0.406979   4.12       101 
Ending compute phase at 1705526972.068214
Starting IO phase at 1705526972.068227
Ending IO phase at 1705526980.455553
write     15.52      15.53      0.226476    16384      1024.00    0.309447   8.24       4.62       8.25       102 
Ending compute phase at 1705526998.625263
Starting IO phase at 1705526998.625278
Ending IO phase at 1705527004.851618
write     20.84      20.86      0.128794    16384      1024.00    0.049246   6.14       4.08       6.14       103 
Ending compute phase at 1705527023.029970
Starting IO phase at 1705527023.029983
Ending IO phase at 1705527024.255649
write     112.10     113.36     0.069087    16384      1024.00    0.064876   1.13       0.030396   1.14       104 
Ending compute phase at 1705527042.490355
Starting IO phase at 1705527042.490368
Ending IO phase at 1705527047.695613
write     24.92      25.01      0.158360    16384      1024.00    0.054337   5.12       2.59       5.14       105 
Ending compute phase at 1705527065.960304
Starting IO phase at 1705527065.960321
Ending IO phase at 1705527069.463555
write     37.65      37.70      0.193607    16384      1024.00    0.056359   3.40       0.298082   3.40       106 
Ending compute phase at 1705527087.606459
Starting IO phase at 1705527087.606472
Ending IO phase at 1705527095.171553
write     17.07      17.08      0.285856    16384      1024.00    0.125054   7.49       2.92       7.50       107 
Ending compute phase at 1705527113.381006
Starting IO phase at 1705527113.381020
Ending IO phase at 1705527120.111573
write     19.28      19.30      0.136329    16384      1024.00    0.377344   6.63       4.45       6.64       108 
Ending compute phase at 1705527138.313267
Starting IO phase at 1705527138.313278
Ending IO phase at 1705527139.615571
write     106.96     107.48     0.035149    16384      1024.00    0.040089   1.19       0.629264   1.20       109 
Ending compute phase at 1705527157.798050
Starting IO phase at 1705527157.798062
Ending IO phase at 1705527165.051609
write     17.86      17.87      0.256113    16384      1024.00    3.34       7.16       4.69       7.17       110 
Ending compute phase at 1705527183.248936
Starting IO phase at 1705527183.248947
Ending IO phase at 1705527187.579653
write     30.18      30.22      0.226110    16384      1024.00    3.62       4.24       1.69       4.24       111 
Ending compute phase at 1705527205.789800
Starting IO phase at 1705527205.789819
Ending IO phase at 1705527211.687540
write     22.00      22.04      0.230445    16384      1024.00    0.987632   5.81       2.13       5.82       112 
Ending compute phase at 1705527230.462101
Starting IO phase at 1705527230.462116
Ending IO phase at 1705527233.495577
write     37.12      39.80      0.184056    16384      1024.00    0.962263   3.22       1.38       3.45       113 
Ending compute phase at 1705527251.670060
Starting IO phase at 1705527251.670075
Ending IO phase at 1705527256.511587
write     26.75      26.96      0.200222    16384      1024.00    2.09       4.75       1.54       4.79       114 
Ending compute phase at 1705527275.974274
Starting IO phase at 1705527275.974290
Ending IO phase at 1705527279.835587
write     24.87      33.47      0.193998    16384      1024.00    1.59       3.82       0.721626   5.15       115 
Ending compute phase at 1705527299.931439
Starting IO phase at 1705527299.931455
Ending IO phase at 1705527303.507596
write     23.95      36.63      0.168388    16384      1024.00    2.90       3.49       0.801509   5.34       116 
Ending compute phase at 1705527323.889708
Starting IO phase at 1705527323.889722
Ending IO phase at 1705527327.291596
write     23.42      38.71      0.135180    16384      1024.00    2.20       3.31       1.14       5.46       117 
Ending compute phase at 1705527345.493306
Starting IO phase at 1705527345.493319
Ending IO phase at 1705527350.103557
write     28.28      28.37      0.205913    16384      1024.00    0.053653   4.51       1.99       4.53       118 
Ending compute phase at 1705527368.280893
Starting IO phase at 1705527368.280908
Ending IO phase at 1705527374.539562
write     20.69      22.41      0.158753    16384      1024.00    0.070823   5.71       3.64       6.19       119 
Ending compute phase at 1705527392.764811
Starting IO phase at 1705527392.764824
Ending IO phase at 1705527398.079598
write     24.43      24.46      0.112389    16384      1024.00    0.504098   5.23       3.44       5.24       120 
Ending compute phase at 1705527416.297760
Starting IO phase at 1705527416.297773
Ending IO phase at 1705527422.819604
write     19.81      19.85      0.214726    16384      1024.00    3.44       6.45       4.04       6.46       121 
Ending compute phase at 1705527441.036915
Starting IO phase at 1705527441.036927
Ending IO phase at 1705527448.803557
write     16.68      16.69      0.209549    16384      1024.00    2.83       7.67       5.27       7.67       122 
Ending compute phase at 1705527467.029779
Starting IO phase at 1705527467.029789
Ending IO phase at 1705527468.182162
write     113.76     114.42     0.065998    16384      1024.00    0.065087   1.12       0.063514   1.13       123 
Ending compute phase at 1705527486.440716
Starting IO phase at 1705527486.440729
Ending IO phase at 1705527493.963631
write     17.16      17.17      0.278346    16384      1024.00    0.082278   7.45       3.00       7.46       124 
Ending compute phase at 1705527512.178554
Starting IO phase at 1705527512.178567
Ending IO phase at 1705527518.263578
write     21.43      21.45      0.222525    16384      1024.00    0.057202   5.97       3.33       5.97       125 
Ending compute phase at 1705527536.496555
Starting IO phase at 1705527536.496568
Ending IO phase at 1705527544.123654
write     16.93      16.94      0.165084    16384      1024.00    0.050626   7.56       4.92       7.56       126 
Ending compute phase at 1705527562.368984
Starting IO phase at 1705527562.368996
Ending IO phase at 1705527571.923648
write     13.51      13.52      0.239520    16384      1024.00    0.056969   9.47       5.64       9.47       127 
Ending compute phase at 1705527590.105529
Starting IO phase at 1705527590.105541
Ending IO phase at 1705527598.387430
write     15.50      15.52      0.288834    16384      1024.00    0.105721   8.25       4.22       8.26       128 
Ending compute phase at 1705527616.572944
Starting IO phase at 1705527616.572957
Ending IO phase at 1705527624.177726
write     16.95      16.97      0.209994    16384      1024.00    0.058702   7.54       4.19       7.55       129 
Ending compute phase at 1705527644.066824
Starting IO phase at 1705527644.066838
Ending IO phase at 1705527651.159604
write     14.64      18.23      0.259853    16384      1024.00    2.92       7.02       2.87       8.74       130 
Ending compute phase at 1705527671.796360
Starting IO phase at 1705527671.796375
Ending IO phase at 1705527678.439565
write     14.32      19.49      0.398601    16384      1024.00    2.94       6.57       1.69       8.94       131 
Ending compute phase at 1705527696.892498
Starting IO phase at 1705527696.892513
Ending IO phase at 1705527702.604142
write     21.66      22.41      0.193173    16384      1024.00    0.762611   5.71       2.62       5.91       132 
Ending compute phase at 1705527723.348671
Starting IO phase at 1705527723.348687
Ending IO phase at 1705527728.589016
write     16.60      24.63      0.219298    16384      1024.00    3.54       5.20       1.69       7.71       133 
Ending compute phase at 1705527749.179873
Starting IO phase at 1705527749.179889
Ending IO phase at 1705527753.535526
write     19.23      29.69      0.240307    16384      1024.00    4.23       4.31       0.670397   6.66       134 
Ending compute phase at 1705527774.003911
Starting IO phase at 1705527774.003927
Ending IO phase at 1705527777.787522
write     21.37      34.44      0.223000    16384      1024.00    3.00       3.72       0.327046   5.99       135 
Ending compute phase at 1705527797.667832
Starting IO phase at 1705527797.667848
Ending IO phase at 1705527803.175578
write     18.07      23.60      0.232452    16384      1024.00    2.20       5.42       1.70       7.08       136 
