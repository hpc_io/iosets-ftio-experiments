############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 21:40:06 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705524006.506476
TestID              : 0
StartTime           : Wed Jan 17 21:40:06 2024
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705524927.279482
Starting IO phase at 1705524927.279501
Ending IO phase at 1705525981.545697
write     1.59       2.43       3.26        327680     1024.00    560.22     1052.63    28.14      1614.23    0   
Ending compute phase at 1705526347.364695
Starting IO phase at 1705526347.364712
Ending IO phase at 1705526483.699513
write     18.09      18.83      0.422715    327680     1024.00    59.36      135.92     0.654589   141.54     1   
Ending compute phase at 1705526844.753090
Starting IO phase at 1705526844.753108
Ending IO phase at 1705526887.990873
write     58.64      59.83      0.133115    327680     1024.00    0.927223   42.79      0.932180   43.66      2   
Ending compute phase at 1705527248.173834
Starting IO phase at 1705527248.173847
Ending IO phase at 1705527280.287360
write     80.52      80.54      0.089843    327680     1024.00    0.057336   31.79      4.37       31.79      3   
Ending compute phase at 1705527640.426975
Starting IO phase at 1705527640.426989
Ending IO phase at 1705527678.487357
write     67.81      67.82      0.110903    327680     1024.00    0.078414   37.75      2.26       37.75      4   
