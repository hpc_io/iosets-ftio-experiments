############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:24 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114764.248825
TestID              : 0
StartTime           : Sat Jan 13 03:59:24 2024
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115260.470165
Starting IO phase at 1705115260.470188
Ending IO phase at 1705115464.004163
write     7.55       11.83      0.614725    327680     1024.00    186.81     216.42     11.56      338.91     0   
Ending compute phase at 1705115947.208957
Starting IO phase at 1705115947.208979
Ending IO phase at 1705116206.448857
write     6.70       9.90       0.091325    327680     1024.00    140.40     258.52     230.04     381.86     1   
Ending compute phase at 1705116590.293536
Starting IO phase at 1705116590.293564
Ending IO phase at 1705116834.072010
write     9.58       10.50      0.656188    327680     1024.00    25.12      243.76     47.01      267.21     2   
Ending compute phase at 1705117260.970899
Starting IO phase at 1705117260.970925
Ending IO phase at 1705117450.301017
write     10.01      13.87      0.576976    327680     1024.00    71.74      184.63     23.41      255.76     3   
Ending compute phase at 1705117812.332337
Starting IO phase at 1705117812.332367
Ending IO phase at 1705117858.945447
write     53.51      59.02      0.117970    327680     1024.00    1.43       43.38      15.67      47.84      4   
Ending compute phase at 1705118224.440340
Starting IO phase at 1705118224.440368
Ending IO phase at 1705118293.616952
write     34.49      37.13      0.211892    327680     1024.00    5.64       68.94      1.68       74.22      5   
