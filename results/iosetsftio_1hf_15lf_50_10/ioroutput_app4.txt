############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:23 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114763.404471
TestID              : 0
StartTime           : Sat Jan 13 03:59:23 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115124.222067
Starting IO phase at 1705115124.222089
Ending IO phase at 1705115359.690645
write     10.85      12.12      0.541484    327680     1024.00    9.22       211.30     62.13      235.94     0   
Ending compute phase at 1705115730.350610
Starting IO phase at 1705115730.350635
Ending IO phase at 1705115819.114713
write     25.86      41.54      0.177098    327680     1024.00    12.72      61.63      34.71      98.98      1   
Ending compute phase at 1705116189.364124
Starting IO phase at 1705116189.364149
Ending IO phase at 1705116257.793611
write     32.83      37.69      0.212122    327680     1024.00    10.09      67.92      4.34       77.97      2   
Ending compute phase at 1705116706.544900
Starting IO phase at 1705116706.544924
Ending IO phase at 1705116791.210693
write     15.86      38.34      0.198742    327680     1024.00    77.39      66.77      33.07      161.41     3   
Ending compute phase at 1705117182.574515
Starting IO phase at 1705117182.574539
Ending IO phase at 1705117279.830507
write     19.98      26.89      0.219625    327680     1024.00    37.44      95.20      26.66      128.15     4   
Ending compute phase at 1705117640.027702
Starting IO phase at 1705117640.027722
Ending IO phase at 1705117663.025981
write     113.81     113.84     0.069740    327680     1024.00    0.057244   22.49      0.578348   22.49      5   
Ending compute phase at 1705118023.259407
Starting IO phase at 1705118023.259429
Ending IO phase at 1705118046.825955
write     110.86     110.89     0.062764    327680     1024.00    0.061199   23.09      3.00       23.09      6   
Ending compute phase at 1705118407.044504
Starting IO phase at 1705118407.044525
Ending IO phase at 1705118443.247568
write     71.36      71.37      0.104572    327680     1024.00    0.053970   35.87      5.63       35.88      7   
