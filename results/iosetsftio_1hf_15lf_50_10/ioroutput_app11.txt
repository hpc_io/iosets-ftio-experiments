############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:24 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_11_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114764.292456
TestID              : 0
StartTime           : Sat Jan 13 03:59:24 2024
Path                : /mnt/beegfs/iosets_11_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_11_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115219.269870
Starting IO phase at 1705115219.269899
Ending IO phase at 1705115320.434993
write     13.08      24.95      0.239314    327680     1024.00    96.59      102.63     41.76      195.66     0   
Ending compute phase at 1705115699.622787
Starting IO phase at 1705115699.622817
Ending IO phase at 1705115780.762601
write     31.22      34.80      0.192590    327680     1024.00    2.37       73.56      21.14      81.99      1   
Ending compute phase at 1705116155.699442
Starting IO phase at 1705116155.699464
Ending IO phase at 1705116222.868238
write     31.34      37.93      0.208433    327680     1024.00    16.04      67.50      5.89       81.69      2   
Ending compute phase at 1705116592.049358
Starting IO phase at 1705116592.049383
Ending IO phase at 1705116717.172256
write     19.12      22.49      0.354017    327680     1024.00    14.90      113.81     16.67      133.87     3   
Ending compute phase at 1705117155.794613
Starting IO phase at 1705117155.794638
Ending IO phase at 1705117322.741035
write     10.44      17.64      0.274897    327680     1024.00    102.92     145.09     84.62      245.12     4   
Ending compute phase at 1705117705.556467
Starting IO phase at 1705117705.556490
Ending IO phase at 1705117759.267973
write     48.01      48.06      0.166398    327680     1024.00    0.099351   53.26      21.69      53.32      5   
Ending compute phase at 1705118119.490309
Starting IO phase at 1705118119.490329
Ending IO phase at 1705118165.111771
write     56.60      56.62      0.107009    327680     1024.00    0.125705   45.21      12.00      45.23      6   
Ending compute phase at 1705118526.242779
Starting IO phase at 1705118526.242800
Ending IO phase at 1705118555.801914
write     87.53      87.61      0.089242    327680     1024.00    0.060685   29.22      4.21       29.25      7   
