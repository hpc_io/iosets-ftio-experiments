############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:24 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114764.020496
TestID              : 0
StartTime           : Sat Jan 13 03:59:24 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115175.445609
Starting IO phase at 1705115175.445632
Ending IO phase at 1705115434.189565
write     8.27       10.17      0.741283    327680     1024.00    70.93      251.61     21.38      309.55     0   
Ending compute phase at 1705115882.231650
Starting IO phase at 1705115882.231677
Ending IO phase at 1705115964.903068
write     15.72      25.72      0.226535    327680     1024.00    80.50      99.55      55.93      162.90     1   
Ending compute phase at 1705116348.516549
Starting IO phase at 1705116348.516572
Ending IO phase at 1705116484.537122
write     18.27      19.06      0.419644    327680     1024.00    15.29      134.29     44.12      140.09     2   
Ending compute phase at 1705116875.039780
Starting IO phase at 1705116875.039805
Ending IO phase at 1705116992.203339
write     17.39      21.64      0.342202    327680     1024.00    34.50      118.31     15.41      147.22     3   
Ending compute phase at 1705117426.006924
Starting IO phase at 1705117426.006947
Ending IO phase at 1705117512.595777
write     16.01      29.68      0.211656    327680     1024.00    75.51      86.25      23.97      159.87     4   
Ending compute phase at 1705117872.836322
Starting IO phase at 1705117872.836343
Ending IO phase at 1705117931.441329
write     43.89      44.34      0.122614    327680     1024.00    5.17       57.73      24.34      58.32      5   
Ending compute phase at 1705118293.080140
Starting IO phase at 1705118293.080161
Ending IO phase at 1705118344.496421
write     48.87      57.67      0.129358    327680     1024.00    1.40       44.39      20.97      52.39      6   
