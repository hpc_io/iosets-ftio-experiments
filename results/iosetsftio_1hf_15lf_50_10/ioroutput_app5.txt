############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:24 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114764.068881
TestID              : 0
StartTime           : Sat Jan 13 03:59:24 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115182.581559
Starting IO phase at 1705115182.581579
Ending IO phase at 1705115434.026237
write     8.27       9.93       0.742753    327680     1024.00    64.37      257.72     26.81      309.41     0   
Ending compute phase at 1705115854.399112
Starting IO phase at 1705115854.399136
Ending IO phase at 1705115964.791117
write     15.02      21.67      0.114672    327680     1024.00    86.97      118.12     83.71      170.41     1   
Ending compute phase at 1705116327.095300
Starting IO phase at 1705116327.095324
Ending IO phase at 1705116413.300058
write     29.50      29.81      0.265403    327680     1024.00    0.946056   85.87      1.65       86.78      2   
Ending compute phase at 1705116833.842235
Starting IO phase at 1705116833.842258
Ending IO phase at 1705116931.798869
write     16.20      21.11      0.235729    327680     1024.00    66.44      121.30     89.02      158.07     3   
Ending compute phase at 1705117381.795273
Starting IO phase at 1705117381.795297
Ending IO phase at 1705117422.187505
write     23.07      44.80      0.122626    327680     1024.00    70.90      57.15      5.29       110.99     4   
Ending compute phase at 1705117783.584353
Starting IO phase at 1705117783.584378
Ending IO phase at 1705117828.292741
write     56.25      58.03      0.063542    327680     1024.00    4.33       44.12      30.86      45.52      5   
Ending compute phase at 1705118188.495368
Starting IO phase at 1705118188.495388
Ending IO phase at 1705118226.077209
write     69.09      69.14      0.087219    327680     1024.00    0.059334   37.03      9.14       37.05      6   
