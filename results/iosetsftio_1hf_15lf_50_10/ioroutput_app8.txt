############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:24 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114764.292912
TestID              : 0
StartTime           : Sat Jan 13 03:59:24 2024
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115265.201881
Starting IO phase at 1705115265.201917
Ending IO phase at 1705115491.593590
write     6.99       11.33      0.595704    327680     1024.00    217.38     226.04     35.43      366.28     0   
Ending compute phase at 1705115964.627673
Starting IO phase at 1705115964.627696
Ending IO phase at 1705116051.442788
write     12.96      39.25      0.117836    327680     1024.00    121.57     65.22      48.73      197.58     1   
Ending compute phase at 1705116412.688431
Starting IO phase at 1705116412.688458
Ending IO phase at 1705116477.577006
write     39.08      39.75      0.200511    327680     1024.00    1.34       64.40      0.567132   65.50      2   
Ending compute phase at 1705116874.773078
Starting IO phase at 1705116874.773103
Ending IO phase at 1705116951.771988
write     22.55      33.02      0.237701    327680     1024.00    38.35      77.54      0.900397   113.54     3   
Ending compute phase at 1705117372.331573
Starting IO phase at 1705117372.331599
Ending IO phase at 1705117464.937628
write     16.78      29.47      0.241604    327680     1024.00    69.46      86.87      16.07      152.55     4   
Ending compute phase at 1705117826.100951
Starting IO phase at 1705117826.100975
Ending IO phase at 1705117872.052582
write     55.05      56.80      0.136314    327680     1024.00    1.27       45.07      2.11       46.50      5   
Ending compute phase at 1705118235.524974
Starting IO phase at 1705118235.524999
Ending IO phase at 1705118279.336376
write     54.64      59.18      0.132918    327680     1024.00    4.44       43.26      3.29       46.85      6   
