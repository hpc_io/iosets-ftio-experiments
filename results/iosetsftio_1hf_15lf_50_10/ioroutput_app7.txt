############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 03:59:23 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705114763.524471
TestID              : 0
StartTime           : Sat Jan 13 03:59:23 2024
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705115144.898062
Starting IO phase at 1705115144.898081
Ending IO phase at 1705115386.189215
write     9.77       10.63      0.707007    327680     1024.00    25.86      240.85     23.18      262.11     0   
Ending compute phase at 1705115761.681264
Starting IO phase at 1705115761.681292
Ending IO phase at 1705115852.451882
write     25.67      43.38      0.179636    327680     1024.00    10.84      59.01      43.23      99.72      1   
Ending compute phase at 1705116244.371800
Starting IO phase at 1705116244.371821
Ending IO phase at 1705116293.282826
write     50.10      52.88      0.151264    327680     1024.00    2.71       48.41      3.59       51.10      2   
Ending compute phase at 1705116705.722433
Starting IO phase at 1705116705.722457
Ending IO phase at 1705116829.704006
write     14.56      27.16      0.281112    327680     1024.00    60.47      94.26      41.44      175.85     3   
Ending compute phase at 1705117257.383681
Starting IO phase at 1705117257.383706
Ending IO phase at 1705117346.991005
write     16.33      29.24      0.269300    327680     1024.00    70.34      87.54      19.77      156.74     4   
Ending compute phase at 1705117707.892405
Starting IO phase at 1705117707.892430
Ending IO phase at 1705117759.120102
write     49.82      50.28      0.144665    327680     1024.00    0.889215   50.91      9.91       51.38      5   
Ending compute phase at 1705118119.331255
Starting IO phase at 1705118119.331277
Ending IO phase at 1705118145.817164
write     97.81      98.88      0.070379    327680     1024.00    0.051365   25.89      16.81      26.17      6   
Ending compute phase at 1705118506.048074
Starting IO phase at 1705118506.048093
Ending IO phase at 1705118532.021725
write     100.22     100.25     0.076604    327680     1024.00    0.048501   25.54      5.80       25.54      7   
