############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 22:44:09 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705527849.054256
TestID              : 0
StartTime           : Wed Jan 17 22:44:09 2024
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705528485.393458
Starting IO phase at 1705528485.393471
Ending IO phase at 1705528590.654245
write     6.72       24.43      0.327331    327680     1024.00    276.08     104.81     5.00       380.69     0   
Ending compute phase at 1705528965.079232
Starting IO phase at 1705528965.079252
Ending IO phase at 1705529020.406192
write     36.90      46.42      0.171120    327680     1024.00    14.25      55.14      0.689115   69.38      1   
Ending compute phase at 1705529386.154820
Starting IO phase at 1705529386.154839
Ending IO phase at 1705529451.616860
write     36.19      39.25      0.167018    327680     1024.00    6.40       65.23      11.78      70.74      2   
Ending compute phase at 1705529831.117903
Starting IO phase at 1705529831.117918
Ending IO phase at 1705530100.676428
write     9.51       9.51       0.100604    327680     1024.00    0.077168   269.27     237.08     269.28     3   
Ending compute phase at 1705530463.299950
Starting IO phase at 1705530463.299965
Ending IO phase at 1705531137.241149
write     3.79       3.83       2.08        327680     1024.00    2.85       668.48     18.31      676.27     4   
Ending compute phase at 1705531545.958281
Starting IO phase at 1705531545.958302
