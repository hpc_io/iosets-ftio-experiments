############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 22:44:07 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705527847.913484
TestID              : 0
StartTime           : Wed Jan 17 22:44:07 2024
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705528207.984710
Starting IO phase at 1705528207.984729
Ending IO phase at 1705528259.608050
write     49.56      49.62      0.156820    327680     1024.00    0.199132   51.59      4.53       51.66      0   
Ending compute phase at 1705528619.688286
Starting IO phase at 1705528619.688301
Ending IO phase at 1705528671.946023
write     49.31      49.32      0.161698    327680     1024.00    0.049408   51.90      13.76      51.92      1   
Ending compute phase at 1705529032.174001
Starting IO phase at 1705529032.174016
Ending IO phase at 1705529076.888874
write     57.60      57.62      0.134390    327680     1024.00    0.119839   44.43      3.49       44.45      2   
Ending compute phase at 1705529441.662160
Starting IO phase at 1705529441.662183
Ending IO phase at 1705529500.170147
write     40.75      43.94      0.181486    327680     1024.00    5.50       58.26      0.399425   62.82      3   
Ending compute phase at 1705529862.162674
Starting IO phase at 1705529862.162697
Ending IO phase at 1705529923.118916
write     41.00      42.43      0.188016    327680     1024.00    2.25       60.34      5.84       62.43      4   
Ending compute phase at 1705530283.320849
Starting IO phase at 1705530283.320862
Ending IO phase at 1705530334.103200
write     50.71      50.71      0.146599    327680     1024.00    0.096548   50.48      5.87       50.48      5   
Ending compute phase at 1705530699.448433
Starting IO phase at 1705530699.448453
Ending IO phase at 1705530761.661683
write     38.23      41.41      0.187729    327680     1024.00    5.74       61.82      5.86       66.96      6   
Ending compute phase at 1705531137.085936
Starting IO phase at 1705531137.085954
Ending IO phase at 1705531181.327604
write     43.31      58.36      0.137063    327680     1024.00    15.22      43.86      14.07      59.11      7   
Ending compute phase at 1705531544.454226
Starting IO phase at 1705531544.454245
Ending IO phase at 1705531608.654381
write     38.28      40.07      0.192658    327680     1024.00    3.71       63.88      8.23       66.88      8   
