############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 22:44:07 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705527847.920666
TestID              : 0
StartTime           : Wed Jan 17 22:44:07 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705527865.932247
Starting IO phase at 1705527865.932268
Ending IO phase at 1705527867.367774
write     95.26      95.72      0.072142    16384      1024.00    0.058941   1.34       0.183704   1.34       0   
Ending compute phase at 1705527885.603059
Starting IO phase at 1705527885.603072
Ending IO phase at 1705527886.919529
write     102.72     103.32     0.075644    16384      1024.00    0.052347   1.24       0.029279   1.25       1   
Ending compute phase at 1705527905.179913
Starting IO phase at 1705527905.179928
Ending IO phase at 1705527906.503508
write     100.78     101.90     0.075369    16384      1024.00    0.060985   1.26       0.596993   1.27       2   
Ending compute phase at 1705527924.729719
Starting IO phase at 1705527924.729733
Ending IO phase at 1705527926.086209
write     102.71     103.24     0.076782    16384      1024.00    0.060847   1.24       0.034959   1.25       3   
Ending compute phase at 1705527944.342697
Starting IO phase at 1705527944.342708
Ending IO phase at 1705527945.808173
write     94.25      97.01      0.082464    16384      1024.00    0.071978   1.32       0.035641   1.36       4   
Ending compute phase at 1705527964.003232
Starting IO phase at 1705527964.003243
Ending IO phase at 1705527965.327558
write     103.99     104.45     0.066587    16384      1024.00    0.048794   1.23       0.160933   1.23       5   
Ending compute phase at 1705527983.537389
Starting IO phase at 1705527983.537400
Ending IO phase at 1705527984.939533
write     96.49      96.91      0.078636    16384      1024.00    0.032533   1.32       0.103531   1.33       6   
Ending compute phase at 1705528003.155749
Starting IO phase at 1705528003.155774
Ending IO phase at 1705528004.527592
write     101.02     101.53     0.076472    16384      1024.00    0.037703   1.26       0.583528   1.27       7   
Ending compute phase at 1705528022.772143
Starting IO phase at 1705528022.772154
Ending IO phase at 1705528024.139605
write     96.29      96.81      0.071942    16384      1024.00    0.057476   1.32       0.166613   1.33       8   
Ending compute phase at 1705528042.422852
Starting IO phase at 1705528042.422864
Ending IO phase at 1705528043.847577
write     97.43      99.21      0.080619    16384      1024.00    0.057217   1.29       0.028898   1.31       9   
Ending compute phase at 1705528062.068868
Starting IO phase at 1705528062.068878
Ending IO phase at 1705528063.559555
write     91.03      91.86      0.053524    16384      1024.00    0.053403   1.39       0.544917   1.41       10  
Ending compute phase at 1705528081.809346
Starting IO phase at 1705528081.809356
Ending IO phase at 1705528083.275578
write     93.53      93.92      0.071951    16384      1024.00    0.054235   1.36       0.212398   1.37       11  
Ending compute phase at 1705528101.604879
Starting IO phase at 1705528101.604889
Ending IO phase at 1705528103.014389
write     97.20      97.61      0.076600    16384      1024.00    0.046471   1.31       0.132155   1.32       12  
Ending compute phase at 1705528121.237169
Starting IO phase at 1705528121.237181
Ending IO phase at 1705528122.627576
write     96.98      97.38      0.068569    16384      1024.00    0.045822   1.31       0.217912   1.32       13  
Ending compute phase at 1705528140.800774
Starting IO phase at 1705528140.800784
Ending IO phase at 1705528142.159645
write     97.16      97.57      0.070915    16384      1024.00    0.044698   1.31       0.177942   1.32       14  
Ending compute phase at 1705528160.437488
Starting IO phase at 1705528160.437499
Ending IO phase at 1705528161.767574
write     99.83      100.74     0.038320    16384      1024.00    0.057119   1.27       0.663550   1.28       15  
Ending compute phase at 1705528180.025341
Starting IO phase at 1705528180.025351
Ending IO phase at 1705528181.371710
write     100.61     101.07     0.049153    16384      1024.00    0.056871   1.27       0.480785   1.27       16  
Ending compute phase at 1705528199.593297
Starting IO phase at 1705528199.593308
Ending IO phase at 1705528201.107574
write     92.39      92.78      0.084791    16384      1024.00    0.046975   1.38       0.023831   1.39       17  
Ending compute phase at 1705528221.464656
Starting IO phase at 1705528221.464683
Ending IO phase at 1705528228.447613
write     14.06      18.34      0.271805    16384      1024.00    2.53       6.98       3.05       9.11       18  
Ending compute phase at 1705528249.139058
Starting IO phase at 1705528249.139077
Ending IO phase at 1705528258.795608
write     10.39      13.25      0.499534    16384      1024.00    7.46       9.66       6.59       12.32      19  
Ending compute phase at 1705528279.127785
Starting IO phase at 1705528279.127803
Ending IO phase at 1705528282.853590
write     21.27      34.13      0.227150    16384      1024.00    2.51       3.75       0.250295   6.02       20  
Ending compute phase at 1705528303.208387
Starting IO phase at 1705528303.208408
Ending IO phase at 1705528309.317880
write     15.22      21.27      0.217015    16384      1024.00    2.84       6.02       2.62       8.41       21  
Ending compute phase at 1705528329.952470
Starting IO phase at 1705528329.952495
Ending IO phase at 1705528334.655665
write     17.86      27.74      0.192571    16384      1024.00    3.41       4.61       1.58       7.17       22  
Ending compute phase at 1705528355.260487
Starting IO phase at 1705528355.260511
Ending IO phase at 1705528361.867721
write     14.19      21.33      0.311220    16384      1024.00    2.78       6.00       1.85       9.02       23  
Ending compute phase at 1705528381.628167
Starting IO phase at 1705528381.628190
Ending IO phase at 1705528385.207554
write     25.04      40.14      0.169973    16384      1024.00    1.88       3.19       0.800294   5.11       24  
Ending compute phase at 1705528405.456048
Starting IO phase at 1705528405.456068
Ending IO phase at 1705528409.263588
write     21.79      34.25      0.179601    16384      1024.00    2.50       3.74       0.868432   5.87       25  
Ending compute phase at 1705528430.607673
Starting IO phase at 1705528430.607695
Ending IO phase at 1705528435.939578
write     15.24      24.66      0.193699    16384      1024.00    3.39       5.19       2.38       8.40       26  
Ending compute phase at 1705528456.432247
Starting IO phase at 1705528456.432269
Ending IO phase at 1705528461.007450
write     18.77      28.64      0.255749    16384      1024.00    3.47       4.47       0.383397   6.82       27  
Ending compute phase at 1705528481.308282
Starting IO phase at 1705528481.308305
Ending IO phase at 1705528485.631611
write     19.96      30.20      0.238114    16384      1024.00    3.36       4.24       0.929881   6.41       28  
Ending compute phase at 1705528506.876498
Starting IO phase at 1705528506.876522
Ending IO phase at 1705528512.835534
write     14.39      21.85      0.285028    16384      1024.00    3.08       5.86       1.30       8.89       29  
Ending compute phase at 1705528533.372669
Starting IO phase at 1705528533.372690
Ending IO phase at 1705528541.831532
write     11.95      15.32      0.282164    16384      1024.00    6.83       8.36       3.89       10.71      30  
Ending compute phase at 1705528560.224797
Starting IO phase at 1705528560.224816
Ending IO phase at 1705528564.495513
write     29.82      31.10      0.225725    16384      1024.00    0.881620   4.12       0.511972   4.29       31  
Ending compute phase at 1705528582.709506
Starting IO phase at 1705528582.709519
Ending IO phase at 1705528589.195581
write     19.96      20.00      0.208186    16384      1024.00    0.067746   6.40       3.08       6.41       32  
Ending compute phase at 1705528607.378179
Starting IO phase at 1705528607.378190
Ending IO phase at 1705528608.635580
write     106.91     107.46     0.071591    16384      1024.00    0.041505   1.19       0.046513   1.20       33  
Ending compute phase at 1705528629.592713
Starting IO phase at 1705528629.592728
Ending IO phase at 1705528637.455533
write     12.25      16.49      0.263369    16384      1024.00    3.75       7.76       3.56       10.45      34  
Ending compute phase at 1705528658.665299
Starting IO phase at 1705528658.665319
Ending IO phase at 1705528665.471554
write     13.16      19.06      0.253095    16384      1024.00    3.58       6.71       2.67       9.73       35  
Ending compute phase at 1705528686.201646
Starting IO phase at 1705528686.201665
Ending IO phase at 1705528695.531520
write     10.77      13.83      0.268130    16384      1024.00    3.57       9.26       5.00       11.88      36  
Ending compute phase at 1705528716.397175
Starting IO phase at 1705528716.397196
Ending IO phase at 1705528724.804685
write     11.58      15.29      0.325924    16384      1024.00    3.28       8.37       3.17       11.05      37  
Ending compute phase at 1705528747.145162
Starting IO phase at 1705528747.145185
Ending IO phase at 1705528753.611540
write     12.22      20.00      0.270594    16384      1024.00    8.49       6.40       2.07       10.48      38  
Ending compute phase at 1705528774.638122
Starting IO phase at 1705528774.638144
Ending IO phase at 1705528779.659510
write     16.38      25.85      0.231911    16384      1024.00    3.33       4.95       1.25       7.82       39  
Ending compute phase at 1705528800.308942
Starting IO phase at 1705528800.308964
Ending IO phase at 1705528806.531526
write     14.95      20.31      0.252775    16384      1024.00    2.72       6.30       2.10       8.56       40  
Ending compute phase at 1705528827.133044
Starting IO phase at 1705528827.133064
Ending IO phase at 1705528833.267418
write     15.05      21.18      0.247716    16384      1024.00    3.45       6.04       2.12       8.50       41  
Ending compute phase at 1705528854.259152
Starting IO phase at 1705528854.259171
Ending IO phase at 1705528859.971481
write     15.26      22.85      0.276929    16384      1024.00    3.30       5.60       2.30       8.39       42  
Ending compute phase at 1705528881.067539
Starting IO phase at 1705528881.067561
Ending IO phase at 1705528886.551599
write     15.32      23.39      0.186993    16384      1024.00    3.43       5.47       2.48       8.36       43  
Ending compute phase at 1705528907.069681
Starting IO phase at 1705528907.069702
Ending IO phase at 1705528912.171518
write     17.57      25.45      0.243963    16384      1024.00    2.78       5.03       1.14       7.29       44  
Ending compute phase at 1705528932.941042
Starting IO phase at 1705528932.941064
Ending IO phase at 1705528939.831658
write     13.64      19.22      0.329429    16384      1024.00    3.08       6.66       1.58       9.38       45  
Ending compute phase at 1705528959.218050
Starting IO phase at 1705528959.218072
Ending IO phase at 1705528965.119703
write     18.23      21.84      0.215604    16384      1024.00    2.89       5.86       2.42       7.02       46  
Ending compute phase at 1705528985.873982
Starting IO phase at 1705528985.874002
Ending IO phase at 1705528992.378453
write     14.26      19.89      0.257763    16384      1024.00    3.48       6.43       2.31       8.98       47  
Ending compute phase at 1705529013.045673
Starting IO phase at 1705529013.045690
Ending IO phase at 1705529019.915524
write     13.89      18.41      0.293744    16384      1024.00    2.88       6.95       2.87       9.22       48  
Ending compute phase at 1705529039.993299
Starting IO phase at 1705529039.993314
Ending IO phase at 1705529047.619554
write     13.54      16.92      0.269452    16384      1024.00    3.38       7.56       3.25       9.45       49  
Ending compute phase at 1705529068.465626
Starting IO phase at 1705529068.465640
Ending IO phase at 1705529076.703570
write     11.91      15.73      0.290222    16384      1024.00    3.77       8.14       3.50       10.75      50  
Ending compute phase at 1705529097.330175
Starting IO phase at 1705529097.330190
Ending IO phase at 1705529105.203514
write     12.48      16.42      0.271487    16384      1024.00    3.42       7.80       3.45       10.26      51  
Ending compute phase at 1705529126.437799
Starting IO phase at 1705529126.437817
Ending IO phase at 1705529134.591501
write     11.59      15.86      0.319354    16384      1024.00    5.18       8.07       2.96       11.05      52  
Ending compute phase at 1705529155.662438
Starting IO phase at 1705529155.662454
Ending IO phase at 1705529163.611522
write     12.02      16.45      0.338151    16384      1024.00    3.82       7.78       3.42       10.65      53  
Ending compute phase at 1705529184.698128
Starting IO phase at 1705529184.698145
Ending IO phase at 1705529193.023508
write     11.54      15.58      0.296568    16384      1024.00    3.98       8.21       3.47       11.09      54  
Ending compute phase at 1705529214.113599
Starting IO phase at 1705529214.113622
Ending IO phase at 1705529221.819892
write     12.20      16.78      0.341269    16384      1024.00    3.97       7.63       3.11       10.49      55  
Ending compute phase at 1705529242.722006
Starting IO phase at 1705529242.722025
Ending IO phase at 1705529250.267556
write     12.61      17.34      0.461447    16384      1024.00    3.84       7.38       2.64       10.15      56  
Ending compute phase at 1705529271.777885
Starting IO phase at 1705529271.777904
Ending IO phase at 1705529278.467545
write     13.64      19.32      0.333623    16384      1024.00    3.94       6.62       1.29       9.39       57  
Ending compute phase at 1705529298.625710
Starting IO phase at 1705529298.625732
Ending IO phase at 1705529305.643585
write     14.31      18.46      0.226976    16384      1024.00    2.41       6.94       3.32       8.95       58  
Ending compute phase at 1705529326.285807
Starting IO phase at 1705529326.285830
Ending IO phase at 1705529334.583450
write     11.98      15.58      0.313180    16384      1024.00    4.73       8.22       3.23       10.69      59  
Ending compute phase at 1705529356.284104
Starting IO phase at 1705529356.284128
Ending IO phase at 1705529361.751878
write     14.45      23.77      0.240737    16384      1024.00    3.98       5.38       1.55       8.86       60  
Ending compute phase at 1705529385.074148
Starting IO phase at 1705529385.074174
Ending IO phase at 1705529389.259511
write     13.83      31.34      0.237760    16384      1024.00    6.11       4.08       0.300039   9.25       61  
Ending compute phase at 1705529412.445581
Starting IO phase at 1705529412.445602
Ending IO phase at 1705529416.611552
write     14.15      31.22      0.202324    16384      1024.00    5.67       4.10       0.870121   9.05       62  
Ending compute phase at 1705529438.802504
Starting IO phase at 1705529438.802526
Ending IO phase at 1705529444.803511
write     12.93      21.56      0.246734    16384      1024.00    5.22       5.94       1.99       9.90       63  
Ending compute phase at 1705529465.770133
Starting IO phase at 1705529465.770147
Ending IO phase at 1705529471.735540
write     14.74      22.15      0.273693    16384      1024.00    3.37       5.78       1.52       8.68       64  
Ending compute phase at 1705529492.562861
Starting IO phase at 1705529492.562880
Ending IO phase at 1705529499.827512
write     13.04      17.84      0.315146    16384      1024.00    3.19       7.17       3.20       9.82       65  
Ending compute phase at 1705529520.798316
Starting IO phase at 1705529520.798333
Ending IO phase at 1705529526.779578
write     14.85      21.72      0.272866    16384      1024.00    3.86       5.89       2.18       8.62       66  
Ending compute phase at 1705529546.454655
Starting IO phase at 1705529546.454687
Ending IO phase at 1705529553.899434
write     14.46      17.35      0.317492    16384      1024.00    1.93       7.38       2.30       8.85       67  
Ending compute phase at 1705529574.322544
Starting IO phase at 1705529574.322564
Ending IO phase at 1705529582.051541
write     12.94      16.73      0.278894    16384      1024.00    2.81       7.65       3.19       9.89       68  
Ending compute phase at 1705529603.187104
Starting IO phase at 1705529603.187124
Ending IO phase at 1705529611.243509
write     11.72      16.04      0.336293    16384      1024.00    4.37       7.98       3.38       10.93      69  
Ending compute phase at 1705529632.206425
Starting IO phase at 1705529632.206444
Ending IO phase at 1705529639.543562
write     12.83      17.69      0.285012    16384      1024.00    3.94       7.24       2.68       9.98       70  
Ending compute phase at 1705529659.002070
Starting IO phase at 1705529659.002088
Ending IO phase at 1705529666.919516
write     14.48      16.28      0.288801    16384      1024.00    1.65       7.86       3.24       8.84       71  
Ending compute phase at 1705529687.378253
Starting IO phase at 1705529687.378272
Ending IO phase at 1705529696.661472
write     11.11      14.90      0.266326    16384      1024.00    2.82       8.59       4.96       11.53      72  
Ending compute phase at 1705529717.311899
Starting IO phase at 1705529717.311919
Ending IO phase at 1705529726.155608
write     11.35      15.10      0.284036    16384      1024.00    3.62       8.48       4.25       11.28      73  
Ending compute phase at 1705529747.006718
Starting IO phase at 1705529747.006739
Ending IO phase at 1705529756.359509
write     10.74      13.79      0.338844    16384      1024.00    3.71       9.29       3.88       11.92      74  
Ending compute phase at 1705529777.282547
Starting IO phase at 1705529777.282566
Ending IO phase at 1705529785.875482
write     11.38      15.10      0.347291    16384      1024.00    5.49       8.48       2.92       11.25      75  
Ending compute phase at 1705529806.931246
Starting IO phase at 1705529806.931268
Ending IO phase at 1705529814.063574
write     12.92      18.48      0.277438    16384      1024.00    3.61       6.93       3.72       9.90       76  
Ending compute phase at 1705529835.410706
Starting IO phase at 1705529835.410722
Ending IO phase at 1705529844.607675
write     10.41      14.18      0.334575    16384      1024.00    4.10       9.03       4.61       12.30      77  
Ending compute phase at 1705529864.887122
Starting IO phase at 1705529864.887144
Ending IO phase at 1705529870.267559
write     17.43      25.83      0.309763    16384      1024.00    2.43       4.96       0.339968   7.34       78  
Ending compute phase at 1705529891.663158
Starting IO phase at 1705529891.663188
Ending IO phase at 1705529896.011589
write     17.07      29.31      0.267699    16384      1024.00    3.69       4.37       1.19       7.50       79  
Ending compute phase at 1705529918.791050
Starting IO phase at 1705529918.791068
Ending IO phase at 1705529922.920063
write     15.01      32.27      0.232081    16384      1024.00    5.62       3.97       0.323668   8.53       80  
Ending compute phase at 1705529943.699408
Starting IO phase at 1705529943.699427
Ending IO phase at 1705529951.801537
write     12.06      16.07      0.338134    16384      1024.00    4.00       7.97       2.63       10.62      81  
Ending compute phase at 1705529973.683692
Starting IO phase at 1705529973.683714
Ending IO phase at 1705529981.075579
write     11.63      17.86      0.281897    16384      1024.00    4.55       7.17       2.85       11.01      82  
Ending compute phase at 1705530003.135575
Starting IO phase at 1705530003.135597
Ending IO phase at 1705530009.951499
write     12.10      18.99      0.301138    16384      1024.00    5.03       6.74       1.93       10.58      83  
Ending compute phase at 1705530032.847045
Starting IO phase at 1705530032.847068
Ending IO phase at 1705530040.071485
write     10.83      17.92      0.278040    16384      1024.00    6.57       7.14       2.70       11.81      84  
Ending compute phase at 1705530062.867267
Starting IO phase at 1705530062.867290
Ending IO phase at 1705530068.995516
write     12.02      21.09      0.307830    16384      1024.00    5.59       6.07       1.15       10.65      85  
Ending compute phase at 1705530091.248759
Starting IO phase at 1705530091.248779
Ending IO phase at 1705530098.467527
write     11.47      18.25      0.226360    16384      1024.00    7.80       7.01       3.49       11.16      86  
Ending compute phase at 1705530119.296609
Starting IO phase at 1705530119.296628
Ending IO phase at 1705530126.141516
write     13.62      18.91      0.320543    16384      1024.00    4.42       6.77       1.66       9.40       87  
Ending compute phase at 1705530146.983704
Starting IO phase at 1705530146.983727
Ending IO phase at 1705530154.679512
write     12.48      16.77      0.297540    16384      1024.00    5.41       7.63       2.87       10.26      88  
Ending compute phase at 1705530175.387556
Starting IO phase at 1705530175.387580
Ending IO phase at 1705530183.480924
write     12.12      16.14      0.260138    16384      1024.00    4.62       7.93       3.88       10.56      89  
Ending compute phase at 1705530205.148273
Starting IO phase at 1705530205.148295
Ending IO phase at 1705530213.611546
write     10.83      15.99      0.289680    16384      1024.00    3.79       8.01       3.77       11.82      90  
Ending compute phase at 1705530234.417485
Starting IO phase at 1705530234.417507
Ending IO phase at 1705530240.891531
write     14.15      20.03      0.266785    16384      1024.00    3.50       6.39       2.12       9.05       91  
Ending compute phase at 1705530261.959940
Starting IO phase at 1705530261.959957
Ending IO phase at 1705530270.635585
write     11.15      15.97      0.305276    16384      1024.00    4.14       8.02       3.73       11.48      92  
Ending compute phase at 1705530291.119979
Starting IO phase at 1705530291.119993
Ending IO phase at 1705530301.275696
write     10.30      12.61      0.326658    16384      1024.00    3.18       10.15      4.93       12.43      93  
Ending compute phase at 1705530322.316178
Starting IO phase at 1705530322.316193
Ending IO phase at 1705530331.219515
write     11.47      14.47      0.350814    16384      1024.00    3.23       8.85       3.97       11.16      94  
Ending compute phase at 1705530351.552184
Starting IO phase at 1705530351.552198
Ending IO phase at 1705530362.047524
write     10.22      12.29      0.425669    16384      1024.00    3.44       10.41      4.64       12.52      95  
Ending compute phase at 1705530383.044071
Starting IO phase at 1705530383.044090
Ending IO phase at 1705530393.983492
write     9.40       11.84      0.303462    16384      1024.00    3.76       10.81      5.98       13.61      96  
Ending compute phase at 1705530415.279837
Starting IO phase at 1705530415.279851
Ending IO phase at 1705530425.207462
write     9.87       13.01      0.334400    16384      1024.00    4.36       9.84       5.25       12.97      97  
Ending compute phase at 1705530446.284181
Starting IO phase at 1705530446.284195
Ending IO phase at 1705530454.019523
write     12.09      16.67      0.262612    16384      1024.00    7.75       7.68       3.48       10.59      98  
Ending compute phase at 1705530475.052490
Starting IO phase at 1705530475.052504
Ending IO phase at 1705530483.123611
write     11.82      17.41      0.280247    16384      1024.00    7.33       7.35       3.49       10.83      99  
Ending compute phase at 1705530503.981927
Starting IO phase at 1705530503.981945
Ending IO phase at 1705530510.993849
write     13.25      18.36      0.318888    16384      1024.00    3.05       6.97       2.83       9.66       100 
Ending compute phase at 1705530534.044235
Starting IO phase at 1705530534.044275
Ending IO phase at 1705530545.307669
write     8.01       11.55      0.319588    16384      1024.00    5.98       11.09      6.06       15.98      101 
Ending compute phase at 1705530567.616498
Starting IO phase at 1705530567.616519
Ending IO phase at 1705530575.434273
write     10.78      16.34      0.364929    16384      1024.00    4.65       7.84       2.85       11.87      102 
Ending compute phase at 1705530597.144791
Starting IO phase at 1705530597.144813
Ending IO phase at 1705530605.650188
write     10.61      15.35      0.323996    16384      1024.00    4.47       8.34       3.24       12.07      103 
Ending compute phase at 1705530627.004235
Starting IO phase at 1705530627.004251
Ending IO phase at 1705530639.991547
write     7.98       9.84       0.459609    16384      1024.00    3.91       13.01      6.60       16.05      104 
Ending compute phase at 1705530661.316842
Starting IO phase at 1705530661.316864
Ending IO phase at 1705530671.879541
write     9.42       12.20      0.514087    16384      1024.00    4.40       10.49      4.41       13.58      105 
Ending compute phase at 1705530692.396539
Starting IO phase at 1705530692.396562
Ending IO phase at 1705530697.859420
write     16.62      26.81      0.265171    16384      1024.00    2.78       4.77       1.80       7.70       106 
Ending compute phase at 1705530718.773367
Starting IO phase at 1705530718.773391
Ending IO phase at 1705530722.721919
write     19.28      35.13      0.185485    16384      1024.00    3.02       3.64       0.931377   6.64       107 
Ending compute phase at 1705530744.884591
Starting IO phase at 1705530744.884614
Ending IO phase at 1705530752.587573
write     11.16      16.98      0.471062    16384      1024.00    8.53       7.54       2.94       11.47      108 
Ending compute phase at 1705530774.293011
Starting IO phase at 1705530774.293031
Ending IO phase at 1705530784.391557
write     9.44       12.76      0.354557    16384      1024.00    8.92       10.03      5.01       13.56      109 
Ending compute phase at 1705530805.625024
Starting IO phase at 1705530805.625044
Ending IO phase at 1705530815.023553
write     10.38      13.78      0.280586    16384      1024.00    7.50       9.29       5.79       12.33      110 
Ending compute phase at 1705530836.337367
Starting IO phase at 1705530836.337390
Ending IO phase at 1705530845.865888
write     10.12      13.62      0.261711    16384      1024.00    4.19       9.39       5.29       12.64      111 
Ending compute phase at 1705530867.872996
Starting IO phase at 1705530867.873017
Ending IO phase at 1705530879.587453
write     8.27       11.67      0.685480    16384      1024.00    5.11       10.97      5.88       15.47      112 
Ending compute phase at 1705530901.169763
Starting IO phase at 1705530901.169785
Ending IO phase at 1705530911.831647
write     9.15       12.12      0.330807    16384      1024.00    4.23       10.56      5.27       13.99      113 
Ending compute phase at 1705530932.572972
Starting IO phase at 1705530932.572991
Ending IO phase at 1705530942.499471
write     10.34      13.16      0.391385    16384      1024.00    3.58       9.73       3.60       12.38      114 
Ending compute phase at 1705530963.237105
Starting IO phase at 1705530963.237125
Ending IO phase at 1705530974.023610
write     9.68       12.02      0.461357    16384      1024.00    4.25       10.65      4.03       13.23      115 
Ending compute phase at 1705530995.076912
Starting IO phase at 1705530995.076939
Ending IO phase at 1705531005.078949
write     9.99       12.92      0.393477    16384      1024.00    3.91       9.90       3.66       12.81      116 
Ending compute phase at 1705531026.734268
Starting IO phase at 1705531026.734291
Ending IO phase at 1705531036.535515
write     9.74       13.11      0.368246    16384      1024.00    4.57       9.77       3.78       13.14      117 
Ending compute phase at 1705531057.865982
Starting IO phase at 1705531057.866003
Ending IO phase at 1705531066.835587
write     10.60      14.36      0.308504    16384      1024.00    4.20       8.91       3.99       12.08      118 
Ending compute phase at 1705531088.921678
Starting IO phase at 1705531088.921711
Ending IO phase at 1705531098.879523
write     9.32       13.00      0.356867    16384      1024.00    4.51       9.85       4.15       13.74      119 
Ending compute phase at 1705531127.843887
Starting IO phase at 1705531127.843912
Ending IO phase at 1705531142.991593
write     4.96       8.96       0.577091    16384      1024.00    19.73      14.29      11.11      25.83      120 
Ending compute phase at 1705531164.214556
Starting IO phase at 1705531164.214578
Ending IO phase at 1705531174.127543
write     9.94       12.72      0.404110    16384      1024.00    6.79       10.06      6.09       12.88      121 
Ending compute phase at 1705531197.889634
Starting IO phase at 1705531197.889654
Ending IO phase at 1705531208.335567
write     8.02       12.36      0.347645    16384      1024.00    6.95       10.36      4.80       15.96      122 
Ending compute phase at 1705531230.457634
Starting IO phase at 1705531230.457655
Ending IO phase at 1705531243.303549
write     7.65       10.02      0.457831    16384      1024.00    11.27      12.78      5.46       16.73      123 
Ending compute phase at 1705531264.637730
Starting IO phase at 1705531264.637752
Ending IO phase at 1705531273.462064
write     10.77      14.61      0.302325    16384      1024.00    4.20       8.76       3.93       11.89      124 
Ending compute phase at 1705531296.114480
Starting IO phase at 1705531296.114503
Ending IO phase at 1705531304.439510
write     10.04      15.57      0.513966    16384      1024.00    4.77       8.22       3.71       12.74      125 
Ending compute phase at 1705531326.437608
Starting IO phase at 1705531326.437638
Ending IO phase at 1705531339.791547
write     7.47       9.96       0.477694    16384      1024.00    5.64       12.85      6.51       17.14      126 
Ending compute phase at 1705531360.382471
Starting IO phase at 1705531360.382491
Ending IO phase at 1705531369.039516
write     11.57      15.03      0.311800    16384      1024.00    3.51       8.52       3.61       11.07      127 
Ending compute phase at 1705531391.454155
Starting IO phase at 1705531391.454174
Ending IO phase at 1705531402.411471
write     8.49       11.78      0.290177    16384      1024.00    5.37       10.87      6.26       15.07      128 
Ending compute phase at 1705531423.214290
Starting IO phase at 1705531423.214308
Ending IO phase at 1705531433.567605
write     9.93       12.56      0.310554    16384      1024.00    4.13       10.19      5.31       12.89      129 
Ending compute phase at 1705531455.646503
Starting IO phase at 1705531455.646526
Ending IO phase at 1705531466.492341
write     8.75       11.89      0.346497    16384      1024.00    4.93       10.77      6.18       14.63      130 
Ending compute phase at 1705531487.458711
Starting IO phase at 1705531487.458730
Ending IO phase at 1705531499.554091
write     8.61       10.69      0.365041    16384      1024.00    3.97       11.98      6.23       14.86      131 
Ending compute phase at 1705531521.026329
Starting IO phase at 1705531521.026347
Ending IO phase at 1705531531.119481
write     9.63       12.67      0.294572    16384      1024.00    3.78       10.10      5.33       13.29      132 
Ending compute phase at 1705531554.754228
Starting IO phase at 1705531554.754248
Ending IO phase at 1705531565.382871
write     7.93       12.08      0.402009    16384      1024.00    11.97      10.59      5.21       16.13      133 
Ending compute phase at 1705531588.846244
Starting IO phase at 1705531588.846263
Ending IO phase at 1705531603.739516
write     6.36       8.64       0.538477    16384      1024.00    6.43       14.81      9.07       20.11      134 
Ending compute phase at 1705531625.558142
Starting IO phase at 1705531625.558167
Ending IO phase at 1705531640.343534
write     6.99       8.68       0.572994    16384      1024.00    4.66       14.75      8.12       18.32      135 
