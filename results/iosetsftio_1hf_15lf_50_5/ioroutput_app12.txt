############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 22:05:39 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705179939.118432
TestID              : 0
StartTime           : Sat Jan 13 22:05:39 2024
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705180300.018601
Starting IO phase at 1705180300.018627
Ending IO phase at 1705180483.249343
write     13.91      15.10      0.421552    327680     1024.00    11.22      169.54     48.27      184.04     0   
Ending compute phase at 1705180843.399901
Starting IO phase at 1705180843.399922
Ending IO phase at 1705180896.695985
write     48.33      48.33      0.082551    327680     1024.00    0.062904   52.96      26.55      52.97      1   
Ending compute phase at 1705181260.124763
Starting IO phase at 1705181260.124788
Ending IO phase at 1705181306.353270
write     52.18      52.84      0.143169    327680     1024.00    3.25       48.45      5.54       49.06      2   
Ending compute phase at 1705181683.813231
Starting IO phase at 1705181683.813254
Ending IO phase at 1705181843.028959
write     14.53      16.11      0.495758    327680     1024.00    17.75      158.90     11.12      176.20     3   
Ending compute phase at 1705182223.090515
Starting IO phase at 1705182223.090541
Ending IO phase at 1705182284.452488
write     31.70      38.42      0.186997    327680     1024.00    19.96      66.63      3.27       80.75      4   
Ending compute phase at 1705182660.035445
Starting IO phase at 1705182660.035468
Ending IO phase at 1705182734.275392
write     28.63      34.91      0.227103    327680     1024.00    16.74      73.33      12.37      89.42      5   
Ending compute phase at 1705183112.060778
Starting IO phase at 1705183112.060804
Ending IO phase at 1705183305.855285
write     12.12      13.99      0.538649    327680     1024.00    18.45      182.97     32.80      211.28     6   
Ending compute phase at 1705183700.225998
Starting IO phase at 1705183700.226023
