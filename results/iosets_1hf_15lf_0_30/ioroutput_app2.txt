############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 03:28:12 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705372092.511203
TestID              : 0
StartTime           : Tue Jan 16 03:28:12 2024
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705372452.716072
Starting IO phase at 1705372452.716089
Ending IO phase at 1705372529.282969
write     33.38      33.39      0.228974    327680     1024.00    0.271725   76.67      6.32       76.69      0   
Ending compute phase at 1705372891.328946
Starting IO phase at 1705372891.328964
Ending IO phase at 1705372984.834195
write     26.85      27.51      0.289351    327680     1024.00    2.15       93.06      4.91       95.34      1   
Ending compute phase at 1705373344.978861
Starting IO phase at 1705373344.978875
Ending IO phase at 1705373367.521235
write     115.74     115.76     0.052387    327680     1024.00    0.067637   22.11      5.35       22.12      2   
Ending compute phase at 1705373727.683476
Starting IO phase at 1705373727.683489
Ending IO phase at 1705373750.225400
write     115.39     115.44     0.050541    327680     1024.00    0.056526   22.18      6.01       22.19      3   
Ending compute phase at 1705374110.366011
Starting IO phase at 1705374110.366025
Ending IO phase at 1705374135.208596
write     104.63     104.66     0.065381    327680     1024.00    0.070531   24.46      3.54       24.47      4   
Ending compute phase at 1705374495.395365
Starting IO phase at 1705374495.395381
Ending IO phase at 1705374532.249226
write     70.20      70.21      0.105440    327680     1024.00    0.058154   36.46      2.72       36.47      5   
Ending compute phase at 1705374892.447212
Starting IO phase at 1705374892.447226
Ending IO phase at 1705374933.128661
write     63.52      63.53      0.120666    327680     1024.00    0.067771   40.29      1.68       40.30      6   
Ending compute phase at 1705375293.312043
Starting IO phase at 1705375293.312060
Ending IO phase at 1705375346.260000
write     48.59      48.60      0.159370    327680     1024.00    0.281875   52.67      4.30       52.68      7   
Ending compute phase at 1705375706.414921
Starting IO phase at 1705375706.414936
Ending IO phase at 1705375762.675659
write     45.71      45.71      0.156687    327680     1024.00    0.194325   56.00      5.87       56.01      8   
