############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 17:37:24 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696174644.956190
TestID              : 0
StartTime           : Sun Oct  1 17:37:25 2023
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696175005.495476
Starting IO phase at 1696175005.495501
Ending IO phase at 1696175401.417759
write     6.46       6.49       1.22        327680     1024.00    0.433131   394.34     9.04       396.01     0   
Ending compute phase at 1696175764.910745
Starting IO phase at 1696175764.910770
Ending IO phase at 1696176099.836468
write     7.62       7.65       1.02        327680     1024.00    1.16       334.78     10.31      335.87     1   
Ending compute phase at 1696176460.348167
Starting IO phase at 1696176460.348187
Ending IO phase at 1696176721.664700
write     9.79       9.80       0.775189    327680     1024.00    0.346375   261.25     14.01      261.59     2   
Ending compute phase at 1696177081.831570
Starting IO phase at 1696177081.831583
Ending IO phase at 1696177121.508928
write     64.96      64.98      0.111795    327680     1024.00    0.075174   39.40      7.59       39.41      3   
Ending compute phase at 1696177481.683774
Starting IO phase at 1696177481.683788
Ending IO phase at 1696177504.569662
write     112.93     112.96     0.055389    327680     1024.00    0.071613   22.66      4.94       22.67      4   
Ending compute phase at 1696177864.739043
Starting IO phase at 1696177864.739057
Ending IO phase at 1696177887.881569
write     111.81     111.84     0.056222    327680     1024.00    0.065681   22.89      4.90       22.90      5   
Ending compute phase at 1696178250.738540
Starting IO phase at 1696178250.738555
Ending IO phase at 1696178281.689219
write     76.80      83.38      0.095424    327680     1024.00    2.64       30.70      3.27       33.34      6   
