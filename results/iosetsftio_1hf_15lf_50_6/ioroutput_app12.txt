############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Dec 21 19:19:03 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703182743.504081
TestID              : 0
StartTime           : Thu Dec 21 19:19:03 2023
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703183132.420289
Starting IO phase at 1703183132.420310
Ending IO phase at 1703183298.646343
write     13.14      15.87      0.416703    327680     1024.00    46.86      161.28     32.86      194.83     0   
Ending compute phase at 1703183658.859236
Starting IO phase at 1703183658.859257
Ending IO phase at 1703183683.566336
write     105.15     105.18     0.068112    327680     1024.00    0.055183   24.34      2.54       24.35      1   
Ending compute phase at 1703184043.816021
Starting IO phase at 1703184043.816041
Ending IO phase at 1703184069.327803
write     101.62     101.64     0.069025    327680     1024.00    0.050741   25.19      3.10       25.19      2   
Ending compute phase at 1703184429.518344
Starting IO phase at 1703184429.518365
Ending IO phase at 1703184473.535778
write     58.59      58.60      0.126542    327680     1024.00    0.092483   43.69      3.69       43.70      3   
Ending compute phase at 1703184838.603257
Starting IO phase at 1703184838.603282
Ending IO phase at 1703184894.055725
write     42.69      44.82      0.172035    327680     1024.00    4.89       57.12      3.77       59.97      4   
Ending compute phase at 1703185254.262323
Starting IO phase at 1703185254.262344
Ending IO phase at 1703185307.842060
write     48.15      48.15      0.138828    327680     1024.00    0.154990   53.16      8.74       53.17      5   
Ending compute phase at 1703185668.062342
Starting IO phase at 1703185668.062363
Ending IO phase at 1703185726.863081
write     43.69      44.54      0.176349    327680     1024.00    0.130913   57.47      3.34       58.60      6   
Ending compute phase at 1703186088.653849
Starting IO phase at 1703186088.653865
Ending IO phase at 1703186146.718168
write     43.09      44.46      0.178409    327680     1024.00    1.66       57.58      1.28       59.41      7   
Ending compute phase at 1703186523.332780
Starting IO phase at 1703186523.332806
