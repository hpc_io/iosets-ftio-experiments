############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Dec 21 19:19:03 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703182743.635800
TestID              : 0
StartTime           : Thu Dec 21 19:19:03 2023
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703183142.908427
Starting IO phase at 1703183142.908449
Ending IO phase at 1703183343.364724
write     10.70      12.96      0.586229    327680     1024.00    55.15      197.60     14.83      239.25     0   
Ending compute phase at 1703183704.149343
Starting IO phase at 1703183704.149364
Ending IO phase at 1703183755.011451
write     50.07      52.62      0.147413    327680     1024.00    2.36       48.65      7.77       51.13      1   
Ending compute phase at 1703184132.284529
Starting IO phase at 1703184132.284550
Ending IO phase at 1703184158.436959
write     99.73      99.76      0.076872    327680     1024.00    0.050269   25.66      1.03       25.67      2   
Ending compute phase at 1703184519.194938
Starting IO phase at 1703184519.194960
Ending IO phase at 1703184550.948804
write     80.57      82.02      0.097051    327680     1024.00    1.63       31.21      5.81       31.77      3   
Ending compute phase at 1703184912.758662
Starting IO phase at 1703184912.758685
Ending IO phase at 1703184966.694848
write     46.44      47.81      0.156636    327680     1024.00    5.68       53.54      5.57       55.13      4   
Ending compute phase at 1703185332.978724
Starting IO phase at 1703185332.978747
Ending IO phase at 1703185387.154850
write     42.82      43.99      0.161788    327680     1024.00    6.62       58.19      18.52      59.78      5   
Ending compute phase at 1703185753.721222
Starting IO phase at 1703185753.721248
Ending IO phase at 1703185810.724996
write     40.72      45.67      0.167570    327680     1024.00    8.17       56.05      4.89       62.87      6   
Ending compute phase at 1703186171.329290
Starting IO phase at 1703186171.329314
Ending IO phase at 1703186232.819337
write     41.63      41.86      0.190408    327680     1024.00    0.372218   61.15      1.60       61.50      7   
