############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Dec 21 19:19:03 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703182743.106948
TestID              : 0
StartTime           : Thu Dec 21 19:19:03 2023
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703183103.680549
Starting IO phase at 1703183103.680572
Ending IO phase at 1703183225.277682
write     20.98      23.67      0.333044    327680     1024.00    0.528059   108.17     27.86      122.05     0   
Ending compute phase at 1703183602.397504
Starting IO phase at 1703183602.397531
Ending IO phase at 1703183647.518922
write     56.42      57.35      0.136072    327680     1024.00    0.926449   44.64      3.24       45.38      1   
Ending compute phase at 1703184009.498719
Starting IO phase at 1703184009.498742
Ending IO phase at 1703184037.932880
write     86.46      90.71      0.087001    327680     1024.00    1.78       28.22      3.59       29.61      2   
Ending compute phase at 1703184400.270616
Starting IO phase at 1703184400.270639
Ending IO phase at 1703184449.215769
write     50.52      52.75      0.151627    327680     1024.00    2.43       48.53      5.45       50.67      3   
Ending compute phase at 1703184814.607475
Starting IO phase at 1703184814.607497
Ending IO phase at 1703184867.327099
write     44.59      48.95      0.120902    327680     1024.00    6.17       52.30      13.64      57.41      4   
Ending compute phase at 1703185227.499231
Starting IO phase at 1703185227.499252
Ending IO phase at 1703185279.000793
write     50.03      50.04      0.146808    327680     1024.00    0.107159   51.16      4.18       51.17      5   
Ending compute phase at 1703185639.311223
Starting IO phase at 1703185639.311243
Ending IO phase at 1703185701.941428
write     41.11      44.39      0.175729    327680     1024.00    0.097183   57.67      15.61      62.27      6   
Ending compute phase at 1703186062.138776
Starting IO phase at 1703186062.138798
Ending IO phase at 1703186116.694304
write     47.18      47.19      0.168626    327680     1024.00    0.779528   54.25      2.78       54.26      7   
Ending compute phase at 1703186492.484321
Starting IO phase at 1703186492.484343
