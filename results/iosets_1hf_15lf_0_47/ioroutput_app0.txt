############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 19:32:07 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705516327.359726
TestID              : 0
StartTime           : Wed Jan 17 19:32:07 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705516345.388414
Starting IO phase at 1705516345.388436
Ending IO phase at 1705516346.582307
write     116.67     117.29     0.051406    16384      1024.00    0.060879   1.09       0.269920   1.10       0   
Ending compute phase at 1705516364.779992
Starting IO phase at 1705516364.780003
Ending IO phase at 1705516366.079547
write     105.41     106.32     0.008095    16384      1024.00    0.048280   1.20       1.08       1.21       1   
Ending compute phase at 1705516384.288576
Starting IO phase at 1705516384.288587
Ending IO phase at 1705516385.519537
write     111.27     112.64     0.070312    16384      1024.00    0.037634   1.14       0.432353   1.15       2   
Ending compute phase at 1705516403.752215
Starting IO phase at 1705516403.752226
Ending IO phase at 1705516404.935573
write     120.94     122.45     0.044258    16384      1024.00    0.057071   1.05       0.345826   1.06       3   
Ending compute phase at 1705516423.160679
Starting IO phase at 1705516423.160691
Ending IO phase at 1705516424.403537
write     110.92     112.20     0.065229    16384      1024.00    0.068675   1.14       0.105892   1.15       4   
Ending compute phase at 1705516442.604972
Starting IO phase at 1705516442.604984
Ending IO phase at 1705516443.818938
write     116.97     118.47     0.047148    16384      1024.00    0.040833   1.08       0.334825   1.09       5   
Ending compute phase at 1705516462.036753
Starting IO phase at 1705516462.036763
Ending IO phase at 1705516463.303621
write     106.82     107.83     0.074192    16384      1024.00    0.046272   1.19       0.137840   1.20       6   
Ending compute phase at 1705516481.524932
Starting IO phase at 1705516481.524942
Ending IO phase at 1705516482.615557
write     124.79     125.49     0.026946    16384      1024.00    0.062798   1.02       0.589451   1.03       7   
Ending compute phase at 1705516500.804773
Starting IO phase at 1705516500.804784
Ending IO phase at 1705516501.955514
write     119.17     121.25     0.042759    16384      1024.00    0.050640   1.06       0.385246   1.07       8   
Ending compute phase at 1705516520.176165
Starting IO phase at 1705516520.176175
Ending IO phase at 1705516521.415575
write     114.43     114.93     0.038543    16384      1024.00    0.051893   1.11       0.497606   1.12       9   
Ending compute phase at 1705516539.608173
Starting IO phase at 1705516539.608184
Ending IO phase at 1705516540.747512
write     125.55     126.18     0.046974    16384      1024.00    0.068414   1.01       0.263590   1.02       10  
Ending compute phase at 1705516558.948647
Starting IO phase at 1705516558.948657
Ending IO phase at 1705516559.931618
write     145.08     147.22     0.030051    16384      1024.00    0.058245   0.869427   0.396788   0.882300   11  
Ending compute phase at 1705516578.176437
Starting IO phase at 1705516578.176448
Ending IO phase at 1705516579.255665
write     124.12     124.78     0.063391    16384      1024.00    0.044783   1.03       0.012367   1.03       12  
Ending compute phase at 1705516597.475744
Starting IO phase at 1705516597.475757
Ending IO phase at 1705516598.730777
write     110.80     112.33     0.066894    16384      1024.00    0.060347   1.14       0.069181   1.16       13  
Ending compute phase at 1705516616.969484
Starting IO phase at 1705516616.969497
Ending IO phase at 1705516618.071518
write     123.42     125.94     0.062179    16384      1024.00    0.051173   1.02       0.298141   1.04       14  
Ending compute phase at 1705516636.311807
Starting IO phase at 1705516636.311817
Ending IO phase at 1705516637.463545
write     116.12     121.33     0.065852    16384      1024.00    0.040585   1.05       0.399552   1.10       15  
Ending compute phase at 1705516655.661253
Starting IO phase at 1705516655.661264
Ending IO phase at 1705516656.807566
write     119.70     120.79     0.018732    16384      1024.00    0.038091   1.06       0.765194   1.07       16  
Ending compute phase at 1705516675.037825
Starting IO phase at 1705516675.037836
Ending IO phase at 1705516676.383597
write     101.40     101.93     0.078489    16384      1024.00    0.041629   1.26       0.121129   1.26       17  
Ending compute phase at 1705516700.332561
Starting IO phase at 1705516700.332629
Ending IO phase at 1705516704.393308
write     13.03      31.83      0.202377    16384      1024.00    6.08       4.02       0.821629   9.82       18  
Ending compute phase at 1705516724.149061
Starting IO phase at 1705516724.149077
Ending IO phase at 1705516729.170216
write     18.94      26.42      0.179402    16384      1024.00    2.07       4.84       2.15       6.76       19  
Ending compute phase at 1705516747.765719
Starting IO phase at 1705516747.765736
Ending IO phase at 1705516756.299551
write     14.05      15.47      0.447450    16384      1024.00    0.641272   8.27       5.83       9.11       20  
Ending compute phase at 1705516777.437296
Starting IO phase at 1705516777.437316
Ending IO phase at 1705516789.570671
write     8.40       10.55      0.333427    16384      1024.00    9.03       12.13      7.29       15.24      21  
Ending compute phase at 1705516809.942139
Starting IO phase at 1705516809.942189
Ending IO phase at 1705516817.727935
write     12.72      16.52      0.268005    16384      1024.00    3.07       7.75       4.43       10.06      22  
Ending compute phase at 1705516837.776431
Starting IO phase at 1705516837.776454
Ending IO phase at 1705516842.203556
write     20.54      31.75      0.249804    16384      1024.00    2.10       4.03       0.966584   6.23       23  
Ending compute phase at 1705516862.460872
Starting IO phase at 1705516862.460894
Ending IO phase at 1705516867.708333
write     17.46      24.43      0.225578    16384      1024.00    2.52       5.24       2.30       7.33       24  
Ending compute phase at 1705516887.663836
Starting IO phase at 1705516887.663861
Ending IO phase at 1705516890.703507
write     27.35      43.41      0.183686    16384      1024.00    1.92       2.95       1.21       4.68       25  
Ending compute phase at 1705516910.636424
Starting IO phase at 1705516910.636448
Ending IO phase at 1705516914.791553
write     22.19      32.27      0.247909    16384      1024.00    1.81       3.97       0.465729   5.77       26  
Ending compute phase at 1705516934.056733
Starting IO phase at 1705516934.056752
Ending IO phase at 1705516939.803399
write     19.02      23.25      0.305510    16384      1024.00    1.66       5.51       0.760344   6.73       27  
Ending compute phase at 1705516959.869059
Starting IO phase at 1705516959.869080
Ending IO phase at 1705516970.718240
write     10.14      11.92      0.405778    16384      1024.00    2.57       10.74      4.26       12.62      28  
Ending compute phase at 1705516991.249168
Starting IO phase at 1705516991.249186
Ending IO phase at 1705516999.699563
write     11.90      16.35      0.314903    16384      1024.00    3.24       7.83       3.39       10.76      29  
Ending compute phase at 1705517018.482394
Starting IO phase at 1705517018.482408
Ending IO phase at 1705517023.775600
write     24.43      24.48      0.224224    16384      1024.00    0.050275   5.23       2.72       5.24       30  
Ending compute phase at 1705517041.952485
Starting IO phase at 1705517041.952497
Ending IO phase at 1705517043.215602
write     109.26     109.76     0.050024    16384      1024.00    0.056775   1.17       0.366552   1.17       31  
Ending compute phase at 1705517061.452547
Starting IO phase at 1705517061.452558
Ending IO phase at 1705517062.447580
write     139.42     140.22     0.037413    16384      1024.00    0.053634   0.912877   0.315030   0.918105   32  
Ending compute phase at 1705517080.612354
Starting IO phase at 1705517080.612365
Ending IO phase at 1705517081.779529
write     119.82     120.43     0.043914    16384      1024.00    0.048736   1.06       0.361042   1.07       33  
Ending compute phase at 1705517099.984476
Starting IO phase at 1705517099.984486
Ending IO phase at 1705517101.159554
write     118.26     119.71     0.041806    16384      1024.00    0.042054   1.07       0.408788   1.08       34  
Ending compute phase at 1705517119.350046
Starting IO phase at 1705517119.350060
Ending IO phase at 1705517128.499540
write     14.11      14.13      0.179825    16384      1024.00    1.80       9.06       6.19       9.07       35  
Ending compute phase at 1705517149.377634
Starting IO phase at 1705517149.377657
Ending IO phase at 1705517156.023532
write     13.86      20.31      0.227808    16384      1024.00    2.97       6.30       2.90       9.23       36  
Ending compute phase at 1705517176.262264
Starting IO phase at 1705517176.262287
Ending IO phase at 1705517180.035887
write     22.09      34.25      0.197841    16384      1024.00    2.39       3.74       0.575739   5.80       37  
Ending compute phase at 1705517200.395551
Starting IO phase at 1705517200.395572
Ending IO phase at 1705517203.553830
write     24.63      41.74      0.159464    16384      1024.00    2.74       3.07       0.523191   5.20       38  
Ending compute phase at 1705517223.533305
Starting IO phase at 1705517223.533349
Ending IO phase at 1705517227.607680
write     22.37      33.17      0.152534    16384      1024.00    2.04       3.86       1.55       5.72       39  
Ending compute phase at 1705517247.777490
Starting IO phase at 1705517247.777512
Ending IO phase at 1705517251.560253
write     22.39      34.92      0.164011    16384      1024.00    2.95       3.67       1.11       5.72       40  
Ending compute phase at 1705517271.828708
Starting IO phase at 1705517271.828733
Ending IO phase at 1705517274.786610
write     25.90      51.24      0.129397    16384      1024.00    2.36       2.50       0.839320   4.94       41  
Ending compute phase at 1705517295.097425
Starting IO phase at 1705517295.097454
Ending IO phase at 1705517302.178351
write     14.02      18.90      0.169348    16384      1024.00    2.68       6.77       4.26       9.13       42  
Ending compute phase at 1705517323.865295
Starting IO phase at 1705517323.865318
Ending IO phase at 1705517332.712236
write     10.37      15.02      0.309248    16384      1024.00    5.34       8.52       3.87       12.35      43  
Ending compute phase at 1705517353.525696
Starting IO phase at 1705517353.525719
Ending IO phase at 1705517360.277909
write     13.75      19.13      0.243378    16384      1024.00    3.23       6.69       2.80       9.31       44  
Ending compute phase at 1705517380.461780
Starting IO phase at 1705517380.461796
Ending IO phase at 1705517387.765379
write     13.80      18.40      0.228421    16384      1024.00    2.46       6.96       3.58       9.27       45  
Ending compute phase at 1705517407.521347
Starting IO phase at 1705517407.521370
Ending IO phase at 1705517411.471526
write     23.51      32.41      0.238599    16384      1024.00    1.88       3.95       0.597862   5.45       46  
Ending compute phase at 1705517431.961835
Starting IO phase at 1705517431.961858
Ending IO phase at 1705517436.711505
write     18.31      27.32      0.187388    16384      1024.00    2.61       4.69       1.69       6.99       47  
Ending compute phase at 1705517454.928391
Starting IO phase at 1705517454.928409
Ending IO phase at 1705517459.631574
write     27.71      27.74      0.194385    16384      1024.00    2.88       4.61       1.50       4.62       48  
Ending compute phase at 1705517477.840690
Starting IO phase at 1705517477.840700
Ending IO phase at 1705517479.323531
write     94.43      94.82      0.066143    16384      1024.00    0.055936   1.35       0.292346   1.36       49  
Ending compute phase at 1705517497.567787
Starting IO phase at 1705517497.567800
Ending IO phase at 1705517506.531564
write     14.39      14.40      0.241174    16384      1024.00    4.45       8.89       5.03       8.89       50  
Ending compute phase at 1705517524.684383
Starting IO phase at 1705517524.684396
Ending IO phase at 1705517533.627643
write     14.43      14.44      0.157860    16384      1024.00    0.072847   8.86       6.34       8.87       51  
Ending compute phase at 1705517551.851178
Starting IO phase at 1705517551.851192
Ending IO phase at 1705517559.092527
write     17.71      17.79      0.230296    16384      1024.00    0.090759   7.19       4.53       7.23       52  
Ending compute phase at 1705517577.424652
Starting IO phase at 1705517577.424665
Ending IO phase at 1705517586.315619
write     14.48      14.49      0.200827    16384      1024.00    0.046950   8.83       5.62       8.84       53  
Ending compute phase at 1705517604.536088
Starting IO phase at 1705517604.536102
Ending IO phase at 1705517611.395649
write     18.77      18.81      0.190336    16384      1024.00    0.059842   6.81       4.35       6.82       54  
Ending compute phase at 1705517629.560192
Starting IO phase at 1705517629.560207
Ending IO phase at 1705517637.078937
write     17.17      17.18      0.152364    16384      1024.00    0.044151   7.45       5.01       7.46       55  
Ending compute phase at 1705517655.291552
Starting IO phase at 1705517655.291565
Ending IO phase at 1705517662.999619
write     16.74      16.75      0.218505    16384      1024.00    0.052537   7.64       4.87       7.65       56  
Ending compute phase at 1705517683.118543
Starting IO phase at 1705517683.118558
Ending IO phase at 1705517690.467571
write     13.92      17.67      0.287932    16384      1024.00    2.81       7.24       2.64       9.19       57  
Ending compute phase at 1705517711.040408
Starting IO phase at 1705517711.040424
Ending IO phase at 1705517715.951618
write     17.82      26.35      0.174192    16384      1024.00    4.02       4.86       2.07       7.18       58  
Ending compute phase at 1705517736.275965
Starting IO phase at 1705517736.275989
Ending IO phase at 1705517740.662511
write     19.66      29.41      0.237355    16384      1024.00    3.09       4.35       0.950014   6.51       59  
Ending compute phase at 1705517761.263950
Starting IO phase at 1705517761.263968
Ending IO phase at 1705517765.695549
write     19.04      29.45      0.190162    16384      1024.00    3.26       4.35       1.30       6.72       60  
Ending compute phase at 1705517786.630572
Starting IO phase at 1705517786.630587
Ending IO phase at 1705517791.024333
write     18.38      29.35      0.175392    16384      1024.00    3.98       4.36       1.56       6.97       61  
Ending compute phase at 1705517811.469840
Starting IO phase at 1705517811.469857
Ending IO phase at 1705517816.179635
write     18.71      27.71      0.181478    16384      1024.00    2.62       4.62       1.72       6.84       62  
Ending compute phase at 1705517836.733996
Starting IO phase at 1705517836.734014
Ending IO phase at 1705517842.283969
write     16.30      23.29      0.218140    16384      1024.00    3.30       5.50       2.01       7.85       63  
Ending compute phase at 1705517860.489325
Starting IO phase at 1705517860.489338
Ending IO phase at 1705517867.359580
write     18.97      18.99      0.187258    16384      1024.00    0.067093   6.74       3.74       6.75       64  
Ending compute phase at 1705517885.589127
Starting IO phase at 1705517885.589140
Ending IO phase at 1705517894.567582
write     14.39      14.40      0.218790    16384      1024.00    0.491805   8.89       5.39       8.90       65  
Ending compute phase at 1705517912.792930
Starting IO phase at 1705517912.792944
Ending IO phase at 1705517920.079588
write     17.82      17.83      0.274256    16384      1024.00    0.234312   7.18       4.36       7.18       66  
Ending compute phase at 1705517938.312007
Starting IO phase at 1705517938.312021
Ending IO phase at 1705517947.771522
write     13.69      13.70      0.189607    16384      1024.00    0.039435   9.34       6.31       9.35       67  
Ending compute phase at 1705517965.984962
Starting IO phase at 1705517965.984974
Ending IO phase at 1705517972.763586
write     19.22      19.24      0.180433    16384      1024.00    0.055382   6.65       3.77       6.66       68  
Ending compute phase at 1705517990.924860
Starting IO phase at 1705517990.924873
Ending IO phase at 1705518000.399549
write     13.61      13.62      0.315967    16384      1024.00    0.273919   9.40       4.34       9.40       69  
Ending compute phase at 1705518018.661434
Starting IO phase at 1705518018.661447
Ending IO phase at 1705518025.665533
write     18.38      18.39      0.195620    16384      1024.00    3.79       6.96       3.83       6.97       70  
Ending compute phase at 1705518043.913047
Starting IO phase at 1705518043.913060
Ending IO phase at 1705518052.347900
write     15.17      15.20      0.276328    16384      1024.00    3.88       8.42       4.56       8.44       71  
Ending compute phase at 1705518070.572819
Starting IO phase at 1705518070.572833
Ending IO phase at 1705518078.798647
write     15.65      15.66      0.260765    16384      1024.00    0.741926   8.17       4.00       8.18       72  
Ending compute phase at 1705518096.970826
Starting IO phase at 1705518096.970840
Ending IO phase at 1705518107.703603
write     11.99      11.99      0.333748    16384      1024.00    0.148278   10.67      6.47       10.68      73  
Ending compute phase at 1705518125.877478
Starting IO phase at 1705518125.877491
Ending IO phase at 1705518136.871526
write     11.71      11.72      0.244961    16384      1024.00    2.54       10.92      7.01       10.93      74  
Ending compute phase at 1705518157.622759
Starting IO phase at 1705518157.622773
Ending IO phase at 1705518166.779529
write     11.00      14.12      0.339905    16384      1024.00    3.56       9.07       4.44       11.63      75  
Ending compute phase at 1705518187.906794
Starting IO phase at 1705518187.906810
Ending IO phase at 1705518197.399086
write     10.32      13.54      0.250006    16384      1024.00    3.99       9.45       5.46       12.40      76  
Ending compute phase at 1705518218.259085
Starting IO phase at 1705518218.259099
Ending IO phase at 1705518227.739496
write     10.60      14.65      0.338413    16384      1024.00    3.37       8.74       3.97       12.08      77  
Ending compute phase at 1705518248.356345
Starting IO phase at 1705518248.356367
Ending IO phase at 1705518256.241970
write     12.48      17.38      0.254126    16384      1024.00    3.40       7.36       3.78       10.26      78  
Ending compute phase at 1705518277.123985
Starting IO phase at 1705518277.124001
Ending IO phase at 1705518282.859473
write     15.27      22.64      0.300520    16384      1024.00    3.84       5.65       1.30       8.38       79  
Ending compute phase at 1705518303.511322
Starting IO phase at 1705518303.511338
Ending IO phase at 1705518309.791593
write     14.71      20.59      0.239467    16384      1024.00    3.26       6.22       2.38       8.70       80  
Ending compute phase at 1705518329.935072
Starting IO phase at 1705518329.935089
Ending IO phase at 1705518336.759500
write     14.74      19.04      0.281120    16384      1024.00    2.38       6.72       2.22       8.68       81  
Ending compute phase at 1705518357.082815
Starting IO phase at 1705518357.082831
Ending IO phase at 1705518364.679583
write     13.31      17.58      0.253243    16384      1024.00    3.02       7.28       3.43       9.61       82  
Ending compute phase at 1705518383.158723
Starting IO phase at 1705518383.158741
Ending IO phase at 1705518391.119496
write     15.64      16.41      0.315630    16384      1024.00    0.663150   7.80       3.83       8.19       83  
Ending compute phase at 1705518411.754916
Starting IO phase at 1705518411.754934
Ending IO phase at 1705518418.683522
write     13.78      18.66      0.261909    16384      1024.00    3.49       6.86       2.68       9.29       84  
Ending compute phase at 1705518439.391014
Starting IO phase at 1705518439.391030
Ending IO phase at 1705518447.203554
write     12.50      16.60      0.364630    16384      1024.00    8.37       7.71       3.56       10.24      85  
Ending compute phase at 1705518468.771849
Starting IO phase at 1705518468.771864
Ending IO phase at 1705518475.643524
write     12.62      18.80      0.304074    16384      1024.00    3.91       6.81       1.94       10.14      86  
Ending compute phase at 1705518496.755207
Starting IO phase at 1705518496.755225
Ending IO phase at 1705518504.547554
write     12.05      16.32      0.276858    16384      1024.00    3.98       7.84       3.29       10.63      87  
Ending compute phase at 1705518526.056410
Starting IO phase at 1705518526.056427
Ending IO phase at 1705518531.787559
write     14.32      22.89      0.204418    16384      1024.00    3.39       5.59       2.33       8.94       88  
Ending compute phase at 1705518552.663195
Starting IO phase at 1705518552.663213
Ending IO phase at 1705518561.015533
write     11.71      15.51      0.307856    16384      1024.00    3.67       8.25       3.33       10.93      89  
Ending compute phase at 1705518581.952024
Starting IO phase at 1705518581.952040
Ending IO phase at 1705518588.871546
write     13.35      18.69      0.247245    16384      1024.00    6.69       6.85       2.89       9.59       90  
Ending compute phase at 1705518609.779178
Starting IO phase at 1705518609.779197
Ending IO phase at 1705518616.311575
write     13.98      19.95      0.327303    16384      1024.00    4.00       6.41       1.92       9.16       91  
Ending compute phase at 1705518637.143757
Starting IO phase at 1705518637.143775
Ending IO phase at 1705518643.903552
write     13.68      19.20      0.302649    16384      1024.00    7.53       6.67       2.58       9.36       92  
Ending compute phase at 1705518664.543371
Starting IO phase at 1705518664.543393
Ending IO phase at 1705518672.095519
write     12.84      17.28      0.276532    16384      1024.00    3.61       7.41       3.04       9.97       93  
Ending compute phase at 1705518693.237875
Starting IO phase at 1705518693.237898
Ending IO phase at 1705518699.079491
write     14.62      22.20      0.277757    16384      1024.00    3.73       5.77       1.72       8.75       94  
Ending compute phase at 1705518719.931502
Starting IO phase at 1705518719.931518
Ending IO phase at 1705518728.095666
write     11.95      16.24      0.325749    16384      1024.00    3.71       7.88       2.88       10.71      95  
Ending compute phase at 1705518748.988158
Starting IO phase at 1705518748.988181
Ending IO phase at 1705518754.680399
write     15.39      22.64      0.276842    16384      1024.00    3.45       5.65       1.23       8.32       96  
Ending compute phase at 1705518774.247517
Starting IO phase at 1705518774.247539
Ending IO phase at 1705518783.271522
write     12.44      14.41      0.308051    16384      1024.00    1.83       8.88       4.06       10.29      97  
Ending compute phase at 1705518804.003495
Starting IO phase at 1705518804.003517
Ending IO phase at 1705518810.572581
write     14.08      20.19      0.254668    16384      1024.00    3.13       6.34       2.43       9.09       98  
Ending compute phase at 1705518831.856003
Starting IO phase at 1705518831.856024
Ending IO phase at 1705518838.595532
write     13.15      19.41      0.377055    16384      1024.00    3.42       6.59       2.97       9.73       99  
Ending compute phase at 1705518859.926788
Starting IO phase at 1705518859.926808
Ending IO phase at 1705518866.407550
write     13.47      20.01      0.253646    16384      1024.00    3.63       6.40       2.96       9.50       100 
Ending compute phase at 1705518887.387747
Starting IO phase at 1705518887.387767
Ending IO phase at 1705518895.463515
write     11.93      16.14      0.297338    16384      1024.00    3.92       7.93       3.22       10.73      101 
Ending compute phase at 1705518916.167830
Starting IO phase at 1705518916.167852
Ending IO phase at 1705518923.975652
write     12.44      16.81      0.424328    16384      1024.00    3.97       7.61       3.16       10.29      102 
Ending compute phase at 1705518944.704175
Starting IO phase at 1705518944.704198
Ending IO phase at 1705518950.945569
write     14.69      20.66      0.292877    16384      1024.00    3.48       6.20       2.25       8.71       103 
Ending compute phase at 1705518971.779798
Starting IO phase at 1705518971.779820
Ending IO phase at 1705518978.911584
write     13.28      18.23      0.316346    16384      1024.00    3.54       7.02       1.99       9.64       104 
Ending compute phase at 1705518999.672994
Starting IO phase at 1705518999.673017
Ending IO phase at 1705519007.331508
write     12.56      16.85      0.290493    16384      1024.00    3.11       7.60       2.93       10.19      105 
Ending compute phase at 1705519027.987981
Starting IO phase at 1705519027.988002
Ending IO phase at 1705519034.941609
write     13.67      18.67      0.300789    16384      1024.00    2.99       6.85       2.08       9.36       106 
Ending compute phase at 1705519055.787920
Starting IO phase at 1705519055.787940
Ending IO phase at 1705519064.479567
write     11.34      14.80      0.341509    16384      1024.00    3.18       8.65       3.95       11.29      107 
Ending compute phase at 1705519082.673074
Starting IO phase at 1705519082.673088
Ending IO phase at 1705519090.228070
write     17.03      17.05      0.234397    16384      1024.00    0.147955   7.51       4.55       7.52       108 
Ending compute phase at 1705519112.984375
Starting IO phase at 1705519112.984397
Ending IO phase at 1705519122.091441
write     9.38       13.91      0.345325    16384      1024.00    6.02       9.20       3.52       13.65      109 
Ending compute phase at 1705519142.428418
Starting IO phase at 1705519142.428440
Ending IO phase at 1705519147.939417
write     16.91      26.60      0.276500    16384      1024.00    2.72       4.81       1.40       7.57       110 
Ending compute phase at 1705519168.762261
Starting IO phase at 1705519168.762282
Ending IO phase at 1705519172.159656
write     21.36      39.12      0.189480    16384      1024.00    2.86       3.27       0.303179   5.99       111 
Ending compute phase at 1705519192.956714
Starting IO phase at 1705519192.956738
Ending IO phase at 1705519202.096402
write     10.97      15.02      0.257241    16384      1024.00    3.60       8.52       4.98       11.67      112 
Ending compute phase at 1705519222.728690
Starting IO phase at 1705519222.728712
Ending IO phase at 1705519231.395504
write     11.53      14.88      0.284817    16384      1024.00    3.50       8.60       4.06       11.10      113 
Ending compute phase at 1705519252.332584
Starting IO phase at 1705519252.332607
Ending IO phase at 1705519260.755497
write     11.53      15.33      0.323512    16384      1024.00    3.79       8.35       3.19       11.10      114 
Ending compute phase at 1705519281.508891
Starting IO phase at 1705519281.508911
Ending IO phase at 1705519290.279546
write     11.32      14.68      0.351829    16384      1024.00    8.21       8.72       3.10       11.31      115 
Ending compute phase at 1705519311.063572
Starting IO phase at 1705519311.063592
Ending IO phase at 1705519320.007523
write     11.22      14.51      0.298575    16384      1024.00    3.55       8.82       4.07       11.40      116 
Ending compute phase at 1705519340.986629
Starting IO phase at 1705519340.986649
Ending IO phase at 1705519351.046559
write     9.99       13.73      0.345199    16384      1024.00    3.73       9.32       4.48       12.82      117 
Ending compute phase at 1705519372.448565
Starting IO phase at 1705519372.448588
Ending IO phase at 1705519380.291584
write     11.59      16.48      0.272418    16384      1024.00    3.96       7.77       3.41       11.05      118 
Ending compute phase at 1705519403.184924
Starting IO phase at 1705519403.184946
Ending IO phase at 1705519412.207447
write     9.38       14.36      0.321056    16384      1024.00    6.58       8.92       3.78       13.65      119 
Ending compute phase at 1705519434.076865
Starting IO phase at 1705519434.076889
Ending IO phase at 1705519442.041784
write     10.99      16.10      0.254244    16384      1024.00    8.24       7.95       3.90       11.65      120 
Ending compute phase at 1705519465.216923
Starting IO phase at 1705519465.216944
Ending IO phase at 1705519474.347558
write     9.10       14.10      0.365206    16384      1024.00    6.10       9.08       4.25       14.06      121 
Ending compute phase at 1705519497.373100
Starting IO phase at 1705519497.373117
Ending IO phase at 1705519505.629597
write     9.78       15.58      0.380877    16384      1024.00    6.67       8.21       3.07       13.08      122 
Ending compute phase at 1705519527.537172
Starting IO phase at 1705519527.537192
Ending IO phase at 1705519534.875504
write     11.70      17.54      0.292126    16384      1024.00    4.43       7.30       2.62       10.94      123 
Ending compute phase at 1705519555.421183
Starting IO phase at 1705519555.421202
Ending IO phase at 1705519559.755563
write     19.27      29.31      0.234730    16384      1024.00    2.56       4.37       0.509055   6.64       124 
Ending compute phase at 1705519580.656979
Starting IO phase at 1705519580.656997
Ending IO phase at 1705519587.875523
write     13.07      17.99      0.269935    16384      1024.00    3.65       7.11       2.81       9.80       125 
Ending compute phase at 1705519610.317199
Starting IO phase at 1705519610.317221
Ending IO phase at 1705519620.347868
write     8.98       12.87      0.333561    16384      1024.00    5.89       9.94       4.62       14.25      126 
Ending compute phase at 1705519642.408076
Starting IO phase at 1705519642.408096
Ending IO phase at 1705519652.407711
write     9.28       12.81      0.318595    16384      1024.00    8.89       9.99       4.90       13.79      127 
Ending compute phase at 1705519674.457483
Starting IO phase at 1705519674.457502
Ending IO phase at 1705519683.770425
write     9.78       13.87      0.315176    16384      1024.00    6.00       9.23       4.19       13.09      128 
Ending compute phase at 1705519705.521451
Starting IO phase at 1705519705.521472
Ending IO phase at 1705519714.362214
write     10.38      15.51      0.266185    16384      1024.00    7.80       8.25       4.53       12.34      129 
Ending compute phase at 1705519735.141243
Starting IO phase at 1705519735.141261
Ending IO phase at 1705519743.208253
write     12.06      16.02      0.294275    16384      1024.00    3.79       7.99       3.28       10.62      130 
Ending compute phase at 1705519764.105498
Starting IO phase at 1705519764.105515
Ending IO phase at 1705519773.399568
write     10.74      13.88      0.287210    16384      1024.00    7.29       9.22       4.63       11.92      131 
Ending compute phase at 1705519796.100704
Starting IO phase at 1705519796.100725
Ending IO phase at 1705519803.062544
write     11.15      17.70      0.297217    16384      1024.00    4.65       7.23       3.72       11.48      132 
Ending compute phase at 1705519824.121329
Starting IO phase at 1705519824.121350
Ending IO phase at 1705519833.680806
write     10.37      13.47      0.270902    16384      1024.00    3.86       9.50       5.17       12.34      133 
Ending compute phase at 1705519855.238535
Starting IO phase at 1705519855.238550
Ending IO phase at 1705519864.147542
write     10.51      14.44      0.313644    16384      1024.00    4.59       8.86       3.85       12.18      134 
Ending compute phase at 1705519885.787844
Starting IO phase at 1705519885.787865
Ending IO phase at 1705519894.315541
write     10.76      15.18      0.372243    16384      1024.00    3.94       8.43       4.94       11.90      135 
Ending compute phase at 1705519912.553268
Starting IO phase at 1705519912.553280
Ending IO phase at 1705519920.516018
write     16.15      16.16      0.251161    16384      1024.00    0.185868   7.92       5.05       7.93       136 
Ending compute phase at 1705519942.793368
Starting IO phase at 1705519942.793393
Ending IO phase at 1705519952.628949
write     9.21       13.10      0.334067    16384      1024.00    5.54       9.77       4.44       13.89      137 
Ending compute phase at 1705519973.265543
Starting IO phase at 1705519973.265561
Ending IO phase at 1705519977.799392
write     18.49      29.44      0.195632    16384      1024.00    2.91       4.35       1.31       6.92       138 
Ending compute phase at 1705520009.778427
Starting IO phase at 1705520009.778444
Ending IO phase at 1705520016.351608
write     6.33       21.02      0.146123    16384      1024.00    13.83      6.09       4.14       20.22      139 
Ending compute phase at 1705520039.389370
Starting IO phase at 1705520039.389393
Ending IO phase at 1705520047.887559
write     9.62       15.06      0.267666    16384      1024.00    9.14       8.50       4.17       13.31      140 
Ending compute phase at 1705520069.838493
Starting IO phase at 1705520069.838514
Ending IO phase at 1705520080.059588
write     9.22       12.65      0.381400    16384      1024.00    8.98       10.11      5.62       13.89      141 
Ending compute phase at 1705520102.069668
Starting IO phase at 1705520102.069688
Ending IO phase at 1705520111.375562
write     9.81       13.88      0.380205    16384      1024.00    5.37       9.22       4.11       13.05      142 
