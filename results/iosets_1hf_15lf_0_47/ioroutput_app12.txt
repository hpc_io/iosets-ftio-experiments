############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 19:32:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705516328.290609
TestID              : 0
StartTime           : Wed Jan 17 19:32:08 2024
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705516697.768697
Starting IO phase at 1705516697.768725
Ending IO phase at 1705516956.381822
write     9.57       9.91       0.770786    327680     1024.00    9.84       258.37     11.72      267.52     0   
Ending compute phase at 1705517334.149624
Starting IO phase at 1705517334.149638
Ending IO phase at 1705517388.352013
write     35.73      47.38      0.164433    327680     1024.00    19.07      54.03      8.39       71.65      1   
Ending compute phase at 1705517748.601536
Starting IO phase at 1705517748.601551
Ending IO phase at 1705517790.963199
write     60.96      60.97      0.123127    327680     1024.00    0.053373   41.99      2.59       42.00      2   
Ending compute phase at 1705518151.128455
Starting IO phase at 1705518151.128468
Ending IO phase at 1705518201.663028
write     50.91      50.92      0.156049    327680     1024.00    0.166634   50.28      1.92       50.28      3   
Ending compute phase at 1705518561.852703
Starting IO phase at 1705518561.852715
Ending IO phase at 1705518616.854362
write     46.85      46.85      0.156080    327680     1024.00    0.254996   54.64      4.69       54.64      4   
Ending compute phase at 1705518978.004881
Starting IO phase at 1705518978.004902
Ending IO phase at 1705519038.491784
write     41.84      42.53      0.187581    327680     1024.00    1.14       60.19      9.77       61.18      5   
Ending compute phase at 1705519408.891041
Starting IO phase at 1705519408.891060
Ending IO phase at 1705519479.686506
write     31.70      36.29      0.215571    327680     1024.00    10.74      70.55      13.63      80.76      6   
Ending compute phase at 1705519839.883233
Starting IO phase at 1705519839.883246
Ending IO phase at 1705519902.742023
write     40.98      40.99      0.195162    327680     1024.00    0.374541   62.45      14.01      62.48      7   
