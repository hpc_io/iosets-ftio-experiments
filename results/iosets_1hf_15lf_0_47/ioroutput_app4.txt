############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 19:32:07 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705516327.436990
TestID              : 0
StartTime           : Wed Jan 17 19:32:07 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705516687.841483
Starting IO phase at 1705516687.841508
Ending IO phase at 1705516786.359515
write     25.90      26.46      0.295117    327680     1024.00    0.336800   96.75      13.39      98.83      0   
Ending compute phase at 1705517153.780960
Starting IO phase at 1705517153.780982
Ending IO phase at 1705517203.323731
write     45.16      51.88      0.149733    327680     1024.00    7.38       49.34      10.44      56.69      1   
Ending compute phase at 1705517563.570529
Starting IO phase at 1705517563.570542
Ending IO phase at 1705517592.763365
write     89.10      89.11      0.087685    327680     1024.00    0.068677   28.73      3.12       28.73      2   
Ending compute phase at 1705517952.968900
Starting IO phase at 1705517952.968913
Ending IO phase at 1705517987.099554
write     75.73      75.74      0.084703    327680     1024.00    0.064642   33.80      6.69       33.80      3   
Ending compute phase at 1705518347.357926
Starting IO phase at 1705518347.357938
Ending IO phase at 1705518391.710622
write     58.04      58.05      0.129999    327680     1024.00    0.230362   44.10      17.81      44.10      4   
Ending compute phase at 1705518753.756394
Starting IO phase at 1705518753.756413
Ending IO phase at 1705518810.755227
write     43.67      45.10      0.177367    327680     1024.00    1.86       56.76      6.64       58.62      5   
Ending compute phase at 1705519172.296508
Starting IO phase at 1705519172.296522
Ending IO phase at 1705519234.880471
write     40.20      41.08      0.194366    327680     1024.00    1.48       62.32      4.15       63.68      6   
Ending compute phase at 1705519595.357711
Starting IO phase at 1705519595.357729
Ending IO phase at 1705519660.435315
write     39.38      39.56      0.199327    327680     1024.00    0.854719   64.71      7.67       65.01      7   
Ending compute phase at 1705520023.001787
Starting IO phase at 1705520023.001816
Ending IO phase at 1705520087.165226
write     38.65      40.11      0.194762    327680     1024.00    2.71       63.82      3.54       66.23      8   
