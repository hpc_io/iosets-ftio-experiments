############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Dec 19 22:23:49 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703021029.324535
TestID              : 0
StartTime           : Tue Dec 19 22:23:49 2023
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703021449.550271
Starting IO phase at 1703021449.550289
Ending IO phase at 1703021696.906074
write     8.33       10.75      0.688783    327680     1024.00    75.47      238.24     35.19      307.28     0   
Ending compute phase at 1703022062.612824
Starting IO phase at 1703022062.612848
Ending IO phase at 1703022203.097102
write     17.55      20.09      0.365328    327680     1024.00    7.24       127.45     26.46      145.85     1   
Ending compute phase at 1703022563.327540
Starting IO phase at 1703022563.327564
Ending IO phase at 1703023059.443281
write     5.16       5.16       1.53        327680     1024.00    0.257740   495.83     16.75      495.92     2   
Ending compute phase at 1703023430.559501
Starting IO phase at 1703023430.559523
Ending IO phase at 1703023509.171974
write     28.68      36.56      0.177770    327680     1024.00    16.78      70.02      33.42      89.27      3   
Ending compute phase at 1703023965.500142
Starting IO phase at 1703023965.500165
Ending IO phase at 1703024255.566530
write     6.63       8.83       0.905657    327680     1024.00    97.14      289.83     26.64      385.99     4   
Ending compute phase at 1703024623.048805
Starting IO phase at 1703024623.048829
