############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Dec 19 22:23:47 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703021027.901127
TestID              : 0
StartTime           : Tue Dec 19 22:23:47 2023
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703021045.919331
Starting IO phase at 1703021045.919351
Ending IO phase at 1703021047.337470
write     96.49      97.30      0.081518    16384      1024.00    0.050808   1.32       0.016791   1.33       0   
Ending compute phase at 1703021065.564445
Starting IO phase at 1703021065.564464
Ending IO phase at 1703021067.085814
write     91.20      91.62      0.081256    16384      1024.00    0.048355   1.40       0.097782   1.40       1   
Ending compute phase at 1703021085.359836
Starting IO phase at 1703021085.359856
Ending IO phase at 1703021086.829831
write     91.24      91.62      0.081962    16384      1024.00    0.064928   1.40       0.086360   1.40       2   
Ending compute phase at 1703021105.075590
Starting IO phase at 1703021105.075612
Ending IO phase at 1703021106.757829
write     81.32      81.62      0.084431    16384      1024.00    0.062079   1.57       0.218118   1.57       3   
Ending compute phase at 1703021124.984132
Starting IO phase at 1703021124.984152
Ending IO phase at 1703021126.577861
write     86.26      86.62      0.087011    16384      1024.00    0.045820   1.48       0.086284   1.48       4   
Ending compute phase at 1703021144.820230
Starting IO phase at 1703021144.820249
Ending IO phase at 1703021146.438000
write     85.22      85.99      0.092321    16384      1024.00    0.064689   1.49       0.139044   1.50       5   
Ending compute phase at 1703021164.695629
Starting IO phase at 1703021164.695648
Ending IO phase at 1703021166.249917
write     88.34      88.71      0.076594    16384      1024.00    0.048073   1.44       0.218240   1.45       6   
Ending compute phase at 1703021184.541430
Starting IO phase at 1703021184.541451
Ending IO phase at 1703021185.957990
write     92.64      94.26      0.083437    16384      1024.00    0.069102   1.36       0.027975   1.38       7   
Ending compute phase at 1703021204.306797
Starting IO phase at 1703021204.306815
Ending IO phase at 1703021205.722019
write     93.69      94.55      0.077816    16384      1024.00    0.065760   1.35       0.116583   1.37       8   
Ending compute phase at 1703021223.992325
Starting IO phase at 1703021223.992344
Ending IO phase at 1703021225.517904
write     89.87      90.28      0.063963    16384      1024.00    0.046657   1.42       0.395231   1.42       9   
Ending compute phase at 1703021243.807253
Starting IO phase at 1703021243.807284
Ending IO phase at 1703021245.385912
write     81.94      83.87      0.095246    16384      1024.00    0.044436   1.53       0.020614   1.56       10  
Ending compute phase at 1703021263.731672
Starting IO phase at 1703021263.731691
Ending IO phase at 1703021265.213880
write     93.68      94.08      0.079679    16384      1024.00    0.054391   1.36       0.109242   1.37       11  
Ending compute phase at 1703021283.429707
Starting IO phase at 1703021283.429726
Ending IO phase at 1703021285.077841
write     80.08      81.71      0.093612    16384      1024.00    0.043663   1.57       0.137974   1.60       12  
Ending compute phase at 1703021303.264640
Starting IO phase at 1703021303.264658
Ending IO phase at 1703021304.941883
write     80.22      80.51      0.088637    16384      1024.00    0.060980   1.59       0.172446   1.60       13  
Ending compute phase at 1703021323.271620
Starting IO phase at 1703021323.271638
Ending IO phase at 1703021324.869871
write     84.98      85.76      0.081489    16384      1024.00    0.059311   1.49       0.196979   1.51       14  
Ending compute phase at 1703021343.075518
Starting IO phase at 1703021343.075538
Ending IO phase at 1703021344.841931
write     80.38      80.67      0.092391    16384      1024.00    0.062977   1.59       0.132303   1.59       15  
Ending compute phase at 1703021363.111128
Starting IO phase at 1703021363.111148
Ending IO phase at 1703021364.726041
write     85.96      86.28      0.086649    16384      1024.00    0.040456   1.48       0.120823   1.49       16  
Ending compute phase at 1703021382.943789
Starting IO phase at 1703021382.943808
Ending IO phase at 1703021384.574156
write     83.53      83.85      0.084316    16384      1024.00    0.053160   1.53       0.178161   1.53       17  
Ending compute phase at 1703021567.157561
Starting IO phase at 1703021567.157581
Ending IO phase at 1703021594.053716
write     0.853403   5.00       0.937302    16384      1024.00    130.04     25.60      15.39      149.99     18  
Ending compute phase at 1703021637.134156
Starting IO phase at 1703021637.134178
Ending IO phase at 1703021649.042275
write     3.49       13.47      0.520179    16384      1024.00    28.11      9.50       3.55       36.68      19  
Ending compute phase at 1703021709.561917
Starting IO phase at 1703021709.561938
Ending IO phase at 1703021736.554240
write     1.85       13.64      0.337229    16384      1024.00    44.72      9.39       21.50      69.23      20  
Ending compute phase at 1703021754.804175
Starting IO phase at 1703021754.804196
Ending IO phase at 1703021756.125254
write     97.21      97.67      0.081186    16384      1024.00    0.051927   1.31       0.012517   1.32       21  
Ending compute phase at 1703021774.367819
Starting IO phase at 1703021774.367840
Ending IO phase at 1703021776.001892
write     84.90      85.22      0.090660    16384      1024.00    0.065082   1.50       0.075023   1.51       22  
Ending compute phase at 1703021794.294305
Starting IO phase at 1703021794.294324
Ending IO phase at 1703021796.001899
write     80.69      82.40      0.096371    16384      1024.00    0.045226   1.55       0.137973   1.59       23  
Ending compute phase at 1703021814.272498
Starting IO phase at 1703021814.272518
Ending IO phase at 1703021815.865251
write     85.91      87.20      0.091731    16384      1024.00    0.034287   1.47       0.019794   1.49       24  
Ending compute phase at 1703021834.155264
Starting IO phase at 1703021834.155284
Ending IO phase at 1703021835.841863
write     79.56      79.85      0.091260    16384      1024.00    0.036537   1.60       0.143795   1.61       25  
Ending compute phase at 1703021854.168934
Starting IO phase at 1703021854.168953
Ending IO phase at 1703021855.857913
write     79.24      80.78      0.099032    16384      1024.00    0.058902   1.58       0.034845   1.62       26  
Ending compute phase at 1703021874.171735
Starting IO phase at 1703021874.171759
Ending IO phase at 1703021875.845905
write     80.64      82.09      0.095342    16384      1024.00    0.065444   1.56       0.052158   1.59       27  
Ending compute phase at 1703021894.116075
Starting IO phase at 1703021894.116093
Ending IO phase at 1703021904.078825
write     12.93      12.94      0.180373    16384      1024.00    0.074812   9.89       7.01       9.90       28  
Ending compute phase at 1703021934.786339
Starting IO phase at 1703021934.786360
Ending IO phase at 1703021958.453703
write     3.54       7.51       0.195813    16384      1024.00    13.02      17.05      20.67      36.12      29  
Ending compute phase at 1703021990.096577
Starting IO phase at 1703021990.096597
Ending IO phase at 1703021994.882088
write     7.20       26.95      0.267444    16384      1024.00    14.31      4.75       0.469647   17.77      30  
Ending compute phase at 1703022015.526017
Starting IO phase at 1703022015.526039
Ending IO phase at 1703022024.589816
write     11.39      14.37      0.259039    16384      1024.00    8.64       8.91       4.76       11.24      31  
Ending compute phase at 1703022053.848548
Starting IO phase at 1703022053.848589
Ending IO phase at 1703022058.081859
write     8.37       33.48      0.190817    16384      1024.00    11.34      3.82       1.77       15.28      32  
Ending compute phase at 1703022098.853875
Starting IO phase at 1703022098.853908
Ending IO phase at 1703022140.741687
write     1.99       6.81       0.990308    16384      1024.00    23.78      18.81      26.96      64.47      33  
Ending compute phase at 1703022224.884427
Starting IO phase at 1703022224.884448
Ending IO phase at 1703022237.257782
write     1.64       12.90      0.373275    16384      1024.00    66.50      9.92       8.20       77.86      34  
Ending compute phase at 1703022257.277767
Starting IO phase at 1703022257.277789
Ending IO phase at 1703022262.450377
write     18.52      25.72      0.202096    16384      1024.00    2.43       4.98       1.85       6.91       35  
Ending compute phase at 1703022282.885955
Starting IO phase at 1703022282.885976
Ending IO phase at 1703022287.365843
write     19.36      30.21      0.240517    16384      1024.00    2.53       4.24       0.523844   6.61       36  
Ending compute phase at 1703022309.993875
Starting IO phase at 1703022309.993898
Ending IO phase at 1703022318.365834
write     10.07      15.48      0.283402    16384      1024.00    8.97       8.27       3.74       12.71      37  
Ending compute phase at 1703022338.950052
Starting IO phase at 1703022338.950072
Ending IO phase at 1703022344.571501
write     16.00      21.48      0.317870    16384      1024.00    3.02       5.96       1.10       8.00       38  
Ending compute phase at 1703022366.981738
Starting IO phase at 1703022366.981758
Ending IO phase at 1703022377.969973
write     8.46       11.71      0.388334    16384      1024.00    10.39      10.93      5.75       15.12      39  
Ending compute phase at 1703022399.042324
Starting IO phase at 1703022399.042345
Ending IO phase at 1703022404.437904
write     15.79      24.43      0.307767    16384      1024.00    3.32       5.24       0.928030   8.11       40  
Ending compute phase at 1703022425.733578
Starting IO phase at 1703022425.733600
Ending IO phase at 1703022430.353694
write     16.75      28.37      0.239938    16384      1024.00    3.54       4.51       0.714440   7.64       41  
Ending compute phase at 1703022451.199470
Starting IO phase at 1703022451.199493
Ending IO phase at 1703022458.085872
write     13.48      18.79      0.207820    16384      1024.00    2.88       6.81       3.50       9.49       42  
Ending compute phase at 1703022476.629912
Starting IO phase at 1703022476.629932
Ending IO phase at 1703022480.765877
write     29.43      31.57      0.218353    16384      1024.00    0.471410   4.05       0.881582   4.35       43  
Ending compute phase at 1703022499.095356
Starting IO phase at 1703022499.095375
Ending IO phase at 1703022506.345904
write     18.00      18.04      0.215968    16384      1024.00    3.47       7.10       3.65       7.11       44  
Ending compute phase at 1703022524.575653
Starting IO phase at 1703022524.575672
Ending IO phase at 1703022526.157947
write     81.35      81.66      0.097254    16384      1024.00    0.045284   1.57       0.023686   1.57       45  
Ending compute phase at 1703022544.464679
Starting IO phase at 1703022544.464701
Ending IO phase at 1703022547.021875
write     50.35      50.63      0.144991    16384      1024.00    0.115192   2.53       0.600914   2.54       46  
Ending compute phase at 1703022567.493657
Starting IO phase at 1703022567.493678
Ending IO phase at 1703022577.949791
write     10.16      12.37      0.284050    16384      1024.00    3.60       10.35      5.82       12.60      47  
Ending compute phase at 1703022598.898508
Starting IO phase at 1703022598.898530
Ending IO phase at 1703022607.663759
write     11.15      15.94      0.340842    16384      1024.00    8.22       8.03       3.26       11.48      48  
Ending compute phase at 1703022628.493756
Starting IO phase at 1703022628.493777
Ending IO phase at 1703022633.668990
write     16.68      25.14      0.277515    16384      1024.00    3.06       5.09       0.653597   7.67       49  
Ending compute phase at 1703022654.177647
Starting IO phase at 1703022654.177669
Ending IO phase at 1703022658.485891
write     19.55      30.55      0.248644    16384      1024.00    2.78       4.19       0.550340   6.55       50  
Ending compute phase at 1703022679.517120
Starting IO phase at 1703022679.517141
Ending IO phase at 1703022682.825853
write     21.41      40.22      0.186006    16384      1024.00    2.81       3.18       0.531734   5.98       51  
Ending compute phase at 1703022722.956356
Starting IO phase at 1703022722.956383
Ending IO phase at 1703022730.821672
write     4.31       19.79      0.315572    16384      1024.00    23.00      6.47       3.13       29.67      52  
Ending compute phase at 1703022764.238373
Starting IO phase at 1703022764.238396
Ending IO phase at 1703022782.205652
write     3.86       8.29       0.414333    16384      1024.00    18.99      15.45      11.27      33.12      53  
Ending compute phase at 1703022829.146666
Starting IO phase at 1703022829.146686
Ending IO phase at 1703022843.555058
write     3.00       9.99       0.681383    16384      1024.00    36.55      12.82      3.45       42.70      54  
Ending compute phase at 1703022912.365554
Starting IO phase at 1703022912.365574
Ending IO phase at 1703022919.413867
write     2.22       7.97       0.376360    16384      1024.00    50.62      16.05      9.66       57.56      55  
Ending compute phase at 1703022945.493847
Starting IO phase at 1703022945.493870
Ending IO phase at 1703022960.373881
write     5.63       8.65       0.691357    16384      1024.00    9.75       14.81      3.77       22.72      56  
Ending compute phase at 1703022991.769864
Starting IO phase at 1703022991.769884
Ending IO phase at 1703023006.039247
write     4.68       9.04       0.571825    16384      1024.00    15.15      14.16      5.03       27.35      57  
Ending compute phase at 1703023027.581745
Starting IO phase at 1703023027.581766
Ending IO phase at 1703023053.777804
write     4.35       9.41       0.551484    16384      1024.00    5.85       13.61      17.31      29.45      58  
Ending compute phase at 1703023072.020943
Starting IO phase at 1703023072.020963
Ending IO phase at 1703023081.937859
write     13.04      13.05      0.235286    16384      1024.00    0.424448   9.81       6.05       9.82       59  
Ending compute phase at 1703023103.810175
Starting IO phase at 1703023103.810198
Ending IO phase at 1703023112.209730
write     10.69      15.88      0.348088    16384      1024.00    4.40       8.06       2.71       11.97      60  
Ending compute phase at 1703023135.714218
Starting IO phase at 1703023135.714239
Ending IO phase at 1703023149.829715
write     6.63       9.11       0.507611    16384      1024.00    13.39      14.04      5.92       19.31      61  
Ending compute phase at 1703023172.193700
Starting IO phase at 1703023172.193720
Ending IO phase at 1703023183.177831
write     8.49       11.74      0.404117    16384      1024.00    10.63      10.90      4.45       15.07      62  
Ending compute phase at 1703023204.185971
Starting IO phase at 1703023204.185992
Ending IO phase at 1703023209.813801
write     15.15      22.67      0.334587    16384      1024.00    3.28       5.65       1.11       8.45       63  
Ending compute phase at 1703023232.493780
Starting IO phase at 1703023232.493800
Ending IO phase at 1703023243.325701
write     8.39       12.10      0.450584    16384      1024.00    5.40       10.58      4.72       15.25      64  
Ending compute phase at 1703023286.990802
Starting IO phase at 1703023286.990824
Ending IO phase at 1703023288.777953
write     4.71       10.35      0.107049    16384      1024.00    26.01      12.37      3.86       27.16      65  
Ending compute phase at 1703023309.825650
Starting IO phase at 1703023309.825672
Ending IO phase at 1703023315.217721
write     15.95      25.50      0.313691    16384      1024.00    3.22       5.02       0.556528   8.03       66  
Ending compute phase at 1703023345.493284
Starting IO phase at 1703023345.493305
Ending IO phase at 1703023355.939427
write     5.70       12.40      0.202243    16384      1024.00    15.34      10.32      7.13       22.44      67  
Ending compute phase at 1703023378.621726
Starting IO phase at 1703023378.621748
Ending IO phase at 1703023393.221785
write     6.73       9.51       0.343983    16384      1024.00    11.62      13.47      9.02       19.01      68  
Ending compute phase at 1703023414.945979
Starting IO phase at 1703023414.946000
Ending IO phase at 1703023421.665705
write     12.58      25.82      0.284474    16384      1024.00    4.02       4.96       2.07       10.17      69  
Ending compute phase at 1703023462.945662
Starting IO phase at 1703023462.945683
Ending IO phase at 1703023475.117663
write     3.64       10.71      0.522194    16384      1024.00    24.77      11.95      3.74       35.18      70  
Ending compute phase at 1703023524.762015
Starting IO phase at 1703023524.762034
Ending IO phase at 1703023534.485702
write     3.12       16.34      0.379461    16384      1024.00    31.78      7.83       7.86       41.03      71  
Ending compute phase at 1703023558.250192
Starting IO phase at 1703023558.250213
Ending IO phase at 1703023567.418202
write     8.73       14.08      0.358925    16384      1024.00    11.30      9.09       3.36       14.66      72  
Ending compute phase at 1703023590.102925
Starting IO phase at 1703023590.102946
Ending IO phase at 1703023596.445765
write     11.87      21.94      0.348888    16384      1024.00    5.15       5.83       1.57       10.78      73  
Ending compute phase at 1703023617.846618
Starting IO phase at 1703023617.846642
Ending IO phase at 1703023629.833803
write     8.50       11.44      0.362761    16384      1024.00    8.99       11.19      6.08       15.06      74  
Ending compute phase at 1703023659.306125
Starting IO phase at 1703023659.306147
Ending IO phase at 1703023669.313879
write     6.03       12.50      0.391637    16384      1024.00    17.56      10.24      3.68       21.24      75  
Ending compute phase at 1703023690.486252
Starting IO phase at 1703023690.486273
Ending IO phase at 1703023695.933697
write     15.27      24.24      0.295778    16384      1024.00    3.63       5.28       0.598699   8.38       76  
Ending compute phase at 1703023722.986823
Starting IO phase at 1703023722.986844
Ending IO phase at 1703023730.465886
write     7.84       17.37      0.246597    16384      1024.00    12.88      7.37       3.49       16.33      77  
Ending compute phase at 1703023752.770116
Starting IO phase at 1703023752.770136
Ending IO phase at 1703023760.705715
write     10.69      16.98      0.471242    16384      1024.00    4.98       7.54       2.08       11.97      78  
Ending compute phase at 1703023800.654120
Starting IO phase at 1703023800.654142
Ending IO phase at 1703023807.297744
write     4.51       20.35      0.317407    16384      1024.00    22.76      6.29       1.52       28.41      79  
Ending compute phase at 1703023828.274432
Starting IO phase at 1703023828.274467
Ending IO phase at 1703023839.973862
write     8.91       11.74      0.391416    16384      1024.00    9.99       10.90      5.32       14.37      80  
Ending compute phase at 1703023862.491010
Starting IO phase at 1703023862.491033
Ending IO phase at 1703023893.925763
write     3.59       4.74       0.500530    16384      1024.00    13.55      27.03      23.38      35.68      81  
Ending compute phase at 1703023917.229866
Starting IO phase at 1703023917.229886
Ending IO phase at 1703023923.429669
write     11.35      22.18      0.353928    16384      1024.00    5.88       5.77       0.697808   11.28      82  
Ending compute phase at 1703023959.333479
Starting IO phase at 1703023959.333500
Ending IO phase at 1703023966.749888
write     5.11       19.28      0.301776    16384      1024.00    17.98      6.64       3.58       25.05      83  
Ending compute phase at 1703023990.198035
Starting IO phase at 1703023990.198056
Ending IO phase at 1703024003.665716
write     6.89       10.78      0.527841    16384      1024.00    6.14       11.88      4.92       18.57      84  
Ending compute phase at 1703024029.025959
Starting IO phase at 1703024029.025985
Ending IO phase at 1703024038.097664
write     7.92       14.28      0.328362    16384      1024.00    11.12      8.96       3.75       16.16      85  
Ending compute phase at 1703024075.057728
Starting IO phase at 1703024075.057749
Ending IO phase at 1703024081.929737
write     5.01       19.74      0.386957    16384      1024.00    19.13      6.48       0.609309   25.56      86  
Ending compute phase at 1703024125.525381
Starting IO phase at 1703024125.525404
Ending IO phase at 1703024130.769766
write     4.19       24.86      0.269528    16384      1024.00    25.59      5.15       1.08       30.55      87  
Ending compute phase at 1703024171.046965
Starting IO phase at 1703024171.046988
Ending IO phase at 1703024190.453892
write     3.09       10.50      0.443143    16384      1024.00    22.23      12.20      12.26      41.40      88  
Ending compute phase at 1703024237.432552
Starting IO phase at 1703024237.432574
Ending IO phase at 1703024254.421864
write     2.83       7.54       0.901415    16384      1024.00    35.82      16.97      12.47      45.29      89  
Ending compute phase at 1703024296.553877
Starting IO phase at 1703024296.553898
Ending IO phase at 1703024309.485721
write     3.47       10.14      0.609857    16384      1024.00    25.80      12.62      4.51       36.84      90  
Ending compute phase at 1703024350.287569
Starting IO phase at 1703024350.287592
Ending IO phase at 1703024359.968949
write     3.97       13.35      0.286310    16384      1024.00    27.16      9.59       5.04       32.20      91  
Ending compute phase at 1703024383.070260
Starting IO phase at 1703024383.070281
Ending IO phase at 1703024391.349700
write     9.76       20.24      0.363458    16384      1024.00    5.83       6.32       2.38       13.12      92  
Ending compute phase at 1703024412.969764
Starting IO phase at 1703024412.969787
Ending IO phase at 1703024420.717844
write     11.63      16.99      0.395022    16384      1024.00    4.04       7.53       2.50       11.01      93  
Ending compute phase at 1703024442.946098
Starting IO phase at 1703024442.946119
Ending IO phase at 1703024450.009672
write     11.64      18.58      0.349007    16384      1024.00    4.89       6.89       1.42       11.00      94  
Ending compute phase at 1703024475.509958
Starting IO phase at 1703024475.509981
Ending IO phase at 1703024482.597710
write     8.98       18.27      0.294919    16384      1024.00    9.13       7.01       2.30       14.25      95  
Ending compute phase at 1703024517.034289
Starting IO phase at 1703024517.034311
Ending IO phase at 1703024548.397887
write     2.69       4.10       1.23        16384      1024.00    32.11      31.18      11.61      47.57      96  
Ending compute phase at 1703024572.041917
Starting IO phase at 1703024572.041938
Ending IO phase at 1703024582.685680
write     8.02       12.11      0.456187    16384      1024.00    6.10       10.57      3.27       15.97      97  
Ending compute phase at 1703024606.949435
Starting IO phase at 1703024606.949455
Ending IO phase at 1703024625.722330
write     5.16       6.91       0.571661    16384      1024.00    15.19      18.52      14.54      24.79      98  
Ending compute phase at 1703024646.802104
Starting IO phase at 1703024646.802125
Ending IO phase at 1703024656.253715
write     10.42      13.68      0.547053    16384      1024.00    3.38       9.36       0.623915   12.28      99  
Ending compute phase at 1703024688.541987
Starting IO phase at 1703024688.542010
Ending IO phase at 1703024696.945885
write     5.70       15.70      0.509436    16384      1024.00    14.16      8.15       4.08       22.45      100 
Ending compute phase at 1703024719.078397
Starting IO phase at 1703024719.078419
Ending IO phase at 1703024731.118042
write     8.02       11.10      0.464134    16384      1024.00    4.87       11.54      4.57       15.96      101 
Ending compute phase at 1703024753.222045
Starting IO phase at 1703024753.222072
Ending IO phase at 1703024759.609719
write     12.52      20.70      0.377257    16384      1024.00    4.37       6.18       0.647656   10.23      102 
Ending compute phase at 1703024785.689960
Starting IO phase at 1703024785.689981
Ending IO phase at 1703024798.024639
write     6.35       10.44      0.478242    16384      1024.00    15.57      12.26      5.60       20.16      103 
