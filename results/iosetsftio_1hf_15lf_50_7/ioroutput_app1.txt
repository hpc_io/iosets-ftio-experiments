############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Dec 19 22:23:47 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703021027.893689
TestID              : 0
StartTime           : Tue Dec 19 22:23:47 2023
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703021387.925882
Starting IO phase at 1703021387.925902
Ending IO phase at 1703021519.469897
write     19.46      22.01      0.326741    327680     1024.00    0.125197   116.29     52.02      131.55     0   
Ending compute phase at 1703021911.434295
Starting IO phase at 1703021911.434317
Ending IO phase at 1703021964.960144
write     48.09      48.10      0.157425    327680     1024.00    0.992521   53.22      2.85       53.23      1   
Ending compute phase at 1703022327.466286
Starting IO phase at 1703022327.466308
Ending IO phase at 1703022389.264498
write     40.06      41.57      0.177123    327680     1024.00    2.58       61.59      4.91       63.91      2   
Ending compute phase at 1703022768.157869
Starting IO phase at 1703022768.157891
Ending IO phase at 1703022843.642927
write     27.29      34.80      0.210580    327680     1024.00    21.97      73.57      7.73       93.82      3   
Ending compute phase at 1703023209.781005
Starting IO phase at 1703023209.781031
Ending IO phase at 1703023288.795413
write     30.39      32.56      0.243755    327680     1024.00    5.58       78.63      9.44       84.24      4   
Ending compute phase at 1703023665.814680
Starting IO phase at 1703023665.814701
Ending IO phase at 1703023731.219988
write     31.32      39.42      0.201692    327680     1024.00    20.12      64.94      5.34       81.75      5   
Ending compute phase at 1703024105.322154
Starting IO phase at 1703024105.322175
Ending IO phase at 1703024171.419034
write     32.14      38.96      0.202236    327680     1024.00    20.10      65.72      3.71       79.64      6   
Ending compute phase at 1703024548.214034
Starting IO phase at 1703024548.214055
Ending IO phase at 1703024626.643037
write     26.97      32.70      0.182321    327680     1024.00    16.76      78.28      55.46      94.92      7   
