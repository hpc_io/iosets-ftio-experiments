############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Dec 19 22:23:48 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_11_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703021028.272521
TestID              : 0
StartTime           : Tue Dec 19 22:23:48 2023
Path                : /mnt/beegfs/iosets_11_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_11_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703021388.626474
Starting IO phase at 1703021388.626497
Ending IO phase at 1703021527.758190
write     18.36      20.18      0.379730    327680     1024.00    0.550308   126.85     21.57      139.45     0   
Ending compute phase at 1703021935.325894
Starting IO phase at 1703021935.325921
Ending IO phase at 1703021994.898371
write     32.24      43.35      0.183619    327680     1024.00    20.82      59.05      4.85       79.41      1   
Ending compute phase at 1703022355.441725
Starting IO phase at 1703022355.441746
Ending IO phase at 1703022423.415767
write     37.68      37.99      0.209883    327680     1024.00    0.578514   67.38      21.01      67.95      2   
Ending compute phase at 1703022823.565725
Starting IO phase at 1703022823.565747
Ending IO phase at 1703022914.663438
write     19.56      28.56      0.207489    327680     1024.00    41.08      89.64      31.49      130.88     3   
Ending compute phase at 1703023288.244134
Starting IO phase at 1703023288.244157
Ending IO phase at 1703023357.867797
write     30.93      35.61      0.202790    327680     1024.00    13.41      71.89      5.55       82.76      4   
Ending compute phase at 1703023760.722123
Starting IO phase at 1703023760.722162
Ending IO phase at 1703023888.221934
write     15.07      16.34      0.357397    327680     1024.00    42.90      156.67     25.41      169.87     5   
Ending compute phase at 1703024253.356464
Starting IO phase at 1703024253.356500
Ending IO phase at 1703024311.004740
write     41.07      44.61      0.120978    327680     1024.00    5.06       57.39      20.25      62.33      6   
Ending compute phase at 1703024692.801083
Starting IO phase at 1703024692.801105
