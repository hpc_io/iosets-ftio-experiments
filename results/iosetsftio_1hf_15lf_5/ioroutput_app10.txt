############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 23:24:30 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696109070.788220
TestID              : 0
StartTime           : Sat Sep 30 23:24:30 2023
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696109555.255746
Starting IO phase at 1696109555.255771
Ending IO phase at 1696109723.587186
write     8.79       15.26      0.436499    327680     1024.00    141.60     167.79     28.11      291.33     0   
Ending compute phase at 1696110179.672959
Starting IO phase at 1696110179.672984
Ending IO phase at 1696110259.972234
write     15.70      32.37      0.118094    327680     1024.00    114.00     79.09      42.41      163.06     1   
Ending compute phase at 1696110623.717232
Starting IO phase at 1696110623.717257
Ending IO phase at 1696110716.504203
write     26.69      27.75      0.288261    327680     1024.00    3.99       92.24      15.14      95.92      2   
Ending compute phase at 1696111078.494624
Starting IO phase at 1696111078.494648
Ending IO phase at 1696111142.399393
write     39.16      40.27      0.189760    327680     1024.00    2.04       63.57      3.50       65.38      3   
Ending compute phase at 1696111507.879475
Starting IO phase at 1696111507.879496
Ending IO phase at 1696111562.465457
write     42.94      47.63      0.166716    327680     1024.00    5.32       53.74      3.42       59.62      4   
Ending compute phase at 1696111926.023337
Starting IO phase at 1696111926.023360
Ending IO phase at 1696111961.584363
write     66.41      72.81      0.094886    327680     1024.00    3.41       35.16      10.55      38.55      5   
Ending compute phase at 1696112321.751616
Starting IO phase at 1696112321.751636
Ending IO phase at 1696112382.910167
write     42.06      42.06      0.180468    327680     1024.00    0.417466   60.87      3.12       60.87      6   
Ending compute phase at 1696112745.440628
Starting IO phase at 1696112745.440653
Ending IO phase at 1696112812.563231
write     37.04      38.32      0.203741    327680     1024.00    2.76       66.80      15.94      69.12      7   
