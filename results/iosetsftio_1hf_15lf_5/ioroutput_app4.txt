############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 23:24:29 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696109069.517379
TestID              : 0
StartTime           : Sat Sep 30 23:24:29 2023
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696109429.999238
Starting IO phase at 1696109429.999261
Ending IO phase at 1696109577.871234
write     17.26      18.63      0.377513    327680     1024.00    0.694553   137.43     46.67      148.30     0   
Ending compute phase at 1696109992.076678
Starting IO phase at 1696109992.076703
Ending IO phase at 1696110049.996376
write     30.74      52.53      0.111686    327680     1024.00    29.45      48.74      25.41      83.27      1   
Ending compute phase at 1696110410.411665
Starting IO phase at 1696110410.411685
Ending IO phase at 1696110464.856110
write     47.30      47.31      0.161666    327680     1024.00    0.082356   54.11      6.14       54.12      2   
Ending compute phase at 1696110825.076234
Starting IO phase at 1696110825.076255
Ending IO phase at 1696110873.500717
write     53.38      53.39      0.145156    327680     1024.00    0.048482   47.95      1.50       47.95      3   
Ending compute phase at 1696111235.450283
Starting IO phase at 1696111235.450307
Ending IO phase at 1696111295.128197
write     41.93      43.16      0.183880    327680     1024.00    2.02       59.32      7.36       61.05      4   
Ending compute phase at 1696111655.349290
Starting IO phase at 1696111655.349311
Ending IO phase at 1696111705.376963
write     51.52      51.52      0.141623    327680     1024.00    0.052574   49.69      4.37       49.69      5   
Ending compute phase at 1696112065.634728
Starting IO phase at 1696112065.634748
Ending IO phase at 1696112127.044286
write     41.99      41.99      0.188944    327680     1024.00    0.197268   60.96      3.17       60.97      6   
Ending compute phase at 1696112491.700915
Starting IO phase at 1696112491.700938
Ending IO phase at 1696112554.309311
write     38.38      41.14      0.184833    327680     1024.00    4.70       62.23      7.90       66.71      7   
