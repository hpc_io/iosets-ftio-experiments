############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 23:24:29 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696109069.952843
TestID              : 0
StartTime           : Sat Sep 30 23:24:29 2023
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696109452.655382
Starting IO phase at 1696109452.655402
Ending IO phase at 1696109641.333719
write     12.13      13.73      0.499407    327680     1024.00    36.22      186.46     32.37      211.12     0   
Ending compute phase at 1696110050.337461
Starting IO phase at 1696110050.337480
Ending IO phase at 1696110255.868045
write     10.08      12.46      0.638174    327680     1024.00    76.69      205.53     117.31     253.98     1   
Ending compute phase at 1696110619.133354
Starting IO phase at 1696110619.133380
Ending IO phase at 1696110685.402841
write     37.14      38.78      0.189721    327680     1024.00    3.31       66.02      7.16       68.92      2   
Ending compute phase at 1696111045.623531
Starting IO phase at 1696111045.623551
Ending IO phase at 1696111116.913377
write     36.06      36.07      0.187655    327680     1024.00    0.068467   70.98      10.93      70.98      3   
Ending compute phase at 1696111480.980470
Starting IO phase at 1696111480.980495
Ending IO phase at 1696111557.402910
write     31.99      33.81      0.231054    327680     1024.00    3.92       75.72      26.32      80.03      4   
Ending compute phase at 1696111926.023401
Starting IO phase at 1696111926.023415
Ending IO phase at 1696112008.859089
write     29.93      33.29      0.236555    327680     1024.00    3.05       76.91      26.54      85.52      5   
Ending compute phase at 1696112380.114174
Starting IO phase at 1696112380.114196
Ending IO phase at 1696112444.463766
write     36.56      39.09      0.198892    327680     1024.00    6.13       65.49      2.17       70.03      6   
Ending compute phase at 1696112818.084932
Starting IO phase at 1696112818.084956
