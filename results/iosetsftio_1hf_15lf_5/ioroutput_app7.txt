############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 23:24:30 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696109070.672653
TestID              : 0
StartTime           : Sat Sep 30 23:24:30 2023
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696109482.632250
Starting IO phase at 1696109482.632269
Ending IO phase at 1696109659.706259
write     11.20      14.01      0.536292    327680     1024.00    51.80      182.72     26.16      228.63     0   
Ending compute phase at 1696110050.337803
Starting IO phase at 1696110050.337835
Ending IO phase at 1696110127.735885
write     23.80      33.95      0.190533    327680     1024.00    59.26      75.41      16.28      107.58     1   
Ending compute phase at 1696110523.233717
Starting IO phase at 1696110523.233748
Ending IO phase at 1696110589.665103
write     38.01      38.94      0.203183    327680     1024.00    1.77       65.74      6.95       67.35      2   
Ending compute phase at 1696110949.896690
Starting IO phase at 1696110949.896712
Ending IO phase at 1696110979.901635
write     86.23      86.26      0.067792    327680     1024.00    0.059238   29.68      7.99       29.69      3   
Ending compute phase at 1696111340.615219
Starting IO phase at 1696111340.615242
Ending IO phase at 1696111388.521295
write     53.37      53.95      0.148272    327680     1024.00    0.516714   47.45      1.66       47.96      4   
Ending compute phase at 1696111752.415031
Starting IO phase at 1696111752.415054
Ending IO phase at 1696111813.739983
write     39.62      41.94      0.189672    327680     1024.00    4.43       61.04      3.77       64.61      5   
Ending compute phase at 1696112179.132332
Starting IO phase at 1696112179.132357
Ending IO phase at 1696112230.152666
write     45.78      50.52      0.153821    327680     1024.00    6.44       50.68      9.60       55.92      6   
Ending compute phase at 1696112594.106431
Starting IO phase at 1696112594.106454
Ending IO phase at 1696112649.593518
write     43.45      46.43      0.160617    327680     1024.00    12.34      55.14      4.96       58.91      7   
