############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 23:09:32 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705183772.176449
TestID              : 0
StartTime           : Sat Jan 13 23:09:32 2024
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705184186.959425
Starting IO phase at 1705184186.959445
Ending IO phase at 1705184497.029481
write     7.02       8.05       0.950521    327680     1024.00    54.56      317.94     40.01      364.47     0   
Ending compute phase at 1705184857.136366
Starting IO phase at 1705184857.136387
Ending IO phase at 1705184879.517476
write     115.39     115.43     0.056961    327680     1024.00    0.052550   22.18      3.95       22.19      1   
Ending compute phase at 1705185239.728083
Starting IO phase at 1705185239.728103
Ending IO phase at 1705185264.058076
write     106.86     106.89     0.065932    327680     1024.00    0.054133   23.95      4.16       23.96      2   
Ending compute phase at 1705185624.259184
Starting IO phase at 1705185624.259207
Ending IO phase at 1705185650.042095
write     101.08     101.10     0.076646    327680     1024.00    0.053128   25.32      4.88       25.33      3   
Ending compute phase at 1705186010.292616
Starting IO phase at 1705186010.292637
Ending IO phase at 1705186036.295435
write     100.00     100.04     0.062842    327680     1024.00    0.053312   25.59      5.48       25.60      4   
Ending compute phase at 1705186396.567948
Starting IO phase at 1705186396.567968
Ending IO phase at 1705186434.300323
write     68.55      68.56      0.095398    327680     1024.00    0.056371   37.34      6.81       37.35      5   
Ending compute phase at 1705186794.715521
Starting IO phase at 1705186794.715542
Ending IO phase at 1705186852.657074
write     44.31      44.49      0.161638    327680     1024.00    0.237988   57.54      6.92       57.78      6   
Ending compute phase at 1705187212.965588
Starting IO phase at 1705187212.965609
Ending IO phase at 1705187271.034110
write     44.27      44.36      0.180324    327680     1024.00    0.080630   57.70      11.33      57.83      7   
