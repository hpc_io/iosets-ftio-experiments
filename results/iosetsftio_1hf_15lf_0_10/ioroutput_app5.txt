############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 23:09:32 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705183772.264461
TestID              : 0
StartTime           : Sat Jan 13 23:09:32 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705184195.783190
Starting IO phase at 1705184195.783210
Ending IO phase at 1705184547.665527
write     6.17       7.64       0.977856    327680     1024.00    86.01      334.97     43.87      414.93     0   
Ending compute phase at 1705184907.916274
Starting IO phase at 1705184907.916296
Ending IO phase at 1705184943.433838
write     72.81      72.83      0.092864    327680     1024.00    5.37       35.15      5.44       35.16      1   
Ending compute phase at 1705185303.614962
Starting IO phase at 1705185303.614982
Ending IO phase at 1705185344.933627
write     62.40      62.41      0.115149    327680     1024.00    0.113562   41.02      6.29       41.03      2   
Ending compute phase at 1705185705.252334
Starting IO phase at 1705185705.252356
Ending IO phase at 1705185735.230065
write     86.75      86.76      0.077143    327680     1024.00    0.082742   29.51      4.82       29.51      3   
Ending compute phase at 1705186095.845106
Starting IO phase at 1705186095.845130
Ending IO phase at 1705186123.142897
write     93.36      94.80      0.071773    327680     1024.00    1.01       27.00      4.04       27.42      4   
Ending compute phase at 1705186484.897719
Starting IO phase at 1705186484.897741
Ending IO phase at 1705186520.722692
write     69.09      71.95      0.110371    327680     1024.00    1.57       35.58      1.76       37.05      5   
Ending compute phase at 1705186880.972773
Starting IO phase at 1705186880.972795
Ending IO phase at 1705186937.304672
write     45.83      45.85      0.167620    327680     1024.00    0.071674   55.84      5.12       55.85      6   
Ending compute phase at 1705187297.492403
Starting IO phase at 1705187297.492425
Ending IO phase at 1705187354.728968
write     45.14      45.15      0.163331    327680     1024.00    0.135050   56.70      6.54       56.71      7   
