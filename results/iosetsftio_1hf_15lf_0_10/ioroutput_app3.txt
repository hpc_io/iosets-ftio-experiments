############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 23:09:32 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705183772.616438
TestID              : 0
StartTime           : Sat Jan 13 23:09:32 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705184384.409138
Starting IO phase at 1705184384.409163
Ending IO phase at 1705184650.942168
write     5.07       8.52       0.764230    327680     1024.00    257.60     300.33     21.59      505.32     0   
Ending compute phase at 1705185012.044995
Starting IO phase at 1705185012.045020
Ending IO phase at 1705185062.808178
write     49.89      51.10      0.129944    327680     1024.00    1.50       50.10      10.31      51.32      1   
Ending compute phase at 1705185424.907803
Starting IO phase at 1705185424.907823
Ending IO phase at 1705185452.321242
write     94.93      94.95      0.072392    327680     1024.00    0.115823   26.96      9.91       26.97      2   
Ending compute phase at 1705185812.551689
Starting IO phase at 1705185812.551711
Ending IO phase at 1705185854.329170
write     61.66      62.03      0.125571    327680     1024.00    0.274530   41.27      2.53       41.52      3   
Ending compute phase at 1705186214.596828
Starting IO phase at 1705186214.596849
Ending IO phase at 1705186250.133525
write     72.78      72.86      0.091819    327680     1024.00    0.042029   35.14      5.76       35.17      4   
Ending compute phase at 1705186610.348199
Starting IO phase at 1705186610.348222
Ending IO phase at 1705186661.334543
write     50.59      50.60      0.157593    327680     1024.00    0.139281   50.60      0.327219   50.60      5   
Ending compute phase at 1705187021.543031
Starting IO phase at 1705187021.543052
Ending IO phase at 1705187076.909922
write     46.41      46.42      0.170007    327680     1024.00    0.100894   55.15      10.46      55.16      6   
Ending compute phase at 1705187438.283663
Starting IO phase at 1705187438.283683
Ending IO phase at 1705187492.888380
write     46.23      47.22      0.148096    327680     1024.00    1.19       54.22      12.92      55.38      7   
