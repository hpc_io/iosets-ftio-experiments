############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 23:09:32 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705183772.444634
TestID              : 0
StartTime           : Sat Jan 13 23:09:32 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705184345.363789
Starting IO phase at 1705184345.363813
Ending IO phase at 1705184650.885800
write     5.00       7.22       0.877620    327680     1024.00    256.26     354.52     73.02      512.07     0   
Ending compute phase at 1705185011.068067
Starting IO phase at 1705185011.068088
Ending IO phase at 1705185072.230236
write     42.15      42.17      0.189730    327680     1024.00    0.091943   60.71      4.15       60.73      1   
Ending compute phase at 1705185434.092993
Starting IO phase at 1705185434.093015
Ending IO phase at 1705185474.445021
write     61.75      64.31      0.124349    327680     1024.00    1.67       39.81      5.15       41.46      2   
Ending compute phase at 1705185834.699104
Starting IO phase at 1705185834.699126
Ending IO phase at 1705185879.309708
write     57.91      57.92      0.125338    327680     1024.00    0.128659   44.20      4.09       44.21      3   
Ending compute phase at 1705186239.584802
Starting IO phase at 1705186239.584823
Ending IO phase at 1705186275.760728
write     71.36      71.37      0.107943    327680     1024.00    0.053979   35.87      5.26       35.88      4   
Ending compute phase at 1705186635.971563
Starting IO phase at 1705186635.971583
Ending IO phase at 1705186689.047411
write     48.62      48.62      0.158889    327680     1024.00    0.342699   52.65      4.82       52.66      5   
Ending compute phase at 1705187049.373725
Starting IO phase at 1705187049.373749
Ending IO phase at 1705187103.433104
write     47.44      47.44      0.168215    327680     1024.00    0.135230   53.96      7.35       53.97      6   
Ending compute phase at 1705187464.285127
Starting IO phase at 1705187464.285147
Ending IO phase at 1705187522.840639
write     43.52      44.04      0.180259    327680     1024.00    1.03       58.13      1.48       58.82      7   
