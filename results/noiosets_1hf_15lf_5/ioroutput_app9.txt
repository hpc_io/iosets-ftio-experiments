############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 22:20:39 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_9_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696105239.740519
TestID              : 0
StartTime           : Sat Sep 30 22:20:39 2023
Path                : /mnt/beegfs/iosets_9_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_9_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696105600.098779
Starting IO phase at 1696105600.098806
Ending IO phase at 1696105974.286797
write     6.84       6.85       1.17        327680     1024.00    0.218745   373.93     6.97       374.20     0   
Ending compute phase at 1696106335.418242
Starting IO phase at 1696106335.418265
Ending IO phase at 1696106674.285927
write     7.54       7.56       1.06        327680     1024.00    1.04       338.69     2.25       339.72     1   
Ending compute phase at 1696107034.671054
Starting IO phase at 1696107034.671089
Ending IO phase at 1696107220.690235
write     13.77      13.79      0.558627    327680     1024.00    0.125176   185.70     10.86      185.96     2   
Ending compute phase at 1696107582.061455
Starting IO phase at 1696107582.061469
Ending IO phase at 1696107628.577368
write     54.45      55.53      0.136424    327680     1024.00    1.87       46.10      4.52       47.01      3   
Ending compute phase at 1696107989.792876
Starting IO phase at 1696107989.792894
Ending IO phase at 1696108025.576878
write     70.34      72.31      0.110604    327680     1024.00    1.27       35.40      9.26       36.40      4   
Ending compute phase at 1696108386.285828
Starting IO phase at 1696108386.285871
Ending IO phase at 1696108458.957970
write     35.11      35.33      0.225944    327680     1024.00    0.464098   72.47      2.07       72.91      5   
Ending compute phase at 1696108819.346559
Starting IO phase at 1696108819.346580
Ending IO phase at 1696108898.591838
write     32.29      32.37      0.235884    327680     1024.00    0.263842   79.07      3.59       79.28      6   
