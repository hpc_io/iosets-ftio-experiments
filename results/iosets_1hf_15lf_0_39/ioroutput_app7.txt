############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 22:24:35 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705440275.638173
TestID              : 0
StartTime           : Tue Jan 16 22:24:35 2024
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705440733.760914
Starting IO phase at 1705440733.760942
Ending IO phase at 1705440869.510310
write     10.98      12.10      0.382487    327680     1024.00    97.76      211.57     13.30      233.20     0   
Ending compute phase at 1705441229.629472
Starting IO phase at 1705441229.629486
Ending IO phase at 1705441253.077430
write     110.86     110.90     0.061453    327680     1024.00    0.071943   23.08      3.42       23.09      1   
Ending compute phase at 1705441614.325247
Starting IO phase at 1705441614.325261
Ending IO phase at 1705441638.928166
write     101.19     105.45     0.061468    327680     1024.00    1.49       24.28      4.61       25.30      2   
Ending compute phase at 1705441999.141403
Starting IO phase at 1705441999.141417
Ending IO phase at 1705442027.776744
write     90.45      90.47      0.085732    327680     1024.00    0.164414   28.30      4.87       28.30      3   
Ending compute phase at 1705442388.070554
Starting IO phase at 1705442388.070570
Ending IO phase at 1705442422.666845
write     74.86      74.87      0.105793    327680     1024.00    0.080870   34.19      3.05       34.20      4   
Ending compute phase at 1705442782.900471
Starting IO phase at 1705442782.900485
Ending IO phase at 1705442831.024550
write     53.57      53.58      0.131117    327680     1024.00    0.051151   47.78      5.83       47.79      5   
Ending compute phase at 1705443191.218817
Starting IO phase at 1705443191.218830
Ending IO phase at 1705443247.829743
write     45.48      45.48      0.169198    327680     1024.00    0.157037   56.29      4.12       56.29      6   
Ending compute phase at 1705443608.008544
Starting IO phase at 1705443608.008557
Ending IO phase at 1705443665.497056
write     44.80      44.80      0.173211    327680     1024.00    0.150753   57.14      5.48       57.15      7   
Ending compute phase at 1705444032.454142
Starting IO phase at 1705444032.454161
