############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 22:24:35 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705440275.553528
TestID              : 0
StartTime           : Tue Jan 16 22:24:35 2024
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705440649.049143
Starting IO phase at 1705440649.049172
Ending IO phase at 1705440929.652158
write     8.72       9.05       0.861653    327680     1024.00    15.19      282.96     4.70       293.73     0   
Ending compute phase at 1705441289.849423
Starting IO phase at 1705441289.849438
Ending IO phase at 1705441319.682062
write     87.00      87.02      0.085345    327680     1024.00    0.053937   29.42      7.76       29.43      1   
Ending compute phase at 1705441679.861888
Starting IO phase at 1705441679.861903
Ending IO phase at 1705441707.911284
write     92.34      92.36      0.080425    327680     1024.00    0.044525   27.72      4.89       27.73      2   
Ending compute phase at 1705442068.129615
Starting IO phase at 1705442068.129631
Ending IO phase at 1705442102.095106
write     76.31      76.56      0.097093    327680     1024.00    0.057664   33.44      4.91       33.55      3   
Ending compute phase at 1705442462.264689
Starting IO phase at 1705442462.264704
Ending IO phase at 1705442501.403774
write     65.66      65.67      0.120341    327680     1024.00    0.047583   38.98      3.72       38.99      4   
Ending compute phase at 1705442861.604949
Starting IO phase at 1705442861.604965
Ending IO phase at 1705442915.426518
write     47.79      47.79      0.164854    327680     1024.00    0.090983   53.57      2.54       53.57      5   
Ending compute phase at 1705443275.636512
Starting IO phase at 1705443275.636525
Ending IO phase at 1705443334.185757
write     43.95      43.95      0.173334    327680     1024.00    0.146729   58.25      2.78       58.25      6   
Ending compute phase at 1705443694.523447
Starting IO phase at 1705443694.523465
Ending IO phase at 1705443752.442062
write     44.31      44.34      0.180242    327680     1024.00    0.086859   57.73      10.84      57.77      7   
