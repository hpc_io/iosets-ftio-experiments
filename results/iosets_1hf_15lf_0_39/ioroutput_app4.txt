############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 22:24:35 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705440275.662140
TestID              : 0
StartTime           : Tue Jan 16 22:24:35 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705440726.860959
Starting IO phase at 1705440726.860974
Ending IO phase at 1705440802.309980
write     15.45      32.31      0.197771    327680     1024.00    92.04      79.24      12.09      165.74     0   
Ending compute phase at 1705441162.410405
Starting IO phase at 1705441162.410418
Ending IO phase at 1705441188.087024
write     101.16     101.18     0.065699    327680     1024.00    0.066241   25.30      5.84       25.31      1   
Ending compute phase at 1705441548.253528
Starting IO phase at 1705441548.253541
Ending IO phase at 1705441571.394535
write     111.75     111.78     0.057420    327680     1024.00    0.048690   22.90      4.53       22.91      2   
Ending compute phase at 1705441931.661083
Starting IO phase at 1705441931.661097
Ending IO phase at 1705441957.663427
write     99.70      99.72      0.069561    327680     1024.00    0.066460   25.67      6.97       25.68      3   
Ending compute phase at 1705442317.832665
Starting IO phase at 1705442317.832682
Ending IO phase at 1705442344.715525
write     96.89      96.91      0.081390    327680     1024.00    0.071423   26.42      1.04       26.42      4   
Ending compute phase at 1705442704.972729
Starting IO phase at 1705442704.972745
Ending IO phase at 1705442747.099032
write     61.16      61.17      0.122670    327680     1024.00    0.066298   41.85      3.27       41.86      5   
Ending compute phase at 1705443107.289831
Starting IO phase at 1705443107.289845
Ending IO phase at 1705443163.526688
write     45.78      45.79      0.172200    327680     1024.00    0.109212   55.91      3.01       55.92      6   
Ending compute phase at 1705443523.712835
Starting IO phase at 1705443523.712848
Ending IO phase at 1705443579.674129
write     45.98      45.98      0.155954    327680     1024.00    0.263043   55.67      5.77       55.68      7   
Ending compute phase at 1705443941.952066
Starting IO phase at 1705443941.952088
Ending IO phase at 1705444005.109690
write     39.42      40.73      0.177817    327680     1024.00    2.38       62.85      5.96       64.95      8   
