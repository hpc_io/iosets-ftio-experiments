############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 22:24:35 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705440275.590146
TestID              : 0
StartTime           : Tue Jan 16 22:24:35 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705440726.549009
Starting IO phase at 1705440726.549026
Ending IO phase at 1705440813.938507
write     14.38      16.16      0.272857    327680     1024.00    90.82      158.38     4.59       178.06     0   
Ending compute phase at 1705441179.429860
Starting IO phase at 1705441179.429875
Ending IO phase at 1705441207.690987
write     77.12      91.54      0.085034    327680     1024.00    5.43       27.97      2.14       33.19      1   
Ending compute phase at 1705441570.895339
Starting IO phase at 1705441570.895356
Ending IO phase at 1705441594.379464
write     97.66      110.16     0.071503    327680     1024.00    3.04       23.24      5.26       26.21      2   
Ending compute phase at 1705441954.588341
Starting IO phase at 1705441954.588354
Ending IO phase at 1705441983.939138
write     87.79      87.80      0.076931    327680     1024.00    0.053660   29.16      8.91       29.16      3   
Ending compute phase at 1705442344.165028
Starting IO phase at 1705442344.165041
Ending IO phase at 1705442370.455070
write     98.19      98.21      0.079490    327680     1024.00    0.075648   26.07      1.04       26.07      4   
Ending compute phase at 1705442730.695766
Starting IO phase at 1705442730.695779
Ending IO phase at 1705442775.531054
write     57.56      57.59      0.133408    327680     1024.00    0.048648   44.45      2.77       44.48      5   
Ending compute phase at 1705443135.729534
Starting IO phase at 1705443135.729548
Ending IO phase at 1705443191.468680
write     46.19      46.19      0.171681    327680     1024.00    0.134833   55.42      1.22       55.42      6   
Ending compute phase at 1705443551.692457
Starting IO phase at 1705443551.692470
Ending IO phase at 1705443608.590111
write     45.33      45.34      0.168218    327680     1024.00    0.367093   56.46      3.39       56.47      7   
Ending compute phase at 1705443974.994509
Starting IO phase at 1705443974.994530
Ending IO phase at 1705444033.917529
write     39.52      43.77      0.182760    327680     1024.00    7.73       58.48      10.88      64.78      8   
