############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Dec 22 03:50:00 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703213400.264005
TestID              : 0
StartTime           : Fri Dec 22 03:50:00 2023
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703213813.191899
Starting IO phase at 1703213813.191922
Ending IO phase at 1703214164.020705
write     6.35       7.54       0.964098    327680     1024.00    67.63      339.73     42.17      403.40     0   
Ending compute phase at 1703214526.717134
Starting IO phase at 1703214526.717156
Ending IO phase at 1703214563.598976
write     69.93      71.05      0.105234    327680     1024.00    0.067416   36.03      4.04       36.61      1   
Ending compute phase at 1703214924.866649
Starting IO phase at 1703214924.866671
Ending IO phase at 1703214949.644970
write     100.67     105.46     0.061786    327680     1024.00    1.57       24.28      14.01      25.43      2   
Ending compute phase at 1703215309.874502
Starting IO phase at 1703215309.874523
Ending IO phase at 1703215333.835446
write     107.65     107.68     0.063379    327680     1024.00    0.064742   23.77      3.50       23.78      3   
Ending compute phase at 1703215697.765234
Starting IO phase at 1703215697.765255
Ending IO phase at 1703215725.096502
write     83.70      95.17      0.082026    327680     1024.00    3.72       26.90      1.50       30.59      4   
Ending compute phase at 1703216085.563528
Starting IO phase at 1703216085.563576
Ending IO phase at 1703216126.516845
write     62.71      63.14      0.125327    327680     1024.00    0.277993   40.55      7.97       40.82      5   
Ending compute phase at 1703216486.674713
Starting IO phase at 1703216486.674735
Ending IO phase at 1703216549.947100
write     40.71      40.71      0.135458    327680     1024.00    0.101868   62.88      19.54      62.89      6   
Ending compute phase at 1703216910.095204
Starting IO phase at 1703216910.095225
Ending IO phase at 1703216960.955751
write     50.66      50.67      0.089543    327680     1024.00    0.060280   50.53      21.88      50.54      7   
