############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 13:15:47 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696158947.782169
TestID              : 0
StartTime           : Sun Oct  1 13:15:47 2023
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696159307.857599
Starting IO phase at 1696159307.857613
Ending IO phase at 1696159591.877075
write     9.01       9.03       0.836881    327680     1024.00    0.137095   283.55     26.87      284.06     0   
Ending compute phase at 1696159952.168425
Starting IO phase at 1696159952.168443
Ending IO phase at 1696160183.321738
write     11.07      11.08      0.711526    327680     1024.00    0.163525   231.07     3.44       231.29     1   
Ending compute phase at 1696160543.342470
Starting IO phase at 1696160543.342484
Ending IO phase at 1696160573.573475
write     85.57      85.58      0.088313    327680     1024.00    0.090876   29.91      4.08       29.92      2   
Ending compute phase at 1696160933.783459
Starting IO phase at 1696160933.783474
Ending IO phase at 1696160964.482205
write     84.42      84.43      0.094252    327680     1024.00    0.055252   30.32      18.10      30.33      3   
Ending compute phase at 1696161327.855927
Starting IO phase at 1696161327.855953
Ending IO phase at 1696161473.929554
write     17.19      17.58      0.448796    327680     1024.00    3.19       145.64     5.13       148.97     4   
Ending compute phase at 1696161835.081653
Starting IO phase at 1696161835.081676
Ending IO phase at 1696162014.021032
write     14.25      14.33      0.552007    327680     1024.00    1.02       178.66     6.04       179.69     5   
Ending compute phase at 1696162374.340232
Starting IO phase at 1696162374.340251
Ending IO phase at 1696162482.669888
write     23.64      23.69      0.335926    327680     1024.00    0.226436   108.07     9.67       108.29     6   
