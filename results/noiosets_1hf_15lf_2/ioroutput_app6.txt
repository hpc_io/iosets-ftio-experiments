############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 13:15:48 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_6_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696158948.284179
TestID              : 0
StartTime           : Sun Oct  1 13:15:48 2023
Path                : /mnt/beegfs/iosets_6_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_6_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696159308.533047
Starting IO phase at 1696159308.533074
Ending IO phase at 1696159599.586487
write     8.79       8.80       0.902018    327680     1024.00    0.168282   290.95     5.63       291.13     0   
Ending compute phase at 1696159960.818684
Starting IO phase at 1696159960.818708
Ending IO phase at 1696160223.028362
write     9.74       9.77       0.817055    327680     1024.00    0.844028   262.04     0.617446   262.88     1   
Ending compute phase at 1696160586.878759
Starting IO phase at 1696160586.878778
Ending IO phase at 1696160681.865241
write     26.03      27.05      0.292104    327680     1024.00    3.47       94.66      4.49       98.36      2   
Ending compute phase at 1696161042.011393
Starting IO phase at 1696161042.011407
Ending IO phase at 1696161080.716960
write     66.72      66.79      0.111076    327680     1024.00    0.045629   38.33      5.35       38.37      3   
Ending compute phase at 1696161445.137240
Starting IO phase at 1696161445.137261
Ending IO phase at 1696161531.039225
write     28.53      29.92      0.263052    327680     1024.00    4.32       85.57      3.95       89.74      4   
Ending compute phase at 1696161893.026269
Starting IO phase at 1696161893.026295
Ending IO phase at 1696162056.008732
write     15.56      15.74      0.500171    327680     1024.00    1.85       162.68     2.63       164.50     5   
Ending compute phase at 1696162417.335853
Starting IO phase at 1696162417.335876
Ending IO phase at 1696162546.381385
write     19.70      19.88      0.400528    327680     1024.00    1.17       128.78     1.08       129.97     6   
