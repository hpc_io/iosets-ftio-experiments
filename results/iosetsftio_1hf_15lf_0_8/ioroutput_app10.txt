############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 21:01:49 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705176109.308852
TestID              : 0
StartTime           : Sat Jan 13 21:01:49 2024
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705176528.729319
Starting IO phase at 1705176528.729337
Ending IO phase at 1705176848.449979
write     6.76       8.22       0.910517    327680     1024.00    84.09      311.48     27.93      378.60     0   
Ending compute phase at 1705177276.347215
Starting IO phase at 1705177276.347240
Ending IO phase at 1705177396.694233
write     13.64      30.40      0.189294    327680     1024.00    87.16      84.21      75.56      187.72     1   
Ending compute phase at 1705177795.576020
Starting IO phase at 1705177795.576042
Ending IO phase at 1705177825.117186
write     87.63      87.65      0.074019    327680     1024.00    0.059672   29.21      15.87      29.21      2   
Ending compute phase at 1705178185.339503
Starting IO phase at 1705178185.339525
Ending IO phase at 1705178210.907565
write     101.34     101.36     0.054546    327680     1024.00    0.049751   25.26      7.80       25.26      3   
Ending compute phase at 1705178571.197921
Starting IO phase at 1705178571.197951
Ending IO phase at 1705178606.337795
write     73.89      74.72      0.105993    327680     1024.00    0.057773   34.26      2.52       34.65      4   
Ending compute phase at 1705178966.554022
Starting IO phase at 1705178966.554043
Ending IO phase at 1705179012.276210
write     56.53      56.56      0.141431    327680     1024.00    0.468084   45.26      2.51       45.29      5   
Ending compute phase at 1705179372.527750
Starting IO phase at 1705179372.527772
Ending IO phase at 1705179402.114053
write     87.58      87.60      0.084819    327680     1024.00    0.062918   29.22      8.10       29.23      6   
Ending compute phase at 1705179766.747777
Starting IO phase at 1705179766.747802
Ending IO phase at 1705179807.101811
write     57.73      58.62      0.124342    327680     1024.00    4.43       43.67      5.81       44.35      7   
