############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 21:01:48 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705176108.086499
TestID              : 0
StartTime           : Sat Jan 13 21:01:48 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705176468.201234
Starting IO phase at 1705176468.201255
Ending IO phase at 1705176688.864086
write     11.60      14.07      0.466197    327680     1024.00    1.60       181.94     93.24      220.72     0   
Ending compute phase at 1705177099.213924
Starting IO phase at 1705177099.213946
Ending IO phase at 1705177159.643999
write     42.51      42.87      0.185795    327680     1024.00    0.788453   59.72      3.54       60.23      1   
Ending compute phase at 1705177542.879441
Starting IO phase at 1705177542.879479
Ending IO phase at 1705177620.607104
write     29.33      32.96      0.242050    327680     1024.00    12.87      77.67      7.14       87.29      2   
Ending compute phase at 1705177983.188399
Starting IO phase at 1705177983.188423
Ending IO phase at 1705178026.520406
write     56.22      59.33      0.132234    327680     1024.00    2.59       43.15      2.08       45.54      3   
Ending compute phase at 1705178386.727525
Starting IO phase at 1705178386.727545
Ending IO phase at 1705178413.628116
write     96.59      96.62      0.052731    327680     1024.00    0.074474   26.50      9.62       26.50      4   
Ending compute phase at 1705178773.864284
Starting IO phase at 1705178773.864305
Ending IO phase at 1705178803.165464
write     88.61      88.63      0.090120    327680     1024.00    0.046405   28.88      4.01       28.89      5   
Ending compute phase at 1705179163.399605
Starting IO phase at 1705179163.399625
Ending IO phase at 1705179192.781821
write     88.49      88.51      0.089078    327680     1024.00    0.067225   28.92      3.64       28.93      6   
Ending compute phase at 1705179553.158073
Starting IO phase at 1705179553.158093
Ending IO phase at 1705179584.438885
write     82.19      82.51      0.095084    327680     1024.00    0.968398   31.03      2.78       31.15      7   
