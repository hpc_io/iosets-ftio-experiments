############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 21:01:48 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705176108.084844
TestID              : 0
StartTime           : Sat Jan 13 21:01:48 2024
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705176468.169579
Starting IO phase at 1705176468.169600
Ending IO phase at 1705176674.011822
write     12.44      15.26      0.481141    327680     1024.00    1.36       167.80     72.96      205.87     0   
Ending compute phase at 1705177034.045447
Starting IO phase at 1705177034.045468
Ending IO phase at 1705177082.269279
write     53.55      54.89      0.143429    327680     1024.00    0.725581   46.64      4.47       47.81      1   
Ending compute phase at 1705177535.888778
Starting IO phase at 1705177535.888792
Ending IO phase at 1705177599.265024
write     16.59      40.65      0.196808    327680     1024.00    92.61      62.98      2.42       154.35     2   
Ending compute phase at 1705177960.768762
Starting IO phase at 1705177960.768785
Ending IO phase at 1705178007.192530
write     53.90      55.52      0.144087    327680     1024.00    1.80       46.11      6.11       47.50      3   
Ending compute phase at 1705178367.396954
Starting IO phase at 1705178367.396974
Ending IO phase at 1705178394.205929
write     96.65      96.80      0.066254    327680     1024.00    0.044240   26.45      6.69       26.49      4   
Ending compute phase at 1705178754.459358
Starting IO phase at 1705178754.459379
Ending IO phase at 1705178781.193843
write     97.10      97.14      0.071010    327680     1024.00    0.047522   26.35      7.68       26.37      5   
Ending compute phase at 1705179141.379259
Starting IO phase at 1705179141.379280
Ending IO phase at 1705179167.869710
write     98.46      98.48      0.075500    327680     1024.00    0.040957   25.99      3.24       26.00      6   
Ending compute phase at 1705179530.466660
Starting IO phase at 1705179530.466684
Ending IO phase at 1705179557.601337
write     88.00      91.75      0.082534    327680     1024.00    2.65       27.90      5.91       29.09      7   
