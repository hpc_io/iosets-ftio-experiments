############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 21:01:49 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705176109.296872
TestID              : 0
StartTime           : Sat Jan 13 21:01:49 2024
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705176534.929004
Starting IO phase at 1705176534.929024
Ending IO phase at 1705176848.502029
write     6.76       8.32       0.868295    327680     1024.00    104.38     307.67     35.41      378.66     0   
Ending compute phase at 1705177292.330663
Starting IO phase at 1705177292.330686
Ending IO phase at 1705177517.318391
write     8.30       13.92      0.500111    327680     1024.00    83.65      183.86     122.70     308.47     1   
Ending compute phase at 1705177893.473345
Starting IO phase at 1705177893.473368
Ending IO phase at 1705178143.066754
write     10.27      10.27      0.762828    327680     1024.00    0.063840   249.16     21.36      249.18     2   
Ending compute phase at 1705178503.287964
Starting IO phase at 1705178503.287984
Ending IO phase at 1705178540.644683
write     69.20      69.21      0.113099    327680     1024.00    0.164884   36.99      2.35       36.99      3   
Ending compute phase at 1705178904.018248
Starting IO phase at 1705178904.018272
Ending IO phase at 1705178962.040678
write     41.96      46.26      0.169770    327680     1024.00    3.37       55.34      9.61       61.00      4   
Ending compute phase at 1705179322.255800
Starting IO phase at 1705179322.255821
Ending IO phase at 1705179353.077705
write     84.44      84.47      0.082116    327680     1024.00    0.102582   30.31      8.02       30.32      5   
Ending compute phase at 1705179713.295157
Starting IO phase at 1705179713.295177
Ending IO phase at 1705179751.197362
write     68.33      68.34      0.107185    327680     1024.00    0.063105   37.46      3.34       37.47      6   
