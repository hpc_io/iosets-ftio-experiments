############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 21:01:48 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705176108.550526
TestID              : 0
StartTime           : Sat Jan 13 21:01:48 2024
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705176494.241039
Starting IO phase at 1705176494.241058
Ending IO phase at 1705176813.094076
write     7.44       8.52       0.809797    327680     1024.00    50.39      300.44     59.59      344.24     0   
Ending compute phase at 1705177198.702438
Starting IO phase at 1705177198.702462
Ending IO phase at 1705177536.038831
write     7.06       7.61       0.739903    327680     1024.00    32.81      336.58     108.44     362.49     1   
Ending compute phase at 1705177896.206199
Starting IO phase at 1705177896.206221
Ending IO phase at 1705178163.333276
write     9.60       9.60       0.833324    327680     1024.00    0.957458   266.78     1.68       266.79     2   
Ending compute phase at 1705178527.333991
Starting IO phase at 1705178527.334016
Ending IO phase at 1705178584.133679
write     42.57      45.44      0.176041    327680     1024.00    15.29      56.34      6.11       60.13      3   
Ending compute phase at 1705178957.597273
Starting IO phase at 1705178957.597297
Ending IO phase at 1705179017.486395
write     35.19      43.04      0.179742    327680     1024.00    16.47      59.48      28.79      72.76      4   
Ending compute phase at 1705179377.706693
Starting IO phase at 1705179377.706714
Ending IO phase at 1705179437.393090
write     43.21      43.21      0.150170    327680     1024.00    0.109558   59.24      18.99      59.25      5   
Ending compute phase at 1705179797.642129
Starting IO phase at 1705179797.642150
Ending IO phase at 1705179837.245684
write     65.29      65.31      0.114329    327680     1024.00    0.731411   39.20      5.08       39.21      6   
