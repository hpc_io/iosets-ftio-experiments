############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Dec 21 23:34:32 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703198072.530221
TestID              : 0
StartTime           : Thu Dec 21 23:34:32 2023
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703198433.145455
Starting IO phase at 1703198433.145481
Ending IO phase at 1703198667.271507
write     10.91      12.19      0.640432    327680     1024.00    0.589218   210.00     43.29      234.64     0   
Ending compute phase at 1703199085.712807
Starting IO phase at 1703199085.712834
Ending IO phase at 1703199132.091728
write     51.55      54.83      0.143103    327680     1024.00    5.06       46.69      2.96       49.66      1   
Ending compute phase at 1703199492.377880
Starting IO phase at 1703199492.377902
Ending IO phase at 1703199532.304766
write     64.74      64.78      0.110701    327680     1024.00    0.167764   39.52      6.16       39.54      2   
Ending compute phase at 1703199892.487449
Starting IO phase at 1703199892.487470
Ending IO phase at 1703199922.547804
write     86.05      86.07      0.086622    327680     1024.00    0.353734   29.74      12.91      29.75      3   
Ending compute phase at 1703200285.097697
Starting IO phase at 1703200285.097728
Ending IO phase at 1703200310.837628
write     92.13      100.82     0.076791    327680     1024.00    2.86       25.39      0.876505   27.79      4   
Ending compute phase at 1703200674.751990
Starting IO phase at 1703200674.752016
Ending IO phase at 1703200726.412571
write     46.52      50.04      0.159859    327680     1024.00    3.91       51.15      4.45       55.03      5   
Ending compute phase at 1703201086.976889
Starting IO phase at 1703201086.976911
Ending IO phase at 1703201143.291898
write     45.54      45.65      0.174579    327680     1024.00    0.347468   56.07      6.28       56.21      6   
Ending compute phase at 1703201503.594105
Starting IO phase at 1703201503.594126
Ending IO phase at 1703201560.337472
write     45.52      45.58      0.174757    327680     1024.00    0.123650   56.17      10.82      56.24      7   
