############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Dec 21 23:34:33 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703198073.699966
TestID              : 0
StartTime           : Thu Dec 21 23:34:33 2023
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703198507.891490
Starting IO phase at 1703198507.891510
Ending IO phase at 1703198805.336673
write     6.90       8.60       0.888418    327680     1024.00    79.50      297.54     17.74      371.22     0   
Ending compute phase at 1703199174.676443
Starting IO phase at 1703199174.676467
Ending IO phase at 1703199245.660018
write     32.01      48.81      0.111698    327680     1024.00    9.15       52.44      35.09      79.98      1   
Ending compute phase at 1703199614.370797
Starting IO phase at 1703199614.370820
Ending IO phase at 1703199656.359738
write     61.47      62.13      0.128766    327680     1024.00    0.483888   41.21      8.78       41.64      2   
Ending compute phase at 1703200016.646307
Starting IO phase at 1703200016.646328
Ending IO phase at 1703200063.239563
write     55.38      55.40      0.128997    327680     1024.00    0.051679   46.21      5.45       46.23      3   
Ending compute phase at 1703200423.467104
Starting IO phase at 1703200423.467124
Ending IO phase at 1703200470.948456
write     54.24      54.25      0.143600    327680     1024.00    0.130093   47.19      1.24       47.20      4   
Ending compute phase at 1703200831.110084
Starting IO phase at 1703200831.110105
Ending IO phase at 1703200864.044804
write     78.60      78.62      0.097901    327680     1024.00    0.765494   32.56      3.98       32.57      5   
Ending compute phase at 1703201224.245283
Starting IO phase at 1703201224.245304
Ending IO phase at 1703201280.835618
write     45.48      45.48      0.171616    327680     1024.00    0.091866   56.29      4.64       56.29      6   
Ending compute phase at 1703201644.880155
Starting IO phase at 1703201644.880178
Ending IO phase at 1703201697.915808
write     45.27      48.56      0.164750    327680     1024.00    4.03       52.72      12.47      56.56      7   
