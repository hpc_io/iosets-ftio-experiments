############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 04:44:10 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696128250.784091
TestID              : 0
StartTime           : Sun Oct  1 04:44:10 2023
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696128695.843882
Starting IO phase at 1696128695.843915
Ending IO phase at 1696128969.069295
write     7.15       9.05       0.798857    327680     1024.00    87.44      282.84     40.20      357.86     0   
Ending compute phase at 1696129412.524744
Starting IO phase at 1696129412.524767
Ending IO phase at 1696129524.527281
write     13.13      31.94      0.232484    327680     1024.00    83.46      80.15      42.19      194.94     1   
Ending compute phase at 1696129892.734997
Starting IO phase at 1696129892.735018
Ending IO phase at 1696129951.392060
write     43.96      43.96      0.176376    327680     1024.00    0.068445   58.23      3.10       58.23      2   
Ending compute phase at 1696130311.554771
Starting IO phase at 1696130311.554791
Ending IO phase at 1696130350.496428
write     66.54      66.55      0.118976    327680     1024.00    0.087173   38.47      0.835559   38.47      3   
Ending compute phase at 1696130710.685210
Starting IO phase at 1696130710.685232
Ending IO phase at 1696130743.769549
write     78.40      78.44      0.078402    327680     1024.00    0.062062   32.64      7.55       32.65      4   
Ending compute phase at 1696131103.948863
Starting IO phase at 1696131103.948884
Ending IO phase at 1696131147.921111
write     58.75      58.76      0.121977    327680     1024.00    0.080583   43.57      4.53       43.57      5   
Ending compute phase at 1696131508.151615
Starting IO phase at 1696131508.151637
Ending IO phase at 1696131561.272823
write     48.44      48.45      0.159608    327680     1024.00    0.174121   52.84      1.98       52.85      6   
Ending compute phase at 1696131921.533687
Starting IO phase at 1696131921.533710
Ending IO phase at 1696131976.331432
write     46.97      47.18      0.168311    327680     1024.00    0.089792   54.26      3.53       54.50      7   
