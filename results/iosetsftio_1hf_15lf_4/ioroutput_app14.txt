############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 04:44:10 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696128250.952254
TestID              : 0
StartTime           : Sun Oct  1 04:44:11 2023
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696128749.823780
Starting IO phase at 1696128749.823805
Ending IO phase at 1696129013.937359
write     6.36       9.68       0.764145    327680     1024.00    152.10     264.38     21.63      402.32     0   
Ending compute phase at 1696129419.760954
Starting IO phase at 1696129419.760978
Ending IO phase at 1696129701.212821
write     7.84       8.94       0.346939    327680     1024.00    151.25     286.46     175.44     326.69     1   
Ending compute phase at 1696130064.388110
Starting IO phase at 1696130064.388134
Ending IO phase at 1696130107.900911
write     55.35      60.04      0.126838    327680     1024.00    3.01       42.64      5.76       46.25      2   
Ending compute phase at 1696130468.067118
Starting IO phase at 1696130468.067140
Ending IO phase at 1696130491.897620
write     109.75     109.78     0.060314    327680     1024.00    0.064526   23.32      4.02       23.33      3   
Ending compute phase at 1696130852.071722
Starting IO phase at 1696130852.071743
Ending IO phase at 1696130877.310070
write     103.06     103.08     0.060798    327680     1024.00    0.050291   24.83      5.38       24.84      4   
Ending compute phase at 1696131237.521902
Starting IO phase at 1696131237.521946
Ending IO phase at 1696131264.789181
write     95.39      95.50      0.076672    327680     1024.00    0.046034   26.81      2.27       26.84      5   
Ending compute phase at 1696131625.045817
Starting IO phase at 1696131625.045838
Ending IO phase at 1696131673.901551
write     52.86      52.87      0.138770    327680     1024.00    0.058708   48.42      9.93       48.43      6   
Ending compute phase at 1696132034.123576
Starting IO phase at 1696132034.123598
