############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 04:44:09 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696128249.692276
TestID              : 0
StartTime           : Sun Oct  1 04:44:09 2023
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696128610.143933
Starting IO phase at 1696128610.143959
Ending IO phase at 1696128783.324439
write     14.75      17.19      0.455401    327680     1024.00    0.734986   148.95     44.41      173.55     0   
Ending compute phase at 1696129193.985527
Starting IO phase at 1696129193.985547
Ending IO phase at 1696129284.563280
write     24.93      28.85      0.272659    327680     1024.00    17.94      88.74      9.20       102.67     1   
Ending compute phase at 1696129741.112287
Starting IO phase at 1696129741.112312
Ending IO phase at 1696129851.163381
write     12.42      23.33      0.342873    327680     1024.00    97.87      109.72     5.84       206.18     2   
Ending compute phase at 1696130211.349617
Starting IO phase at 1696130211.349638
Ending IO phase at 1696130237.118722
write     100.29     100.31     0.079408    327680     1024.00    0.077420   25.52      4.36       25.53      3   
Ending compute phase at 1696130597.479568
Starting IO phase at 1696130597.479589
Ending IO phase at 1696130629.121724
write     81.94      81.95      0.082933    327680     1024.00    0.055526   31.24      4.70       31.24      4   
Ending compute phase at 1696130989.287583
Starting IO phase at 1696130989.287603
Ending IO phase at 1696131023.360940
write     75.91      75.92      0.097561    327680     1024.00    0.665769   33.72      5.33       33.72      5   
Ending compute phase at 1696131383.594215
Starting IO phase at 1696131383.594235
Ending IO phase at 1696131425.957342
write     61.03      61.05      0.118034    327680     1024.00    0.097553   41.93      5.34       41.95      6   
Ending compute phase at 1696131786.130935
Starting IO phase at 1696131786.130957
Ending IO phase at 1696131839.796663
write     47.93      47.94      0.133911    327680     1024.00    2.18       53.40      10.55      53.41      7   
