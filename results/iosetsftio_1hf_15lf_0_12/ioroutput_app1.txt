############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 12:30:47 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705145447.116991
TestID              : 0
StartTime           : Sat Jan 13 12:30:47 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705145807.235446
Starting IO phase at 1705145807.235470
Ending IO phase at 1705145901.272451
write     27.20      30.57      0.212169    327680     1024.00    0.191424   83.73      48.63      94.11      0   
Ending compute phase at 1705146282.651103
Starting IO phase at 1705146282.651125
Ending IO phase at 1705146324.580378
write     61.59      61.60      0.120054    327680     1024.00    0.057499   41.56      3.81       41.56      1   
Ending compute phase at 1705146687.061426
Starting IO phase at 1705146687.061447
Ending IO phase at 1705146714.422014
write     87.85      95.24      0.077544    327680     1024.00    3.56       26.88      5.29       29.14      2   
Ending compute phase at 1705147077.535589
Starting IO phase at 1705147077.535613
Ending IO phase at 1705147126.634097
write     49.53      52.51      0.151753    327680     1024.00    3.79       48.75      2.56       51.68      3   
Ending compute phase at 1705147486.874199
Starting IO phase at 1705147486.874221
Ending IO phase at 1705147545.064958
write     44.38      44.39      0.176677    327680     1024.00    0.405427   57.67      3.36       57.69      4   
Ending compute phase at 1705147907.433420
Starting IO phase at 1705147907.433449
Ending IO phase at 1705147974.097846
write     37.41      38.61      0.200312    327680     1024.00    2.40       66.31      3.94       68.43      5   
Ending compute phase at 1705148334.294254
Starting IO phase at 1705148334.294274
Ending IO phase at 1705148370.797169
write     71.04      71.05      0.081856    327680     1024.00    0.088817   36.03      20.58      36.04      6   
Ending compute phase at 1705148730.995755
Starting IO phase at 1705148730.995776
Ending IO phase at 1705148782.865320
write     49.70      49.70      0.132712    327680     1024.00    0.063434   51.51      15.50      51.51      7   
Ending compute phase at 1705149145.384513
Starting IO phase at 1705149145.384540
Ending IO phase at 1705149209.995887
write     38.36      40.91      0.194767    327680     1024.00    2.66       62.58      6.98       66.74      8   
