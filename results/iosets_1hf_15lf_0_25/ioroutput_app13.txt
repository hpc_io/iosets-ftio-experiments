############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 00:16:13 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705360573.404398
TestID              : 0
StartTime           : Tue Jan 16 00:16:13 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705360946.583356
Starting IO phase at 1705360946.583379
Ending IO phase at 1705361250.824223
write     8.08       8.42       0.940830    327680     1024.00    269.19     303.87     6.08       316.74     0   
Ending compute phase at 1705361616.345168
Starting IO phase at 1705361616.345185
Ending IO phase at 1705361642.167431
write     83.34      98.55      0.078072    327680     1024.00    5.35       25.98      4.67       30.72      1   
Ending compute phase at 1705362002.366118
Starting IO phase at 1705362002.366131
Ending IO phase at 1705362035.635030
write     77.78      77.80      0.093196    327680     1024.00    0.054073   32.90      7.52       32.91      2   
Ending compute phase at 1705362395.848522
Starting IO phase at 1705362395.848536
Ending IO phase at 1705362429.437451
write     76.89      76.91      0.100996    327680     1024.00    0.063216   33.29      2.45       33.29      3   
Ending compute phase at 1705362789.647483
Starting IO phase at 1705362789.647498
Ending IO phase at 1705362835.477559
write     56.36      56.37      0.134707    327680     1024.00    0.067344   45.42      2.31       45.42      4   
Ending compute phase at 1705363196.582480
Starting IO phase at 1705363196.582494
Ending IO phase at 1705363243.944756
write     53.51      54.58      0.145326    327680     1024.00    4.15       46.90      5.37       47.84      5   
Ending compute phase at 1705363604.130824
Starting IO phase at 1705363604.130837
Ending IO phase at 1705363659.173116
write     46.74      46.75      0.153624    327680     1024.00    0.245881   54.76      5.61       54.77      6   
Ending compute phase at 1705364027.457122
Starting IO phase at 1705364027.457140
Ending IO phase at 1705364086.460181
write     38.28      43.63      0.175476    327680     1024.00    10.31      58.67      7.43       66.88      7   
