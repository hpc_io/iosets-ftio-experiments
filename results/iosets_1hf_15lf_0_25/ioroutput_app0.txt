############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 00:16:12 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705360572.254992
TestID              : 0
StartTime           : Tue Jan 16 00:16:12 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705360590.265976
Starting IO phase at 1705360590.265994
Ending IO phase at 1705360592.497523
write     58.97      59.13      0.133851    16384      1024.00    0.055638   2.16       0.023816   2.17       0   
Ending compute phase at 1705360610.692171
Starting IO phase at 1705360610.692184
Ending IO phase at 1705360612.857342
write     63.44      63.75      0.124057    16384      1024.00    0.051916   2.01       0.026864   2.02       1   
Ending compute phase at 1705360631.107446
Starting IO phase at 1705360631.107459
Ending IO phase at 1705360633.285315
write     61.23      61.41      0.128527    16384      1024.00    0.062869   2.08       0.028783   2.09       2   
Ending compute phase at 1705360651.514387
Starting IO phase at 1705360651.514398
Ending IO phase at 1705360653.733348
write     60.88      61.23      0.130656    16384      1024.00    0.078538   2.09       0.018626   2.10       3   
Ending compute phase at 1705360671.912674
Starting IO phase at 1705360671.912684
Ending IO phase at 1705360674.048027
write     59.84      60.01      0.131880    16384      1024.00    0.066022   2.13       0.023681   2.14       4   
Ending compute phase at 1705360692.319265
Starting IO phase at 1705360692.319276
Ending IO phase at 1705360694.453337
write     62.44      63.04      0.126178    16384      1024.00    0.041784   2.03       0.025833   2.05       5   
Ending compute phase at 1705360712.698632
Starting IO phase at 1705360712.698642
Ending IO phase at 1705360714.844216
write     61.71      62.03      0.128239    16384      1024.00    0.044872   2.06       0.017501   2.07       6   
Ending compute phase at 1705360733.040498
Starting IO phase at 1705360733.040512
Ending IO phase at 1705360735.157337
write     62.08      62.53      0.126210    16384      1024.00    0.042104   2.05       0.019590   2.06       7   
Ending compute phase at 1705360753.363045
Starting IO phase at 1705360753.363056
Ending IO phase at 1705360755.565345
write     61.47      61.86      0.128601    16384      1024.00    0.063755   2.07       0.019296   2.08       8   
Ending compute phase at 1705360773.796366
Starting IO phase at 1705360773.796377
Ending IO phase at 1705360776.089414
write     58.55      59.67      0.133369    16384      1024.00    0.054097   2.15       0.025340   2.19       9   
Ending compute phase at 1705360794.283328
Starting IO phase at 1705360794.283339
Ending IO phase at 1705360796.397359
write     63.16      63.35      0.116908    16384      1024.00    0.062557   2.02       0.150776   2.03       10  
Ending compute phase at 1705360814.655484
Starting IO phase at 1705360814.655503
Ending IO phase at 1705360816.837356
write     60.89      61.57      0.129062    16384      1024.00    0.067655   2.08       0.018922   2.10       11  
Ending compute phase at 1705360835.102718
Starting IO phase at 1705360835.102728
Ending IO phase at 1705360837.317387
write     59.76      60.33      0.132599    16384      1024.00    0.060979   2.12       0.027222   2.14       12  
Ending compute phase at 1705360855.623117
Starting IO phase at 1705360855.623128
Ending IO phase at 1705360857.793572
write     60.97      61.14      0.121560    16384      1024.00    0.054941   2.09       0.149536   2.10       13  
Ending compute phase at 1705360876.066844
Starting IO phase at 1705360876.066855
Ending IO phase at 1705360878.189368
write     62.20      62.41      0.126742    16384      1024.00    0.049728   2.05       0.025237   2.06       14  
Ending compute phase at 1705360896.378920
Starting IO phase at 1705360896.378930
Ending IO phase at 1705360898.529311
write     61.64      61.80      0.128007    16384      1024.00    0.061188   2.07       0.023678   2.08       15  
Ending compute phase at 1705360916.745581
Starting IO phase at 1705360916.745593
Ending IO phase at 1705360919.005331
write     59.64      60.14      0.132306    16384      1024.00    0.060136   2.13       0.025205   2.15       16  
Ending compute phase at 1705360961.463449
Starting IO phase at 1705360961.463470
Ending IO phase at 1705360971.578256
write     4.73       12.09      0.553060    16384      1024.00    17.46      10.58      3.37       27.09      17  
Ending compute phase at 1705360992.343366
Starting IO phase at 1705360992.343383
Ending IO phase at 1705360998.416468
write     14.54      22.00      0.363477    16384      1024.00    3.37       5.82       0.616607   8.80       18  
Ending compute phase at 1705361019.271179
Starting IO phase at 1705361019.271195
Ending IO phase at 1705361026.128872
write     13.21      18.54      0.420431    16384      1024.00    3.66       6.90       2.43       9.69       19  
Ending compute phase at 1705361048.450789
Starting IO phase at 1705361048.450816
Ending IO phase at 1705361060.272381
write     7.94       10.83      0.445527    16384      1024.00    6.16       11.81      4.69       16.11      20  
Ending compute phase at 1705361080.226668
Starting IO phase at 1705361080.226685
Ending IO phase at 1705361085.949511
write     16.80      23.44      0.213765    16384      1024.00    2.30       5.46       2.28       7.62       21  
Ending compute phase at 1705361105.730583
Starting IO phase at 1705361105.730602
Ending IO phase at 1705361108.477454
write     30.20      48.03      0.166567    16384      1024.00    1.73       2.67       0.945582   4.24       22  
Ending compute phase at 1705361129.402261
Starting IO phase at 1705361129.402279
Ending IO phase at 1705361137.425566
write     12.01      16.02      0.251693    16384      1024.00    4.29       7.99       3.97       10.66      23  
Ending compute phase at 1705361157.650780
Starting IO phase at 1705361157.650799
Ending IO phase at 1705361164.553310
write     14.54      18.88      0.313391    16384      1024.00    2.41       6.78       3.35       8.80       24  
Ending compute phase at 1705361184.725780
Starting IO phase at 1705361184.725800
Ending IO phase at 1705361187.521437
write     27.01      47.37      0.159425    16384      1024.00    2.34       2.70       0.508653   4.74       25  
Ending compute phase at 1705361207.355740
Starting IO phase at 1705361207.355775
Ending IO phase at 1705361210.350801
write     28.40      44.97      0.177896    16384      1024.00    1.75       2.85       0.327514   4.51       26  
Ending compute phase at 1705361230.161783
Starting IO phase at 1705361230.161800
Ending IO phase at 1705361240.553301
write     10.72      12.40      0.433500    16384      1024.00    1.99       10.33      3.39       11.94      27  
Ending compute phase at 1705361260.835504
Starting IO phase at 1705361260.835521
Ending IO phase at 1705361269.069318
write     12.54      16.15      0.198907    16384      1024.00    2.61       7.93       4.94       10.21      28  
Ending compute phase at 1705361287.235384
Starting IO phase at 1705361287.235396
Ending IO phase at 1705361291.745383
write     29.01      29.05      0.237545    16384      1024.00    0.808690   4.41       1.20       4.41       29  
Ending compute phase at 1705361310.006711
Starting IO phase at 1705361310.006722
Ending IO phase at 1705361311.165363
write     117.83     120.10     0.021610    16384      1024.00    0.048038   1.07       0.735472   1.09       30  
Ending compute phase at 1705361329.419923
Starting IO phase at 1705361329.419934
Ending IO phase at 1705361330.617339
write     118.42     119.10     0.064404    16384      1024.00    0.045455   1.07       0.492409   1.08       31  
Ending compute phase at 1705361348.859175
Starting IO phase at 1705361348.859187
Ending IO phase at 1705361354.821375
write     21.75      21.77      0.185664    16384      1024.00    0.056609   5.88       3.74       5.89       32  
Ending compute phase at 1705361372.982401
Starting IO phase at 1705361372.982414
Ending IO phase at 1705361378.985397
write     21.62      21.64      0.136832    16384      1024.00    0.058920   5.92       3.73       5.92       33  
Ending compute phase at 1705361397.677076
Starting IO phase at 1705361397.677090
Ending IO phase at 1705361404.977372
write     16.55      17.77      0.450255    16384      1024.00    0.525302   7.20       4.81       7.73       34  
Ending compute phase at 1705361423.195303
Starting IO phase at 1705361423.195315
Ending IO phase at 1705361424.385349
write     120.08     121.34     0.025925    16384      1024.00    0.052968   1.05       0.645856   1.07       35  
Ending compute phase at 1705361442.669203
Starting IO phase at 1705361442.669220
Ending IO phase at 1705361447.205352
write     28.69      28.76      0.226172    16384      1024.00    0.045444   4.45       2.26       4.46       36  
Ending compute phase at 1705361465.423134
Starting IO phase at 1705361465.423149
Ending IO phase at 1705361469.027144
write     35.75      35.81      0.151480    16384      1024.00    0.070967   3.57       1.15       3.58       37  
Ending compute phase at 1705361488.651827
Starting IO phase at 1705361488.651841
Ending IO phase at 1705361492.333319
write     26.03      36.02      0.201768    16384      1024.00    1.40       3.55       0.492322   4.92       38  
Ending compute phase at 1705361510.719147
Starting IO phase at 1705361510.719168
Ending IO phase at 1705361515.193368
write     27.42      29.25      0.240299    16384      1024.00    0.545406   4.38       0.532350   4.67       39  
Ending compute phase at 1705361533.281909
Starting IO phase at 1705361533.281922
Ending IO phase at 1705361538.333045
write     25.70      25.77      0.276152    16384      1024.00    0.049872   4.97       3.47       4.98       40  
Ending compute phase at 1705361556.505656
Starting IO phase at 1705361556.505668
Ending IO phase at 1705361566.725872
write     12.68      12.69      0.206014    16384      1024.00    4.22       10.09      7.49       10.09      41  
Ending compute phase at 1705361584.900069
Starting IO phase at 1705361584.900082
Ending IO phase at 1705361591.485398
write     19.58      19.60      0.189966    16384      1024.00    0.103483   6.53       4.20       6.54       42  
Ending compute phase at 1705361609.668064
Starting IO phase at 1705361609.668078
Ending IO phase at 1705361616.656805
write     18.44      18.46      0.183721    16384      1024.00    0.055222   6.93       4.79       6.94       43  
Ending compute phase at 1705361637.781667
Starting IO phase at 1705361637.781679
Ending IO phase at 1705361641.981098
write     18.23      31.12      0.221651    16384      1024.00    2.94       4.11       0.567164   7.02       44  
Ending compute phase at 1705361660.347472
Starting IO phase at 1705361660.347485
Ending IO phase at 1705361665.761336
write     23.36      23.97      0.214148    16384      1024.00    0.969038   5.34       2.26       5.48       45  
Ending compute phase at 1705361683.950880
Starting IO phase at 1705361683.950893
Ending IO phase at 1705361685.185408
write     113.70     115.08     0.068805    16384      1024.00    0.055206   1.11       0.020290   1.13       46  
Ending compute phase at 1705361703.403454
Starting IO phase at 1705361703.403465
Ending IO phase at 1705361704.665391
write     109.80     111.11     0.071276    16384      1024.00    0.061698   1.15       0.020083   1.17       47  
Ending compute phase at 1705361722.857841
Starting IO phase at 1705361722.857854
Ending IO phase at 1705361724.617354
write     76.77      77.80      0.092968    16384      1024.00    0.095266   1.65       0.345984   1.67       48  
Ending compute phase at 1705361742.831291
Starting IO phase at 1705361742.831303
Ending IO phase at 1705361745.513412
write     50.05      50.17      0.150874    16384      1024.00    0.096023   2.55       0.401295   2.56       49  
Ending compute phase at 1705361763.663450
Starting IO phase at 1705361763.663463
Ending IO phase at 1705361768.797421
write     25.32      25.35      0.166602    16384      1024.00    0.045449   5.05       3.08       5.06       50  
Ending compute phase at 1705361787.062601
Starting IO phase at 1705361787.062614
Ending IO phase at 1705361792.721046
write     23.13      23.15      0.291218    16384      1024.00    0.056835   5.53       3.45       5.53       51  
Ending compute phase at 1705361810.989685
Starting IO phase at 1705361810.989699
Ending IO phase at 1705361822.353376
write     11.30      11.32      0.455596    16384      1024.00    0.052911   11.30      4.00       11.33      52  
Ending compute phase at 1705361840.530875
Starting IO phase at 1705361840.530889
Ending IO phase at 1705361848.969379
write     15.35      15.36      0.474967    16384      1024.00    0.645519   8.33       5.25       8.34       53  
Ending compute phase at 1705361867.143456
Starting IO phase at 1705361867.143470
Ending IO phase at 1705361874.285520
write     18.13      18.14      0.157291    16384      1024.00    0.079225   7.06       4.54       7.06       54  
Ending compute phase at 1705361892.482709
Starting IO phase at 1705361892.482722
Ending IO phase at 1705361898.829397
write     20.27      20.28      0.264650    16384      1024.00    0.252665   6.31       3.59       6.32       55  
Ending compute phase at 1705361917.188948
Starting IO phase at 1705361917.188962
Ending IO phase at 1705361921.517407
write     28.53      28.73      0.208725    16384      1024.00    0.311371   4.45       0.938309   4.49       56  
Ending compute phase at 1705361940.277061
Starting IO phase at 1705361940.277078
Ending IO phase at 1705361944.265376
write     29.21      29.40      0.228532    16384      1024.00    0.857510   4.35       1.14       4.38       57  
Ending compute phase at 1705361963.757143
Starting IO phase at 1705361963.757158
Ending IO phase at 1705361966.889416
write     29.59      41.65      0.163785    16384      1024.00    1.32       3.07       0.418330   4.33       58  
Ending compute phase at 1705361986.584048
Starting IO phase at 1705361986.584061
Ending IO phase at 1705361990.705479
write     22.98      31.39      0.121282    16384      1024.00    1.51       4.08       2.14       5.57       59  
Ending compute phase at 1705362010.634247
Starting IO phase at 1705362010.634260
Ending IO phase at 1705362013.493361
write     28.73      44.09      0.151652    16384      1024.00    1.70       2.90       0.698585   4.46       60  
Ending compute phase at 1705362031.958233
Starting IO phase at 1705362031.958245
Ending IO phase at 1705362035.489379
write     34.45      36.93      0.180172    16384      1024.00    0.474382   3.47       0.583991   3.72       61  
Ending compute phase at 1705362056.160412
Starting IO phase at 1705362056.160426
Ending IO phase at 1705362059.193328
write     23.73      43.30      0.161045    16384      1024.00    3.06       2.96       0.563453   5.40       62  
Ending compute phase at 1705362077.368168
Starting IO phase at 1705362077.368180
Ending IO phase at 1705362081.585381
write     30.88      30.92      0.236559    16384      1024.00    0.058880   4.14       0.366872   4.15       63  
Ending compute phase at 1705362099.782929
Starting IO phase at 1705362099.782940
Ending IO phase at 1705362101.109872
write     100.83     101.32     0.050038    16384      1024.00    0.043661   1.26       0.463698   1.27       64  
Ending compute phase at 1705362119.362950
Starting IO phase at 1705362119.362962
Ending IO phase at 1705362126.049389
write     19.35      19.39      0.209988    16384      1024.00    0.044314   6.60       3.25       6.61       65  
Ending compute phase at 1705362144.251003
Starting IO phase at 1705362144.251016
Ending IO phase at 1705362151.493454
write     17.83      17.84      0.223732    16384      1024.00    0.052271   7.17       4.43       7.18       66  
Ending compute phase at 1705362169.691255
Starting IO phase at 1705362169.691269
Ending IO phase at 1705362177.129362
write     17.39      17.41      0.248647    16384      1024.00    0.473409   7.35       4.25       7.36       67  
Ending compute phase at 1705362195.350700
Starting IO phase at 1705362195.350714
Ending IO phase at 1705362204.541370
write     14.09      14.10      0.285888    16384      1024.00    0.050692   9.08       4.50       9.08       68  
Ending compute phase at 1705362222.739019
Starting IO phase at 1705362222.739032
Ending IO phase at 1705362232.921357
write     12.67      12.68      0.143044    16384      1024.00    0.216226   10.10      7.81       10.10      69  
Ending compute phase at 1705362251.142471
Starting IO phase at 1705362251.142484
Ending IO phase at 1705362259.673586
write     15.17      15.18      0.129241    16384      1024.00    0.055872   8.43       6.37       8.44       70  
Ending compute phase at 1705362277.831064
Starting IO phase at 1705362277.831078
Ending IO phase at 1705362284.977411
write     18.11      18.13      0.204014    16384      1024.00    0.091698   7.06       3.80       7.07       71  
Ending compute phase at 1705362303.124310
Starting IO phase at 1705362303.124323
Ending IO phase at 1705362312.005427
write     14.52      14.54      0.273825    16384      1024.00    0.058812   8.81       5.05       8.81       72  
Ending compute phase at 1705362331.709652
Starting IO phase at 1705362331.709667
Ending IO phase at 1705362336.073329
write     22.03      29.90      0.201476    16384      1024.00    2.20       4.28       1.58       5.81       73  
Ending compute phase at 1705362355.617418
Starting IO phase at 1705362355.617434
Ending IO phase at 1705362360.308038
write     21.46      27.89      0.200699    16384      1024.00    2.11       4.59       1.38       5.97       74  
Ending compute phase at 1705362380.082766
Starting IO phase at 1705362380.082779
Ending IO phase at 1705362383.077371
write     28.25      43.80      0.171559    16384      1024.00    2.03       2.92       0.178087   4.53       75  
Ending compute phase at 1705362402.864869
Starting IO phase at 1705362402.864887
Ending IO phase at 1705362410.253367
write     14.53      17.53      0.184344    16384      1024.00    1.95       7.30       4.35       8.81       76  
Ending compute phase at 1705362428.535853
Starting IO phase at 1705362428.535865
Ending IO phase at 1705362430.861385
write     54.28      57.16      0.099515    16384      1024.00    0.125213   2.24       0.659857   2.36       77  
Ending compute phase at 1705362450.391095
Starting IO phase at 1705362450.391108
Ending IO phase at 1705362454.697406
write     23.00      30.48      0.262445    16384      1024.00    1.53       4.20       0.220234   5.57       78  
Ending compute phase at 1705362472.935371
Starting IO phase at 1705362472.935383
Ending IO phase at 1705362478.305414
write     24.26      24.29      0.179307    16384      1024.00    0.045159   5.27       2.40       5.28       79  
Ending compute phase at 1705362496.570936
Starting IO phase at 1705362496.570949
Ending IO phase at 1705362504.057378
write     17.23      17.25      0.267633    16384      1024.00    0.071697   7.42       3.14       7.43       80  
Ending compute phase at 1705362522.259043
Starting IO phase at 1705362522.259056
Ending IO phase at 1705362530.697429
write     15.32      15.33      0.292422    16384      1024.00    4.91       8.35       3.67       8.35       81  
Ending compute phase at 1705362548.922661
Starting IO phase at 1705362548.922674
Ending IO phase at 1705362555.405440
write     19.90      19.91      0.229132    16384      1024.00    0.083844   6.43       4.03       6.43       82  
Ending compute phase at 1705362573.663236
Starting IO phase at 1705362573.663248
Ending IO phase at 1705362574.914479
write     105.18     105.72     0.074957    16384      1024.00    0.052905   1.21       0.012463   1.22       83  
Ending compute phase at 1705362593.110550
Starting IO phase at 1705362593.110563
Ending IO phase at 1705362600.053379
write     18.68      18.69      0.137854    16384      1024.00    3.01       6.85       4.64       6.85       84  
Ending compute phase at 1705362618.223061
Starting IO phase at 1705362618.223074
Ending IO phase at 1705362626.205463
write     16.11      16.16      0.296541    16384      1024.00    0.058460   7.92       5.21       7.94       85  
Ending compute phase at 1705362644.412079
Starting IO phase at 1705362644.412092
Ending IO phase at 1705362650.443263
write     21.51      21.55      0.207911    16384      1024.00    3.33       5.94       3.54       5.95       86  
Ending compute phase at 1705362668.694172
Starting IO phase at 1705362668.694185
Ending IO phase at 1705362678.268186
write     13.41      13.91      0.314164    16384      1024.00    0.064990   9.20       5.05       9.55       87  
Ending compute phase at 1705362696.407956
Starting IO phase at 1705362696.407969
Ending IO phase at 1705362704.529423
write     15.89      16.51      0.187355    16384      1024.00    0.290962   7.75       5.05       8.06       88  
Ending compute phase at 1705362724.474843
Starting IO phase at 1705362724.474857
Ending IO phase at 1705362730.613400
write     16.41      21.58      0.231718    16384      1024.00    2.03       5.93       2.37       7.80       89  
Ending compute phase at 1705362750.358643
Starting IO phase at 1705362750.358656
Ending IO phase at 1705362757.209342
write     15.38      18.87      0.246751    16384      1024.00    1.94       6.78       2.84       8.32       90  
Ending compute phase at 1705362777.610931
Starting IO phase at 1705362777.610945
Ending IO phase at 1705362783.481379
write     15.94      21.99      0.264870    16384      1024.00    3.61       5.82       1.58       8.03       91  
Ending compute phase at 1705362804.518367
Starting IO phase at 1705362804.518382
Ending IO phase at 1705362809.029471
write     17.48      28.73      0.256654    16384      1024.00    3.35       4.46       1.44       7.32       92  
Ending compute phase at 1705362829.889724
Starting IO phase at 1705362829.889739
Ending IO phase at 1705362835.285337
write     16.17      24.00      0.224735    16384      1024.00    6.01       5.33       1.74       7.91       93  
Ending compute phase at 1705362856.265312
Starting IO phase at 1705362856.265328
Ending IO phase at 1705362861.313398
write     16.38      25.56      0.234758    16384      1024.00    3.28       5.01       1.75       7.81       94  
Ending compute phase at 1705362880.838519
Starting IO phase at 1705362880.838534
Ending IO phase at 1705362887.537350
write     16.16      19.45      0.217035    16384      1024.00    2.09       6.58       3.11       7.92       95  
Ending compute phase at 1705362907.005368
Starting IO phase at 1705362907.005382
Ending IO phase at 1705362914.113365
write     15.44      18.26      0.239377    16384      1024.00    1.99       7.01       3.18       8.29       96  
Ending compute phase at 1705362934.835769
Starting IO phase at 1705362934.835783
Ending IO phase at 1705362941.189447
write     14.47      20.31      0.321013    16384      1024.00    4.78       6.30       1.55       8.85       97  
Ending compute phase at 1705362959.435047
Starting IO phase at 1705362959.435059
Ending IO phase at 1705362964.373492
write     26.20      26.24      0.202051    16384      1024.00    0.044532   4.88       2.28       4.89       98  
Ending compute phase at 1705362982.539282
Starting IO phase at 1705362982.539297
Ending IO phase at 1705362990.449425
write     16.28      16.30      0.266897    16384      1024.00    0.053963   7.85       5.61       7.86       99  
Ending compute phase at 1705363008.615392
Starting IO phase at 1705363008.615405
Ending IO phase at 1705363016.125336
write     17.31      17.32      0.257441    16384      1024.00    0.577939   7.39       4.16       7.40       100 
Ending compute phase at 1705363034.342724
Starting IO phase at 1705363034.342736
Ending IO phase at 1705363042.725489
write     15.31      15.58      0.234438    16384      1024.00    0.112273   8.22       5.49       8.36       101 
Ending compute phase at 1705363060.891124
Starting IO phase at 1705363060.891136
Ending IO phase at 1705363068.173426
write     17.78      17.80      0.146566    16384      1024.00    0.060889   7.19       4.85       7.20       102 
Ending compute phase at 1705363086.379273
Starting IO phase at 1705363086.379285
Ending IO phase at 1705363094.549415
write     15.78      15.80      0.329978    16384      1024.00    0.107476   8.10       4.92       8.11       103 
Ending compute phase at 1705363113.217225
Starting IO phase at 1705363113.217240
Ending IO phase at 1705363120.849434
write     15.92      16.98      0.246144    16384      1024.00    0.491053   7.54       3.81       8.04       104 
Ending compute phase at 1705363139.050633
Starting IO phase at 1705363139.050646
Ending IO phase at 1705363147.846663
write     14.58      14.60      0.214355    16384      1024.00    0.046619   8.77       5.34       8.78       105 
Ending compute phase at 1705363166.095273
Starting IO phase at 1705363166.095285
Ending IO phase at 1705363173.305426
write     17.91      17.96      0.208328    16384      1024.00    0.055031   7.13       4.75       7.15       106 
Ending compute phase at 1705363191.583319
Starting IO phase at 1705363191.583332
Ending IO phase at 1705363200.081386
write     15.16      15.19      0.268111    16384      1024.00    0.323726   8.43       4.15       8.44       107 
Ending compute phase at 1705363218.269357
Starting IO phase at 1705363218.269369
Ending IO phase at 1705363226.161381
write     16.35      16.37      0.226253    16384      1024.00    0.818096   7.82       4.20       7.83       108 
Ending compute phase at 1705363244.338586
Starting IO phase at 1705363244.338600
Ending IO phase at 1705363253.300081
write     14.42      14.43      0.217958    16384      1024.00    3.49       8.87       5.63       8.88       109 
Ending compute phase at 1705363271.474443
Starting IO phase at 1705363271.474456
Ending IO phase at 1705363279.560732
write     16.00      16.05      0.201925    16384      1024.00    0.044413   7.97       4.77       8.00       110 
Ending compute phase at 1705363297.712450
Starting IO phase at 1705363297.712463
Ending IO phase at 1705363302.513292
write     27.29      27.33      0.206663    16384      1024.00    0.147165   4.68       1.38       4.69       111 
Ending compute phase at 1705363323.159519
Starting IO phase at 1705363323.159534
Ending IO phase at 1705363327.317397
write     19.53      31.37      0.228221    16384      1024.00    3.63       4.08       0.430354   6.56       112 
Ending compute phase at 1705363349.085467
Starting IO phase at 1705363349.085484
Ending IO phase at 1705363353.235555
write     16.84      31.47      0.243502    16384      1024.00    3.73       4.07       0.949390   7.60       113 
Ending compute phase at 1705363374.045991
Starting IO phase at 1705363374.046006
Ending IO phase at 1705363381.101512
write     13.30      18.25      0.236578    16384      1024.00    3.48       7.02       3.24       9.62       114 
Ending compute phase at 1705363401.937898
Starting IO phase at 1705363401.937914
Ending IO phase at 1705363409.969420
write     12.08      16.02      0.272198    16384      1024.00    3.65       7.99       3.64       10.60      115 
Ending compute phase at 1705363431.592559
Starting IO phase at 1705363431.592577
Ending IO phase at 1705363435.961363
write     16.68      29.60      0.232330    16384      1024.00    3.51       4.32       0.607162   7.67       116 
Ending compute phase at 1705363456.774025
Starting IO phase at 1705363456.774043
Ending IO phase at 1705363463.119888
write     14.38      20.77      0.217298    16384      1024.00    3.05       6.16       2.83       8.90       117 
Ending compute phase at 1705363483.905784
Starting IO phase at 1705363483.905802
Ending IO phase at 1705363491.139102
write     13.12      17.87      0.258972    16384      1024.00    3.17       7.16       3.03       9.76       118 
Ending compute phase at 1705363512.017195
Starting IO phase at 1705363512.017212
Ending IO phase at 1705363518.973381
write     13.42      18.75      0.293787    16384      1024.00    3.90       6.83       2.96       9.53       119 
Ending compute phase at 1705363539.376676
Starting IO phase at 1705363539.376693
Ending IO phase at 1705363546.633336
write     13.50      18.96      0.280478    16384      1024.00    2.82       6.75       2.72       9.48       120 
Ending compute phase at 1705363567.604639
Starting IO phase at 1705363567.604657
Ending IO phase at 1705363575.245678
write     12.29      16.94      0.260473    16384      1024.00    4.69       7.56       3.40       10.41      121 
Ending compute phase at 1705363596.000624
Starting IO phase at 1705363596.000640
Ending IO phase at 1705363602.337352
write     14.55      20.44      0.290354    16384      1024.00    3.54       6.26       1.62       8.80       122 
Ending compute phase at 1705363623.452764
Starting IO phase at 1705363623.452779
Ending IO phase at 1705363630.797334
write     12.57      17.68      0.281710    16384      1024.00    7.34       7.24       2.73       10.18      123 
Ending compute phase at 1705363652.078167
Starting IO phase at 1705363652.078184
Ending IO phase at 1705363658.561355
write     13.42      19.89      0.246875    16384      1024.00    3.96       6.44       2.49       9.54       124 
Ending compute phase at 1705363679.426995
Starting IO phase at 1705363679.427013
Ending IO phase at 1705363686.505380
write     13.17      18.27      0.437957    16384      1024.00    3.74       7.01       2.59       9.72       125 
Ending compute phase at 1705363707.383145
Starting IO phase at 1705363707.383164
Ending IO phase at 1705363711.641452
write     18.73      30.82      0.237747    16384      1024.00    2.71       4.15       0.356883   6.83       126 
Ending compute phase at 1705363732.615239
Starting IO phase at 1705363732.615254
Ending IO phase at 1705363741.469362
write     11.09      14.56      0.302787    16384      1024.00    3.87       8.79       3.96       11.54      127 
Ending compute phase at 1705363762.862954
Starting IO phase at 1705363762.862971
Ending IO phase at 1705363768.345896
write     14.96      23.99      0.333414    16384      1024.00    3.87       5.33       0.522234   8.55       128 
Ending compute phase at 1705363790.068626
Starting IO phase at 1705363790.068640
Ending IO phase at 1705363796.801352
write     12.58      19.23      0.257348    16384      1024.00    3.96       6.66       2.57       10.18      129 
Ending compute phase at 1705363817.427653
Starting IO phase at 1705363817.427670
Ending IO phase at 1705363825.671997
write     12.03      15.69      0.287038    16384      1024.00    2.82       8.16       3.57       10.64      130 
Ending compute phase at 1705363846.670438
Starting IO phase at 1705363846.670454
Ending IO phase at 1705363856.429363
write     10.20      13.52      0.514447    16384      1024.00    7.32       9.47       5.23       12.55      131 
Ending compute phase at 1705363877.334426
Starting IO phase at 1705363877.334444
Ending IO phase at 1705363885.637361
write     11.66      15.54      0.301834    16384      1024.00    3.71       8.24       3.41       10.98      132 
Ending compute phase at 1705363906.482108
Starting IO phase at 1705363906.482124
Ending IO phase at 1705363915.019915
write     11.45      14.92      0.298306    16384      1024.00    3.20       8.58       3.75       11.18      133 
Ending compute phase at 1705363935.809918
Starting IO phase at 1705363935.809935
Ending IO phase at 1705363944.565281
write     11.45      14.75      0.381053    16384      1024.00    4.62       8.68       2.58       11.18      134 
Ending compute phase at 1705363966.594148
Starting IO phase at 1705363966.594166
Ending IO phase at 1705363972.977400
write     12.56      20.35      0.263026    16384      1024.00    5.02       6.29       2.14       10.19      135 
Ending compute phase at 1705363995.513459
Starting IO phase at 1705363995.513477
Ending IO phase at 1705364001.405312
write     12.72      22.25      0.268367    16384      1024.00    5.10       5.75       1.51       10.06      136 
Ending compute phase at 1705364022.933290
Starting IO phase at 1705364022.933307
Ending IO phase at 1705364029.305663
write     13.27      20.42      0.222037    16384      1024.00    4.14       6.27       2.72       9.64       137 
Ending compute phase at 1705364052.121055
Starting IO phase at 1705364052.121073
Ending IO phase at 1705364058.933361
write     11.29      19.05      0.229533    16384      1024.00    5.64       6.72       3.08       11.34      138 
Ending compute phase at 1705364082.051699
Starting IO phase at 1705364082.051716
Ending IO phase at 1705364086.393329
write     13.99      30.28      0.254911    16384      1024.00    5.37       4.23       0.538607   9.15       139 
Ending compute phase at 1705364109.589086
Starting IO phase at 1705364109.589104
Ending IO phase at 1705364114.661308
write     12.84      25.73      0.214073    16384      1024.00    5.71       4.97       1.55       9.97       140 
Ending compute phase at 1705364135.924798
Starting IO phase at 1705364135.924815
Ending IO phase at 1705364143.703463
write     11.89      16.40      0.483331    16384      1024.00    4.04       7.81       3.05       10.77      141 
Ending compute phase at 1705364165.739910
Starting IO phase at 1705364165.739924
Ending IO phase at 1705364171.565744
write     13.41      22.04      0.246010    16384      1024.00    4.79       5.81       1.84       9.54       142 
Ending compute phase at 1705364194.024056
Starting IO phase at 1705364194.024074
Ending IO phase at 1705364200.381297
write     12.18      21.45      0.245633    16384      1024.00    4.91       5.97       2.32       10.51      143 
Ending compute phase at 1705364220.479815
Starting IO phase at 1705364220.479831
Ending IO phase at 1705364224.688766
write     21.12      30.76      0.180347    16384      1024.00    2.52       4.16       1.28       6.06       144 
Ending compute phase at 1705364245.623283
Starting IO phase at 1705364245.623300
Ending IO phase at 1705364253.377595
write     12.24      16.47      0.302758    16384      1024.00    4.11       7.77       2.84       10.46      145 
Ending compute phase at 1705364274.689378
Starting IO phase at 1705364274.689393
Ending IO phase at 1705364281.401325
write     13.09      19.30      0.188613    16384      1024.00    4.24       6.63       3.63       9.78       146 
Ending compute phase at 1705364302.130990
Starting IO phase at 1705364302.131006
Ending IO phase at 1705364308.617301
write     14.26      19.96      0.251376    16384      1024.00    6.59       6.41       2.39       8.98       147 
Ending compute phase at 1705364329.228297
Starting IO phase at 1705364329.228314
Ending IO phase at 1705364337.825398
write     11.72      15.11      0.334178    16384      1024.00    3.35       8.47       3.21       10.92      148 
Ending compute phase at 1705364358.327128
Starting IO phase at 1705364358.327145
Ending IO phase at 1705364366.385370
write     12.45      16.11      0.294732    16384      1024.00    2.83       7.95       3.24       10.28      149 
