############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 00:16:12 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705360572.678802
TestID              : 0
StartTime           : Tue Jan 16 00:16:12 2024
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705360932.730048
Starting IO phase at 1705360932.730065
Ending IO phase at 1705361145.552713
write     12.03      12.04      0.653604    327680     1024.00    0.236225   212.71     4.90       212.72     0   
Ending compute phase at 1705361505.655024
Starting IO phase at 1705361505.655040
Ending IO phase at 1705361533.450418
write     93.50      93.52      0.077099    327680     1024.00    0.068315   27.38      8.55       27.38      1   
Ending compute phase at 1705361897.527487
Starting IO phase at 1705361897.527502
Ending IO phase at 1705361921.721256
write     91.92      103.50     0.074772    327680     1024.00    3.92       24.74      3.09       27.85      2   
Ending compute phase at 1705362283.824058
Starting IO phase at 1705362283.824072
Ending IO phase at 1705362312.737267
write     83.89      89.63      0.088759    327680     1024.00    2.53       28.56      4.88       30.52      3   
Ending compute phase at 1705362676.230858
Starting IO phase at 1705362676.230871
Ending IO phase at 1705362706.180423
write     77.61      86.17      0.088836    327680     1024.00    3.32       29.71      1.78       32.99      4   
Ending compute phase at 1705363067.519617
Starting IO phase at 1705363067.519633
Ending IO phase at 1705363101.637123
write     73.39      75.84      0.104390    327680     1024.00    1.58       33.75      1.97       34.88      5   
Ending compute phase at 1705363462.553329
Starting IO phase at 1705363462.553347
Ending IO phase at 1705363519.667495
write     44.53      45.12      0.169264    327680     1024.00    1.24       56.74      6.08       57.49      6   
Ending compute phase at 1705363883.897547
Starting IO phase at 1705363883.897563
Ending IO phase at 1705363946.759474
write     38.40      40.89      0.195657    327680     1024.00    4.07       62.61      10.89      66.66      7   
Ending compute phase at 1705364308.404978
Starting IO phase at 1705364308.404994
Ending IO phase at 1705364368.067056
write     42.04      43.05      0.183908    327680     1024.00    1.44       59.47      11.43      60.90      8   
