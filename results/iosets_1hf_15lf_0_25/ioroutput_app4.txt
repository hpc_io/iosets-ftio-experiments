############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Tue Jan 16 00:16:12 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705360572.253311
TestID              : 0
StartTime           : Tue Jan 16 00:16:12 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705360932.477163
Starting IO phase at 1705360932.477180
Ending IO phase at 1705361064.680768
write     19.35      19.37      0.412647    327680     1024.00    0.249983   132.17     0.235242   132.32     0   
Ending compute phase at 1705361424.765577
Starting IO phase at 1705361424.765590
Ending IO phase at 1705361447.433964
write     114.60     114.65     0.047805    327680     1024.00    0.058004   22.33      7.04       22.34      1   
Ending compute phase at 1705361807.651463
Starting IO phase at 1705361807.651477
Ending IO phase at 1705361830.789529
write     112.21     112.25     0.059610    327680     1024.00    0.055752   22.81      3.73       22.81      2   
Ending compute phase at 1705362190.947729
Starting IO phase at 1705362190.947742
Ending IO phase at 1705362217.701294
write     96.83      96.85      0.070454    327680     1024.00    0.066395   26.43      5.80       26.44      3   
Ending compute phase at 1705362577.960972
Starting IO phase at 1705362577.960986
Ending IO phase at 1705362603.980510
write     99.07      99.10      0.075993    327680     1024.00    0.044960   25.83      2.14       25.84      4   
Ending compute phase at 1705362964.263821
Starting IO phase at 1705362964.263845
Ending IO phase at 1705362992.922997
write     89.37      89.39      0.089087    327680     1024.00    0.148229   28.64      3.11       28.65      5   
Ending compute phase at 1705363353.127592
Starting IO phase at 1705363353.127610
Ending IO phase at 1705363410.207902
write     45.06      45.07      0.170055    327680     1024.00    0.663770   56.80      8.07       56.81      6   
Ending compute phase at 1705363770.920968
Starting IO phase at 1705363770.920983
Ending IO phase at 1705363829.326680
write     43.64      43.98      0.166598    327680     1024.00    0.728060   58.21      12.01      58.66      7   
Ending compute phase at 1705364200.277845
Starting IO phase at 1705364200.277862
Ending IO phase at 1705364252.752872
write     40.69      49.09      0.150913    327680     1024.00    10.77      52.14      3.86       62.91      8   
