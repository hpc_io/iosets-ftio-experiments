############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 14:38:38 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705153118.255386
TestID              : 0
StartTime           : Sat Jan 13 14:38:38 2024
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705153478.285433
Starting IO phase at 1705153478.285451
Ending IO phase at 1705153839.590533
write     7.09       7.09       1.08        327680     1024.00    0.108599   361.04     16.63      361.30     0   
Ending compute phase at 1705154199.736532
Starting IO phase at 1705154199.736550
Ending IO phase at 1705154370.007958
write     15.03      15.06      0.492704    327680     1024.00    0.190291   170.03     12.58      170.38     1   
Ending compute phase at 1705154730.042207
Starting IO phase at 1705154730.042221
Ending IO phase at 1705154753.507597
write     110.79     110.84     0.072099    327680     1024.00    0.080740   23.10      0.653212   23.11      2   
Ending compute phase at 1705155113.707657
Starting IO phase at 1705155113.707670
Ending IO phase at 1705155136.797084
write     112.65     112.68     0.068175    327680     1024.00    0.062300   22.72      1.95       22.72      3   
Ending compute phase at 1705155497.912095
Starting IO phase at 1705155497.912142
Ending IO phase at 1705155573.724906
write     33.48      33.88      0.236117    327680     1024.00    0.910084   75.57      2.12       76.45      4   
Ending compute phase at 1705155934.633670
Starting IO phase at 1705155934.633694
Ending IO phase at 1705156027.174486
write     27.52      27.75      0.288334    327680     1024.00    0.778693   92.27      5.51       93.02      5   
Ending compute phase at 1705156387.678139
Starting IO phase at 1705156387.678161
Ending IO phase at 1705156467.253934
write     32.17      32.30      0.247198    327680     1024.00    0.322988   79.25      5.44       79.59      6   
Ending compute phase at 1705156827.695015
Starting IO phase at 1705156827.695035
Ending IO phase at 1705156891.809482
write     39.88      40.06      0.191871    327680     1024.00    0.237391   63.91      3.59       64.19      7   
