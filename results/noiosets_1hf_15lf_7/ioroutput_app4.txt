############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 00:28:29 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696112909.888605
TestID              : 0
StartTime           : Sun Oct  1 00:28:30 2023
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696113270.680663
Starting IO phase at 1696113270.680684
Ending IO phase at 1696113694.337144
write     6.04       6.05       1.31        327680     1024.00    0.348898   423.32     11.18      423.66     0   
Ending compute phase at 1696114056.975243
Starting IO phase at 1696114056.975256
Ending IO phase at 1696114451.025823
write     6.46       6.50       1.23        327680     1024.00    2.81       393.71     7.77       396.15     1   
Ending compute phase at 1696114813.980362
Starting IO phase at 1696114813.980389
Ending IO phase at 1696115171.161238
write     7.12       7.17       1.11        327680     1024.00    3.06       356.92     10.69      359.71     2   
Ending compute phase at 1696115531.918295
Starting IO phase at 1696115531.918325
Ending IO phase at 1696115618.933805
write     29.31      29.47      0.262755    327680     1024.00    0.520439   86.85      4.29       87.34      3   
Ending compute phase at 1696115979.238914
Starting IO phase at 1696115979.238933
Ending IO phase at 1696116048.965612
write     36.78      36.87      0.202643    327680     1024.00    0.151547   69.44      8.32       69.60      4   
Ending compute phase at 1696116409.115246
Starting IO phase at 1696116409.115260
Ending IO phase at 1696116447.131635
write     67.89      67.90      0.102242    327680     1024.00    1.03       37.70      4.99       37.71      5   
