############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 00:28:28 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696112908.601106
TestID              : 0
StartTime           : Sun Oct  1 00:28:28 2023
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696113268.645500
Starting IO phase at 1696113268.645519
Ending IO phase at 1696113634.430473
write     7.00       7.01       1.09        327680     1024.00    0.118101   365.44     17.88      365.75     0   
Ending compute phase at 1696113995.147298
Starting IO phase at 1696113995.147317
Ending IO phase at 1696114324.849544
write     7.75       7.77       1.03        327680     1024.00    0.664645   329.41     4.72       330.26     1   
Ending compute phase at 1696114685.244925
Starting IO phase at 1696114685.244939
Ending IO phase at 1696114726.823163
write     62.02      62.04      0.128030    327680     1024.00    0.097838   41.26      4.07       41.28      2   
Ending compute phase at 1696115089.524870
Starting IO phase at 1696115089.524897
Ending IO phase at 1696115256.110033
write     15.17      15.45      0.511111    327680     1024.00    2.48       165.72     8.46       168.76     3   
Ending compute phase at 1696115616.854943
Starting IO phase at 1696115616.854964
Ending IO phase at 1696115667.355655
write     50.52      50.98      0.151939    327680     1024.00    0.455346   50.22      1.98       50.67      4   
Ending compute phase at 1696116027.855502
Starting IO phase at 1696116027.855525
Ending IO phase at 1696116091.823309
write     39.96      40.15      0.198559    327680     1024.00    0.333147   63.76      0.959895   64.07      5   
Ending compute phase at 1696116452.339757
Starting IO phase at 1696116452.339777
Ending IO phase at 1696116511.993362
write     42.94      43.12      0.181428    327680     1024.00    0.275344   59.37      3.21       59.62      6   
