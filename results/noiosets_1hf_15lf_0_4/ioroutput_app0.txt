############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 00:13:28 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705187608.775363
TestID              : 0
StartTime           : Sun Jan 14 00:13:28 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705187626.785736
Starting IO phase at 1705187626.785757
Ending IO phase at 1705187628.406035
write     82.38      82.67      0.086414    16384      1024.00    0.054349   1.55       0.206464   1.55       0   
Ending compute phase at 1705187646.635061
Starting IO phase at 1705187646.635073
Ending IO phase at 1705187648.097701
write     93.88      94.32      0.084109    16384      1024.00    0.068767   1.36       0.012862   1.36       1   
Ending compute phase at 1705187666.346160
Starting IO phase at 1705187666.346173
Ending IO phase at 1705187668.005711
write     83.23      84.49      0.093647    16384      1024.00    0.036598   1.51       0.019378   1.54       2   
Ending compute phase at 1705187686.247074
Starting IO phase at 1705187686.247086
Ending IO phase at 1705187687.949689
write     80.07      80.36      0.086325    16384      1024.00    0.066765   1.59       0.212314   1.60       3   
Ending compute phase at 1705187706.122886
Starting IO phase at 1705187706.122896
Ending IO phase at 1705187707.863866
write     77.62      77.87      0.091306    16384      1024.00    0.067315   1.64       0.206326   1.65       4   
Ending compute phase at 1705187726.152826
Starting IO phase at 1705187726.152837
Ending IO phase at 1705187727.725816
write     86.06      86.96      0.091275    16384      1024.00    0.059006   1.47       0.034971   1.49       5   
Ending compute phase at 1705187745.979942
Starting IO phase at 1705187745.979953
Ending IO phase at 1705187747.538587
write     87.10      87.48      0.085370    16384      1024.00    0.066665   1.46       0.097848   1.47       6   
Ending compute phase at 1705187765.735290
Starting IO phase at 1705187765.735301
Ending IO phase at 1705187767.273724
write     90.30      90.68      0.086437    16384      1024.00    0.044525   1.41       0.029354   1.42       7   
Ending compute phase at 1705187785.592620
Starting IO phase at 1705187785.592630
Ending IO phase at 1705187787.104703
write     84.17      84.52      0.087213    16384      1024.00    0.059736   1.51       0.143851   1.52       8   
Ending compute phase at 1705187805.396586
Starting IO phase at 1705187805.396596
Ending IO phase at 1705187806.907754
write     89.51      90.44      0.088458    16384      1024.00    0.039620   1.42       0.126420   1.43       9   
Ending compute phase at 1705187825.115447
Starting IO phase at 1705187825.115458
Ending IO phase at 1705187826.681713
write     88.17      88.51      0.086456    16384      1024.00    0.058278   1.45       0.063583   1.45       10  
Ending compute phase at 1705187844.955698
Starting IO phase at 1705187844.955709
Ending IO phase at 1705187846.657736
write     80.51      81.09      0.085791    16384      1024.00    0.042995   1.58       0.211972   1.59       11  
Ending compute phase at 1705187864.780434
Starting IO phase at 1705187864.780445
Ending IO phase at 1705187866.437214
write     81.53      82.32      0.090759    16384      1024.00    0.054606   1.55       0.246696   1.57       12  
Ending compute phase at 1705187884.679255
Starting IO phase at 1705187884.679267
Ending IO phase at 1705187886.373739
write     83.28      83.58      0.088938    16384      1024.00    0.062516   1.53       0.132232   1.54       13  
Ending compute phase at 1705187904.599641
Starting IO phase at 1705187904.599651
Ending IO phase at 1705187906.285707
write     78.33      80.42      0.099336    16384      1024.00    0.088535   1.59       0.236055   1.63       14  
Ending compute phase at 1705187924.496425
Starting IO phase at 1705187924.496436
Ending IO phase at 1705187926.177704
write     81.87      82.23      0.083700    16384      1024.00    0.051987   1.56       0.218041   1.56       15  
Ending compute phase at 1705187944.415040
Starting IO phase at 1705187944.415050
Ending IO phase at 1705187945.953708
write     88.97      89.31      0.089580    16384      1024.00    0.065319   1.43       0.120738   1.44       16  
Ending compute phase at 1705187964.119043
Starting IO phase at 1705187964.119055
Ending IO phase at 1705187965.625754
write     91.53      91.89      0.084215    16384      1024.00    0.050856   1.39       0.046193   1.40       17  
Ending compute phase at 1705187987.544562
Starting IO phase at 1705187987.544591
Ending IO phase at 1705188008.833546
write     5.13       6.12       1.28        16384      1024.00    3.67       20.91      1.37       24.96      18  
Ending compute phase at 1705188030.580729
Starting IO phase at 1705188030.580762
Ending IO phase at 1705188051.725941
write     5.24       6.17       1.30        16384      1024.00    3.30       20.75      0.525966   24.45      19  
Ending compute phase at 1705188073.740517
Starting IO phase at 1705188073.740553
Ending IO phase at 1705188096.373759
write     4.88       5.78       1.38        16384      1024.00    3.61       22.15      1.62       26.24      20  
Ending compute phase at 1705188118.396976
Starting IO phase at 1705188118.397014
Ending IO phase at 1705188139.895004
write     5.12       6.07       1.32        16384      1024.00    3.53       21.07      1.03       25.02      21  
Ending compute phase at 1705188162.028906
Starting IO phase at 1705188162.028926
Ending IO phase at 1705188184.965603
write     4.81       5.67       1.39        16384      1024.00    3.65       22.56      0.910471   26.59      22  
Ending compute phase at 1705188206.617219
Starting IO phase at 1705188206.617253
Ending IO phase at 1705188228.741705
write     5.05       5.90       1.33        16384      1024.00    3.22       21.71      1.04       25.34      23  
Ending compute phase at 1705188250.212896
Starting IO phase at 1705188250.212922
Ending IO phase at 1705188269.970556
write     5.63       6.59       1.20        16384      1024.00    2.99       19.43      0.666332   22.75      24  
Ending compute phase at 1705188290.724919
Starting IO phase at 1705188290.724943
Ending IO phase at 1705188309.449615
write     5.98       6.93       1.14        16384      1024.00    2.76       18.48      0.726195   21.41      25  
Ending compute phase at 1705188337.863412
Starting IO phase at 1705188337.863428
Ending IO phase at 1705188342.124510
write     8.85       30.55      0.186812    16384      1024.00    10.28      4.19       1.20       14.46      26  
Ending compute phase at 1705188360.419790
Starting IO phase at 1705188360.419803
Ending IO phase at 1705188361.993891
write     86.20      86.54      0.089583    16384      1024.00    0.059213   1.48       0.046409   1.48       27  
Ending compute phase at 1705188380.223598
Starting IO phase at 1705188380.223609
Ending IO phase at 1705188381.865765
write     81.93      82.25      0.092262    16384      1024.00    0.052594   1.56       0.080679   1.56       28  
Ending compute phase at 1705188400.110917
Starting IO phase at 1705188400.110928
Ending IO phase at 1705188401.773708
write     83.44      84.15      0.094368    16384      1024.00    0.055449   1.52       0.105326   1.53       29  
Ending compute phase at 1705188419.986864
Starting IO phase at 1705188419.986874
Ending IO phase at 1705188421.689679
write     78.04      78.29      0.091107    16384      1024.00    0.066735   1.63       0.177929   1.64       30  
Ending compute phase at 1705188439.887614
Starting IO phase at 1705188439.887625
Ending IO phase at 1705188441.555832
write     80.09      80.68      0.093802    16384      1024.00    0.044267   1.59       0.091873   1.60       31  
Ending compute phase at 1705188459.827339
Starting IO phase at 1705188459.827350
Ending IO phase at 1705188461.313597
write     88.47      88.83      0.088337    16384      1024.00    0.049187   1.44       0.028306   1.45       32  
Ending compute phase at 1705188479.531530
Starting IO phase at 1705188479.531541
Ending IO phase at 1705188481.225680
write     79.32      79.62      0.090468    16384      1024.00    0.053130   1.61       0.160946   1.61       33  
Ending compute phase at 1705188499.443777
Starting IO phase at 1705188499.443788
Ending IO phase at 1705188500.897689
write     92.20      92.62      0.084594    16384      1024.00    0.041775   1.38       0.029163   1.39       34  
Ending compute phase at 1705188519.119461
Starting IO phase at 1705188519.119472
Ending IO phase at 1705188520.781651
write     81.74      82.41      0.084922    16384      1024.00    0.052479   1.55       0.202102   1.57       35  
Ending compute phase at 1705188539.019042
Starting IO phase at 1705188539.019054
Ending IO phase at 1705188540.617679
write     86.65      86.97      0.047693    16384      1024.00    0.036886   1.47       0.709289   1.48       36  
Ending compute phase at 1705188558.815198
Starting IO phase at 1705188558.815209
Ending IO phase at 1705188560.399668
write     86.03      86.35      0.088004    16384      1024.00    0.088482   1.48       0.074972   1.49       37  
Ending compute phase at 1705188578.681245
Starting IO phase at 1705188578.681256
Ending IO phase at 1705188580.353711
write     79.90      80.92      0.097877    16384      1024.00    0.048846   1.58       0.142386   1.60       38  
Ending compute phase at 1705188598.610963
Starting IO phase at 1705188598.610975
Ending IO phase at 1705188607.461704
write     14.66      14.67      0.255621    16384      1024.00    0.111613   8.73       4.64       8.73       39  
Ending compute phase at 1705188625.675238
Starting IO phase at 1705188625.675249
Ending IO phase at 1705188627.096642
write     90.54      91.32      0.049028    16384      1024.00    0.064877   1.40       0.624782   1.41       40  
Ending compute phase at 1705188645.566047
Starting IO phase at 1705188645.566066
Ending IO phase at 1705188655.729590
write     12.43      12.88      0.554644    16384      1024.00    0.227554   9.94       4.68       10.30      41  
Ending compute phase at 1705188674.358431
Starting IO phase at 1705188674.358448
Ending IO phase at 1705188687.336640
write     9.58       9.91       0.589738    16384      1024.00    0.487372   12.91      3.49       13.36      42  
Ending compute phase at 1705188707.878113
Starting IO phase at 1705188707.878137
Ending IO phase at 1705188725.692146
write     6.35       7.33       1.08        16384      1024.00    2.33       17.45      0.512311   20.14      43  
Ending compute phase at 1705188747.206212
Starting IO phase at 1705188747.206235
Ending IO phase at 1705188767.420013
write     5.48       6.46       1.24        16384      1024.00    3.15       19.81      0.772087   23.36      44  
Ending compute phase at 1705188788.874508
Starting IO phase at 1705188788.874533
Ending IO phase at 1705188808.316137
write     5.69       6.70       1.19        16384      1024.00    3.04       19.11      0.330629   22.48      45  
Ending compute phase at 1705188829.714428
Starting IO phase at 1705188829.714461
Ending IO phase at 1705188849.583894
write     5.59       6.55       1.22        16384      1024.00    3.01       19.55      1.21       22.88      46  
Ending compute phase at 1705188870.822272
Starting IO phase at 1705188870.822308
Ending IO phase at 1705188889.454506
write     6.04       6.91       1.13        16384      1024.00    2.61       18.53      0.788293   21.19      47  
Ending compute phase at 1705188910.250778
Starting IO phase at 1705188910.250802
Ending IO phase at 1705188926.309676
write     6.97       8.15       0.939388    16384      1024.00    2.33       15.71      1.19       18.36      48  
Ending compute phase at 1705188946.886665
Starting IO phase at 1705188946.886697
Ending IO phase at 1705188960.799942
write     8.13       9.38       0.840348    16384      1024.00    1.98       13.65      0.665448   15.75      49  
Ending compute phase at 1705188986.045321
Starting IO phase at 1705188986.045340
Ending IO phase at 1705188999.337729
write     6.38       9.67       0.805353    16384      1024.00    6.83       13.24      0.354426   20.06      50  
Ending compute phase at 1705189017.631973
Starting IO phase at 1705189017.631988
Ending IO phase at 1705189026.121918
write     15.16      15.18      0.186320    16384      1024.00    0.053674   8.43       5.46       8.44       51  
Ending compute phase at 1705189044.283751
Starting IO phase at 1705189044.283766
Ending IO phase at 1705189053.620250
write     13.81      13.82      0.372699    16384      1024.00    0.063863   9.26       5.83       9.27       52  
Ending compute phase at 1705189071.779382
Starting IO phase at 1705189071.779397
Ending IO phase at 1705189080.497755
write     14.80      14.81      0.230635    16384      1024.00    0.064814   8.65       4.96       8.65       53  
Ending compute phase at 1705189098.698996
Starting IO phase at 1705189098.699008
Ending IO phase at 1705189100.337894
write     78.15      78.71      0.100916    16384      1024.00    0.053533   1.63       0.018675   1.64       54  
Ending compute phase at 1705189118.614677
Starting IO phase at 1705189118.614687
Ending IO phase at 1705189120.229694
write     85.22      86.32      0.091950    16384      1024.00    0.063643   1.48       0.026439   1.50       55  
Ending compute phase at 1705189138.413470
Starting IO phase at 1705189138.413481
Ending IO phase at 1705189140.177748
write     78.61      79.20      0.100292    16384      1024.00    0.065522   1.62       0.018175   1.63       56  
Ending compute phase at 1705189158.347161
Starting IO phase at 1705189158.347172
Ending IO phase at 1705189160.041769
write     79.70      80.40      0.055204    16384      1024.00    0.054463   1.59       0.717903   1.61       57  
Ending compute phase at 1705189178.248937
Starting IO phase at 1705189178.248948
Ending IO phase at 1705189179.858039
write     83.66      85.18      0.091061    16384      1024.00    0.062423   1.50       0.059918   1.53       58  
Ending compute phase at 1705189198.030964
Starting IO phase at 1705189198.030974
Ending IO phase at 1705189199.617742
write     82.97      83.26      0.095101    16384      1024.00    0.057021   1.54       0.039077   1.54       59  
Ending compute phase at 1705189217.839211
Starting IO phase at 1705189217.839223
Ending IO phase at 1705189221.501706
write     35.56      35.61      0.190719    16384      1024.00    0.056424   3.59       0.543847   3.60       60  
Ending compute phase at 1705189239.691159
Starting IO phase at 1705189239.691170
Ending IO phase at 1705189247.225746
write     17.13      17.14      0.270152    16384      1024.00    0.057407   7.47       4.17       7.47       61  
Ending compute phase at 1705189265.763933
Starting IO phase at 1705189265.763957
Ending IO phase at 1705189273.589759
write     15.84      16.52      0.416611    16384      1024.00    0.333752   7.75       4.48       8.08       62  
Ending compute phase at 1705189291.829400
Starting IO phase at 1705189291.829418
Ending IO phase at 1705189294.977790
write     41.24      41.55      0.135715    16384      1024.00    0.079808   3.08       0.919482   3.10       63  
Ending compute phase at 1705189313.323182
Starting IO phase at 1705189313.323201
Ending IO phase at 1705189322.085846
write     14.42      14.73      0.420732    16384      1024.00    0.216385   8.69       1.96       8.88       64  
Ending compute phase at 1705189344.629576
Starting IO phase at 1705189344.629600
Ending IO phase at 1705189350.165781
write     13.08      20.85      0.320170    16384      1024.00    4.38       6.14       2.52       9.79       65  
Ending compute phase at 1705189370.539433
Starting IO phase at 1705189370.539458
Ending IO phase at 1705189384.881587
write     7.85       9.08       0.861157    16384      1024.00    2.06       14.09      0.716495   16.31      66  
Ending compute phase at 1705189405.504010
Starting IO phase at 1705189405.504036
Ending IO phase at 1705189421.669608
write     6.97       8.09       0.989044    16384      1024.00    2.26       15.83      0.706385   18.37      67  
Ending compute phase at 1705189442.507606
Starting IO phase at 1705189442.507666
Ending IO phase at 1705189459.527617
write     6.58       7.67       1.04        16384      1024.00    2.45       16.69      0.550499   19.46      68  
Ending compute phase at 1705189480.359715
Starting IO phase at 1705189480.359745
Ending IO phase at 1705189499.221591
write     5.94       6.84       1.17        16384      1024.00    2.74       18.73      0.855300   21.56      69  
Ending compute phase at 1705189520.303938
Starting IO phase at 1705189520.303987
Ending IO phase at 1705189539.381578
write     5.89       6.85       1.17        16384      1024.00    2.69       18.68      0.626291   21.72      70  
Ending compute phase at 1705189560.503788
Starting IO phase at 1705189560.503813
Ending IO phase at 1705189576.433617
write     6.92       8.08       0.989988    16384      1024.00    2.65       15.84      0.239903   18.51      71  
Ending compute phase at 1705189596.096296
Starting IO phase at 1705189596.096322
Ending IO phase at 1705189615.165642
write     6.25       6.93       0.934199    16384      1024.00    1.52       18.47      4.06       20.47      72  
Ending compute phase at 1705189633.744157
Starting IO phase at 1705189633.744178
Ending IO phase at 1705189649.861578
write     7.79       8.22       0.803761    16384      1024.00    0.457366   15.56      4.02       16.43      73  
Ending compute phase at 1705189668.248242
Starting IO phase at 1705189668.248260
Ending IO phase at 1705189674.823855
write     19.04      19.49      0.410549    16384      1024.00    0.213707   6.57       0.296869   6.72       74  
Ending compute phase at 1705189693.221333
Starting IO phase at 1705189693.221351
Ending IO phase at 1705189704.537582
write     11.22      11.40      0.413512    16384      1024.00    0.238524   11.22      5.67       11.41      75  
Ending compute phase at 1705189723.134593
Starting IO phase at 1705189723.134613
Ending IO phase at 1705189727.949642
write     24.60      26.88      0.267170    16384      1024.00    0.440647   4.76       0.487399   5.20       76  
Ending compute phase at 1705189746.408496
Starting IO phase at 1705189746.408516
Ending IO phase at 1705189752.853574
write     19.65      20.35      0.393107    16384      1024.00    0.225444   6.29       0.262050   6.51       77  
Ending compute phase at 1705189772.655878
Starting IO phase at 1705189772.655896
Ending IO phase at 1705189787.657717
write     7.78       9.13       0.523627    16384      1024.00    1.57       14.02      6.50       16.45      78  
Ending compute phase at 1705189805.887569
Starting IO phase at 1705189805.887580
Ending IO phase at 1705189807.705735
write     73.99      74.55      0.106600    16384      1024.00    0.041087   1.72       0.019158   1.73       79  
Ending compute phase at 1705189825.923248
Starting IO phase at 1705189825.923258
Ending IO phase at 1705189827.741748
write     73.12      73.36      0.106921    16384      1024.00    0.052737   1.74       0.034812   1.75       80  
Ending compute phase at 1705189845.959496
Starting IO phase at 1705189845.959507
Ending IO phase at 1705189847.825704
write     75.93      76.40      0.103272    16384      1024.00    0.062626   1.68       0.028466   1.69       81  
Ending compute phase at 1705189866.059077
Starting IO phase at 1705189866.059088
Ending IO phase at 1705189867.881731
write     74.16      74.92      0.106043    16384      1024.00    0.077761   1.71       0.024548   1.73       82  
Ending compute phase at 1705189886.095466
Starting IO phase at 1705189886.095477
Ending IO phase at 1705189887.837749
write     75.92      76.78      0.103466    16384      1024.00    0.080735   1.67       0.025395   1.69       83  
Ending compute phase at 1705189906.026912
Starting IO phase at 1705189906.026924
Ending IO phase at 1705189907.793733
write     75.47      75.71      0.102461    16384      1024.00    0.052130   1.69       0.052076   1.70       84  
Ending compute phase at 1705189926.221098
Starting IO phase at 1705189926.221113
Ending IO phase at 1705189936.510002
write     12.26      12.49      0.640488    16384      1024.00    0.222290   10.25      3.01       10.44      85  
Ending compute phase at 1705189955.801096
Starting IO phase at 1705189955.801117
Ending IO phase at 1705189965.001608
write     12.54      14.20      0.546661    16384      1024.00    1.10       9.01       0.364672   10.21      86  
Ending compute phase at 1705189984.713149
Starting IO phase at 1705189984.713186
Ending IO phase at 1705189998.925612
write     8.18       9.16       0.858876    16384      1024.00    1.48       13.98      0.720317   15.64      87  
Ending compute phase at 1705190018.993272
Starting IO phase at 1705190018.993302
Ending IO phase at 1705190036.574131
write     6.60       7.42       1.08        16384      1024.00    1.85       17.24      0.316871   19.39      88  
Ending compute phase at 1705190057.729882
Starting IO phase at 1705190057.729907
Ending IO phase at 1705190076.614758
write     5.94       6.93       1.15        16384      1024.00    2.65       18.48      0.667967   21.53      89  
Ending compute phase at 1705190098.117322
Starting IO phase at 1705190098.117396
Ending IO phase at 1705190119.361757
write     5.20       6.08       1.28        16384      1024.00    3.42       21.05      1.31       24.62      90  
Ending compute phase at 1705190140.761285
Starting IO phase at 1705190140.761309
Ending IO phase at 1705190162.583106
write     5.18       5.96       1.33        16384      1024.00    2.98       21.49      1.07       24.72      91  
Ending compute phase at 1705190183.661505
Starting IO phase at 1705190183.661530
Ending IO phase at 1705190202.314707
write     5.99       6.95       1.15        16384      1024.00    2.76       18.41      1.51       21.38      92  
Ending compute phase at 1705190223.557487
Starting IO phase at 1705190223.557511
Ending IO phase at 1705190242.766715
write     5.85       6.81       1.11        16384      1024.00    2.77       18.80      1.35       21.86      93  
Ending compute phase at 1705190262.925821
Starting IO phase at 1705190262.925847
Ending IO phase at 1705190279.349013
write     7.10       7.91       0.991284    16384      1024.00    1.71       16.19      0.775957   18.03      94  
Ending compute phase at 1705190298.649751
Starting IO phase at 1705190298.649772
Ending IO phase at 1705190312.485583
write     8.60       9.38       0.834879    16384      1024.00    1.20       13.64      0.400510   14.89      95  
Ending compute phase at 1705190331.970220
Starting IO phase at 1705190331.970242
Ending IO phase at 1705190348.491244
write     7.22       7.81       1.02        16384      1024.00    1.33       16.39      0.324026   17.74      96  
Ending compute phase at 1705190380.186974
Starting IO phase at 1705190380.186994
Ending IO phase at 1705190387.821686
write     6.07       16.85      0.474899    16384      1024.00    13.48      7.60       2.91       21.08      97  
Ending compute phase at 1705190406.039534
Starting IO phase at 1705190406.039548
Ending IO phase at 1705190415.077764
write     14.31      14.33      0.278673    16384      1024.00    0.063991   8.93       4.48       8.95       98  
Ending compute phase at 1705190433.327194
Starting IO phase at 1705190433.327205
Ending IO phase at 1705190435.349741
write     64.83      65.00      0.120210    16384      1024.00    0.063044   1.97       0.046456   1.97       99  
Ending compute phase at 1705190453.531501
Starting IO phase at 1705190453.531512
Ending IO phase at 1705190455.362532
write     69.77      70.00      0.113571    16384      1024.00    0.050898   1.83       0.012391   1.83       100 
Ending compute phase at 1705190473.611715
Starting IO phase at 1705190473.611728
Ending IO phase at 1705190482.577740
write     14.37      14.38      0.267974    16384      1024.00    0.137983   8.90       4.61       8.91       101 
Ending compute phase at 1705190500.794343
Starting IO phase at 1705190500.794354
Ending IO phase at 1705190502.734413
write     65.89      66.06      0.120381    16384      1024.00    0.047179   1.94       0.012354   1.94       102 
Ending compute phase at 1705190521.127087
Starting IO phase at 1705190521.127099
Ending IO phase at 1705190523.077202
write     67.01      68.28      0.116428    16384      1024.00    0.048904   1.87       0.025871   1.91       103 
Ending compute phase at 1705190542.344824
Starting IO phase at 1705190542.344843
Ending IO phase at 1705190553.125563
write     10.97      12.01      0.627702    16384      1024.00    1.01       10.65      1.39       11.66      104 
Ending compute phase at 1705190571.522512
Starting IO phase at 1705190571.522530
Ending IO phase at 1705190578.537549
write     18.03      18.57      0.429729    16384      1024.00    0.228208   6.89       0.287088   7.10       105 
Ending compute phase at 1705190597.010372
Starting IO phase at 1705190597.010391
Ending IO phase at 1705190608.797566
write     10.73      11.90      0.621976    16384      1024.00    0.232184   10.75      5.73       11.93      106 
Ending compute phase at 1705190627.150859
Starting IO phase at 1705190627.150874
Ending IO phase at 1705190633.773661
write     18.98      19.50      0.410349    16384      1024.00    0.210789   6.57       0.007981   6.74       107 
Ending compute phase at 1705190652.606510
Starting IO phase at 1705190652.606533
Ending IO phase at 1705190662.724764
write     11.90      12.73      0.628669    16384      1024.00    0.674529   10.06      0.059245   10.76      108 
Ending compute phase at 1705190681.974488
Starting IO phase at 1705190681.974509
Ending IO phase at 1705190692.865044
write     10.91      12.01      0.642285    16384      1024.00    0.972151   10.66      0.487818   11.73      109 
Ending compute phase at 1705190712.838444
Starting IO phase at 1705190712.838466
Ending IO phase at 1705190727.097553
write     8.10       9.12       0.850022    16384      1024.00    1.62       14.03      0.585530   15.81      110 
Ending compute phase at 1705190747.191034
Starting IO phase at 1705190747.191058
Ending IO phase at 1705190765.381676
write     6.37       7.07       1.10        16384      1024.00    2.02       18.10      0.934075   20.09      111 
Ending compute phase at 1705190786.467115
Starting IO phase at 1705190786.467149
Ending IO phase at 1705190807.421967
write     5.43       6.21       1.27        16384      1024.00    2.65       20.62      0.588830   23.58      112 
Ending compute phase at 1705190826.739739
Starting IO phase at 1705190826.739763
Ending IO phase at 1705190841.124935
write     8.42       9.01       0.885896    16384      1024.00    0.861285   14.20      0.868499   15.20      113 
Ending compute phase at 1705190861.870926
Starting IO phase at 1705190861.870950
Ending IO phase at 1705190885.165703
write     5.01       5.51       1.33        16384      1024.00    2.28       23.25      2.01       25.53      114 
Ending compute phase at 1705190905.626488
Starting IO phase at 1705190905.626512
Ending IO phase at 1705190922.865553
write     6.59       7.53       1.06        16384      1024.00    2.33       17.00      0.838420   19.43      115 
Ending compute phase at 1705190942.519135
Starting IO phase at 1705190942.519159
Ending IO phase at 1705190958.177948
write     7.48       8.24       0.953843    16384      1024.00    1.55       15.53      1.01       17.12      116 
Ending compute phase at 1705190978.303335
Starting IO phase at 1705190978.303361
Ending IO phase at 1705190996.696487
write     6.38       6.97       1.03        16384      1024.00    1.75       18.35      2.88       20.07      117 
Ending compute phase at 1705191017.984879
Starting IO phase at 1705191017.984908
Ending IO phase at 1705191034.107181
write     6.72       7.70       0.996989    16384      1024.00    3.01       16.62      0.476265   19.04      118 
Ending compute phase at 1705191053.379649
Starting IO phase at 1705191053.379675
Ending IO phase at 1705191065.405641
write     9.83       10.85      0.714850    16384      1024.00    1.14       11.80      0.522306   13.02      119 
Ending compute phase at 1705191084.999325
Starting IO phase at 1705191084.999346
Ending IO phase at 1705191098.784771
write     8.56       9.36       0.855061    16384      1024.00    1.30       13.68      0.847498   14.96      120 
Ending compute phase at 1705191117.863866
Starting IO phase at 1705191117.863909
Ending IO phase at 1705191137.393584
write     6.28       6.75       1.11        16384      1024.00    0.929984   18.96      4.62       20.37      121 
Ending compute phase at 1705191160.172064
Starting IO phase at 1705191160.172083
Ending IO phase at 1705191174.389776
write     6.92       9.05       0.438366    16384      1024.00    4.37       14.15      7.14       18.50      122 
Ending compute phase at 1705191193.003966
Starting IO phase at 1705191193.003982
Ending IO phase at 1705191203.553554
write     11.75      12.25      0.522606    16384      1024.00    0.508574   10.45      2.09       10.89      123 
Ending compute phase at 1705191221.944264
Starting IO phase at 1705191221.944284
Ending IO phase at 1705191231.689575
write     12.95      13.29      0.547353    16384      1024.00    0.269417   9.63       0.906198   9.88       124 
Ending compute phase at 1705191250.239774
Starting IO phase at 1705191250.239790
Ending IO phase at 1705191269.269610
write     6.62       6.76       0.661619    16384      1024.00    0.445457   18.94      8.37       19.33      125 
Ending compute phase at 1705191287.495880
Starting IO phase at 1705191287.495896
Ending IO phase at 1705191296.137561
write     14.71      14.99      0.516709    16384      1024.00    0.166390   8.54       0.321103   8.70       126 
Ending compute phase at 1705191315.136146
Starting IO phase at 1705191315.136165
Ending IO phase at 1705191330.577567
write     7.89       8.33       0.625847    16384      1024.00    0.854156   15.37      5.37       16.22      127 
Ending compute phase at 1705191349.548699
Starting IO phase at 1705191349.548792
Ending IO phase at 1705191359.897564
write     11.61      12.58      0.613598    16384      1024.00    0.846421   10.18      0.434889   11.03      128 
Ending compute phase at 1705191378.720690
Starting IO phase at 1705191378.720716
Ending IO phase at 1705191385.914586
write     16.47      17.97      0.444990    16384      1024.00    0.638382   7.12       0.015786   7.77       129 
Ending compute phase at 1705191406.653608
Starting IO phase at 1705191406.653680
