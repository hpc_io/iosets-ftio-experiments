############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 00:13:30 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705187610.152571
TestID              : 0
StartTime           : Sun Jan 14 00:13:30 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705187971.108463
Starting IO phase at 1705187971.108480
Ending IO phase at 1705188329.517571
write     7.14       7.20       1.11        327680     1024.00    0.794262   355.79     9.77       358.74     0   
Ending compute phase at 1705188697.798114
Starting IO phase at 1705188697.798139
Ending IO phase at 1705188985.615673
write     8.87       9.02       0.863987    327680     1024.00    1.24       283.69     11.24      288.73     1   
Ending compute phase at 1705189349.242109
Starting IO phase at 1705189349.242133
Ending IO phase at 1705189572.258282
write     11.31      11.49      0.673852    327680     1024.00    3.98       222.81     12.09      226.33     2   
Ending compute phase at 1705189936.021341
Starting IO phase at 1705189936.021359
Ending IO phase at 1705190164.489952
write     11.04      11.13      0.710580    327680     1024.00    3.55       229.94     8.35       231.99     3   
Ending compute phase at 1705190524.650153
Starting IO phase at 1705190524.650177
Ending IO phase at 1705190612.718365
write     29.14      29.15      0.274321    327680     1024.00    0.098343   87.83      14.76      87.84      4   
Ending compute phase at 1705190976.939584
Starting IO phase at 1705190976.939615
Ending IO phase at 1705191172.499195
write     12.84      13.17      0.543778    327680     1024.00    4.02       194.44     22.49      199.42     5   
