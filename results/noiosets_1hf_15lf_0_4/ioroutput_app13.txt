############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 00:13:30 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705187610.264412
TestID              : 0
StartTime           : Sun Jan 14 00:13:30 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705187971.760513
Starting IO phase at 1705187971.760532
Ending IO phase at 1705188342.540954
write     6.90       6.91       1.14        327680     1024.00    1.95       370.67     13.42      371.21     0   
Ending compute phase at 1705188704.610431
Starting IO phase at 1705188704.610454
Ending IO phase at 1705188999.217195
write     8.64       8.70       0.919753    327680     1024.00    1.90       294.36     7.17       296.17     1   
Ending compute phase at 1705189360.727299
Starting IO phase at 1705189360.727318
Ending IO phase at 1705189616.630968
write     9.96       10.01      0.793244    327680     1024.00    1.28       255.77     6.10       257.05     2   
Ending compute phase at 1705189977.773195
Starting IO phase at 1705189977.773218
Ending IO phase at 1705190274.007604
write     8.62       8.65       0.924890    327680     1024.00    0.994708   295.97     3.96       297.13     3   
Ending compute phase at 1705190634.323248
Starting IO phase at 1705190634.323262
Ending IO phase at 1705190826.145843
write     13.35      13.52      0.562546    327680     1024.00    0.164200   189.29     14.85      191.80     4   
Ending compute phase at 1705191186.387977
Starting IO phase at 1705191186.387992
Ending IO phase at 1705191261.151795
write     34.39      36.07      0.190825    327680     1024.00    0.051857   70.97      40.34      74.44      5   
