############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 21:00:28 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696186828.016180
TestID              : 0
StartTime           : Sun Oct  1 21:00:28 2023
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696187264.678045
Starting IO phase at 1696187264.678068
Ending IO phase at 1696187523.913592
write     7.64       9.92       0.755337    327680     1024.00    156.32     258.12     17.18      335.16     0   
Ending compute phase at 1696187899.288495
Starting IO phase at 1696187899.288518
Ending IO phase at 1696187971.359200
write     29.41      40.25      0.168845    327680     1024.00    25.83      63.61      35.99      87.06      1   
Ending compute phase at 1696188359.128937
Starting IO phase at 1696188359.128967
Ending IO phase at 1696188395.283375
write     71.33      71.89      0.111243    327680     1024.00    0.072705   35.61      15.71      35.89      2   
Ending compute phase at 1696188755.475989
Starting IO phase at 1696188755.476010
Ending IO phase at 1696188781.397571
write     101.00     101.70     0.077889    327680     1024.00    0.050367   25.17      5.38       25.35      3   
Ending compute phase at 1696189141.587252
Starting IO phase at 1696189141.587272
Ending IO phase at 1696189169.265518
write     93.99      94.01      0.069576    327680     1024.00    0.056859   27.23      4.97       27.24      4   
Ending compute phase at 1696189529.485873
Starting IO phase at 1696189529.485894
Ending IO phase at 1696189567.613822
write     67.65      67.67      0.107880    327680     1024.00    0.066899   37.83      8.86       37.84      5   
Ending compute phase at 1696189929.893012
Starting IO phase at 1696189929.893034
Ending IO phase at 1696189984.408424
write     45.55      47.49      0.167873    327680     1024.00    2.19       53.91      1.64       56.21      6   
Ending compute phase at 1696190344.662814
Starting IO phase at 1696190344.662835
Ending IO phase at 1696190404.213955
write     43.19      43.22      0.149816    327680     1024.00    0.134225   59.24      11.33      59.27      7   
