############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 21:00:27 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_6_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696186827.936234
TestID              : 0
StartTime           : Sun Oct  1 21:00:28 2023
Path                : /mnt/beegfs/iosets_6_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_6_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696187208.449885
Starting IO phase at 1696187208.449914
Ending IO phase at 1696187459.477685
write     9.44       9.86       0.740794    327680     1024.00    22.42      259.60     50.89      271.16     0   
Ending compute phase at 1696187821.556840
Starting IO phase at 1696187821.556864
Ending IO phase at 1696187862.961891
write     59.65      62.18      0.127237    327680     1024.00    1.90       41.17      4.86       42.92      1   
Ending compute phase at 1696188223.180323
Starting IO phase at 1696188223.180344
Ending IO phase at 1696188250.477474
write     95.27      95.32      0.083392    327680     1024.00    1.01       26.86      11.62      26.87      2   
Ending compute phase at 1696188610.676470
Starting IO phase at 1696188610.676491
Ending IO phase at 1696188640.385728
write     87.67      87.74      0.090661    327680     1024.00    0.043472   29.18      1.84       29.20      3   
Ending compute phase at 1696189000.567551
Starting IO phase at 1696189000.567572
Ending IO phase at 1696189047.073350
write     55.45      55.45      0.132541    327680     1024.00    0.079648   46.16      5.53       46.17      4   
Ending compute phase at 1696189408.288860
Starting IO phase at 1696189408.288890
Ending IO phase at 1696189460.775929
write     48.23      49.14      0.162809    327680     1024.00    0.973451   52.10      10.84      53.07      5   
Ending compute phase at 1696189820.989495
Starting IO phase at 1696189820.989514
Ending IO phase at 1696189873.648108
write     48.93      48.94      0.158901    327680     1024.00    0.753703   52.31      1.46       52.32      6   
Ending compute phase at 1696190236.698282
Starting IO phase at 1696190236.698305
Ending IO phase at 1696190289.052588
write     46.73      49.28      0.146466    327680     1024.00    4.82       51.95      5.06       54.79      7   
