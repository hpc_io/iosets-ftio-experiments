############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 15:29:26 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696166966.412182
TestID              : 0
StartTime           : Sun Oct  1 15:29:26 2023
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 1.2%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 
WARNING: The file "/mnt/beegfs/iosets_7_.00000000" exists already and will be overwritten

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
WARNING: The file "/mnt/beegfs/iosets_7_.00000001" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_7_.00000004" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_7_.00000003" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_7_.00000002" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_7_.00000007" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_7_.00000006" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_7_.00000005" exists already and will be overwritten
Ending compute phase at 1696167326.694177
Starting IO phase at 1696167326.694201
Ending IO phase at 1696167678.213099
write     7.29       7.30       1.09        327680     1024.00    0.164545   350.45     5.40       351.40     0   
Ending compute phase at 1696168038.572892
Starting IO phase at 1696168038.572914
Ending IO phase at 1696168238.581835
write     12.79      12.82      0.616754    327680     1024.00    0.310439   199.73     2.59       200.20     1   
Ending compute phase at 1696168598.948022
Starting IO phase at 1696168598.948035
Ending IO phase at 1696168647.527959
write     52.77      52.97      0.131089    327680     1024.00    0.175086   48.33      14.61      48.51      2   
Ending compute phase at 1696169007.701052
Starting IO phase at 1696169007.701065
Ending IO phase at 1696169032.867161
write     103.37     103.40     0.066618    327680     1024.00    0.046457   24.76      3.44       24.77      3   
Ending compute phase at 1696169393.581987
Starting IO phase at 1696169393.582000
Ending IO phase at 1696169425.968978
write     78.80      79.99      0.087257    327680     1024.00    0.687706   32.00      4.08       32.49      4   
Ending compute phase at 1696169787.783380
Starting IO phase at 1696169787.783404
Ending IO phase at 1696169877.756914
write     28.04      28.54      0.280238    327680     1024.00    1.61       89.71      3.33       91.29      5   
Ending compute phase at 1696170238.428724
Starting IO phase at 1696170238.428748
Ending IO phase at 1696170349.641566
write     22.98      23.08      0.346148    327680     1024.00    0.513565   110.91     0.803600   111.42     6   
Ending compute phase at 1696170714.039931
Starting IO phase at 1696170714.039949
