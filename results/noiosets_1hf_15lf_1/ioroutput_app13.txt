############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 15:29:26 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696166966.600364
TestID              : 0
StartTime           : Sun Oct  1 15:29:26 2023
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 1.2%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696167327.452435
Starting IO phase at 1696167327.452463
Ending IO phase at 1696167693.169665
write     7.00       7.01       1.13        327680     1024.00    0.521831   365.38     4.98       365.96     0   
Ending compute phase at 1696168054.453617
Starting IO phase at 1696168054.453644
Ending IO phase at 1696168271.088346
write     11.77      11.84      0.675589    327680     1024.00    1.21       216.19     5.32       217.43     1   
Ending compute phase at 1696168632.091034
Starting IO phase at 1696168632.091051
Ending IO phase at 1696168795.884755
write     15.58      15.66      0.506453    327680     1024.00    0.836304   163.50     1.44       164.30     2   
Ending compute phase at 1696169156.355236
Starting IO phase at 1696169156.355283
Ending IO phase at 1696169298.086162
write     18.04      18.08      0.440437    327680     1024.00    0.424529   141.57     5.93       141.92     3   
Ending compute phase at 1696169658.396660
Starting IO phase at 1696169658.396673
Ending IO phase at 1696169712.107230
write     47.79      47.93      0.143085    327680     1024.00    0.090748   53.41      8.20       53.57      4   
Ending compute phase at 1696170072.339091
Starting IO phase at 1696170072.339104
Ending IO phase at 1696170122.632033
write     51.21      51.22      0.119018    327680     1024.00    0.059739   49.98      27.92      49.99      5   
Ending compute phase at 1696170482.847197
Starting IO phase at 1696170482.847210
Ending IO phase at 1696170533.176308
write     51.20      51.21      0.152520    327680     1024.00    0.058893   49.99      35.73      50.00      6   
