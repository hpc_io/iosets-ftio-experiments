############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 15:29:26 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696166966.492185
TestID              : 0
StartTime           : Sun Oct  1 15:29:26 2023
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 1.2%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696167326.722201
Starting IO phase at 1696167326.722226
Ending IO phase at 1696167678.834017
write     7.27       7.28       1.09        327680     1024.00    0.143102   351.71     5.23       352.04     0   
Ending compute phase at 1696168039.405468
Starting IO phase at 1696168039.405491
Ending IO phase at 1696168242.851575
write     12.57      12.59      0.635155    327680     1024.00    0.406543   203.30     2.53       203.72     1   
Ending compute phase at 1696168603.151220
Starting IO phase at 1696168603.151235
Ending IO phase at 1696168707.042190
write     24.64      24.67      0.312851    327680     1024.00    0.181640   103.76     3.68       103.90     2   
Ending compute phase at 1696169067.175758
Starting IO phase at 1696169067.175773
Ending IO phase at 1696169092.440824
write     102.78     102.81     0.077780    327680     1024.00    0.053130   24.90      9.58       24.91      3   
Ending compute phase at 1696169452.698648
Starting IO phase at 1696169452.698662
Ending IO phase at 1696169479.125224
write     98.34      98.37      0.068110    327680     1024.00    0.058223   26.03      4.23       26.03      4   
Ending compute phase at 1696169839.683577
Starting IO phase at 1696169839.683600
Ending IO phase at 1696169904.751075
write     39.33      39.63      0.195686    327680     1024.00    0.473436   64.60      2.67       65.09      5   
Ending compute phase at 1696170266.140533
Starting IO phase at 1696170266.140556
Ending IO phase at 1696170368.913607
write     24.74      25.00      0.318205    327680     1024.00    1.11       102.40     2.28       103.50     6   
Ending compute phase at 1696170729.273891
Starting IO phase at 1696170729.273911
