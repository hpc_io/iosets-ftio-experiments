############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 15:29:26 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696166966.580187
TestID              : 0
StartTime           : Sun Oct  1 15:29:26 2023
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 1.2%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696167327.036864
Starting IO phase at 1696167327.036890
Ending IO phase at 1696167689.249092
write     7.07       7.14       1.11        327680     1024.00    0.350860   358.62     7.65       362.24     0   
Ending compute phase at 1696168050.993518
Starting IO phase at 1696168050.993546
Ending IO phase at 1696168266.041389
write     11.87      11.96      0.668702    327680     1024.00    0.897511   213.99     4.28       215.66     1   
Ending compute phase at 1696168626.426319
Starting IO phase at 1696168626.426342
Ending IO phase at 1696168779.042031
write     16.77      16.80      0.475626    327680     1024.00    0.294743   152.36     1.40       152.62     2   
Ending compute phase at 1696169139.195293
Starting IO phase at 1696169139.195307
Ending IO phase at 1696169163.777549
write     105.18     105.24     0.075535    327680     1024.00    0.055487   24.33      5.76       24.34      3   
Ending compute phase at 1696169524.002906
Starting IO phase at 1696169524.002920
Ending IO phase at 1696169551.017298
write     96.16      96.18      0.082857    327680     1024.00    0.052644   26.62      3.38       26.62      4   
Ending compute phase at 1696169911.210692
Starting IO phase at 1696169911.210706
Ending IO phase at 1696169954.160981
write     60.33      60.33      0.095353    327680     1024.00    0.042434   42.43      11.92      42.44      5   
Ending compute phase at 1696170315.992386
Starting IO phase at 1696170315.992409
Ending IO phase at 1696170403.414406
write     28.85      29.40      0.268769    327680     1024.00    1.64       87.09      1.08       88.73      6   
Ending compute phase at 1696170764.415248
Starting IO phase at 1696170764.415272
