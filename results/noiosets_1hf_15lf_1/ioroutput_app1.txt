############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 15:29:25 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696166965.202309
TestID              : 0
StartTime           : Sun Oct  1 15:29:25 2023
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 1.2%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 
WARNING: The file "/mnt/beegfs/iosets_1_.00000006" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_1_.00000002" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_1_.00000005" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_1_.00000000" exists already and will be overwritten

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
WARNING: The file "/mnt/beegfs/iosets_1_.00000007" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_1_.00000001" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_1_.00000003" exists already and will be overwritten
WARNING: The file "/mnt/beegfs/iosets_1_.00000004" exists already and will be overwritten
Ending compute phase at 1696167325.395751
Starting IO phase at 1696167325.395770
Ending IO phase at 1696167647.546600
write     7.95       7.95       0.990003    327680     1024.00    0.141970   321.89     20.71      322.21     0   
Ending compute phase at 1696168010.567547
Starting IO phase at 1696168010.567563
Ending IO phase at 1696168047.754240
write     64.70      69.39      0.083108    327680     1024.00    2.64       36.90      13.60      39.57      1   
Ending compute phase at 1696168407.946213
Starting IO phase at 1696168407.946227
Ending IO phase at 1696168431.768237
write     108.87     108.91     0.068221    327680     1024.00    0.055064   23.51      8.27       23.51      2   
Ending compute phase at 1696168794.664337
Starting IO phase at 1696168794.664358
Ending IO phase at 1696168848.560076
write     45.47      47.79      0.166934    327680     1024.00    2.76       53.57      5.63       56.30      3   
Ending compute phase at 1696169209.914809
Starting IO phase at 1696169209.914836
Ending IO phase at 1696169349.950036
write     18.17      18.32      0.397437    327680     1024.00    1.18       139.74     12.57      140.89     4   
Ending compute phase at 1696169710.635757
Starting IO phase at 1696169710.635792
Ending IO phase at 1696169827.524310
write     21.87      21.96      0.356895    327680     1024.00    0.494030   116.58     2.38       117.05     5   
Ending compute phase at 1696170191.465641
Starting IO phase at 1696170191.465680
Ending IO phase at 1696170276.302655
write     28.95      30.28      0.229845    327680     1024.00    3.80       84.54      11.08      88.43      6   
Ending compute phase at 1696170636.488481
Starting IO phase at 1696170636.488495
Ending IO phase at 1696170669.061737
write     79.13      79.15      0.091271    327680     1024.00    0.070612   32.34      6.02       32.35      7   
