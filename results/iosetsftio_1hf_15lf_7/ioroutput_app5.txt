############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 10:04:02 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696147442.188095
TestID              : 0
StartTime           : Sun Oct  1 10:04:02 2023
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696147842.236902
Starting IO phase at 1696147842.236931
Ending IO phase at 1696148139.557264
write     7.60       8.63       0.801695    327680     1024.00    65.79      296.49     43.39      336.93     0   
Ending compute phase at 1696148502.758296
Starting IO phase at 1696148502.758325
Ending IO phase at 1696148614.097441
write     22.41      29.72      0.265820    327680     1024.00    5.99       86.14      29.06      114.24     1   
Ending compute phase at 1696149058.039175
Starting IO phase at 1696149058.039198
Ending IO phase at 1696149144.830497
write     23.82      29.57      0.265082    327680     1024.00    21.15      86.57      10.50      107.46     2   
Ending compute phase at 1696149506.987797
Starting IO phase at 1696149506.987821
Ending IO phase at 1696149543.794299
write     66.59      70.49      0.107824    327680     1024.00    2.26       36.32      4.77       38.44      3   
Ending compute phase at 1696149903.965657
Starting IO phase at 1696149903.965679
Ending IO phase at 1696149934.958586
write     83.41      83.43      0.088980    327680     1024.00    0.061102   30.69      2.88       30.69      4   
Ending compute phase at 1696150295.279230
Starting IO phase at 1696150295.279252
Ending IO phase at 1696150324.369530
write     89.61      89.63      0.078559    327680     1024.00    0.071473   28.56      3.42       28.57      5   
Ending compute phase at 1696150684.571259
Starting IO phase at 1696150684.571280
Ending IO phase at 1696150715.437797
write     83.81      83.83      0.088307    327680     1024.00    0.678643   30.54      2.28       30.55      6   
Ending compute phase at 1696151075.684141
Starting IO phase at 1696151075.684161
Ending IO phase at 1696151111.629325
write     72.11      72.12      0.102869    327680     1024.00    0.093939   35.50      5.39       35.50      7   
