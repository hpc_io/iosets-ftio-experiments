############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 10:04:02 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696147442.648249
TestID              : 0
StartTime           : Sun Oct  1 10:04:02 2023
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696147950.913313
Starting IO phase at 1696147950.913336
Ending IO phase at 1696148195.129745
write     6.54       9.60       0.612391    327680     1024.00    173.59     266.77     82.88      391.36     0   
Ending compute phase at 1696148677.734267
Starting IO phase at 1696148677.734295
Ending IO phase at 1696148837.519020
write     9.08       16.04      0.389689    327680     1024.00    166.06     159.62     38.10      282.03     1   
Ending compute phase at 1696149203.804885
Starting IO phase at 1696149203.804908
Ending IO phase at 1696149494.955890
write     8.62       8.80       0.908491    327680     1024.00    6.62       290.87     0.178804   296.96     2   
Ending compute phase at 1696149858.695979
Starting IO phase at 1696149858.695993
Ending IO phase at 1696149893.565484
write     67.25      74.12      0.107907    327680     1024.00    3.53       34.54      7.93       38.07      3   
Ending compute phase at 1696150253.767586
Starting IO phase at 1696150253.767606
Ending IO phase at 1696150278.845344
write     103.38     103.43     0.060786    327680     1024.00    0.071884   24.75      5.31       24.76      4   
Ending compute phase at 1696150639.020249
Starting IO phase at 1696150639.020270
Ending IO phase at 1696150666.961271
write     92.71      92.75      0.080017    327680     1024.00    0.061350   27.60      4.36       27.61      5   
Ending compute phase at 1696151027.172144
Starting IO phase at 1696151027.172166
Ending IO phase at 1696151059.841472
write     79.63      79.65      0.090865    327680     1024.00    0.047208   32.14      4.43       32.15      6   
