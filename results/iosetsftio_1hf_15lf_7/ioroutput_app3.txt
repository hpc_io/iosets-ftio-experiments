############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 10:04:01 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696147441.048968
TestID              : 0
StartTime           : Sun Oct  1 10:04:01 2023
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696147801.226443
Starting IO phase at 1696147801.226477
Ending IO phase at 1696147939.010282
write     18.56      21.97      0.346866    327680     1024.00    0.140933   116.55     62.55      137.91     0   
Ending compute phase at 1696148303.385087
Starting IO phase at 1696148303.385111
Ending IO phase at 1696148342.881711
write     65.57      65.58      0.108972    327680     1024.00    0.059601   39.04      8.92       39.04      1   
Ending compute phase at 1696148832.126358
Starting IO phase at 1696148832.126382
Ending IO phase at 1696148989.416988
write     8.95       16.32      0.484788    327680     1024.00    129.34     156.84     9.35       286.06     2   
Ending compute phase at 1696149353.827727
Starting IO phase at 1696149353.827751
Ending IO phase at 1696149435.799216
write     29.83      31.32      0.249499    327680     1024.00    4.45       81.74      3.88       85.82      3   
Ending compute phase at 1696149795.940893
Starting IO phase at 1696149795.940915
Ending IO phase at 1696149819.277867
write     110.91     111.08     0.066909    327680     1024.00    0.059526   23.05      2.46       23.08      4   
Ending compute phase at 1696150179.499962
Starting IO phase at 1696150179.499982
Ending IO phase at 1696150205.193675
write     101.17     101.32     0.066377    327680     1024.00    0.049406   25.27      10.08      25.30      5   
Ending compute phase at 1696150565.411236
Starting IO phase at 1696150565.411258
Ending IO phase at 1696150593.097660
write     93.97      93.98      0.052005    327680     1024.00    0.064737   27.24      10.60      27.24      6   
Ending compute phase at 1696150953.315408
Starting IO phase at 1696150953.315429
Ending IO phase at 1696150982.529993
write     88.77      88.79      0.075779    327680     1024.00    0.032241   28.83      6.90       28.84      7   
