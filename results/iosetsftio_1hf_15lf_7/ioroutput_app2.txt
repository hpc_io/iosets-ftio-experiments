############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 10:04:01 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696147441.025746
TestID              : 0
StartTime           : Sun Oct  1 10:04:01 2023
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696147801.042674
Starting IO phase at 1696147801.042695
Ending IO phase at 1696147947.301817
write     17.50      20.39      0.291009    327680     1024.00    0.188455   125.58     64.52      146.25     0   
Ending compute phase at 1696148362.444489
Starting IO phase at 1696148362.444512
Ending IO phase at 1696148400.941405
write     67.15      67.63      0.110199    327680     1024.00    0.135280   37.85      10.88      38.12      1   
Ending compute phase at 1696148832.359047
Starting IO phase at 1696148832.359070
Ending IO phase at 1696148946.339375
write     13.85      22.53      0.331074    327680     1024.00    72.78      113.63     7.70       184.84     2   
Ending compute phase at 1696149309.500050
Starting IO phase at 1696149309.500074
Ending IO phase at 1696149405.365468
write     25.96      26.86      0.297883    327680     1024.00    3.82       95.32      20.89      98.63      3   
Ending compute phase at 1696149765.571493
Starting IO phase at 1696149765.571515
Ending IO phase at 1696149789.285972
write     110.65     110.70     0.056635    327680     1024.00    0.057920   23.12      5.01       23.14      4   
Ending compute phase at 1696150149.535845
Starting IO phase at 1696150149.535866
Ending IO phase at 1696150173.501561
write     108.73     108.76     0.070197    327680     1024.00    0.084486   23.54      4.33       23.54      5   
Ending compute phase at 1696150533.698928
Starting IO phase at 1696150533.698948
Ending IO phase at 1696150558.993478
write     102.84     102.88     0.053107    327680     1024.00    0.068268   24.88      7.90       24.89      6   
Ending compute phase at 1696150919.179005
Starting IO phase at 1696150919.179025
Ending IO phase at 1696150946.817748
write     93.93      93.95      0.076079    327680     1024.00    0.057551   27.25      2.90       27.25      7   
