############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 10:04:02 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696147442.352260
TestID              : 0
StartTime           : Sun Oct  1 10:04:02 2023
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696147869.508913
Starting IO phase at 1696147869.508938
Ending IO phase at 1696148163.821496
write     7.09       8.85       0.859060    327680     1024.00    79.62      289.24     19.85      360.85     0   
Ending compute phase at 1696148555.590595
Starting IO phase at 1696148555.590618
Ending IO phase at 1696148677.198641
write     16.72      25.53      0.294926    327680     1024.00    33.97      100.28     27.17      153.14     1   
Ending compute phase at 1696149058.475037
Starting IO phase at 1696149058.475059
Ending IO phase at 1696149170.967009
write     19.26      22.84      0.338863    327680     1024.00    21.57      112.10     5.10       132.93     2   
Ending compute phase at 1696149534.920040
Starting IO phase at 1696149534.920064
Ending IO phase at 1696149576.032702
write     57.83      63.23      0.119543    327680     1024.00    4.18       40.49      2.54       44.27      3   
Ending compute phase at 1696149936.285582
Starting IO phase at 1696149936.285604
Ending IO phase at 1696149978.828268
write     60.82      60.86      0.124900    327680     1024.00    0.133946   42.06      2.09       42.09      4   
Ending compute phase at 1696150341.574698
Starting IO phase at 1696150341.574724
Ending IO phase at 1696150370.517337
write     82.07      89.25      0.082916    327680     1024.00    2.70       28.68      3.49       31.19      5   
Ending compute phase at 1696150730.724560
Starting IO phase at 1696150730.724581
Ending IO phase at 1696150766.616038
write     71.70      71.76      0.095929    327680     1024.00    0.051032   35.68      5.00       35.71      6   
Ending compute phase at 1696151126.882972
Starting IO phase at 1696151126.882993
Ending IO phase at 1696151164.729582
write     68.39      68.40      0.116965    327680     1024.00    0.047100   37.43      3.82       37.43      7   
