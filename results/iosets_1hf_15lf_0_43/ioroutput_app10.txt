############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Jan 18 00:52:14 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705535534.223014
TestID              : 0
StartTime           : Thu Jan 18 00:52:14 2024
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705536080.410927
Starting IO phase at 1705536080.410946
Ending IO phase at 1705536142.166473
write     10.35      11.13      0.179814    327680     1024.00    185.66     229.94     4.06       247.23     0   
Ending compute phase at 1705536502.283152
Starting IO phase at 1705536502.283167
Ending IO phase at 1705536538.275165
write     72.01      72.02      0.096133    327680     1024.00    0.078862   35.55      4.79       35.55      1   
Ending compute phase at 1705536898.477481
Starting IO phase at 1705536898.477495
Ending IO phase at 1705536937.538537
write     65.91      65.92      0.112832    327680     1024.00    0.060462   38.83      3.47       38.84      2   
Ending compute phase at 1705537297.725266
Starting IO phase at 1705537297.725279
Ending IO phase at 1705537337.736016
write     64.40      64.41      0.106049    327680     1024.00    0.053395   39.74      5.81       39.75      3   
Ending compute phase at 1705537697.925137
Starting IO phase at 1705537697.925151
Ending IO phase at 1705537741.387563
write     59.42      59.43      0.134203    327680     1024.00    0.054555   43.08      3.67       43.08      4   
Ending compute phase at 1705538101.702016
Starting IO phase at 1705538101.702030
Ending IO phase at 1705538156.918798
write     46.56      46.63      0.168667    327680     1024.00    0.079632   54.91      3.84       54.98      5   
Ending compute phase at 1705538517.623585
Starting IO phase at 1705538517.623599
Ending IO phase at 1705538575.342136
write     44.20      44.45      0.157605    327680     1024.00    0.793025   57.59      7.01       57.91      6   
Ending compute phase at 1705538936.518448
Starting IO phase at 1705538936.518475
Ending IO phase at 1705538997.977905
write     41.15      41.79      0.184153    327680     1024.00    0.968009   61.26      3.74       62.21      7   
