############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Thu Jan 18 00:52:12 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705535532.866704
TestID              : 0
StartTime           : Thu Jan 18 00:52:12 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705535892.881887
Starting IO phase at 1705535892.881900
Ending IO phase at 1705535975.456747
write     31.01      31.01      0.256069    327680     1024.00    0.079104   82.55      5.04       82.56      0   
Ending compute phase at 1705536337.723848
Starting IO phase at 1705536337.723887
Ending IO phase at 1705536389.577638
write     47.62      49.70      0.140605    327680     1024.00    2.38       51.51      6.51       53.76      1   
Ending compute phase at 1705536750.901699
Starting IO phase at 1705536750.901715
Ending IO phase at 1705536778.795578
write     89.33      92.90      0.086112    327680     1024.00    4.11       27.56      4.15       28.66      2   
Ending compute phase at 1705537139.144811
Starting IO phase at 1705537139.144825
Ending IO phase at 1705537168.218102
write     88.82      89.32      0.087979    327680     1024.00    0.942358   28.66      4.63       28.82      3   
Ending compute phase at 1705537528.408860
Starting IO phase at 1705537528.408874
Ending IO phase at 1705537557.148045
write     89.67      89.69      0.085029    327680     1024.00    0.206136   28.54      1.33       28.55      4   
Ending compute phase at 1705537917.409022
Starting IO phase at 1705537917.409035
Ending IO phase at 1705537961.687068
write     58.14      58.14      0.131858    327680     1024.00    0.049843   44.03      5.30       44.04      5   
Ending compute phase at 1705538321.904370
Starting IO phase at 1705538321.904383
Ending IO phase at 1705538377.386347
write     46.53      46.53      0.170241    327680     1024.00    0.258468   55.02      2.21       55.02      6   
Ending compute phase at 1705538738.649491
Starting IO phase at 1705538738.649509
Ending IO phase at 1705538795.241988
write     44.47      45.29      0.152269    327680     1024.00    2.12       56.52      7.80       57.56      7   
Ending compute phase at 1705539158.472823
Starting IO phase at 1705539158.472848
Ending IO phase at 1705539221.797824
write     38.78      40.64      0.191480    327680     1024.00    3.26       62.99      7.92       66.02      8   
