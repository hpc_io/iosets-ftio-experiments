############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Dec 20 03:43:31 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703040211.820880
TestID              : 0
StartTime           : Wed Dec 20 03:43:31 2023
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703040641.975629
Starting IO phase at 1703040641.975646
Ending IO phase at 1703040793.997403
write     11.53      18.28      0.372597    327680     1024.00    77.29      140.04     44.08      221.98     0   
Ending compute phase at 1703041199.047503
Starting IO phase at 1703041199.047526
Ending IO phase at 1703041340.636797
write     15.71      21.66      0.364136    327680     1024.00    31.38      118.20     26.10      162.91     1   
Ending compute phase at 1703041767.828142
Starting IO phase at 1703041767.828160
Ending IO phase at 1703041818.045242
write     51.38      51.39      0.121446    327680     1024.00    0.075213   49.81      15.31      49.82      2   
Ending compute phase at 1703042193.995420
Starting IO phase at 1703042193.995443
Ending IO phase at 1703042302.398569
write     20.67      23.69      0.337688    327680     1024.00    17.22      108.06     35.82      123.85     3   
Ending compute phase at 1703042715.059024
Starting IO phase at 1703042715.059040
Ending IO phase at 1703042838.138818
write     14.62      22.23      0.353836    327680     1024.00    52.45      115.15     14.49      175.12     4   
Ending compute phase at 1703043273.255720
Starting IO phase at 1703043273.255744
Ending IO phase at 1703043415.278610
write     13.50      18.29      0.437112    327680     1024.00    49.48      139.98     20.66      189.57     5   
Ending compute phase at 1703043798.364128
Starting IO phase at 1703043798.364160
Ending IO phase at 1703043894.005823
write     22.04      27.14      0.294747    327680     1024.00    21.72      94.32      5.08       116.13     6   
