############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Dec 20 03:43:31 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703040211.893152
TestID              : 0
StartTime           : Wed Dec 20 03:43:32 2023
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703040741.079278
Starting IO phase at 1703040741.079310
Ending IO phase at 1703040934.228075
write     7.09       12.43      0.529745    327680     1024.00    176.59     206.02     40.82      361.21     0   
Ending compute phase at 1703041348.739753
Starting IO phase at 1703041348.739780
Ending IO phase at 1703041490.520729
write     13.54      23.88      0.260632    327680     1024.00    91.71      107.20     62.12      189.12     1   
Ending compute phase at 1703041856.008679
Starting IO phase at 1703041856.008704
Ending IO phase at 1703041936.334023
write     31.56      32.02      0.246839    327680     1024.00    1.91       79.96      19.28      81.12      2   
Ending compute phase at 1703042298.606186
Starting IO phase at 1703042298.606208
Ending IO phase at 1703042374.676564
write     32.88      33.78      0.235033    327680     1024.00    2.11       75.78      4.65       77.85      3   
Ending compute phase at 1703042806.046687
Starting IO phase at 1703042806.046709
Ending IO phase at 1703042872.787889
write     18.59      39.99      0.133848    327680     1024.00    78.18      64.02      23.61      137.68     4   
Ending compute phase at 1703043275.735877
Starting IO phase at 1703043275.735898
Ending IO phase at 1703043367.792795
write     19.03      29.74      0.253609    327680     1024.00    52.64      86.07      12.16      134.56     5   
Ending compute phase at 1703043753.943552
Starting IO phase at 1703043753.943575
Ending IO phase at 1703043858.495024
write     19.64      28.69      0.257291    327680     1024.00    30.95      89.22      22.07      130.36     6   
