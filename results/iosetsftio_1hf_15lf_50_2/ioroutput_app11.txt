############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Dec 20 03:43:31 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_11_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1703040211.700951
TestID              : 0
StartTime           : Wed Dec 20 03:43:31 2023
Path                : /mnt/beegfs/iosets_11_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_11_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1703040635.243261
Starting IO phase at 1703040635.243288
Ending IO phase at 1703040731.546100
write     16.04      32.29      0.223437    327680     1024.00    72.34      79.29      32.46      159.60     0   
Ending compute phase at 1703041103.527039
Starting IO phase at 1703041103.527061
Ending IO phase at 1703041135.481472
write     81.11      81.12      0.086741    327680     1024.00    0.041530   31.56      3.80       31.56      1   
Ending compute phase at 1703041529.844439
Starting IO phase at 1703041529.844461
Ending IO phase at 1703041660.039171
write     16.26      19.72      0.359013    327680     1024.00    33.04      129.81     14.93      157.40     2   
Ending compute phase at 1703042023.882723
Starting IO phase at 1703042023.882746
Ending IO phase at 1703042093.263065
write     35.14      37.00      0.214069    327680     1024.00    3.73       69.18      2.60       72.86      3   
Ending compute phase at 1703042466.283225
Starting IO phase at 1703042466.283245
Ending IO phase at 1703042545.346077
write     27.90      44.84      0.159832    327680     1024.00    13.56      57.10      27.75      91.75      4   
Ending compute phase at 1703042916.475787
Starting IO phase at 1703042916.475810
Ending IO phase at 1703043019.095150
write     22.62      25.84      0.306906    327680     1024.00    13.93      99.06      10.77      113.20     5   
Ending compute phase at 1703043454.999490
Starting IO phase at 1703043454.999511
Ending IO phase at 1703043948.062236
write     4.50       5.21       1.53        327680     1024.00    78.64      491.49     7.16       568.60     6   
