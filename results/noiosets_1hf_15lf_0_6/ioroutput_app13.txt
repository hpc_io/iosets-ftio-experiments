############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:09 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133949.724410
TestID              : 0
StartTime           : Sat Jan 13 09:19:09 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134310.548120
Starting IO phase at 1705134310.548139
Ending IO phase at 1705134704.089363
write     6.50       6.53       1.21        327680     1024.00    0.669360   392.21     6.42       393.85     0   
Ending compute phase at 1705135075.219778
Starting IO phase at 1705135075.219802
Ending IO phase at 1705135440.477901
write     6.98       7.10       1.09        327680     1024.00    1.84       360.51     17.48      366.85     1   
Ending compute phase at 1705135802.185301
Starting IO phase at 1705135802.185325
Ending IO phase at 1705136167.227470
write     6.99       7.02       1.12        327680     1024.00    1.58       364.79     5.28       366.33     2   
Ending compute phase at 1705136534.659015
Starting IO phase at 1705136534.659032
Ending IO phase at 1705136636.502006
write     23.50      25.18      0.312249    327680     1024.00    7.26       101.66     1.76       108.93     3   
Ending compute phase at 1705136996.723472
Starting IO phase at 1705136996.723486
Ending IO phase at 1705137037.336508
write     63.52      63.78      0.078070    327680     1024.00    0.055654   40.14      15.74      40.30      4   
Ending compute phase at 1705137397.515952
Starting IO phase at 1705137397.515967
Ending IO phase at 1705137439.528860
write     61.30      61.53      0.125719    327680     1024.00    0.055233   41.60      25.62      41.76      5   
