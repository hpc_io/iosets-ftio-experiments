############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133948.513044
TestID              : 0
StartTime           : Sat Jan 13 09:19:08 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134308.562087
Starting IO phase at 1705134308.562100
Ending IO phase at 1705134672.302677
write     7.04       7.04       0.987732    327680     1024.00    0.290556   363.58     47.59      363.67     0   
Ending compute phase at 1705135032.536264
Starting IO phase at 1705135032.536278
Ending IO phase at 1705135336.605880
write     8.42       8.43       0.949058    327680     1024.00    0.077057   303.70     2.32       304.05     1   
Ending compute phase at 1705135697.027451
Starting IO phase at 1705135697.027464
Ending IO phase at 1705135737.673312
write     63.33      63.36      0.105205    327680     1024.00    0.047377   40.40      8.19       40.42      2   
Ending compute phase at 1705136100.146563
Starting IO phase at 1705136100.146587
Ending IO phase at 1705136277.010147
write     14.32      14.83      0.526029    327680     1024.00    2.31       172.65     27.03      178.78     3   
Ending compute phase at 1705136637.634930
Starting IO phase at 1705136637.634954
Ending IO phase at 1705136724.310896
write     29.53      30.27      0.242261    327680     1024.00    0.473340   84.56      14.38      86.70      4   
Ending compute phase at 1705137085.007952
Starting IO phase at 1705137085.007974
Ending IO phase at 1705137172.275884
write     29.23      29.41      0.267578    327680     1024.00    0.561661   87.04      1.47       87.58      5   
Ending compute phase at 1705137532.502356
Starting IO phase at 1705137532.502372
Ending IO phase at 1705137589.032544
write     45.52      45.56      0.175055    327680     1024.00    0.098690   56.19      6.29       56.23      6   
