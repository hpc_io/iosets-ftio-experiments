############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133948.724169
TestID              : 0
StartTime           : Sat Jan 13 09:19:08 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134309.127755
Starting IO phase at 1705134309.127779
Ending IO phase at 1705134675.009741
write     6.99       7.00       1.12        327680     1024.00    0.319161   365.61     17.83      366.03     0   
Ending compute phase at 1705135035.687645
Starting IO phase at 1705135035.687682
Ending IO phase at 1705135373.640118
write     7.56       7.58       1.05        327680     1024.00    0.604147   337.86     1.63       338.45     1   
Ending compute phase at 1705135734.556849
Starting IO phase at 1705135734.556864
Ending IO phase at 1705135979.091953
write     10.44      10.48      0.756430    327680     1024.00    1.70       244.24     7.55       245.27     2   
Ending compute phase at 1705136339.589087
Starting IO phase at 1705136339.589104
Ending IO phase at 1705136412.809076
write     35.22      35.22      0.217097    327680     1024.00    0.084303   72.68      4.23       72.68      3   
Ending compute phase at 1705136773.119534
Starting IO phase at 1705136773.119547
Ending IO phase at 1705136795.561768
write     116.23     116.35     0.067560    327680     1024.00    0.056777   22.00      7.07       22.03      4   
Ending compute phase at 1705137156.084808
Starting IO phase at 1705137156.084829
Ending IO phase at 1705137209.896640
write     47.58      47.87      0.163357    327680     1024.00    0.374462   53.48      1.77       53.80      5   
Ending compute phase at 1705137570.409624
Starting IO phase at 1705137570.409651
Ending IO phase at 1705137646.651065
write     33.56      33.70      0.231230    327680     1024.00    0.342889   75.96      12.18      76.29      6   
