############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133948.485342
TestID              : 0
StartTime           : Sat Jan 13 09:19:08 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134308.497104
Starting IO phase at 1705134308.497117
Ending IO phase at 1705134635.779567
write     7.82       7.83       0.881053    327680     1024.00    0.089746   326.80     45.33      327.27     0   
Ending compute phase at 1705134995.799083
Starting IO phase at 1705134995.799098
Ending IO phase at 1705135030.940598
write     73.58      73.59      0.096843    327680     1024.00    0.058778   34.78      4.86       34.79      1   
Ending compute phase at 1705135393.024130
Starting IO phase at 1705135393.024152
Ending IO phase at 1705135496.018761
write     24.47      24.91      0.321054    327680     1024.00    1.88       102.75     12.50      104.62     2   
Ending compute phase at 1705135858.965593
Starting IO phase at 1705135858.965618
Ending IO phase at 1705136201.718099
write     7.41       7.48       1.06        327680     1024.00    2.76       342.40     8.26       345.26     3   
Ending compute phase at 1705136564.331165
Starting IO phase at 1705136564.331207
Ending IO phase at 1705136682.879355
write     21.24      21.67      0.369222    327680     1024.00    2.34       118.15     0.216086   120.53     4   
Ending compute phase at 1705137043.487883
Starting IO phase at 1705137043.487905
Ending IO phase at 1705137130.925861
write     29.21      29.38      0.272265    327680     1024.00    0.471676   87.12      3.68       87.64      5   
Ending compute phase at 1705137491.104052
Starting IO phase at 1705137491.104066
Ending IO phase at 1705137534.629431
write     59.12      59.13      0.134798    327680     1024.00    0.104375   43.29      3.69       43.30      6   
