############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_6_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133948.718235
TestID              : 0
StartTime           : Sat Jan 13 09:19:08 2024
Path                : /mnt/beegfs/iosets_6_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_6_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134308.969340
Starting IO phase at 1705134308.969363
Ending IO phase at 1705134674.333685
write     7.01       7.01       1.11        327680     1024.00    0.269310   365.02     19.03      365.42     0   
Ending compute phase at 1705135034.815537
Starting IO phase at 1705135034.815555
Ending IO phase at 1705135371.997695
write     7.59       7.64       1.04        327680     1024.00    0.364867   334.95     5.26       337.41     1   
Ending compute phase at 1705135732.356971
Starting IO phase at 1705135732.356986
Ending IO phase at 1705135965.565996
write     10.97      10.99      0.696474    327680     1024.00    0.194404   232.88     10.32      233.29     2   
Ending compute phase at 1705136325.586581
Starting IO phase at 1705136325.586594
Ending IO phase at 1705136369.418650
write     58.90      58.92      0.121103    327680     1024.00    0.067901   43.45      4.70       43.46      3   
Ending compute phase at 1705136729.615669
Starting IO phase at 1705136729.615682
Ending IO phase at 1705136752.577464
write     114.00     114.03     0.068439    327680     1024.00    0.058777   22.45      2.60       22.46      4   
Ending compute phase at 1705137113.552700
Starting IO phase at 1705137113.552725
Ending IO phase at 1705137188.514573
write     33.90      34.23      0.233078    327680     1024.00    0.783126   74.78      2.15       75.52      5   
Ending compute phase at 1705137549.150159
Starting IO phase at 1705137549.150179
Ending IO phase at 1705137620.780803
write     35.65      35.88      0.222273    327680     1024.00    0.476329   71.36      0.438413   71.81      6   
