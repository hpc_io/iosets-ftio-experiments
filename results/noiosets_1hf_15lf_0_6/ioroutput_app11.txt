############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:09 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_11_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133949.668422
TestID              : 0
StartTime           : Sat Jan 13 09:19:09 2024
Path                : /mnt/beegfs/iosets_11_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_11_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134310.544057
Starting IO phase at 1705134310.544072
Ending IO phase at 1705134704.089494
write     6.50       6.54       1.20        327680     1024.00    0.638656   391.27     10.61      393.86     0   
Ending compute phase at 1705135075.220242
Starting IO phase at 1705135075.220265
Ending IO phase at 1705135442.385880
write     6.94       7.01       1.14        327680     1024.00    1.89       365.36     17.51      368.70     1   
Ending compute phase at 1705135805.501321
Starting IO phase at 1705135805.501341
Ending IO phase at 1705136167.201818
write     7.02       7.08       1.13        327680     1024.00    2.97       361.47     0.579751   364.43     2   
Ending compute phase at 1705136534.653530
Starting IO phase at 1705136534.653548
Ending IO phase at 1705136636.786303
write     23.45      25.13      0.312266    327680     1024.00    7.28       101.88     2.14       109.17     3   
Ending compute phase at 1705136996.982473
Starting IO phase at 1705136996.982487
Ending IO phase at 1705137037.544626
write     63.55      63.58      0.122967    327680     1024.00    0.066012   40.26      15.97      40.28      4   
Ending compute phase at 1705137397.728256
Starting IO phase at 1705137397.728269
Ending IO phase at 1705137439.780427
write     61.34      61.35      0.079102    327680     1024.00    0.069555   41.73      19.11      41.74      5   
