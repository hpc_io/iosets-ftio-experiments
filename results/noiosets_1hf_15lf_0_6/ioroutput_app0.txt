############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133948.793458
TestID              : 0
StartTime           : Sat Jan 13 09:19:08 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705133966.875052
Starting IO phase at 1705133966.875073
Ending IO phase at 1705133968.577910
write     75.71      77.47      0.098710    16384      1024.00    0.063985   1.65       0.075016   1.69       0   
Ending compute phase at 1705133986.867209
Starting IO phase at 1705133986.867222
Ending IO phase at 1705133988.597702
write     77.75      78.35      0.101381    16384      1024.00    0.050719   1.63       0.019013   1.65       1   
Ending compute phase at 1705134006.834822
Starting IO phase at 1705134006.834834
Ending IO phase at 1705134008.538760
write     78.89      79.51      0.087402    16384      1024.00    0.068989   1.61       0.219071   1.62       2   
Ending compute phase at 1705134026.799314
Starting IO phase at 1705134026.799325
Ending IO phase at 1705134028.341690
write     87.47      87.81      0.084679    16384      1024.00    0.053944   1.46       0.103561   1.46       3   
Ending compute phase at 1705134046.614694
Starting IO phase at 1705134046.614704
Ending IO phase at 1705134048.305654
write     79.94      80.38      0.095209    16384      1024.00    0.047515   1.59       0.069348   1.60       4   
Ending compute phase at 1705134066.515103
Starting IO phase at 1705134066.515114
Ending IO phase at 1705134068.273776
write     78.53      79.10      0.100412    16384      1024.00    0.043505   1.62       0.018630   1.63       5   
Ending compute phase at 1705134086.514580
Starting IO phase at 1705134086.514593
Ending IO phase at 1705134088.156004
write     80.91      83.67      0.095512    16384      1024.00    0.055842   1.53       0.024987   1.58       6   
Ending compute phase at 1705134106.423186
Starting IO phase at 1705134106.423196
Ending IO phase at 1705134108.093779
write     81.29      81.58      0.096633    16384      1024.00    0.049901   1.57       0.023698   1.57       7   
Ending compute phase at 1705134126.291636
Starting IO phase at 1705134126.291647
Ending IO phase at 1705134127.973598
write     80.91      81.22      0.095647    16384      1024.00    0.041871   1.58       0.063660   1.58       8   
Ending compute phase at 1705134146.191655
Starting IO phase at 1705134146.191667
Ending IO phase at 1705134147.645824
write     88.30      89.08      0.089077    16384      1024.00    0.062910   1.44       0.019432   1.45       9   
Ending compute phase at 1705134165.930738
Starting IO phase at 1705134165.930750
Ending IO phase at 1705134167.633695
write     77.96      79.03      0.101165    16384      1024.00    0.061660   1.62       0.189804   1.64       10  
Ending compute phase at 1705134185.890988
Starting IO phase at 1705134185.890998
Ending IO phase at 1705134187.465675
write     86.60      87.37      0.090841    16384      1024.00    0.042368   1.47       0.019881   1.48       11  
Ending compute phase at 1705134205.687589
Starting IO phase at 1705134205.687600
Ending IO phase at 1705134207.341462
write     83.19      83.51      0.092548    16384      1024.00    0.049441   1.53       0.052560   1.54       12  
Ending compute phase at 1705134225.518765
Starting IO phase at 1705134225.518776
Ending IO phase at 1705134227.169750
write     81.32      82.40      0.096360    16384      1024.00    0.065689   1.55       0.027807   1.57       13  
Ending compute phase at 1705134245.432062
Starting IO phase at 1705134245.432074
Ending IO phase at 1705134247.053735
write     82.78      83.53      0.095765    16384      1024.00    0.056330   1.53       0.019443   1.55       14  
Ending compute phase at 1705134265.279160
Starting IO phase at 1705134265.279170
Ending IO phase at 1705134266.919171
write     83.88      84.18      0.093594    16384      1024.00    0.051002   1.52       0.023731   1.53       15  
Ending compute phase at 1705134285.235284
Starting IO phase at 1705134285.235296
Ending IO phase at 1705134286.940943
write     78.72      80.56      0.098193    16384      1024.00    0.056609   1.59       0.017531   1.63       16  
Ending compute phase at 1705134305.147797
Starting IO phase at 1705134305.147809
Ending IO phase at 1705134306.801715
write     82.53      82.84      0.093003    16384      1024.00    0.049102   1.55       0.057803   1.55       17  
Ending compute phase at 1705134329.938084
Starting IO phase at 1705134329.938122
Ending IO phase at 1705134353.759792
write     4.46       5.47       1.45        16384      1024.00    4.86       23.38      0.671080   28.68      18  
Ending compute phase at 1705134376.218043
Starting IO phase at 1705134376.218089
Ending IO phase at 1705134399.543491
write     4.69       5.59       1.43        16384      1024.00    3.94       22.89      0.888784   27.27      19  
Ending compute phase at 1705134421.746035
Starting IO phase at 1705134421.746072
Ending IO phase at 1705134445.314815
write     4.69       5.52       1.45        16384      1024.00    3.75       23.21      1.29       27.32      20  
Ending compute phase at 1705134467.654284
Starting IO phase at 1705134467.654316
Ending IO phase at 1705134491.635146
write     4.61       5.44       1.46        16384      1024.00    3.81       23.54      1.42       27.78      21  
Ending compute phase at 1705134513.986326
Starting IO phase at 1705134513.986388
Ending IO phase at 1705134537.915658
write     4.61       5.45       1.47        16384      1024.00    3.89       23.48      0.657981   27.79      22  
Ending compute phase at 1705134560.382537
Starting IO phase at 1705134560.382568
Ending IO phase at 1705134584.151856
write     4.61       5.50       1.45        16384      1024.00    3.97       23.26      1.50       27.74      23  
Ending compute phase at 1705134606.658260
Starting IO phase at 1705134606.658296
Ending IO phase at 1705134631.223947
write     4.48       5.30       1.51        16384      1024.00    3.98       24.13      1.15       28.54      24  
Ending compute phase at 1705134652.946701
Starting IO phase at 1705134652.946728
Ending IO phase at 1705134672.285655
write     5.68       6.67       1.17        16384      1024.00    3.25       19.19      0.585407   22.55      25  
Ending compute phase at 1705134713.301168
Starting IO phase at 1705134713.301185
Ending IO phase at 1705134718.921730
write     4.51       23.15      0.344721    16384      1024.00    22.82      5.53       0.729490   28.35      26  
Ending compute phase at 1705134737.235520
Starting IO phase at 1705134737.235541
Ending IO phase at 1705134738.917775
write     75.19      77.19      0.102248    16384      1024.00    0.067409   1.66       0.016293   1.70       27  
Ending compute phase at 1705134757.139910
Starting IO phase at 1705134757.139922
Ending IO phase at 1705134758.813717
write     81.34      81.99      0.096842    16384      1024.00    0.053319   1.56       0.018935   1.57       28  
Ending compute phase at 1705134777.007070
Starting IO phase at 1705134777.007081
Ending IO phase at 1705134778.709696
write     81.00      81.29      0.096978    16384      1024.00    0.048650   1.57       0.023620   1.58       29  
Ending compute phase at 1705134796.939623
Starting IO phase at 1705134796.939634
Ending IO phase at 1705134798.628377
write     76.28      76.82      0.102337    16384      1024.00    0.034560   1.67       0.035309   1.68       30  
Ending compute phase at 1705134816.851000
Starting IO phase at 1705134816.851011
Ending IO phase at 1705134818.509897
write     77.19      77.73      0.102213    16384      1024.00    0.065442   1.65       0.017911   1.66       31  
Ending compute phase at 1705134836.807322
Starting IO phase at 1705134836.807347
Ending IO phase at 1705134838.553715
write     75.21      76.59      0.103565    16384      1024.00    0.056840   1.67       0.019416   1.70       32  
Ending compute phase at 1705134856.707855
Starting IO phase at 1705134856.707866
Ending IO phase at 1705134858.361762
write     82.16      82.79      0.095900    16384      1024.00    0.045597   1.55       0.018067   1.56       33  
Ending compute phase at 1705134876.591430
Starting IO phase at 1705134876.591440
Ending IO phase at 1705134878.393717
write     76.45      76.70      0.103590    16384      1024.00    0.060646   1.67       0.051871   1.67       34  
Ending compute phase at 1705134896.574890
Starting IO phase at 1705134896.574900
Ending IO phase at 1705134898.245745
write     78.32      78.84      0.101472    16384      1024.00    0.054297   1.62       0.017452   1.63       35  
Ending compute phase at 1705134916.474685
Starting IO phase at 1705134916.474697
Ending IO phase at 1705134918.177702
write     79.50      79.98      0.097783    16384      1024.00    0.057741   1.60       0.016721   1.61       36  
Ending compute phase at 1705134936.406879
Starting IO phase at 1705134936.406890
Ending IO phase at 1705134938.109826
write     75.56      76.24      0.104932    16384      1024.00    0.061890   1.68       0.021913   1.69       37  
Ending compute phase at 1705134956.396961
Starting IO phase at 1705134956.396972
Ending IO phase at 1705134958.131639
write     79.10      79.80      0.100245    16384      1024.00    0.072928   1.60       0.019014   1.62       38  
Ending compute phase at 1705134976.355327
Starting IO phase at 1705134976.355337
Ending IO phase at 1705134978.005771
write     79.29      79.97      0.099318    16384      1024.00    0.067305   1.60       0.019907   1.61       39  
Ending compute phase at 1705134996.232324
Starting IO phase at 1705134996.232338
Ending IO phase at 1705134998.441742
write     59.82      60.24      0.116700    16384      1024.00    0.102491   2.12       0.264795   2.14       40  
Ending compute phase at 1705135016.635726
Starting IO phase at 1705135016.635739
Ending IO phase at 1705135025.793789
write     14.07      14.08      0.212713    16384      1024.00    0.080075   9.09       5.69       9.10       41  
Ending compute phase at 1705135045.183536
Starting IO phase at 1705135045.183567
Ending IO phase at 1705135056.857858
write     9.95       11.10      0.721007    16384      1024.00    1.26       11.54      0.376846   12.86      42  
Ending compute phase at 1705135077.119571
Starting IO phase at 1705135077.119597
Ending IO phase at 1705135098.605280
write     5.44       6.02       1.32        16384      1024.00    2.28       21.26      0.580790   23.53      43  
Ending compute phase at 1705135120.859549
Starting IO phase at 1705135120.859576
Ending IO phase at 1705135145.028868
write     4.56       5.33       1.48        16384      1024.00    3.93       24.00      0.690377   28.05      44  
Ending compute phase at 1705135167.055557
Starting IO phase at 1705135167.055578
Ending IO phase at 1705135190.236796
write     4.78       5.60       1.41        16384      1024.00    3.60       22.87      1.61       26.78      45  
Ending compute phase at 1705135212.112259
Starting IO phase at 1705135212.112283
Ending IO phase at 1705135234.552900
write     4.93       5.81       1.36        16384      1024.00    3.52       22.04      1.21       25.96      46  
Ending compute phase at 1705135256.319881
Starting IO phase at 1705135256.319908
Ending IO phase at 1705135278.613585
write     5.00       5.80       1.38        16384      1024.00    3.33       22.07      0.802800   25.62      47  
Ending compute phase at 1705135300.696728
Starting IO phase at 1705135300.696749
Ending IO phase at 1705135324.101734
write     4.73       5.54       1.44        16384      1024.00    3.64       23.10      1.32       27.04      48  
Ending compute phase at 1705135345.200157
Starting IO phase at 1705135345.200182
Ending IO phase at 1705135371.977082
write     4.35       4.83       1.62        16384      1024.00    2.65       26.49      0.909161   29.41      49  
Ending compute phase at 1705135392.820153
Starting IO phase at 1705135392.820177
Ending IO phase at 1705135409.237646
write     6.75       7.82       1.00        16384      1024.00    2.65       16.36      0.603852   18.97      50  
Ending compute phase at 1705135431.770868
Starting IO phase at 1705135431.770894
Ending IO phase at 1705135452.417233
write     5.13       6.22       1.29        16384      1024.00    4.37       20.57      0.277113   24.94      51  
Ending compute phase at 1705135470.857209
Starting IO phase at 1705135470.857230
Ending IO phase at 1705135477.393821
write     18.98      19.62      0.407750    16384      1024.00    0.245973   6.52       0.274510   6.75       52  
Ending compute phase at 1705135495.639504
Starting IO phase at 1705135495.639520
Ending IO phase at 1705135504.906303
write     13.89      13.91      0.498105    16384      1024.00    0.132986   9.20       4.23       9.22       53  
Ending compute phase at 1705135523.155823
Starting IO phase at 1705135523.155836
Ending IO phase at 1705135525.113775
write     68.45      69.14      0.115702    16384      1024.00    0.080133   1.85       0.024782   1.87       54  
Ending compute phase at 1705135543.353600
Starting IO phase at 1705135543.353621
Ending IO phase at 1705135545.307051
write     66.59      68.17      0.117325    16384      1024.00    0.036585   1.88       0.025075   1.92       55  
Ending compute phase at 1705135563.512482
Starting IO phase at 1705135563.512503
Ending IO phase at 1705135565.490724
write     66.73      67.70      0.118164    16384      1024.00    0.048682   1.89       0.032846   1.92       56  
Ending compute phase at 1705135583.791118
Starting IO phase at 1705135583.791129
Ending IO phase at 1705135585.681763
write     70.56      71.28      0.112224    16384      1024.00    0.055726   1.80       0.025197   1.81       57  
Ending compute phase at 1705135603.904451
Starting IO phase at 1705135603.904464
Ending IO phase at 1705135605.829725
write     71.02      71.66      0.110907    16384      1024.00    0.065855   1.79       0.021368   1.80       58  
Ending compute phase at 1705135624.044299
Starting IO phase at 1705135624.044310
Ending IO phase at 1705135625.897746
write     70.87      71.80      0.109723    16384      1024.00    0.052136   1.78       0.019983   1.81       59  
Ending compute phase at 1705135644.172261
Starting IO phase at 1705135644.172272
Ending IO phase at 1705135646.069812
write     67.87      68.50      0.116787    16384      1024.00    0.057910   1.87       0.023137   1.89       60  
Ending compute phase at 1705135664.278465
Starting IO phase at 1705135664.278476
Ending IO phase at 1705135666.165746
write     71.03      71.44      0.111261    16384      1024.00    0.063877   1.79       0.017690   1.80       61  
Ending compute phase at 1705135684.416874
Starting IO phase at 1705135684.416885
Ending IO phase at 1705135686.409764
write     67.86      68.41      0.116948    16384      1024.00    0.042021   1.87       0.019778   1.89       62  
Ending compute phase at 1705135704.635211
Starting IO phase at 1705135704.635225
Ending IO phase at 1705135716.929644
write     10.47      10.48      0.338784    16384      1024.00    0.162405   12.21      6.80       12.23      63  
Ending compute phase at 1705135735.648821
Starting IO phase at 1705135735.648842
Ending IO phase at 1705135742.141569
write     18.45      20.01      0.399343    16384      1024.00    0.542641   6.40       0.377693   6.94       64  
Ending compute phase at 1705135761.052963
Starting IO phase at 1705135761.052984
Ending IO phase at 1705135773.869593
write     9.53       10.07      0.794668    16384      1024.00    0.702948   12.71      0.783240   13.43      65  
Ending compute phase at 1705135793.069024
Starting IO phase at 1705135793.069048
Ending IO phase at 1705135805.169617
write     9.85       10.78      0.741885    16384      1024.00    0.970701   11.87      1.16       12.99      66  
Ending compute phase at 1705135826.453675
Starting IO phase at 1705135826.453704
Ending IO phase at 1705135847.913951
write     5.21       6.07       1.32        16384      1024.00    3.12       21.09      0.653698   24.58      67  
Ending compute phase at 1705135870.041848
Starting IO phase at 1705135870.041882
Ending IO phase at 1705135895.358975
write     4.42       5.14       1.51        16384      1024.00    3.62       24.91      1.43       28.94      68  
Ending compute phase at 1705135917.849657
Starting IO phase at 1705135917.849689
Ending IO phase at 1705135943.565491
write     4.31       5.07       1.58        16384      1024.00    4.01       25.23      0.744128   29.72      69  
Ending compute phase at 1705135965.793738
Starting IO phase at 1705135965.793759
Ending IO phase at 1705135988.363399
write     4.87       5.75       1.37        16384      1024.00    3.74       22.27      0.652486   26.27      70  
Ending compute phase at 1705136009.854029
Starting IO phase at 1705136009.854062
Ending IO phase at 1705136032.772951
write     4.87       5.64       1.42        16384      1024.00    3.34       22.70      1.12       26.26      71  
Ending compute phase at 1705136054.713699
Starting IO phase at 1705136054.713726
Ending IO phase at 1705136076.817872
write     5.03       5.83       1.31        16384      1024.00    3.41       21.96      1.07       25.47      72  
Ending compute phase at 1705136097.453667
Starting IO phase at 1705136097.453693
Ending IO phase at 1705136119.581686
write     5.29       5.82       1.37        16384      1024.00    2.12       21.99      0.424324   24.18      73  
Ending compute phase at 1705136140.450112
Starting IO phase at 1705136140.450138
Ending IO phase at 1705136163.433666
write     5.00       5.62       1.35        16384      1024.00    2.78       22.78      1.30       25.60      74  
Ending compute phase at 1705136182.644696
Starting IO phase at 1705136182.644722
Ending IO phase at 1705136187.649809
write     21.51      26.76      0.209915    16384      1024.00    0.992145   4.78       1.60       5.95       75  
Ending compute phase at 1705136206.738312
Starting IO phase at 1705136206.738362
Ending IO phase at 1705136216.153637
write     12.48      13.56      0.532398    16384      1024.00    0.879763   9.44       0.861399   10.26      76  
Ending compute phase at 1705136234.562553
Starting IO phase at 1705136234.562573
Ending IO phase at 1705136242.009825
write     16.67      17.20      0.464752    16384      1024.00    0.232227   7.44       0.381468   7.68       77  
Ending compute phase at 1705136268.478385
Starting IO phase at 1705136268.478432
Ending IO phase at 1705136283.685789
write     5.48       8.46       0.806464    16384      1024.00    8.27       15.12      10.83      23.37      78  
Ending compute phase at 1705136301.896929
Starting IO phase at 1705136301.896941
Ending IO phase at 1705136304.069765
write     60.43      61.24      0.129433    16384      1024.00    0.053461   2.09       0.020077   2.12       79  
Ending compute phase at 1705136322.291975
Starting IO phase at 1705136322.291985
Ending IO phase at 1705136324.533736
write     60.45      60.63      0.130162    16384      1024.00    0.042579   2.11       0.029636   2.12       80  
Ending compute phase at 1705136342.906764
Starting IO phase at 1705136342.906780
Ending IO phase at 1705136358.309635
write     8.22       8.33       0.550832    16384      1024.00    0.236518   15.36      8.17       15.57      81  
Ending compute phase at 1705136376.478890
Starting IO phase at 1705136376.478905
Ending IO phase at 1705136390.113777
write     9.43       9.43       0.428308    16384      1024.00    0.128198   13.57      8.22       13.58      82  
Ending compute phase at 1705136408.367006
Starting IO phase at 1705136408.367017
Ending IO phase at 1705136412.581804
write     31.01      31.05      0.256952    16384      1024.00    0.694983   4.12       0.223725   4.13       83  
Ending compute phase at 1705136430.846219
Starting IO phase at 1705136430.846234
Ending IO phase at 1705136444.817645
write     9.20       9.20       0.363230    16384      1024.00    0.168526   13.91      8.10       13.92      84  
Ending compute phase at 1705136463.211042
Starting IO phase at 1705136463.211057
Ending IO phase at 1705136471.323829
write     15.59      15.91      0.485462    16384      1024.00    0.212866   8.05       0.549933   8.21       85  
Ending compute phase at 1705136489.859145
Starting IO phase at 1705136489.859163
Ending IO phase at 1705136506.745916
write     7.48       7.62       0.833400    16384      1024.00    0.328324   16.80      7.48       17.11      86  
Ending compute phase at 1705136525.079661
Starting IO phase at 1705136525.079675
Ending IO phase at 1705136536.149693
write     11.47      11.64      0.307414    16384      1024.00    0.266254   11.00      6.94       11.16      87  
Ending compute phase at 1705136555.178759
Starting IO phase at 1705136555.178784
Ending IO phase at 1705136564.753749
write     12.48      13.56      0.587298    16384      1024.00    0.769123   9.44       1.68       10.25      88  
Ending compute phase at 1705136584.024016
Starting IO phase at 1705136584.024059
Ending IO phase at 1705136593.841580
write     11.79      13.13      0.599744    16384      1024.00    1.09       9.75       0.680447   10.85      89  
Ending compute phase at 1705136613.163386
Starting IO phase at 1705136613.163419
Ending IO phase at 1705136621.937582
write     13.19      14.81      0.532930    16384      1024.00    0.989822   8.64       0.368344   9.70       90  
Ending compute phase at 1705136640.911311
Starting IO phase at 1705136640.911336
Ending IO phase at 1705136648.281578
write     16.15      17.89      0.447015    16384      1024.00    0.677333   7.15       0.123792   7.92       91  
Ending compute phase at 1705136667.156087
Starting IO phase at 1705136667.156111
Ending IO phase at 1705136676.000147
write     13.46      14.58      0.521633    16384      1024.00    0.752823   8.78       0.446032   9.51       92  
Ending compute phase at 1705136694.331395
Starting IO phase at 1705136694.331418
Ending IO phase at 1705136697.376351
write     40.57      42.65      0.183992    16384      1024.00    0.194471   3.00       0.172573   3.15       93  
Ending compute phase at 1705136716.396564
Starting IO phase at 1705136716.396583
Ending IO phase at 1705136726.501712
write     11.82      12.82      0.362404    16384      1024.00    1.13       9.98       4.70       10.83      94  
Ending compute phase at 1705136744.715462
Starting IO phase at 1705136744.715477
Ending IO phase at 1705136751.141773
write     20.32      20.36      0.230338    16384      1024.00    0.069470   6.29       4.14       6.30       95  
Ending compute phase at 1705136769.349745
Starting IO phase at 1705136769.349758
Ending IO phase at 1705136770.573718
write     109.41     112.46     0.070530    16384      1024.00    0.050648   1.14       0.017702   1.17       96  
Ending compute phase at 1705136788.860280
Starting IO phase at 1705136788.860294
Ending IO phase at 1705136794.797758
write     21.75      21.77      0.341671    16384      1024.00    0.055363   5.88       3.54       5.89       97  
Ending compute phase at 1705136812.948478
Starting IO phase at 1705136812.948490
Ending IO phase at 1705136814.065761
write     121.22     121.98     0.064148    16384      1024.00    0.058872   1.05       0.023755   1.06       98  
Ending compute phase at 1705136832.223100
Starting IO phase at 1705136832.223113
Ending IO phase at 1705136841.413736
write     14.02      14.03      0.287454    16384      1024.00    0.101610   9.13       4.53       9.13       99  
Ending compute phase at 1705136859.644298
Starting IO phase at 1705136859.644308
Ending IO phase at 1705136860.881737
write     111.34     112.62     0.070314    16384      1024.00    0.054446   1.14       0.019002   1.15       100 
Ending compute phase at 1705136879.126923
Starting IO phase at 1705136879.126938
Ending IO phase at 1705136886.109732
write     18.58      18.59      0.156691    16384      1024.00    0.079783   6.89       4.38       6.89       101 
Ending compute phase at 1705136904.302854
Starting IO phase at 1705136904.302869
Ending IO phase at 1705136913.061713
write     14.73      14.73      0.358951    16384      1024.00    0.138825   8.69       3.42       8.69       102 
Ending compute phase at 1705136931.259014
Starting IO phase at 1705136931.259025
Ending IO phase at 1705136932.413816
write     116.99     117.61     0.067306    16384      1024.00    0.063013   1.09       0.012372   1.09       103 
Ending compute phase at 1705136950.663640
Starting IO phase at 1705136950.663651
Ending IO phase at 1705136951.924381
write     113.77     114.38     0.029220    16384      1024.00    0.047805   1.12       0.652357   1.13       104 
Ending compute phase at 1705136970.140399
Starting IO phase at 1705136970.140413
Ending IO phase at 1705136971.453738
write     106.83     108.96     0.073420    16384      1024.00    0.069025   1.17       0.028803   1.20       105 
Ending compute phase at 1705136989.669185
Starting IO phase at 1705136989.669225
Ending IO phase at 1705136993.969719
write     30.07      30.12      0.263043    16384      1024.00    0.042575   4.25       0.160685   4.26       106 
Ending compute phase at 1705137012.344651
Starting IO phase at 1705137012.344670
Ending IO phase at 1705137021.345561
write     14.05      14.31      0.558978    16384      1024.00    0.169855   8.94       3.36       9.11       107 
Ending compute phase at 1705137039.924696
Starting IO phase at 1705137039.924732
Ending IO phase at 1705137045.169613
write     22.85      24.62      0.324942    16384      1024.00    0.418088   5.20       0.220224   5.60       108 
Ending compute phase at 1705137064.480100
Starting IO phase at 1705137064.480123
Ending IO phase at 1705137072.313515
write     14.47      16.55      0.470553    16384      1024.00    1.12       7.74       0.437419   8.85       109 
Ending compute phase at 1705137091.884872
Starting IO phase at 1705137091.884897
Ending IO phase at 1705137100.877611
write     12.40      14.38      0.542464    16384      1024.00    1.43       8.90       0.256031   10.32      110 
Ending compute phase at 1705137119.592043
Starting IO phase at 1705137119.592068
Ending IO phase at 1705137126.281619
write     17.87      19.49      0.397575    16384      1024.00    0.565838   6.57       0.584156   7.16       111 
Ending compute phase at 1705137144.840109
Starting IO phase at 1705137144.840131
Ending IO phase at 1705137148.669876
write     31.02      33.68      0.225751    16384      1024.00    0.310041   3.80       0.212192   4.13       112 
Ending compute phase at 1705137171.896436
Starting IO phase at 1705137171.896486
Ending IO phase at 1705137176.426828
write     13.53      28.85      0.277323    16384      1024.00    5.02       4.44       1.70       9.46       113 
Ending compute phase at 1705137194.661729
Starting IO phase at 1705137194.661745
Ending IO phase at 1705137199.865802
write     25.14      25.26      0.141663    16384      1024.00    0.060240   5.07       2.82       5.09       114 
Ending compute phase at 1705137218.099622
Starting IO phase at 1705137218.099636
Ending IO phase at 1705137225.168520
write     18.24      18.25      0.147064    16384      1024.00    0.064679   7.01       4.66       7.02       115 
Ending compute phase at 1705137243.375018
Starting IO phase at 1705137243.375030
Ending IO phase at 1705137244.609545
write     106.83     107.81     0.071951    16384      1024.00    0.059409   1.19       0.017411   1.20       116 
Ending compute phase at 1705137262.863354
Starting IO phase at 1705137262.863368
Ending IO phase at 1705137267.817728
write     26.31      26.34      0.153685    16384      1024.00    0.086608   4.86       2.40       4.87       117 
Ending compute phase at 1705137286.082925
Starting IO phase at 1705137286.082939
Ending IO phase at 1705137294.221834
write     15.80      15.81      0.314878    16384      1024.00    0.077749   8.10       3.06       8.10       118 
Ending compute phase at 1705137312.471414
Starting IO phase at 1705137312.471425
Ending IO phase at 1705137313.649781
write     116.95     117.58     0.066596    16384      1024.00    0.044161   1.09       0.023914   1.09       119 
Ending compute phase at 1705137331.847763
Starting IO phase at 1705137331.847774
Ending IO phase at 1705137333.117767
write     107.25     107.79     0.072777    16384      1024.00    0.041029   1.19       0.023591   1.19       120 
Ending compute phase at 1705137351.359573
Starting IO phase at 1705137351.359584
Ending IO phase at 1705137352.533892
write     116.98     117.64     0.012293    16384      1024.00    0.044029   1.09       0.891968   1.09       121 
Ending compute phase at 1705137370.788160
Starting IO phase at 1705137370.788173
Ending IO phase at 1705137376.497751
write     22.78      22.81      0.170333    16384      1024.00    0.069798   5.61       3.71       5.62       122 
Ending compute phase at 1705137394.711969
Starting IO phase at 1705137394.711980
Ending IO phase at 1705137395.973752
write     109.43     111.49     0.071025    16384      1024.00    0.050894   1.15       0.027959   1.17       123 
Ending compute phase at 1705137414.309071
Starting IO phase at 1705137414.309089
Ending IO phase at 1705137422.082687
write     16.35      16.67      0.479988    16384      1024.00    0.186297   7.68       0.015448   7.83       124 
Ending compute phase at 1705137440.307364
Starting IO phase at 1705137440.307376
Ending IO phase at 1705137449.281670
write     14.36      14.37      0.207613    16384      1024.00    0.134716   8.91       5.59       8.91       125 
Ending compute phase at 1705137467.608712
Starting IO phase at 1705137467.608726
Ending IO phase at 1705137476.777808
write     13.79      13.80      0.568915    16384      1024.00    0.178488   9.28       6.45       9.28       126 
Ending compute phase at 1705137495.081354
Starting IO phase at 1705137495.081372
Ending IO phase at 1705137506.009649
write     11.66      11.81      0.382559    16384      1024.00    0.167405   10.84      5.55       10.98      127 
Ending compute phase at 1705137524.381862
Starting IO phase at 1705137524.381882
Ending IO phase at 1705137532.465205
write     15.67      15.94      0.263766    16384      1024.00    0.159423   8.03       3.81       8.17       128 
Ending compute phase at 1705137551.201790
Starting IO phase at 1705137551.201810
Ending IO phase at 1705137556.165698
write     23.50      26.40      0.216613    16384      1024.00    0.615265   4.85       1.42       5.45       129 
Ending compute phase at 1705137574.777375
Starting IO phase at 1705137574.777399
Ending IO phase at 1705137582.887173
write     15.18      15.99      0.453392    16384      1024.00    0.458924   8.00       0.757704   8.43       130 
Ending compute phase at 1705137601.446982
Starting IO phase at 1705137601.447010
Ending IO phase at 1705137605.618514
write     28.56      31.23      0.256022    16384      1024.00    0.372944   4.10       0.397387   4.48       131 
Ending compute phase at 1705137623.917294
Starting IO phase at 1705137623.917313
Ending IO phase at 1705137634.277586
write     12.28      12.48      0.455158    16384      1024.00    0.228803   10.26      2.98       10.42      132 
Ending compute phase at 1705137652.742213
Starting IO phase at 1705137652.742229
Ending IO phase at 1705137659.013528
write     19.94      20.77      0.291498    16384      1024.00    0.413450   6.16       1.50       6.42       133 
Ending compute phase at 1705137677.373417
Starting IO phase at 1705137677.373433
Ending IO phase at 1705137681.997584
write     27.36      28.30      0.247304    16384      1024.00    0.155724   4.52       0.915525   4.68       134 
Ending compute phase at 1705137700.248355
Starting IO phase at 1705137700.248373
Ending IO phase at 1705137704.669764
write     29.66      29.71      0.267526    16384      1024.00    0.057084   4.31       1.52       4.32       135 
Ending compute phase at 1705137722.803431
Starting IO phase at 1705137722.803443
Ending IO phase at 1705137724.145778
write     100.29     100.75     0.075838    16384      1024.00    0.054617   1.27       0.057788   1.28       136 
Ending compute phase at 1705137742.370676
Starting IO phase at 1705137742.370689
