############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:09 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133949.692693
TestID              : 0
StartTime           : Sat Jan 13 09:19:09 2024
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134310.545285
Starting IO phase at 1705134310.545305
Ending IO phase at 1705134704.067422
write     6.50       6.53       1.21        327680     1024.00    0.651542   392.21     10.60      393.84     0   
Ending compute phase at 1705135075.220259
Starting IO phase at 1705135075.220277
Ending IO phase at 1705135439.151112
write     7.00       7.13       1.12        327680     1024.00    2.13       359.16     14.32      365.50     1   
Ending compute phase at 1705135800.857063
Starting IO phase at 1705135800.857086
Ending IO phase at 1705136163.213579
write     7.04       7.09       1.12        327680     1024.00    1.57       360.85     6.54       363.65     2   
Ending compute phase at 1705136524.052362
Starting IO phase at 1705136524.052378
Ending IO phase at 1705136611.046884
write     29.28      29.49      0.262607    327680     1024.00    0.511697   86.81      7.67       87.42      3   
Ending compute phase at 1705136971.393371
Starting IO phase at 1705136971.393385
Ending IO phase at 1705136994.269400
write     113.77     113.80     0.044885    327680     1024.00    0.054580   22.50      8.13       22.50      4   
Ending compute phase at 1705137354.482668
Starting IO phase at 1705137354.482681
Ending IO phase at 1705137377.765429
write     111.73     111.75     0.053719    327680     1024.00    0.063821   22.91      5.72       22.91      5   
Ending compute phase at 1705137737.979441
Starting IO phase at 1705137737.979454
