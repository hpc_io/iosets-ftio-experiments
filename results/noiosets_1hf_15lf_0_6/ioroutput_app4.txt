############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 09:19:08 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705133948.504159
TestID              : 0
StartTime           : Sat Jan 13 09:19:08 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705134308.554373
Starting IO phase at 1705134308.554387
Ending IO phase at 1705134668.756055
write     7.11       7.11       0.977431    327680     1024.00    0.278048   359.83     47.36      360.17     0   
Ending compute phase at 1705135029.300202
Starting IO phase at 1705135029.300216
Ending IO phase at 1705135076.096626
write     54.73      54.96      0.130049    327680     1024.00    0.657918   46.58      30.21      46.78      1   
Ending compute phase at 1705135447.752718
Starting IO phase at 1705135447.752734
Ending IO phase at 1705135516.138978
write     32.16      37.63      0.208383    327680     1024.00    11.58      68.04      3.44       79.61      2   
Ending compute phase at 1705135880.457726
Starting IO phase at 1705135880.457752
Ending IO phase at 1705136215.511952
write     7.56       7.66       1.05        327680     1024.00    4.04       334.40     2.73       338.84     3   
Ending compute phase at 1705136576.726866
Starting IO phase at 1705136576.726890
Ending IO phase at 1705136694.970203
write     21.52      21.72      0.368334    327680     1024.00    1.10       117.87     1.68       118.95     4   
Ending compute phase at 1705137055.688473
Starting IO phase at 1705137055.688497
Ending IO phase at 1705137146.293560
write     28.15      28.31      0.282079    327680     1024.00    0.537993   90.42      1.52       90.94      5   
Ending compute phase at 1705137506.508955
Starting IO phase at 1705137506.508969
Ending IO phase at 1705137552.645964
write     55.70      55.79      0.142168    327680     1024.00    0.110274   45.88      0.416406   45.96      6   
