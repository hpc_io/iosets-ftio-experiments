############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 02:55:33 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705110933.161495
TestID              : 0
StartTime           : Sat Jan 13 02:55:33 2024
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705111493.874192
Starting IO phase at 1705111493.874216
Ending IO phase at 1705111600.646882
write     8.54       21.81      0.303865    327680     1024.00    205.91     117.37     14.94      299.61     0   
Ending compute phase at 1705112019.742637
Starting IO phase at 1705112019.742661
Ending IO phase at 1705112107.984903
write     18.10      28.26      0.269897    327680     1024.00    71.30      90.60      3.61       141.43     1   
Ending compute phase at 1705112471.027222
Starting IO phase at 1705112471.027244
Ending IO phase at 1705112562.344671
write     27.31      28.16      0.283133    327680     1024.00    3.14       90.92      2.51       93.74      2   
Ending compute phase at 1705112922.468664
Starting IO phase at 1705112922.468683
Ending IO phase at 1705112965.977130
write     59.43      59.44      0.121736    327680     1024.00    0.047510   43.07      4.11       43.07      3   
Ending compute phase at 1705113331.764167
Starting IO phase at 1705113331.764202
Ending IO phase at 1705113377.413498
write     50.55      56.81      0.140821    327680     1024.00    5.57       45.06      4.25       50.65      4   
Ending compute phase at 1705113737.611712
Starting IO phase at 1705113737.611732
Ending IO phase at 1705113793.777009
write     45.88      45.92      0.162087    327680     1024.00    0.047263   55.75      4.60       55.79      5   
Ending compute phase at 1705114153.975972
Starting IO phase at 1705114153.975992
Ending IO phase at 1705114209.848503
write     46.09      46.10      0.173034    327680     1024.00    0.243189   55.54      15.65      55.54      6   
Ending compute phase at 1705114573.002957
Starting IO phase at 1705114573.002978
Ending IO phase at 1705114637.220110
write     38.33      40.18      0.193919    327680     1024.00    4.14       63.72      19.83      66.79      7   
