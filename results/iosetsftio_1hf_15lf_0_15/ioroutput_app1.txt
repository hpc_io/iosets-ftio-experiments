############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 02:55:31 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705110931.313239
TestID              : 0
StartTime           : Sat Jan 13 02:55:31 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705111291.358236
Starting IO phase at 1705111291.358258
Ending IO phase at 1705111408.118132
write     21.92      24.38      0.302285    327680     1024.00    0.054431   105.01     36.83      116.78     0   
Ending compute phase at 1705111781.924626
Starting IO phase at 1705111781.924651
Ending IO phase at 1705111804.301610
write     116.83     116.91     0.068298    327680     1024.00    0.052636   21.90      7.64       21.91      1   
Ending compute phase at 1705112164.467432
Starting IO phase at 1705112164.467452
Ending IO phase at 1705112188.594103
write     107.93     107.96     0.073388    327680     1024.00    0.042943   23.71      2.90       23.72      2   
Ending compute phase at 1705112548.747910
Starting IO phase at 1705112548.747929
Ending IO phase at 1705112587.105495
write     67.45      67.46      0.103203    327680     1024.00    0.056065   37.95      4.92       37.95      3   
Ending compute phase at 1705112947.340467
Starting IO phase at 1705112947.340487
Ending IO phase at 1705112990.456837
write     59.73      59.73      0.127726    327680     1024.00    0.065197   42.86      1.98       42.86      4   
Ending compute phase at 1705113350.730656
Starting IO phase at 1705113350.730679
Ending IO phase at 1705113406.993226
write     45.80      45.80      0.162640    327680     1024.00    0.331067   55.89      5.17       55.90      5   
Ending compute phase at 1705113767.202501
Starting IO phase at 1705113767.202521
Ending IO phase at 1705113822.210716
write     46.80      46.92      0.169955    327680     1024.00    0.156944   54.56      1.70       54.70      6   
Ending compute phase at 1705114182.433799
Starting IO phase at 1705114182.433820
Ending IO phase at 1705114238.932518
write     45.66      45.67      0.171512    327680     1024.00    0.538234   56.06      6.60       56.07      7   
Ending compute phase at 1705114601.164218
Starting IO phase at 1705114601.164242
Ending IO phase at 1705114661.773816
write     41.10      42.72      0.175248    327680     1024.00    2.28       59.92      4.21       62.29      8   
