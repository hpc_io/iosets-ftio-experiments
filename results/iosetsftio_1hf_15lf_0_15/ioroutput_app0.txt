############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 02:55:31 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705110931.321962
TestID              : 0
StartTime           : Sat Jan 13 02:55:31 2024
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705110949.341150
Starting IO phase at 1705110949.341173
Ending IO phase at 1705110951.481986
write     62.35      62.72      0.071468    16384      1024.00    0.073960   2.04       0.903687   2.05       0   
Ending compute phase at 1705110969.726935
Starting IO phase at 1705110969.726955
Ending IO phase at 1705110971.861822
write     61.37      61.75      0.129540    16384      1024.00    0.044562   2.07       0.020184   2.09       1   
Ending compute phase at 1705110990.063519
Starting IO phase at 1705110990.063537
Ending IO phase at 1705110992.218003
write     62.05      62.23      0.127126    16384      1024.00    0.049227   2.06       0.023732   2.06       2   
Ending compute phase at 1705111010.545090
Starting IO phase at 1705111010.545111
Ending IO phase at 1705111012.729812
write     60.90      62.19      0.128171    16384      1024.00    0.044892   2.06       0.028014   2.10       3   
Ending compute phase at 1705111030.972468
Starting IO phase at 1705111030.972490
Ending IO phase at 1705111033.057777
write     63.72      64.14      0.124011    16384      1024.00    0.048226   2.00       0.019686   2.01       4   
Ending compute phase at 1705111051.291879
Starting IO phase at 1705111051.291900
Ending IO phase at 1705111053.460816
write     62.57      63.01      0.126238    16384      1024.00    0.051499   2.03       0.020618   2.05       5   
Ending compute phase at 1705111071.707938
Starting IO phase at 1705111071.707959
Ending IO phase at 1705111073.865833
write     61.72      61.91      0.127786    16384      1024.00    0.042915   2.07       0.023724   2.07       6   
Ending compute phase at 1705111092.095277
Starting IO phase at 1705111092.095296
Ending IO phase at 1705111094.269743
write     61.13      61.50      0.130064    16384      1024.00    0.067025   2.08       0.019242   2.09       7   
Ending compute phase at 1705111112.456149
Starting IO phase at 1705111112.456173
Ending IO phase at 1705111114.669785
write     61.16      61.34      0.127221    16384      1024.00    0.058334   2.09       0.052024   2.09       8   
Ending compute phase at 1705111132.908372
Starting IO phase at 1705111132.908394
Ending IO phase at 1705111135.153738
write     60.79      61.50      0.129352    16384      1024.00    0.066450   2.08       0.022311   2.11       9   
Ending compute phase at 1705111153.356904
Starting IO phase at 1705111153.356926
Ending IO phase at 1705111155.633757
write     59.71      59.92      0.132805    16384      1024.00    0.058590   2.14       0.051999   2.14       10  
Ending compute phase at 1705111173.895597
Starting IO phase at 1705111173.895619
Ending IO phase at 1705111176.125797
write     60.22      60.58      0.131343    16384      1024.00    0.046183   2.11       0.019239   2.13       11  
Ending compute phase at 1705111194.490087
Starting IO phase at 1705111194.490110
Ending IO phase at 1705111196.717789
write     60.79      62.06      0.128176    16384      1024.00    0.055276   2.06       0.044060   2.11       12  
Ending compute phase at 1705111214.931360
Starting IO phase at 1705111214.931382
Ending IO phase at 1705111217.073803
write     62.57      62.94      0.125297    16384      1024.00    0.046491   2.03       0.036212   2.05       13  
Ending compute phase at 1705111235.267464
Starting IO phase at 1705111235.267484
Ending IO phase at 1705111237.462010
write     60.67      61.01      0.130404    16384      1024.00    0.062466   2.10       0.018416   2.11       14  
Ending compute phase at 1705111255.723680
Starting IO phase at 1705111255.723701
Ending IO phase at 1705111257.861890
write     61.36      61.79      0.129458    16384      1024.00    0.073022   2.07       0.021152   2.09       15  
Ending compute phase at 1705111276.085234
Starting IO phase at 1705111276.085254
Ending IO phase at 1705111278.285908
write     61.25      62.16      0.127967    16384      1024.00    0.060986   2.06       0.027635   2.09       16  
Ending compute phase at 1705111491.681450
Starting IO phase at 1705111491.681493
Ending IO phase at 1705111518.893799
write     0.586507   3.45       0.612947    16384      1024.00    203.02     37.09      33.96      218.24     17  
Ending compute phase at 1705111570.262079
Starting IO phase at 1705111570.262103
Ending IO phase at 1705111576.621643
write     3.26       15.31      0.320979    16384      1024.00    33.49      8.36       4.52       39.26      18  
Ending compute phase at 1705111594.808055
Starting IO phase at 1705111594.808076
Ending IO phase at 1705111599.977785
write     25.07      25.10      0.136551    16384      1024.00    2.19       5.10       2.92       5.11       19  
Ending compute phase at 1705111618.120169
Starting IO phase at 1705111618.120192
Ending IO phase at 1705111619.253792
write     124.75     125.52     0.014450    16384      1024.00    0.059669   1.02       0.789391   1.03       20  
Ending compute phase at 1705111637.459755
Starting IO phase at 1705111637.459774
Ending IO phase at 1705111638.625828
write     121.55     122.28     0.063284    16384      1024.00    0.046299   1.05       0.035054   1.05       21  
Ending compute phase at 1705111656.835173
Starting IO phase at 1705111656.835191
Ending IO phase at 1705111657.833818
write     132.05     132.79     0.016676    16384      1024.00    0.066807   0.963926   0.697706   0.969348   22  
Ending compute phase at 1705111676.007653
Starting IO phase at 1705111676.007675
Ending IO phase at 1705111677.212846
write     117.01     118.09     0.067018    16384      1024.00    0.063604   1.08       0.016381   1.09       23  
Ending compute phase at 1705111695.426294
Starting IO phase at 1705111695.426315
Ending IO phase at 1705111696.701778
write     106.92     109.50     0.057705    16384      1024.00    0.059387   1.17       0.246435   1.20       24  
Ending compute phase at 1705111714.938614
Starting IO phase at 1705111714.938635
Ending IO phase at 1705111716.158356
write     112.10     115.35     0.068593    16384      1024.00    0.068121   1.11       0.020281   1.14       25  
Ending compute phase at 1705111734.480054
Starting IO phase at 1705111734.480076
Ending IO phase at 1705111735.697807
write     117.49     118.85     0.066587    16384      1024.00    0.060130   1.08       0.018886   1.09       26  
Ending compute phase at 1705111753.985392
Starting IO phase at 1705111753.985413
Ending IO phase at 1705111755.277896
write     104.81     109.93     0.072572    16384      1024.00    0.055688   1.16       0.025480   1.22       27  
Ending compute phase at 1705111773.585616
Starting IO phase at 1705111773.585637
Ending IO phase at 1705111774.790253
write     109.04     113.54     0.069738    16384      1024.00    0.064753   1.13       0.018888   1.17       28  
Ending compute phase at 1705111793.071814
Starting IO phase at 1705111793.071835
Ending IO phase at 1705111798.705789
write     23.25      23.29      0.192717    16384      1024.00    0.048476   5.50       3.19       5.51       29  
Ending compute phase at 1705111816.896242
Starting IO phase at 1705111816.896264
Ending IO phase at 1705111818.165811
write     110.57     112.60     0.070323    16384      1024.00    0.064876   1.14       0.026898   1.16       30  
Ending compute phase at 1705111836.376532
Starting IO phase at 1705111836.376553
Ending IO phase at 1705111842.565847
write     20.96      20.98      0.164412    16384      1024.00    0.047393   6.10       4.13       6.11       31  
Ending compute phase at 1705111860.794679
Starting IO phase at 1705111860.794698
Ending IO phase at 1705111876.385741
write     8.20       8.24       0.172533    16384      1024.00    3.38       15.53      12.77      15.61      32  
Ending compute phase at 1705111901.123866
Starting IO phase at 1705111901.123886
Ending IO phase at 1705111914.681772
write     6.40       16.56      0.210109    16384      1024.00    7.28       7.73       10.28      20.01      33  
Ending compute phase at 1705112003.406500
Starting IO phase at 1705112003.406533
Ending IO phase at 1705112020.677703
write     1.47       6.95       0.691999    16384      1024.00    74.82      18.43      8.03       87.23      34  
Ending compute phase at 1705112079.919424
Starting IO phase at 1705112079.919446
Ending IO phase at 1705112085.495107
write     2.75       24.42      0.301457    16384      1024.00    41.11      5.24       2.91       46.58      35  
Ending compute phase at 1705112103.760524
Starting IO phase at 1705112103.760557
Ending IO phase at 1705112107.649844
write     34.12      34.19      0.146823    16384      1024.00    2.12       3.74       1.85       3.75       36  
Ending compute phase at 1705112125.871556
Starting IO phase at 1705112125.871576
Ending IO phase at 1705112127.121863
write     111.94     112.48     0.071121    16384      1024.00    0.048910   1.14       0.012344   1.14       37  
Ending compute phase at 1705112145.360039
Starting IO phase at 1705112145.360060
Ending IO phase at 1705112146.577835
write     116.16     117.40     0.038852    16384      1024.00    0.051514   1.09       0.474654   1.10       38  
Ending compute phase at 1705112164.908620
Starting IO phase at 1705112164.908646
Ending IO phase at 1705112166.709783
write     73.93      76.41      0.032488    16384      1024.00    0.135726   1.68       1.14       1.73       39  
Ending compute phase at 1705112184.902808
Starting IO phase at 1705112184.902830
Ending IO phase at 1705112188.313817
write     38.85      38.92      0.190552    16384      1024.00    0.376232   3.29       0.240957   3.29       40  
Ending compute phase at 1705112206.597305
Starting IO phase at 1705112206.597325
Ending IO phase at 1705112213.701811
write     18.11      18.13      0.439505    16384      1024.00    0.120153   7.06       4.31       7.07       41  
Ending compute phase at 1705112231.855892
Starting IO phase at 1705112231.855913
Ending IO phase at 1705112240.041738
write     15.78      15.79      0.199288    16384      1024.00    0.092352   8.10       4.92       8.11       42  
Ending compute phase at 1705112258.251370
Starting IO phase at 1705112258.251391
Ending IO phase at 1705112264.917864
write     19.38      19.41      0.192845    16384      1024.00    3.10       6.60       4.22       6.60       43  
Ending compute phase at 1705112283.080160
Starting IO phase at 1705112283.080181
Ending IO phase at 1705112291.537903
write     15.31      15.32      0.176752    16384      1024.00    2.83       8.35       5.53       8.36       44  
Ending compute phase at 1705112309.764002
Starting IO phase at 1705112309.764023
Ending IO phase at 1705112310.929825
write     115.78     117.28     0.067483    16384      1024.00    0.074139   1.09       0.020648   1.11       45  
Ending compute phase at 1705112329.141056
Starting IO phase at 1705112329.141077
Ending IO phase at 1705112335.525830
write     20.39      20.41      0.165479    16384      1024.00    2.65       6.27       3.62       6.28       46  
Ending compute phase at 1705112353.757781
Starting IO phase at 1705112353.757803
Ending IO phase at 1705112355.077948
write     101.44     106.08     0.074690    16384      1024.00    0.054363   1.21       0.027512   1.26       47  
Ending compute phase at 1705112373.335630
Starting IO phase at 1705112373.335653
Ending IO phase at 1705112379.002041
write     22.57      22.77      0.238419    16384      1024.00    1.31       5.62       2.41       5.67       48  
Ending compute phase at 1705112397.453062
Starting IO phase at 1705112397.453083
Ending IO phase at 1705112401.029773
write     34.54      36.59      0.178967    16384      1024.00    1.03       3.50       0.635463   3.71       49  
Ending compute phase at 1705112420.979274
Starting IO phase at 1705112420.979297
Ending IO phase at 1705112423.045801
write     34.53      65.70      0.058852    16384      1024.00    1.79       1.95       1.01       3.71       50  
Ending compute phase at 1705112441.360692
Starting IO phase at 1705112441.360715
Ending IO phase at 1705112445.757777
write     28.79      29.67      0.199933    16384      1024.00    0.381560   4.31       1.12       4.45       51  
Ending compute phase at 1705112465.603341
Starting IO phase at 1705112465.603365
Ending IO phase at 1705112469.193761
write     24.88      39.01      0.135772    16384      1024.00    1.91       3.28       1.34       5.15       52  
Ending compute phase at 1705112489.007667
Starting IO phase at 1705112489.007693
Ending IO phase at 1705112493.297788
write     22.02      30.46      0.182922    16384      1024.00    1.91       4.20       1.28       5.81       53  
Ending compute phase at 1705112513.019932
Starting IO phase at 1705112513.019956
Ending IO phase at 1705112515.865114
write     30.04      47.62      0.112477    16384      1024.00    1.78       2.69       0.912115   4.26       54  
Ending compute phase at 1705112534.743674
Starting IO phase at 1705112534.743699
Ending IO phase at 1705112540.353779
write     20.69      22.60      0.230815    16384      1024.00    1.00       5.66       3.46       6.19       55  
Ending compute phase at 1705112560.652990
Starting IO phase at 1705112560.653013
Ending IO phase at 1705112563.665792
write     25.79      45.03      0.177315    16384      1024.00    2.91       2.84       0.356703   4.96       56  
Ending compute phase at 1705112581.863744
Starting IO phase at 1705112581.863763
Ending IO phase at 1705112586.445881
write     28.30      28.34      0.134346    16384      1024.00    0.057805   4.52       2.37       4.52       57  
Ending compute phase at 1705112604.651679
Starting IO phase at 1705112604.651700
Ending IO phase at 1705112613.854008
write     14.01      14.26      0.239342    16384      1024.00    0.044847   8.98       5.52       9.14       58  
Ending compute phase at 1705112632.107829
Starting IO phase at 1705112632.107848
Ending IO phase at 1705112637.150732
write     25.71      25.74      0.267969    16384      1024.00    0.060436   4.97       1.11       4.98       59  
Ending compute phase at 1705112655.654937
Starting IO phase at 1705112655.654961
Ending IO phase at 1705112661.685844
write     20.67      22.44      0.310461    16384      1024.00    0.254975   5.70       3.49       6.19       60  
Ending compute phase at 1705112679.916162
Starting IO phase at 1705112679.916183
Ending IO phase at 1705112685.969900
write     21.43      21.49      0.186519    16384      1024.00    0.080483   5.96       2.98       5.97       61  
Ending compute phase at 1705112704.195934
Starting IO phase at 1705112704.195955
Ending IO phase at 1705112708.181784
write     32.89      32.94      0.232131    16384      1024.00    0.073426   3.89       0.172662   3.89       62  
Ending compute phase at 1705112726.399136
Starting IO phase at 1705112726.399158
Ending IO phase at 1705112728.745866
write     56.58      56.72      0.139604    16384      1024.00    0.080857   2.26       0.269841   2.26       63  
Ending compute phase at 1705112746.995977
Starting IO phase at 1705112746.995998
Ending IO phase at 1705112752.881813
write     22.11      22.13      0.173889    16384      1024.00    2.79       5.78       3.00       5.79       64  
Ending compute phase at 1705112771.155677
Starting IO phase at 1705112771.155698
Ending IO phase at 1705112782.077822
write     11.80      11.81      0.314969    16384      1024.00    0.063512   10.84      6.37       10.85      65  
Ending compute phase at 1705112800.337605
Starting IO phase at 1705112800.337629
Ending IO phase at 1705112807.741863
write     17.33      17.39      0.460053    16384      1024.00    0.367134   7.36       4.35       7.39       66  
Ending compute phase at 1705112825.951470
Starting IO phase at 1705112825.951489
Ending IO phase at 1705112836.005934
write     12.79      12.81      0.293391    16384      1024.00    0.062488   9.99       6.05       10.01      67  
Ending compute phase at 1705112854.312758
Starting IO phase at 1705112854.312782
Ending IO phase at 1705112861.984390
write     16.53      16.76      0.286176    16384      1024.00    0.202788   7.64       3.79       7.74       68  
Ending compute phase at 1705112882.328739
Starting IO phase at 1705112882.328764
Ending IO phase at 1705112890.877815
write     12.07      15.16      0.320200    16384      1024.00    2.97       8.44       3.32       10.60      69  
Ending compute phase at 1705112911.309648
Starting IO phase at 1705112911.309673
Ending IO phase at 1705112915.105626
write     21.89      34.63      0.187415    16384      1024.00    2.45       3.70       0.961452   5.85       70  
Ending compute phase at 1705112935.598038
Starting IO phase at 1705112935.598063
Ending IO phase at 1705112940.229740
write     18.82      28.17      0.199265    16384      1024.00    5.44       4.54       1.36       6.80       71  
Ending compute phase at 1705112961.291340
Starting IO phase at 1705112961.291363
Ending IO phase at 1705112965.757763
write     17.94      29.07      0.203375    16384      1024.00    3.45       4.40       1.72       7.13       72  
Ending compute phase at 1705112986.261370
Starting IO phase at 1705112986.261393
Ending IO phase at 1705112990.777741
write     18.92      28.69      0.209514    16384      1024.00    4.92       4.46       1.11       6.77       73  
Ending compute phase at 1705113011.272807
Starting IO phase at 1705113011.272831
Ending IO phase at 1705113016.793788
write     16.69      23.54      0.325527    16384      1024.00    3.01       5.44       2.62       7.67       74  
Ending compute phase at 1705113037.465239
Starting IO phase at 1705113037.465265
Ending IO phase at 1705113042.381716
write     17.71      26.30      0.121248    16384      1024.00    4.30       4.87       2.93       7.23       75  
Ending compute phase at 1705113062.752000
Starting IO phase at 1705113062.752030
Ending IO phase at 1705113065.901736
write     24.53      41.61      0.172230    16384      1024.00    2.19       3.08       0.452425   5.22       76  
Ending compute phase at 1705113084.165173
Starting IO phase at 1705113084.165195
Ending IO phase at 1705113090.781853
write     19.54      19.67      0.174095    16384      1024.00    0.046893   6.51       3.72       6.55       77  
Ending compute phase at 1705113108.980194
Starting IO phase at 1705113108.980213
Ending IO phase at 1705113116.713898
write     16.82      16.83      0.237400    16384      1024.00    4.15       7.61       3.81       7.61       78  
Ending compute phase at 1705113134.940175
Starting IO phase at 1705113134.940194
Ending IO phase at 1705113142.445803
write     17.27      17.29      0.171548    16384      1024.00    4.70       7.40       4.66       7.41       79  
Ending compute phase at 1705113160.691821
Starting IO phase at 1705113160.691842
Ending IO phase at 1705113169.809818
write     14.22      14.23      0.296389    16384      1024.00    4.75       8.99       5.96       9.00       80  
Ending compute phase at 1705113188.027376
Starting IO phase at 1705113188.027397
Ending IO phase at 1705113194.665975
write     19.54      19.56      0.192931    16384      1024.00    0.128823   6.55       3.46       6.55       81  
Ending compute phase at 1705113212.975500
Starting IO phase at 1705113212.975521
Ending IO phase at 1705113223.297472
write     12.46      12.47      0.216163    16384      1024.00    5.61       10.27      6.81       10.27      82  
Ending compute phase at 1705113241.563169
Starting IO phase at 1705113241.563188
Ending IO phase at 1705113250.901773
write     13.87      13.88      0.576322    16384      1024.00    0.518649   9.22       4.25       9.23       83  
Ending compute phase at 1705113269.147507
Starting IO phase at 1705113269.147528
Ending IO phase at 1705113278.373741
write     13.98      14.01      0.271140    16384      1024.00    0.293640   9.13       4.81       9.16       84  
Ending compute phase at 1705113296.600899
Starting IO phase at 1705113296.600919
Ending IO phase at 1705113307.189793
write     12.18      12.20      0.250678    16384      1024.00    0.345661   10.49      6.48       10.51      85  
Ending compute phase at 1705113325.432199
Starting IO phase at 1705113325.432220
Ending IO phase at 1705113333.856609
write     15.40      15.42      0.299831    16384      1024.00    0.414732   8.30       4.53       8.31       86  
Ending compute phase at 1705113354.745339
Starting IO phase at 1705113354.745362
Ending IO phase at 1705113363.953757
write     10.77      13.98      0.325270    16384      1024.00    7.92       9.15       3.96       11.88      87  
Ending compute phase at 1705113384.373818
Starting IO phase at 1705113384.373841
Ending IO phase at 1705113394.625736
write     10.36      12.63      0.351701    16384      1024.00    7.67       10.13      5.13       12.36      88  
Ending compute phase at 1705113415.905805
Starting IO phase at 1705113415.905828
Ending IO phase at 1705113425.433813
write     10.25      13.56      0.268861    16384      1024.00    7.65       9.44       5.14       12.48      89  
Ending compute phase at 1705113446.450603
Starting IO phase at 1705113446.450626
Ending IO phase at 1705113456.585734
write     9.93       13.75      0.315601    16384      1024.00    7.89       9.31       5.20       12.89      90  
Ending compute phase at 1705113477.361937
Starting IO phase at 1705113477.361961
Ending IO phase at 1705113485.112911
write     12.44      16.63      0.285127    16384      1024.00    7.15       7.70       3.14       10.29      91  
Ending compute phase at 1705113505.773673
Starting IO phase at 1705113505.773698
Ending IO phase at 1705113514.465744
write     11.57      15.32      0.313297    16384      1024.00    2.89       8.36       4.28       11.06      92  
Ending compute phase at 1705113533.170381
Starting IO phase at 1705113533.170405
Ending IO phase at 1705113537.817757
write     25.27      28.67      0.276708    16384      1024.00    0.525923   4.46       1.11       5.07       93  
Ending compute phase at 1705113565.097771
Starting IO phase at 1705113565.097797
Ending IO phase at 1705113570.777800
write     8.73       22.24      0.348747    16384      1024.00    9.36       5.76       2.17       14.66      94  
Ending compute phase at 1705113591.447322
Starting IO phase at 1705113591.447346
Ending IO phase at 1705113595.397780
write     20.13      32.89      0.197519    16384      1024.00    2.53       3.89       0.732490   6.36       95  
Ending compute phase at 1705113619.633974
Starting IO phase at 1705113619.633999
Ending IO phase at 1705113623.221177
write     13.47      34.56      0.190798    16384      1024.00    6.20       3.70       0.658401   9.50       96  
Ending compute phase at 1705113644.282043
Starting IO phase at 1705113644.282068
Ending IO phase at 1705113651.161922
write     13.19      18.43      0.286615    16384      1024.00    7.35       6.95       2.88       9.70       97  
Ending compute phase at 1705113672.186450
Starting IO phase at 1705113672.186473
Ending IO phase at 1705113679.505768
write     12.76      17.68      0.284286    16384      1024.00    7.34       7.24       2.69       10.03      98  
Ending compute phase at 1705113700.550248
Starting IO phase at 1705113700.550271
Ending IO phase at 1705113707.677742
write     12.98      18.18      0.307187    16384      1024.00    7.74       7.04       2.13       9.86       99  
Ending compute phase at 1705113728.730069
Starting IO phase at 1705113728.730092
Ending IO phase at 1705113735.293762
write     13.89      19.86      0.268376    16384      1024.00    3.59       6.44       2.15       9.22       100 
Ending compute phase at 1705113756.550980
Starting IO phase at 1705113756.551004
Ending IO phase at 1705113763.997763
write     12.28      17.31      0.322011    16384      1024.00    7.95       7.39       3.11       10.43      101 
Ending compute phase at 1705113785.098512
Starting IO phase at 1705113785.098535
Ending IO phase at 1705113793.129755
write     11.85      16.43      0.273814    16384      1024.00    7.24       7.79       3.56       10.80      102 
Ending compute phase at 1705113814.034404
Starting IO phase at 1705113814.034426
Ending IO phase at 1705113821.544262
write     12.59      17.92      0.267724    16384      1024.00    7.04       7.14       3.13       10.16      103 
Ending compute phase at 1705113842.312461
Starting IO phase at 1705113842.312484
Ending IO phase at 1705113848.541740
write     14.71      20.84      0.187789    16384      1024.00    5.56       6.14       3.14       8.70       104 
Ending compute phase at 1705113869.042960
Starting IO phase at 1705113869.042984
Ending IO phase at 1705113876.969819
write     12.67      16.85      0.272458    16384      1024.00    2.73       7.59       3.50       10.10      105 
Ending compute phase at 1705113898.395492
Starting IO phase at 1705113898.395517
Ending IO phase at 1705113904.601797
write     13.75      20.91      0.253989    16384      1024.00    7.25       6.12       2.06       9.31       106 
Ending compute phase at 1705113925.199151
Starting IO phase at 1705113925.199174
Ending IO phase at 1705113933.609778
write     11.88      15.42      0.306533    16384      1024.00    2.89       8.30       3.43       10.77      107 
Ending compute phase at 1705113955.444007
Starting IO phase at 1705113955.444031
Ending IO phase at 1705113960.849711
write     14.32      18.62      0.267297    16384      1024.00    3.77       6.87       2.44       8.94       108 
Ending compute phase at 1705113980.151285
Starting IO phase at 1705113980.151308
Ending IO phase at 1705113986.197747
write     18.00      26.36      0.274900    16384      1024.00    1.66       4.86       1.57       7.11       109 
Ending compute phase at 1705114009.828576
Starting IO phase at 1705114009.828599
Ending IO phase at 1705114014.457715
write     12.72      27.92      0.246206    16384      1024.00    5.63       4.59       0.841233   10.06      110 
Ending compute phase at 1705114037.236171
Starting IO phase at 1705114037.236193
Ending IO phase at 1705114043.997693
write     11.39      19.17      0.251937    16384      1024.00    5.52       6.68       2.64       11.24      111 
Ending compute phase at 1705114063.599074
Starting IO phase at 1705114063.599097
Ending IO phase at 1705114068.756640
write     19.80      25.40      0.149247    16384      1024.00    1.44       5.04       2.65       6.46       112 
Ending compute phase at 1705114094.533638
Starting IO phase at 1705114094.533661
Ending IO phase at 1705114100.593776
write     9.47       21.47      0.255498    16384      1024.00    8.67       5.96       1.88       13.52      113 
Ending compute phase at 1705114120.971656
Starting IO phase at 1705114120.971679
Ending IO phase at 1705114125.933800
write     18.22      29.64      0.204169    16384      1024.00    2.20       4.32       2.10       7.02       114 
Ending compute phase at 1705114146.720477
Starting IO phase at 1705114146.720501
Ending IO phase at 1705114153.031149
write     14.43      20.56      0.243063    16384      1024.00    3.81       6.23       2.34       8.87       115 
Ending compute phase at 1705114173.483450
Starting IO phase at 1705114173.483472
Ending IO phase at 1705114180.333814
write     14.15      18.79      0.263538    16384      1024.00    2.66       6.81       2.60       9.05       116 
Ending compute phase at 1705114200.111463
Starting IO phase at 1705114200.111487
Ending IO phase at 1705114205.396025
write     18.79      24.40      0.327873    16384      1024.00    1.98       5.25       0.719686   6.81       117 
Ending compute phase at 1705114226.235885
Starting IO phase at 1705114226.235909
Ending IO phase at 1705114236.037796
write     10.39      13.11      0.282135    16384      1024.00    8.06       9.76       6.18       12.32      118 
Ending compute phase at 1705114256.695725
Starting IO phase at 1705114256.695749
Ending IO phase at 1705114265.201846
write     11.79      15.21      0.304131    16384      1024.00    2.85       8.41       3.55       10.86      119 
Ending compute phase at 1705114285.907741
Starting IO phase at 1705114285.907765
Ending IO phase at 1705114293.593806
write     12.70      17.80      0.445550    16384      1024.00    6.50       7.19       3.58       10.08      120 
Ending compute phase at 1705114314.387577
Starting IO phase at 1705114314.387605
Ending IO phase at 1705114321.777787
write     12.94      17.35      0.276547    16384      1024.00    3.14       7.38       2.92       9.89       121 
Ending compute phase at 1705114341.979637
Starting IO phase at 1705114341.979662
Ending IO phase at 1705114351.621846
write     11.07      14.02      0.286730    16384      1024.00    6.58       9.13       4.99       11.57      122 
Ending compute phase at 1705114379.236287
Starting IO phase at 1705114379.236316
Ending IO phase at 1705114383.101780
write     9.66       27.70      0.236908    16384      1024.00    9.59       4.62       1.26       13.25      123 
Ending compute phase at 1705114402.149411
Starting IO phase at 1705114402.149436
Ending IO phase at 1705114421.913702
write     6.25       7.72       0.594654    16384      1024.00    4.31       16.58      12.46      20.48      124 
Ending compute phase at 1705114457.480363
Starting IO phase at 1705114457.480387
Ending IO phase at 1705114462.805830
write     5.65       24.94      0.256390    16384      1024.00    17.71      5.13       1.59       22.64      125 
Ending compute phase at 1705114483.508116
Starting IO phase at 1705114483.508139
Ending IO phase at 1705114490.293814
write     14.03      19.13      0.276394    16384      1024.00    6.85       6.69       2.27       9.12       126 
Ending compute phase at 1705114518.619699
Starting IO phase at 1705114518.619723
Ending IO phase at 1705114527.997742
write     6.60       12.71      0.317012    16384      1024.00    11.06      10.07      4.23       19.38      127 
Ending compute phase at 1705114549.164625
Starting IO phase at 1705114549.164647
Ending IO phase at 1705114558.829645
write     10.20      13.37      0.341614    16384      1024.00    4.19       9.58       4.12       12.55      128 
Ending compute phase at 1705114582.624100
Starting IO phase at 1705114582.624123
Ending IO phase at 1705114592.553846
write     8.31       13.01      0.352927    16384      1024.00    11.21      9.84       4.20       15.40      129 
Ending compute phase at 1705114613.545666
Starting IO phase at 1705114613.545690
Ending IO phase at 1705114619.529637
write     14.77      21.77      0.332870    16384      1024.00    3.43       5.88       1.32       8.67       130 
Ending compute phase at 1705114641.276439
Starting IO phase at 1705114641.276465
Ending IO phase at 1705114650.505803
write     10.04      13.98      0.278540    16384      1024.00    9.00       9.15       4.71       12.75      131 
Ending compute phase at 1705114672.572989
Starting IO phase at 1705114672.573011
Ending IO phase at 1705114682.149790
write     9.58       13.46      0.308310    16384      1024.00    9.40       9.51       4.58       13.37      132 
Ending compute phase at 1705114703.624456
Starting IO phase at 1705114703.624480
Ending IO phase at 1705114713.349760
write     9.91       13.92      0.355120    16384      1024.00    8.43       9.20       4.51       12.92      133 
