############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 04:48:37 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705463317.653206
TestID              : 0
StartTime           : Wed Jan 17 04:48:37 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705463678.378586
Starting IO phase at 1705463678.378613
Ending IO phase at 1705463830.747749
write     16.73      16.80      0.472998    327680     1024.00    0.723634   152.35     6.82       153.04     0   
Ending compute phase at 1705464192.681517
Starting IO phase at 1705464192.681542
Ending IO phase at 1705464240.895548
write     51.39      53.39      0.149836    327680     1024.00    2.21       47.95      8.30       49.82      1   
Ending compute phase at 1705464614.758942
Starting IO phase at 1705464614.758960
Ending IO phase at 1705464641.919023
write     63.02      95.25      0.075346    327680     1024.00    18.05      26.88      2.78       40.62      2   
Ending compute phase at 1705465002.417703
Starting IO phase at 1705465002.417729
Ending IO phase at 1705465045.865133
write     58.86      59.23      0.129375    327680     1024.00    0.433436   43.22      2.61       43.49      3   
Ending compute phase at 1705465406.045144
Starting IO phase at 1705465406.045160
Ending IO phase at 1705465446.270727
write     64.20      64.20      0.115134    327680     1024.00    0.053476   39.87      3.03       39.88      4   
Ending compute phase at 1705465807.102512
Starting IO phase at 1705465807.102527
Ending IO phase at 1705465838.180889
write     81.22      82.92      0.093243    327680     1024.00    0.716823   30.87      1.56       31.52      5   
Ending compute phase at 1705466198.420845
Starting IO phase at 1705466198.420860
Ending IO phase at 1705466240.955169
write     60.76      60.78      0.123466    327680     1024.00    0.052178   42.12      3.72       42.13      6   
Ending compute phase at 1705466601.151487
Starting IO phase at 1705466601.151502
Ending IO phase at 1705466656.944069
write     46.22      46.23      0.171502    327680     1024.00    0.097953   55.37      4.98       55.39      7   
Ending compute phase at 1705467017.107746
Starting IO phase at 1705467017.107766
Ending IO phase at 1705467075.081822
write     44.35      44.35      0.156755    327680     1024.00    1.30       57.72      7.56       57.73      8   
