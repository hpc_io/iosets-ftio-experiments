############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Wed Jan 17 04:48:38 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705463318.538333
TestID              : 0
StartTime           : Wed Jan 17 04:48:38 2024
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705463691.459982
Starting IO phase at 1705463691.460007
Ending IO phase at 1705464213.617190
write     4.79       4.91       1.63        327680     1024.00    392.00     521.90     23.11      534.54     0   
Ending compute phase at 1705464573.741166
Starting IO phase at 1705464573.741180
Ending IO phase at 1705464619.374143
write     56.46      56.48      0.120613    327680     1024.00    0.049038   45.32      21.20      45.34      1   
Ending compute phase at 1705464979.740697
Starting IO phase at 1705464979.740713
Ending IO phase at 1705465023.593944
write     58.74      58.76      0.135939    327680     1024.00    0.124181   43.56      12.24      43.58      2   
Ending compute phase at 1705465384.295803
Starting IO phase at 1705465384.295827
Ending IO phase at 1705465420.954943
write     69.63      70.50      0.111267    327680     1024.00    0.456287   36.31      4.59       36.77      3   
Ending compute phase at 1705465784.381700
Starting IO phase at 1705465784.381716
Ending IO phase at 1705465812.619695
write     82.49      89.02      0.087019    327680     1024.00    3.19       28.76      8.76       31.03      4   
Ending compute phase at 1705466172.773442
Starting IO phase at 1705466172.773457
Ending IO phase at 1705466212.967716
write     64.17      64.18      0.111005    327680     1024.00    0.104127   39.89      4.37       39.90      5   
Ending compute phase at 1705466573.530296
Starting IO phase at 1705466573.530319
Ending IO phase at 1705466630.086310
write     45.25      45.52      0.168623    327680     1024.00    0.570091   56.24      17.78      56.58      6   
Ending compute phase at 1705466990.317453
Starting IO phase at 1705466990.317467
Ending IO phase at 1705467062.875943
write     35.41      35.41      0.150030    327680     1024.00    0.414368   72.29      24.75      72.30      7   
