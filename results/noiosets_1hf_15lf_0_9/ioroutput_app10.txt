############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 03:25:16 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705199116.796413
TestID              : 0
StartTime           : Sun Jan 14 03:25:16 2024
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705199477.615473
Starting IO phase at 1705199477.615497
Ending IO phase at 1705199914.303213
write     5.86       5.87       1.35        327680     1024.00    0.514432   436.28     10.84      436.83     0   
Ending compute phase at 1705200280.509289
Starting IO phase at 1705200280.509312
Ending IO phase at 1705200637.294183
write     7.15       7.18       1.11        327680     1024.00    1.37       356.56     3.92       358.01     1   
Ending compute phase at 1705200997.695224
Starting IO phase at 1705200997.695243
Ending IO phase at 1705201165.905823
write     15.21      15.23      0.519020    327680     1024.00    0.220802   168.13     4.68       168.30     2   
Ending compute phase at 1705201526.083028
Starting IO phase at 1705201526.083040
Ending IO phase at 1705201555.476720
write     87.79      87.97      0.054038    327680     1024.00    0.052029   29.10      17.17      29.16      3   
Ending compute phase at 1705201915.699594
Starting IO phase at 1705201915.699607
Ending IO phase at 1705201940.078934
write     106.48     106.55     0.065233    327680     1024.00    0.093981   24.03      4.91       24.04      4   
Ending compute phase at 1705202303.419191
Starting IO phase at 1705202303.419209
Ending IO phase at 1705202338.225472
write     68.22      74.51      0.102077    327680     1024.00    3.17       34.36      4.44       37.52      5   
Ending compute phase at 1705202699.463050
Starting IO phase at 1705202699.463077
Ending IO phase at 1705202814.401942
write     22.15      22.35      0.331647    327680     1024.00    1.03       114.55     9.56       115.58     6   
