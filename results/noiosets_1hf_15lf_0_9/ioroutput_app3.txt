############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 03:25:16 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705199116.800413
TestID              : 0
StartTime           : Sun Jan 14 03:25:16 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705199477.699862
Starting IO phase at 1705199477.699891
Ending IO phase at 1705199914.287257
write     5.86       5.87       1.36        327680     1024.00    0.625308   436.20     12.46      436.81     0   
Ending compute phase at 1705200280.513552
Starting IO phase at 1705200280.513567
Ending IO phase at 1705200637.405777
write     7.15       7.18       1.11        327680     1024.00    1.56       356.55     1.97       357.99     1   
Ending compute phase at 1705200997.731890
Starting IO phase at 1705200997.731915
Ending IO phase at 1705201166.718128
write     15.14      15.16      0.518905    327680     1024.00    0.293312   168.85     2.91       169.10     2   
Ending compute phase at 1705201526.983344
Starting IO phase at 1705201526.983357
Ending IO phase at 1705201582.879240
write     46.00      46.03      0.160873    327680     1024.00    0.056980   55.62      12.18      55.65      3   
Ending compute phase at 1705201943.111512
Starting IO phase at 1705201943.111525
Ending IO phase at 1705201967.633240
write     105.98     106.00     0.075270    327680     1024.00    0.046840   24.15      0.751542   24.16      4   
Ending compute phase at 1705202327.822520
Starting IO phase at 1705202327.822544
Ending IO phase at 1705202362.825372
write     73.91      74.00      0.108102    327680     1024.00    0.049905   34.59      3.24       34.64      5   
Ending compute phase at 1705202723.754912
Starting IO phase at 1705202723.754936
Ending IO phase at 1705202826.449847
write     24.84      25.02      0.310143    327680     1024.00    0.808406   102.30     3.06       103.04     6   
