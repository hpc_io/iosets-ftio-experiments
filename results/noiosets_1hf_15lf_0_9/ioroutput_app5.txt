############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 03:25:16 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705199116.792424
TestID              : 0
StartTime           : Sun Jan 14 03:25:16 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705199477.052185
Starting IO phase at 1705199477.052208
Ending IO phase at 1705199901.603261
write     6.03       6.04       1.30        327680     1024.00    0.286957   424.08     10.20      424.58     0   
Ending compute phase at 1705200262.965280
Starting IO phase at 1705200262.965301
Ending IO phase at 1705200621.450620
write     7.13       7.14       1.11        327680     1024.00    0.713328   358.40     2.78       359.11     1   
Ending compute phase at 1705200985.555517
Starting IO phase at 1705200985.555531
Ending IO phase at 1705201071.521838
write     28.53      29.85      0.222264    327680     1024.00    3.83       85.77      14.79      89.74      2   
Ending compute phase at 1705201431.642511
Starting IO phase at 1705201431.642526
Ending IO phase at 1705201474.669069
write     60.03      60.04      0.120731    327680     1024.00    0.070811   42.64      4.01       42.65      3   
Ending compute phase at 1705201834.846845
Starting IO phase at 1705201834.846859
Ending IO phase at 1705201860.278085
write     101.95     102.00     0.078397    327680     1024.00    0.067627   25.10      1.84       25.11      4   
Ending compute phase at 1705202227.056357
Starting IO phase at 1705202227.056383
Ending IO phase at 1705202313.278654
write     27.69      29.82      0.267144    327680     1024.00    6.61       85.84      6.84       92.45      5   
Ending compute phase at 1705202674.278637
Starting IO phase at 1705202674.278661
Ending IO phase at 1705202796.216475
write     20.90      21.12      0.378697    327680     1024.00    0.843079   121.20     5.20       122.46     6   
