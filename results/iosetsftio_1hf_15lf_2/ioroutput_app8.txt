############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 02:36:20 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696120580.820552
TestID              : 0
StartTime           : Sun Oct  1 02:36:21 2023
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696121133.519443
Starting IO phase at 1696121133.519469
Ending IO phase at 1696121404.322430
write     5.54       9.47       0.807548    327680     1024.00    256.60     270.44     13.39      462.42     0   
Ending compute phase at 1696121844.294735
Starting IO phase at 1696121844.294762
Ending IO phase at 1696121941.066561
write     17.09      26.50      0.243114    327680     1024.00    74.64      96.62      18.83      149.79     1   
Ending compute phase at 1696122302.322183
Starting IO phase at 1696122302.322207
Ending IO phase at 1696122350.970470
write     51.66      52.94      0.141375    327680     1024.00    1.22       48.35      3.24       49.56      2   
Ending compute phase at 1696122711.323081
Starting IO phase at 1696122711.323102
Ending IO phase at 1696122756.310751
write     57.33      57.34      0.136311    327680     1024.00    0.143086   44.65      2.11       44.65      3   
Ending compute phase at 1696123116.567817
Starting IO phase at 1696123116.567836
Ending IO phase at 1696123141.773664
write     103.58     103.60     0.077058    327680     1024.00    0.095627   24.71      0.749822   24.72      4   
Ending compute phase at 1696123501.992302
Starting IO phase at 1696123501.992323
Ending IO phase at 1696123528.665583
write     97.22      97.24      0.079302    327680     1024.00    0.220039   26.33      2.57       26.33      5   
Ending compute phase at 1696123888.883265
Starting IO phase at 1696123888.883287
Ending IO phase at 1696123936.148868
write     54.64      54.65      0.145275    327680     1024.00    0.102846   46.84      2.06       46.85      6   
Ending compute phase at 1696124306.813385
Starting IO phase at 1696124306.813408
Ending IO phase at 1696124363.962732
write     38.04      45.07      0.148634    327680     1024.00    10.55      56.80      12.34      67.29      7   
