############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 02:36:20 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696120580.696569
TestID              : 0
StartTime           : Sun Oct  1 02:36:20 2023
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696121105.431262
Starting IO phase at 1696121105.431282
Ending IO phase at 1696121401.477365
write     5.57       8.02       0.836448    327680     1024.00    166.00     319.31     50.84      459.60     0   
Ending compute phase at 1696121815.764808
Starting IO phase at 1696121815.764831
Ending IO phase at 1696121892.087656
write     19.65      43.93      0.149598    327680     1024.00    75.83      58.27      36.99      130.29     1   
Ending compute phase at 1696122252.247629
Starting IO phase at 1696122252.247652
Ending IO phase at 1696122275.853426
write     111.13     111.17     0.070005    327680     1024.00    0.078463   23.03      1.27       23.04      2   
Ending compute phase at 1696122636.103380
Starting IO phase at 1696122636.103402
Ending IO phase at 1696122659.037503
write     113.90     113.93     0.068896    327680     1024.00    0.072507   22.47      2.58       22.48      3   
Ending compute phase at 1696123019.251776
Starting IO phase at 1696123019.251797
Ending IO phase at 1696123045.241660
write     100.00     100.02     0.076713    327680     1024.00    0.585321   25.59      1.05       25.60      4   
Ending compute phase at 1696123405.419102
Starting IO phase at 1696123405.419123
Ending IO phase at 1696123439.361559
write     76.23      76.25      0.102675    327680     1024.00    0.056333   33.58      6.01       33.58      5   
Ending compute phase at 1696123801.892602
Starting IO phase at 1696123801.892628
Ending IO phase at 1696123854.756649
write     46.75      48.58      0.163342    327680     1024.00    2.61       52.69      0.341026   54.76      6   
Ending compute phase at 1696124228.080582
Starting IO phase at 1696124228.080607
Ending IO phase at 1696124284.125153
write     37.21      45.86      0.171227    327680     1024.00    13.37      55.82      4.66       68.79      7   
