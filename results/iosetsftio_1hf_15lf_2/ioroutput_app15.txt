############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 02:36:20 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696120580.896219
TestID              : 0
StartTime           : Sun Oct  1 02:36:21 2023
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696121094.263300
Starting IO phase at 1696121094.263321
Ending IO phase at 1696121402.213311
write     5.56       8.32       0.823043    327680     1024.00    177.35     307.74     44.36      460.55     0   
Ending compute phase at 1696121829.304339
Starting IO phase at 1696121829.304364
Ending IO phase at 1696121944.273562
write     14.11      19.80      0.357078    327680     1024.00    78.37      129.31     1.91       181.38     1   
Ending compute phase at 1696122306.545296
Starting IO phase at 1696122306.545320
Ending IO phase at 1696122471.016793
write     15.40      15.60      0.510985    327680     1024.00    2.38       164.12     10.57      166.22     2   
Ending compute phase at 1696122836.198215
Starting IO phase at 1696122836.198240
Ending IO phase at 1696122927.871775
write     26.57      27.21      0.285395    327680     1024.00    5.98       94.08      2.44       96.33      3   
Ending compute phase at 1696123293.115417
Starting IO phase at 1696123293.115443
Ending IO phase at 1696123354.258133
write     38.86      42.07      0.146076    327680     1024.00    5.30       60.85      14.42      65.88      4   
Ending compute phase at 1696123714.475833
Starting IO phase at 1696123714.475855
Ending IO phase at 1696123776.456010
write     41.55      41.55      0.126589    327680     1024.00    0.042351   61.61      21.10      61.62      5   
Ending compute phase at 1696124136.659649
Starting IO phase at 1696124136.659672
Ending IO phase at 1696124361.787591
write     11.39      11.58      0.691030    327680     1024.00    0.199495   221.13     14.43      224.69     6   
