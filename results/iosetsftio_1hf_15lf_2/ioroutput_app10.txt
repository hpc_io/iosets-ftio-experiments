############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 02:36:20 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696120580.808228
TestID              : 0
StartTime           : Sun Oct  1 02:36:20 2023
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696121086.819577
Starting IO phase at 1696121086.819597
Ending IO phase at 1696121393.417182
write     5.67       9.19       0.837785    327680     1024.00    149.35     278.63     40.18      451.73     0   
Ending compute phase at 1696121800.062462
Starting IO phase at 1696121800.062487
Ending IO phase at 1696121928.578403
write     14.75      20.48      0.372729    327680     1024.00    72.30      125.00     12.23      173.52     1   
Ending compute phase at 1696122294.485008
Starting IO phase at 1696122294.485030
Ending IO phase at 1696122348.007660
write     43.38      48.23      0.119086    327680     1024.00    8.83       53.08      15.19      59.01      2   
Ending compute phase at 1696122708.231837
Starting IO phase at 1696122708.231859
Ending IO phase at 1696122731.172745
write     112.94     112.98     0.043126    327680     1024.00    0.063659   22.66      8.86       22.67      3   
Ending compute phase at 1696123091.391281
Starting IO phase at 1696123091.391302
Ending IO phase at 1696123116.435410
write     103.96     103.98     0.058769    327680     1024.00    0.045961   24.62      5.81       24.63      4   
Ending compute phase at 1696123476.692647
Starting IO phase at 1696123476.692668
Ending IO phase at 1696123503.229964
write     98.19      98.21      0.077472    327680     1024.00    0.080443   26.07      4.31       26.07      5   
Ending compute phase at 1696123863.416116
Starting IO phase at 1696123863.416137
Ending IO phase at 1696123908.713011
write     56.96      56.97      0.139400    327680     1024.00    0.070829   44.94      1.57       44.95      6   
Ending compute phase at 1696124276.274119
Starting IO phase at 1696124276.274143
Ending IO phase at 1696124355.366546
write     29.72      33.96      0.235582    327680     1024.00    7.39       75.39      24.33      86.13      7   
