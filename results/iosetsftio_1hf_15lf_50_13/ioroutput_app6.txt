############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Jan 14 05:33:04 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_6_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705206784.396869
TestID              : 0
StartTime           : Sun Jan 14 05:33:04 2024
Path                : /mnt/beegfs/iosets_6_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_6_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705207216.917064
Starting IO phase at 1705207216.917085
Ending IO phase at 1705207526.417842
write     6.71       8.29       0.927028    327680     1024.00    72.29      308.80     29.15      381.37     0   
Ending compute phase at 1705207919.436528
Starting IO phase at 1705207919.436550
Ending IO phase at 1705208064.732150
write     14.39      23.06      0.210148    327680     1024.00    101.42     111.03     77.87      177.96     1   
Ending compute phase at 1705208430.524172
Starting IO phase at 1705208430.524192
Ending IO phase at 1705208473.860513
write     58.70      59.72      0.133917    327680     1024.00    0.861378   42.86      0.378101   43.62      2   
Ending compute phase at 1705208848.901679
Starting IO phase at 1705208848.901703
Ending IO phase at 1705208900.063200
write     39.02      50.44      0.153085    327680     1024.00    15.28      50.75      2.52       65.61      3   
Ending compute phase at 1705209260.249557
Starting IO phase at 1705209260.249578
Ending IO phase at 1705209309.487600
write     52.26      52.28      0.116733    327680     1024.00    3.45       48.97      11.62      48.98      4   
Ending compute phase at 1705209669.696343
Starting IO phase at 1705209669.696363
Ending IO phase at 1705209714.748786
write     57.16      57.17      0.126778    327680     1024.00    0.093168   44.78      4.21       44.78      5   
Ending compute phase at 1705210074.927418
Starting IO phase at 1705210074.927440
Ending IO phase at 1705210116.096910
write     62.80      62.82      0.104612    327680     1024.00    0.058905   40.75      7.28       40.76      6   
Ending compute phase at 1705210476.322301
Starting IO phase at 1705210476.322324
Ending IO phase at 1705210530.376535
write     47.72      47.72      0.161461    327680     1024.00    0.170447   53.65      8.19       53.65      7   
