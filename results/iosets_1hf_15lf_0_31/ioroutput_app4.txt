############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Mon Jan 15 21:04:16 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705349056.300365
TestID              : 0
StartTime           : Mon Jan 15 21:04:16 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705349427.191221
Starting IO phase at 1705349427.191241
Ending IO phase at 1705349520.401604
write     24.75      27.48      0.287257    327680     1024.00    10.93      93.17      2.14       103.45     0   
Ending compute phase at 1705349880.434240
Starting IO phase at 1705349880.434255
Ending IO phase at 1705349925.119719
write     57.57      57.57      0.135092    327680     1024.00    0.073849   44.46      1.24       44.47      1   
Ending compute phase at 1705350285.732393
Starting IO phase at 1705350285.732411
Ending IO phase at 1705350312.227437
write     96.04      97.63      0.081936    327680     1024.00    0.437047   26.22      2.88       26.66      2   
Ending compute phase at 1705350672.414962
Starting IO phase at 1705350672.414976
Ending IO phase at 1705350701.229360
write     89.86      89.87      0.081242    327680     1024.00    0.064495   28.48      2.49       28.49      3   
Ending compute phase at 1705351061.405521
Starting IO phase at 1705351061.405535
Ending IO phase at 1705351092.512607
write     83.28      83.30      0.094290    327680     1024.00    0.049761   30.73      3.35       30.74      4   
Ending compute phase at 1705351452.655997
Starting IO phase at 1705351452.656011
Ending IO phase at 1705351496.820647
write     58.27      58.28      0.126448    327680     1024.00    0.243926   43.93      3.47       43.93      5   
Ending compute phase at 1705351857.618012
Starting IO phase at 1705351857.618028
Ending IO phase at 1705351913.955442
write     45.23      45.70      0.156370    327680     1024.00    0.588340   56.02      5.99       56.60      6   
Ending compute phase at 1705352274.151210
Starting IO phase at 1705352274.151225
Ending IO phase at 1705352334.973850
write     42.34      42.34      0.177695    327680     1024.00    0.444438   60.46      15.91      60.47      7   
Ending compute phase at 1705352695.166694
Starting IO phase at 1705352695.166708
Ending IO phase at 1705352754.655236
write     43.17      43.17      0.181052    327680     1024.00    2.45       59.30      16.54      59.30      8   
