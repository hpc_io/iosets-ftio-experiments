############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Mon Jan 15 21:04:16 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705349056.164044
TestID              : 0
StartTime           : Mon Jan 15 21:04:16 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705349421.471862
Starting IO phase at 1705349421.471879
Ending IO phase at 1705349705.276343
write     8.87       9.03       0.884478    327680     1024.00    7.18       283.59     2.71       288.55     0   
Ending compute phase at 1705350068.842676
Starting IO phase at 1705350068.842692
Ending IO phase at 1705350130.642853
write     39.45      41.67      0.191995    327680     1024.00    3.79       61.44      1.12       64.89      1   
Ending compute phase at 1705350490.785814
Starting IO phase at 1705350490.785827
Ending IO phase at 1705350523.161375
write     79.98      80.07      0.090481    327680     1024.00    0.696122   31.97      4.80       32.01      2   
Ending compute phase at 1705350883.418225
Starting IO phase at 1705350883.418238
Ending IO phase at 1705350932.496660
write     52.28      52.28      0.152471    327680     1024.00    0.230975   48.96      2.97       48.97      3   
Ending compute phase at 1705351292.710861
Starting IO phase at 1705351292.710873
Ending IO phase at 1705351331.133228
write     67.37      67.38      0.107491    327680     1024.00    0.065347   37.99      3.60       38.00      4   
Ending compute phase at 1705351691.396824
Starting IO phase at 1705351691.396842
Ending IO phase at 1705351747.372035
write     45.89      45.91      0.170083    327680     1024.00    0.070510   55.76      5.88       55.78      5   
Ending compute phase at 1705352107.550143
Starting IO phase at 1705352107.550157
Ending IO phase at 1705352163.919385
write     45.57      45.58      0.164738    327680     1024.00    0.067948   56.17      4.26       56.18      6   
Ending compute phase at 1705352526.892162
Starting IO phase at 1705352526.892179
Ending IO phase at 1705352585.452546
write     41.89      43.87      0.179376    327680     1024.00    3.05       58.36      0.956430   61.12      7   
