############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Mon Jan 15 21:04:15 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_15_ -i 10 -d 360000000
Machine             : Linux grisou-7.nancy.grid5000.fr
Starting application at 1705349055.312849
TestID              : 0
StartTime           : Mon Jan 15 21:04:15 2024
Path                : /mnt/beegfs/iosets_15_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_15_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705349415.328605
Starting IO phase at 1705349415.328617
Ending IO phase at 1705349746.792777
write     7.73       7.73       1.01        327680     1024.00    1.21       331.06     7.94       331.07     0   
Ending compute phase at 1705350110.006682
Starting IO phase at 1705350110.006701
Ending IO phase at 1705350178.207623
write     36.08      37.67      0.203955    327680     1024.00    4.39       67.95      5.21       70.95      1   
Ending compute phase at 1705350538.391254
Starting IO phase at 1705350538.391269
Ending IO phase at 1705350571.196940
write     79.02      79.04      0.087812    327680     1024.00    0.468734   32.39      4.29       32.40      2   
Ending compute phase at 1705350932.361806
Starting IO phase at 1705350932.361822
Ending IO phase at 1705350984.155938
write     48.84      49.75      0.159663    327680     1024.00    0.990484   51.45      0.995429   52.41      3   
Ending compute phase at 1705351344.346743
Starting IO phase at 1705351344.346756
Ending IO phase at 1705351386.696876
write     60.92      60.93      0.113897    327680     1024.00    0.118318   42.01      5.57       42.02      4   
Ending compute phase at 1705351746.883189
Starting IO phase at 1705351746.883201
Ending IO phase at 1705351812.330519
write     39.31      39.31      0.203509    327680     1024.00    0.100565   65.12      16.63      65.13      5   
Ending compute phase at 1705352188.878534
Starting IO phase at 1705352188.878547
Ending IO phase at 1705352246.644464
write     44.44      44.44      0.180016    327680     1024.00    0.560757   57.61      12.56      57.61      6   
Ending compute phase at 1705352612.928988
Starting IO phase at 1705352612.929004
Ending IO phase at 1705352669.008167
write     41.37      45.89      0.158775    327680     1024.00    6.24       55.79      9.68       61.88      7   
