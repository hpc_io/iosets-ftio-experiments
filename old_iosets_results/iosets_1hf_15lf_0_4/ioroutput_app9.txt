############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Jan 12 23:43:43 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_9_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705099423.760501
TestID              : 0
StartTime           : Fri Jan 12 23:43:43 2024
Path                : /mnt/beegfs/iosets_9_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_9_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705100070.133909
Starting IO phase at 1705100070.133932
Ending IO phase at 1705100159.177624
write     6.83       29.02      0.275000    327680     1024.00    286.11     88.20      12.61      374.76     0   
Ending compute phase at 1705100519.344545
Starting IO phase at 1705100519.344559
Ending IO phase at 1705100542.178044
write     114.39     114.42     0.066667    327680     1024.00    0.053947   22.37      3.53       22.38      1   
Ending compute phase at 1705100902.318329
Starting IO phase at 1705100902.318342
Ending IO phase at 1705100925.837590
write     110.83     110.89     0.057285    327680     1024.00    0.067820   23.09      4.76       23.10      2   
Ending compute phase at 1705101286.007783
Starting IO phase at 1705101286.007797
Ending IO phase at 1705101311.862662
write     100.58     100.61     0.079518    327680     1024.00    0.041128   25.45      3.14       25.45      3   
Ending compute phase at 1705101672.040859
Starting IO phase at 1705101672.040874
Ending IO phase at 1705101707.945546
write     71.82      71.83      0.101688    327680     1024.00    0.060198   35.64      3.83       35.65      4   
Ending compute phase at 1705102072.116578
Starting IO phase at 1705102072.116596
Ending IO phase at 1705102129.456483
write     41.95      44.87      0.176355    327680     1024.00    4.66       57.06      1.91       61.02      5   
Ending compute phase at 1705102489.721731
Starting IO phase at 1705102489.721746
Ending IO phase at 1705102539.616402
write     51.65      51.65      0.154213    327680     1024.00    0.055973   49.56      2.70       49.57      6   
Ending compute phase at 1705102899.753030
Starting IO phase at 1705102899.753048
Ending IO phase at 1705102954.928488
write     46.71      46.71      0.170127    327680     1024.00    0.054293   54.80      3.80       54.81      7   
