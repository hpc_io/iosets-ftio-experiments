############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Jan 12 23:43:43 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705099423.604420
TestID              : 0
StartTime           : Fri Jan 12 23:43:43 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705100184.622286
Starting IO phase at 1705100184.622301
Ending IO phase at 1705100288.134780
write     5.08       24.80      0.322534    327680     1024.00    400.93     103.23     7.51       503.81     0   
Ending compute phase at 1705100648.312510
Starting IO phase at 1705100648.312523
Ending IO phase at 1705100706.654058
write     44.10      44.10      0.128091    327680     1024.00    0.926366   58.05      17.06      58.06      1   
Ending compute phase at 1705101066.926542
Starting IO phase at 1705101066.926555
Ending IO phase at 1705101109.644209
write     60.31      60.34      0.131462    327680     1024.00    0.103212   42.43      8.53       42.45      2   
Ending compute phase at 1705101469.809133
Starting IO phase at 1705101469.809147
Ending IO phase at 1705101514.560971
write     57.65      57.66      0.133199    327680     1024.00    0.060582   44.40      1.77       44.40      3   
Ending compute phase at 1705101877.812817
Starting IO phase at 1705101877.812836
Ending IO phase at 1705101910.269655
write     72.83      79.69      0.093568    327680     1024.00    3.03       32.13      3.80       35.15      4   
Ending compute phase at 1705102271.592633
Starting IO phase at 1705102271.592647
Ending IO phase at 1705102312.943604
write     60.74      62.41      0.127694    327680     1024.00    1.15       41.02      3.50       42.15      5   
Ending compute phase at 1705102677.146450
Starting IO phase at 1705102677.146468
Ending IO phase at 1705102744.188131
write     36.19      38.36      0.168400    327680     1024.00    4.16       66.74      12.83      70.74      6   
Ending compute phase at 1705103104.323419
Starting IO phase at 1705103104.323435
Ending IO phase at 1705103179.072204
write     34.37      36.19      0.117696    327680     1024.00    0.112048   70.74      36.82      74.48      7   
