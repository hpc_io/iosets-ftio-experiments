############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Jan 12 23:43:42 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705099422.492583
TestID              : 0
StartTime           : Fri Jan 12 23:43:42 2024
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705099782.792611
Starting IO phase at 1705099782.792632
Ending IO phase at 1705099902.221144
write     21.39      21.44      0.364164    327680     1024.00    0.350792   119.40     4.36       119.68     0   
Ending compute phase at 1705100264.511552
Starting IO phase at 1705100264.511567
Ending IO phase at 1705100303.816031
write     61.94      65.70      0.120710    327680     1024.00    3.10       38.96      2.12       41.33      1   
Ending compute phase at 1705100666.026351
Starting IO phase at 1705100666.026368
Ending IO phase at 1705100706.568833
write     60.59      63.65      0.115924    327680     1024.00    3.27       40.22      3.14       42.25      2   
Ending compute phase at 1705101066.907401
Starting IO phase at 1705101066.907414
Ending IO phase at 1705101103.440981
write     70.74      70.77      0.043091    327680     1024.00    0.104714   36.17      22.38      36.19      3   
Ending compute phase at 1705101463.651308
Starting IO phase at 1705101463.651322
Ending IO phase at 1705101493.913497
write     85.30      85.60      0.084412    327680     1024.00    0.066198   29.91      5.84       30.01      4   
Ending compute phase at 1705101854.120431
Starting IO phase at 1705101854.120445
Ending IO phase at 1705101882.333644
write     91.85      91.87      0.074501    327680     1024.00    0.060025   27.86      4.02       27.87      5   
Ending compute phase at 1705102245.688649
Starting IO phase at 1705102245.688665
Ending IO phase at 1705102284.645028
write     61.23      65.86      0.120769    327680     1024.00    3.18       38.87      2.32       41.81      6   
Ending compute phase at 1705102651.283851
Starting IO phase at 1705102651.283870
Ending IO phase at 1705102705.196845
write     42.60      47.59      0.166202    327680     1024.00    7.68       53.80      1.92       60.09      7   
Ending compute phase at 1705103068.391316
Starting IO phase at 1705103068.391333
Ending IO phase at 1705103126.433341
write     42.07      44.30      0.171526    327680     1024.00    3.08       57.79      2.89       60.85      8   
