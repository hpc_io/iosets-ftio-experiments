############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Fri Jan 12 23:43:43 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705099423.424422
TestID              : 0
StartTime           : Fri Jan 12 23:43:43 2024
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705099784.251942
Starting IO phase at 1705099784.251962
Ending IO phase at 1705100072.059318
write     8.88       8.90       0.888225    327680     1024.00    0.808148   287.66     3.92       288.30     0   
Ending compute phase at 1705100432.218315
Starting IO phase at 1705100432.218328
Ending IO phase at 1705100454.696769
write     115.45     115.49     0.060700    327680     1024.00    0.054766   22.17      7.28       22.17      1   
Ending compute phase at 1705100814.948149
Starting IO phase at 1705100814.948162
Ending IO phase at 1705100838.265546
write     111.75     111.77     0.069411    327680     1024.00    0.052271   22.90      0.693334   22.91      2   
Ending compute phase at 1705101202.710777
Starting IO phase at 1705101202.710792
Ending IO phase at 1705101243.961120
write     56.83      62.72      0.119889    327680     1024.00    5.28       40.82      4.55       45.05      3   
Ending compute phase at 1705101608.022239
Starting IO phase at 1705101608.022257
Ending IO phase at 1705101658.497157
write     47.16      50.78      0.157541    327680     1024.00    3.93       50.41      2.68       54.28      4   
Ending compute phase at 1705102018.691514
Starting IO phase at 1705102018.691528
Ending IO phase at 1705102074.172178
write     46.35      46.36      0.171511    327680     1024.00    0.111753   55.22      3.09       55.24      5   
Ending compute phase at 1705102434.319059
Starting IO phase at 1705102434.319075
Ending IO phase at 1705102482.414610
write     53.57      53.58      0.139525    327680     1024.00    0.060090   47.78      3.13       47.79      6   
Ending compute phase at 1705102842.856358
Starting IO phase at 1705102842.856372
Ending IO phase at 1705102897.279365
write     46.97      47.20      0.168413    327680     1024.00    0.277476   54.24      3.24       54.50      7   
