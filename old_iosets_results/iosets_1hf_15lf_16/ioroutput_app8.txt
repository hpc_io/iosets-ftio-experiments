############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Mon Oct  2 15:32:09 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-40.nancy.grid5000.fr
Starting application at 1696253529.337671
TestID              : 0
StartTime           : Mon Oct  2 15:32:09 2023
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696254119.494737
Starting IO phase at 1696254119.494752
Ending IO phase at 1696254224.013325
write     7.66       24.63      0.322946    327680     1024.00    230.11     103.92     6.98       334.11     0   
Ending compute phase at 1696254584.176004
Starting IO phase at 1696254584.176017
Ending IO phase at 1696254607.194659
write     113.19     113.22     0.069766    327680     1024.00    0.052370   22.61      2.80       22.62      1   
Ending compute phase at 1696254967.456401
Starting IO phase at 1696254967.456413
Ending IO phase at 1696254991.598718
write     108.00     108.02     0.068360    327680     1024.00    0.053274   23.70      3.63       23.70      2   
Ending compute phase at 1696255351.800925
Starting IO phase at 1696255351.800938
Ending IO phase at 1696255377.681784
write     99.89      99.92      0.079566    327680     1024.00    0.053885   25.62      7.08       25.63      3   
Ending compute phase at 1696255737.897617
Starting IO phase at 1696255737.897630
Ending IO phase at 1696255769.326298
write     82.30      82.31      0.077042    327680     1024.00    0.047190   31.10      6.45       31.11      4   
Ending compute phase at 1696256129.508973
Starting IO phase at 1696256129.508986
Ending IO phase at 1696256174.350405
write     57.40      57.41      0.139359    327680     1024.00    0.074408   44.59      1.63       44.60      5   
Ending compute phase at 1696256534.944374
Starting IO phase at 1696256534.944388
Ending IO phase at 1696256592.852876
write     44.13      44.41      0.180146    327680     1024.00    0.388132   57.65      5.85       58.02      6   
Ending compute phase at 1696256953.811970
Starting IO phase at 1696256953.811984
Ending IO phase at 1696257013.213963
write     42.71      43.24      0.175003    327680     1024.00    0.983168   59.21      7.73       59.94      7   
