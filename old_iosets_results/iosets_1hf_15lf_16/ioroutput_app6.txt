############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Mon Oct  2 15:32:09 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_6_ -i 10 -d 360000000
Machine             : Linux grisou-40.nancy.grid5000.fr
Starting application at 1696253529.181750
TestID              : 0
StartTime           : Mon Oct  2 15:32:09 2023
Path                : /mnt/beegfs/iosets_6_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_6_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696253889.566571
Starting IO phase at 1696253889.566597
Ending IO phase at 1696254138.419021
write     10.29      10.31      0.775738    327680     1024.00    0.696291   248.24     10.57      248.78     0   
Ending compute phase at 1696254498.580361
Starting IO phase at 1696254498.580374
Ending IO phase at 1696254521.170113
write     115.03     115.10     0.064218    327680     1024.00    0.246276   22.24      3.57       22.25      1   
Ending compute phase at 1696254886.551920
Starting IO phase at 1696254886.551942
Ending IO phase at 1696254910.618667
write     88.84      108.27     0.073791    327680     1024.00    5.19       23.64      3.20       28.82      2   
Ending compute phase at 1696255270.857890
Starting IO phase at 1696255270.857905
Ending IO phase at 1696255313.836223
write     59.89      60.01      0.124925    327680     1024.00    2.02       42.66      13.04      42.74      3   
Ending compute phase at 1696255674.016884
Starting IO phase at 1696255674.016898
Ending IO phase at 1696255720.042403
write     56.04      56.05      0.136463    327680     1024.00    0.094190   45.67      2.01       45.68      4   
Ending compute phase at 1696256080.204827
Starting IO phase at 1696256080.204841
Ending IO phase at 1696256119.186838
write     66.22      66.23      0.097626    327680     1024.00    0.082610   38.65      7.41       38.66      5   
Ending compute phase at 1696256479.509242
Starting IO phase at 1696256479.509256
Ending IO phase at 1696256536.649489
write     45.01      45.08      0.175310    327680     1024.00    0.114270   56.79      7.63       56.88      6   
Ending compute phase at 1696256896.862859
Starting IO phase at 1696256896.862874
Ending IO phase at 1696256955.565862
write     43.87      43.91      0.170524    327680     1024.00    0.943503   58.29      3.73       58.35      7   
Ending compute phase at 1696257321.860415
Starting IO phase at 1696257321.860436
