############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 01:32:28 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_14_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696116748.632394
TestID              : 0
StartTime           : Sun Oct  1 01:32:28 2023
Path                : /mnt/beegfs/iosets_14_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_14_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696117119.872647
Starting IO phase at 1696117119.872674
Ending IO phase at 1696117450.455965
write     7.51       7.75       1.03        327680     1024.00    10.99      330.35     1.75       341.05     0   
Ending compute phase at 1696117825.713761
Starting IO phase at 1696117825.713778
Ending IO phase at 1696117882.711474
write     35.73      45.18      0.175324    327680     1024.00    15.11      56.66      4.30       71.64      1   
Ending compute phase at 1696118246.892963
Starting IO phase at 1696118246.892979
Ending IO phase at 1696118303.768184
write     42.28      45.25      0.176157    327680     1024.00    3.98       56.57      2.80       60.55      2   
Ending compute phase at 1696118666.180286
Starting IO phase at 1696118666.180304
Ending IO phase at 1696118727.096142
write     40.66      42.18      0.187635    327680     1024.00    2.38       60.69      3.41       62.96      3   
Ending compute phase at 1696119093.712160
Starting IO phase at 1696119093.712177
Ending IO phase at 1696119173.831241
write     29.66      32.17      0.181664    327680     1024.00    6.70       79.57      22.36      86.32      4   
Ending compute phase at 1696119538.733435
Starting IO phase at 1696119538.733454
Ending IO phase at 1696119959.834483
write     6.02       6.08       1.29        327680     1024.00    5.51       420.78     7.68       425.54     5   
Ending compute phase at 1696120325.410784
Starting IO phase at 1696120325.410803
