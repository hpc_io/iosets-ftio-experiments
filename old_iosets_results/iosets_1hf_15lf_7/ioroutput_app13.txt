############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 01:32:28 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696116748.384263
TestID              : 0
StartTime           : Sun Oct  1 01:32:28 2023
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696117109.432452
Starting IO phase at 1696117109.432473
Ending IO phase at 1696117425.943338
write     8.08       8.10       0.987484    327680     1024.00    9.66       315.99     4.34       316.96     0   
Ending compute phase at 1696117799.952037
Starting IO phase at 1696117799.952056
Ending IO phase at 1696117856.495469
write     36.53      45.46      0.171383    327680     1024.00    14.04      56.31      8.62       70.09      1   
Ending compute phase at 1696118218.823378
Starting IO phase at 1696118218.823396
Ending IO phase at 1696118275.178835
write     43.94      45.61      0.174696    327680     1024.00    2.27       56.13      14.38      58.26      2   
Ending compute phase at 1696118639.348184
Starting IO phase at 1696118639.348201
Ending IO phase at 1696118698.387058
write     40.85      43.61      0.179036    327680     1024.00    4.49       58.70      2.96       62.67      3   
Ending compute phase at 1696119064.560768
Starting IO phase at 1696119064.560786
Ending IO phase at 1696119126.987351
write     37.50      41.34      0.180244    327680     1024.00    6.05       61.92      4.57       68.27      4   
Ending compute phase at 1696119489.826652
Starting IO phase at 1696119489.826669
Ending IO phase at 1696119552.827026
write     39.20      40.94      0.195393    327680     1024.00    2.74       62.53      7.48       65.31      5   
Ending compute phase at 1696119915.870317
Starting IO phase at 1696119915.870332
Ending IO phase at 1696119992.895247
write     32.22      33.41      0.236077    327680     1024.00    5.92       76.62      5.04       79.46      6   
Ending compute phase at 1696120359.650198
Starting IO phase at 1696120359.650218
Ending IO phase at 1696120428.127560
write     34.23      37.52      0.203868    327680     1024.00    7.15       68.23      12.13      74.78      7   
