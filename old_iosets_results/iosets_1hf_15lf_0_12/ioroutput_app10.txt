############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 15:42:38 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705156958.072730
TestID              : 0
StartTime           : Sat Jan 13 15:42:38 2024
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705157538.992600
Starting IO phase at 1705157538.992621
Ending IO phase at 1705157594.291942
write     9.34       45.56      0.171726    327680     1024.00    219.08     56.19      6.82       274.14     0   
Ending compute phase at 1705157964.380827
Starting IO phase at 1705157964.380845
Ending IO phase at 1705158020.007901
write     39.28      46.33      0.168082    327680     1024.00    10.12      55.26      4.37       65.18      1   
Ending compute phase at 1705158383.097468
Starting IO phase at 1705158383.097487
Ending IO phase at 1705158443.302144
write     40.76      42.74      0.187198    327680     1024.00    3.10       59.90      9.77       62.81      2   
Ending compute phase at 1705158804.872651
Starting IO phase at 1705158804.872669
Ending IO phase at 1705158868.032469
write     39.80      40.69      0.190153    327680     1024.00    3.34       62.91      5.90       64.31      3   
Ending compute phase at 1705159228.214038
Starting IO phase at 1705159228.214053
Ending IO phase at 1705159293.995694
write     39.12      39.19      0.204124    327680     1024.00    0.082989   65.32      8.23       65.44      4   
Ending compute phase at 1705159659.645879
Starting IO phase at 1705159659.645897
Ending IO phase at 1705159720.252203
write     38.93      42.53      0.188116    327680     1024.00    7.12       60.20      16.93      65.76      5   
Ending compute phase at 1705160089.518971
Starting IO phase at 1705160089.518987
Ending IO phase at 1705160151.409028
write     36.25      41.38      0.160343    327680     1024.00    10.70      61.87      13.16      70.63      6   
Ending compute phase at 1705160520.132712
Starting IO phase at 1705160520.132731
Ending IO phase at 1705160578.783041
write     38.23      44.01      0.175128    327680     1024.00    8.97       58.17      7.17       66.96      7   
