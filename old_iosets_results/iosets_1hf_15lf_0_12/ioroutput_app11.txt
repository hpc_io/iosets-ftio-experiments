############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 15:42:38 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_11_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705156958.184432
TestID              : 0
StartTime           : Sat Jan 13 15:42:38 2024
Path                : /mnt/beegfs/iosets_11_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_11_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705157539.184764
Starting IO phase at 1705157539.184781
Ending IO phase at 1705157623.468106
write     8.44       30.45      0.262251    327680     1024.00    219.48     84.07      10.64      303.24     0   
Ending compute phase at 1705157991.116210
Starting IO phase at 1705157991.116229
Ending IO phase at 1705158047.696245
write     40.08      45.41      0.175195    327680     1024.00    8.08       56.38      3.83       63.88      1   
Ending compute phase at 1705158412.620603
Starting IO phase at 1705158412.620621
Ending IO phase at 1705158471.702764
write     40.40      43.62      0.183404    327680     1024.00    4.70       58.69      4.12       63.37      2   
Ending compute phase at 1705158838.319233
Starting IO phase at 1705158838.319249
Ending IO phase at 1705158896.687806
write     39.69      43.94      0.180817    327680     1024.00    6.44       58.26      10.75      64.50      3   
Ending compute phase at 1705159257.096827
Starting IO phase at 1705159257.096845
Ending IO phase at 1705159319.013836
write     41.42      41.54      0.191059    327680     1024.00    0.202406   61.62      4.03       61.81      4   
Ending compute phase at 1705159680.495118
Starting IO phase at 1705159680.495134
Ending IO phase at 1705159750.024036
write     36.33      36.99      0.194688    327680     1024.00    1.33       69.21      6.91       70.47      5   
Ending compute phase at 1705160111.837417
Starting IO phase at 1705160111.837438
Ending IO phase at 1705160178.221855
write     37.80      38.74      0.197424    327680     1024.00    1.65       66.07      2.93       67.72      6   
Ending compute phase at 1705160540.771899
Starting IO phase at 1705160540.771919
Ending IO phase at 1705160611.876718
write     34.98      36.23      0.220509    327680     1024.00    2.65       70.66      12.61      73.19      7   
