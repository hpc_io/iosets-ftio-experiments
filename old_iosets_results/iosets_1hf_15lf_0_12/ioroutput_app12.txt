############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 15:42:37 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705156957.848412
TestID              : 0
StartTime           : Sat Jan 13 15:42:38 2024
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705157320.680353
Starting IO phase at 1705157320.680371
Ending IO phase at 1705157652.263745
write     7.67       7.73       1.01        327680     1024.00    6.18       331.34     7.62       333.98     0   
Ending compute phase at 1705158019.272332
Starting IO phase at 1705158019.272352
Ending IO phase at 1705158075.471796
write     40.70      45.70      0.175028    327680     1024.00    6.88       56.02      7.35       62.89      1   
Ending compute phase at 1705158440.759621
Starting IO phase at 1705158440.759639
Ending IO phase at 1705158500.230544
write     39.85      43.30      0.184769    327680     1024.00    5.58       59.13      11.05      64.24      2   
Ending compute phase at 1705158866.604826
Starting IO phase at 1705158866.604846
Ending IO phase at 1705158925.965795
write     39.21      43.34      0.184605    327680     1024.00    6.25       59.07      8.03       65.29      3   
Ending compute phase at 1705159286.814008
Starting IO phase at 1705159286.814024
Ending IO phase at 1705159348.648568
write     41.20      41.64      0.189781    327680     1024.00    0.854462   61.48      6.33       62.14      4   
Ending compute phase at 1705159714.189658
Starting IO phase at 1705159714.189674
Ending IO phase at 1705159780.746124
write     35.68      38.58      0.195348    327680     1024.00    5.66       66.35      3.84       71.75      5   
Ending compute phase at 1705160142.370862
Starting IO phase at 1705160142.370877
Ending IO phase at 1705160215.523240
write     34.45      35.16      0.225739    327680     1024.00    1.63       72.81      9.56       74.30      6   
Ending compute phase at 1705160577.605033
Starting IO phase at 1705160577.605056
Ending IO phase at 1705160647.467455
write     35.76      36.79      0.190186    327680     1024.00    1.98       69.59      9.17       71.58      7   
