############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 15:42:37 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_6_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705156957.928713
TestID              : 0
StartTime           : Sat Jan 13 15:42:38 2024
Path                : /mnt/beegfs/iosets_6_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_6_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705157377.968381
Starting IO phase at 1705157377.968394
Ending IO phase at 1705157482.931905
write     15.57      24.19      0.293233    327680     1024.00    61.10      105.84     10.89      164.39     0   
Ending compute phase at 1705157848.113476
Starting IO phase at 1705157848.113494
Ending IO phase at 1705157909.211228
write     38.84      42.06      0.185801    327680     1024.00    5.55       60.86      6.55       65.91      1   
Ending compute phase at 1705158270.521988
Starting IO phase at 1705158270.522008
Ending IO phase at 1705158328.310811
write     43.63      44.50      0.172161    327680     1024.00    1.17       57.53      6.63       58.67      2   
Ending compute phase at 1705158690.243295
Starting IO phase at 1705158690.243316
Ending IO phase at 1705158750.980136
write     41.16      42.16      0.188727    327680     1024.00    1.74       60.72      8.19       62.20      3   
Ending compute phase at 1705159111.140619
Starting IO phase at 1705159111.140633
Ending IO phase at 1705159168.592687
write     44.81      44.81      0.171442    327680     1024.00    0.201466   57.12      2.26       57.13      4   
Ending compute phase at 1705159530.832290
Starting IO phase at 1705159530.832315
Ending IO phase at 1705159592.945719
write     40.04      41.38      0.192850    327680     1024.00    2.19       61.87      24.94      63.94      5   
Ending compute phase at 1705159954.882671
Starting IO phase at 1705159954.882688
Ending IO phase at 1705160018.251668
write     39.44      40.53      0.192242    327680     1024.00    2.08       63.17      7.96       64.92      6   
Ending compute phase at 1705160378.399566
Starting IO phase at 1705160378.399580
Ending IO phase at 1705160430.797469
write     49.20      49.20      0.161718    327680     1024.00    0.063646   52.03      5.59       52.04      7   
