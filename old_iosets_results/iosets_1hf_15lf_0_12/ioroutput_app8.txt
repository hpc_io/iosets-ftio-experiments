############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 15:42:37 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_8_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705156957.984747
TestID              : 0
StartTime           : Sat Jan 13 15:42:38 2024
Path                : /mnt/beegfs/iosets_8_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_8_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705157375.244182
Starting IO phase at 1705157375.244202
Ending IO phase at 1705157540.093130
write     11.55      15.49      0.510169    327680     1024.00    57.45      165.30     8.43       221.70     0   
Ending compute phase at 1705157904.580457
Starting IO phase at 1705157904.580475
Ending IO phase at 1705157965.631693
write     39.32      41.65      0.188143    327680     1024.00    4.38       61.47      5.20       65.10      1   
Ending compute phase at 1705158325.854605
Starting IO phase at 1705158325.854623
Ending IO phase at 1705158384.938638
write     43.46      43.50      0.177779    327680     1024.00    0.734280   58.84      3.70       58.90      2   
Ending compute phase at 1705158747.533815
Starting IO phase at 1705158747.533830
Ending IO phase at 1705158810.648130
write     39.18      40.70      0.185638    327680     1024.00    2.51       62.89      11.96      65.34      3   
Ending compute phase at 1705159170.931722
Starting IO phase at 1705159170.931736
Ending IO phase at 1705159228.293136
write     44.93      44.96      0.174680    327680     1024.00    0.281880   56.95      2.55       56.98      4   
Ending compute phase at 1705159590.081902
Starting IO phase at 1705159590.081918
Ending IO phase at 1705159655.293176
write     38.41      39.82      0.198730    327680     1024.00    1.93       64.30      16.47      66.65      5   
Ending compute phase at 1705160017.424467
Starting IO phase at 1705160017.424483
Ending IO phase at 1705160080.311936
write     39.70      40.94      0.188338    327680     1024.00    2.18       62.53      11.12      64.49      6   
Ending compute phase at 1705160440.580014
Starting IO phase at 1705160440.580027
Ending IO phase at 1705160496.269138
write     46.25      46.29      0.131432    327680     1024.00    0.071097   55.31      13.28      55.35      7   
