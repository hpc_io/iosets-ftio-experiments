############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 21:16:42 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_7_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696101402.428185
TestID              : 0
StartTime           : Sat Sep 30 21:16:42 2023
Path                : /mnt/beegfs/iosets_7_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_7_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696101774.103039
Starting IO phase at 1696101774.103073
Ending IO phase at 1696101926.038250
write     15.69      16.85      0.466529    327680     1024.00    13.16      151.90     4.22       163.18     0   
Ending compute phase at 1696102288.831815
Starting IO phase at 1696102288.831841
Ending IO phase at 1696102335.115343
write     52.67      55.75      0.143025    327680     1024.00    2.64       45.92      2.57       48.61      1   
Ending compute phase at 1696102698.553921
Starting IO phase at 1696102698.553946
Ending IO phase at 1696102729.177023
write     76.37      84.47      0.085987    327680     1024.00    3.41       30.31      2.79       33.52      2   
Ending compute phase at 1696103091.512344
Starting IO phase at 1696103091.512359
Ending IO phase at 1696103134.964963
write     56.58      59.40      0.131706    327680     1024.00    2.15       43.10      1.49       45.25      3   
Ending compute phase at 1696103495.414757
Starting IO phase at 1696103495.414775
Ending IO phase at 1696103552.843764
write     44.54      44.78      0.164612    327680     1024.00    0.302360   57.17      5.55       57.48      4   
Ending compute phase at 1696103913.330145
Starting IO phase at 1696103913.330163
Ending IO phase at 1696103973.039782
write     42.92      43.12      0.175336    327680     1024.00    0.306678   59.37      7.75       59.64      5   
Ending compute phase at 1696104338.735538
Starting IO phase at 1696104338.735552
Ending IO phase at 1696104401.960137
write     37.39      40.73      0.195126    327680     1024.00    6.84       62.86      1.22       68.47      6   
Ending compute phase at 1696104763.882796
Starting IO phase at 1696104763.882812
Ending IO phase at 1696104812.176364
write     51.35      53.27      0.147775    327680     1024.00    1.80       48.05      14.27      49.86      7   
Ending compute phase at 1696105175.151996
Starting IO phase at 1696105175.152015
