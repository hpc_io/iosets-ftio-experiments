############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 21:16:41 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696101401.936192
TestID              : 0
StartTime           : Sat Sep 30 21:16:42 2023
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696101762.259780
Starting IO phase at 1696101762.259802
Ending IO phase at 1696102060.439871
write     8.59       8.59       0.930137    327680     1024.00    0.254261   297.92     8.52       297.99     0   
Ending compute phase at 1696102431.619246
Starting IO phase at 1696102431.619266
Ending IO phase at 1696102479.443589
write     43.78      53.99      0.144459    327680     1024.00    10.96      47.41      2.97       58.47      1   
Ending compute phase at 1696102839.631217
Starting IO phase at 1696102839.631232
Ending IO phase at 1696102887.885596
write     53.44      53.44      0.148678    327680     1024.00    0.141173   47.90      2.25       47.91      2   
Ending compute phase at 1696103248.083411
Starting IO phase at 1696103248.083427
Ending IO phase at 1696103303.346831
write     46.49      46.49      0.171407    327680     1024.00    0.059012   55.06      2.21       55.07      3   
Ending compute phase at 1696103663.646349
Starting IO phase at 1696103663.646362
Ending IO phase at 1696103720.505236
write     45.29      45.29      0.174333    327680     1024.00    0.087715   56.52      8.08       56.53      4   
Ending compute phase at 1696104083.303503
Starting IO phase at 1696104083.303521
Ending IO phase at 1696104141.913177
write     41.93      43.78      0.180917    327680     1024.00    3.22       58.47      15.46      61.05      5   
Ending compute phase at 1696104516.163885
Starting IO phase at 1696104516.163914
Ending IO phase at 1696104576.161453
write     34.71      42.87      0.186614    327680     1024.00    14.05      59.72      12.54      73.76      6   
Ending compute phase at 1696104941.328727
Starting IO phase at 1696104941.328748
Ending IO phase at 1696105005.135349
write     37.40      40.34      0.190337    327680     1024.00    4.99       63.46      2.59       68.45      7   
