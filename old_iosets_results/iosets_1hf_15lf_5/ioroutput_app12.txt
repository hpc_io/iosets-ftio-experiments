############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Sep 30 21:16:42 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_12_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696101402.428091
TestID              : 0
StartTime           : Sat Sep 30 21:16:42 2023
Path                : /mnt/beegfs/iosets_12_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_12_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696101775.027272
Starting IO phase at 1696101775.027295
Ending IO phase at 1696102036.849200
write     9.35       9.73       0.810700    327680     1024.00    12.92      263.10     15.57      273.87     0   
Ending compute phase at 1696102406.787514
Starting IO phase at 1696102406.787531
Ending IO phase at 1696102455.555855
write     43.92      52.81      0.151042    327680     1024.00    11.23      48.48      3.06       58.28      1   
Ending compute phase at 1696102815.744466
Starting IO phase at 1696102815.744481
Ending IO phase at 1696102860.477606
write     57.54      57.55      0.137774    327680     1024.00    0.059983   44.49      2.28       44.49      2   
Ending compute phase at 1696103220.683721
Starting IO phase at 1696103220.683735
Ending IO phase at 1696103274.345148
write     47.95      47.95      0.163601    327680     1024.00    0.105801   53.39      2.80       53.39      3   
Ending compute phase at 1696103634.720922
Starting IO phase at 1696103634.720940
Ending IO phase at 1696103692.080045
write     44.70      44.91      0.176794    327680     1024.00    0.230001   57.00      5.88       57.27      4   
Ending compute phase at 1696104054.398100
Starting IO phase at 1696104054.398118
Ending IO phase at 1696104113.495221
write     41.96      43.50      0.180991    327680     1024.00    2.18       58.86      4.18       61.01      5   
Ending compute phase at 1696104487.004691
Starting IO phase at 1696104487.004710
Ending IO phase at 1696104546.522336
write     35.27      42.87      0.185165    327680     1024.00    13.33      59.71      19.27      72.58      6   
Ending compute phase at 1696104913.348255
Starting IO phase at 1696104913.348275
Ending IO phase at 1696104973.343625
write     38.56      42.82      0.174882    327680     1024.00    6.71       59.79      3.82       66.39      7   
