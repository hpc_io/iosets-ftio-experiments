import os
import subprocess
from time import sleep

from file_functions import create_folder,get_first_line

#HARDCODED EXPERIMENT PARAMETERS
procs = 8 #number of processes for each application
nodes = 1 #number of nodes for each application
test_duration=3800 #in seconds
ior = "/root/ior_schedclient_tmio/src/ior" #path to executable
ior_general_parameters = "-a MPIIO -F -t 1M -w -e"
output_path = "/mnt/beegfs/iosets_" #prefix of where ior will write (during the test)
running_path = "/root/running_ior/" #from where we'll call ior (where tmio will write the trace)
caller = "/root/call_predictor/call_predictor"
hf_block = "16M" #amount of data per process (-b for ior)
lf_block = "320M"
hf_iter = 200 #number of iterations (-i for ior)
lf_iter = 10
hf_comp = 18000000 #in microseconds, time between I/O phases (-d for ior)
lf_comp = 360000000
client_file = "/root/clients" #path to the file containing the list of nodes that are clients
#priorities for when not using ftio
hf_prio = 1000
lf_prio = 10

class IOSetsExperiment:
    def __init__(self, test_string):
        """
        test_string format (in a single line, separated by blank spaces, without the []):
        [version (no_iosets, iosets, or iosets_ftio)] [number of high-frequency apps]
        [number of low-frequency apps] [path for all log files (a directory, which will be erased)] [induced percentual error (optional)]
        """
        parsed = test_string.split()
        self.version = parsed[0]
        assert self.version in ["no_iosets", "iosets", "iosets_ftio"]
        self.hf = int(parsed[1])
        self.lf = int(parsed[2])
        self.napps = self.hf+self.lf
        print(f"I will generate {self.hf} high-frequency apps and {self.lf} low-frequency ones, for a total of {self.napps} applications")
        self.log_path = parsed[3]
        if self.log_path[-1] != "/":
            self.log_path += "/"
        if len(parsed) == 5:
            self.error = int(parsed[4])
        else:
            self.error = 0

    def get_first_client(self):
        filename = os.getenv("OAR_JOB_ID")
        assert filename != None
        filename +=  "_hostfile"
        return get_first_line(filename)

    def run(self):
        create_folder(self.log_path)
        caller_node = self.get_first_client() #TODO below we just run everything on this node, if we don't want that we need to change it
        subprocess.run(f"ssh root@{caller_node} mkdir {running_path}", shell=True)
        #restart beegfs using iosets or not (depending on the test)
        if self.version == "no_iosets":
            subprocess.run("/home/fboito/disable_iosets", shell=True)
        else:
            subprocess.run("/home/fboito/enable_iosets", shell=True)
        #first we'll generate all command lines, then we'll launch them.
        #Each application makes two lines, one for the call_predictor and another for IOR.
        commands = []
        caller_commands = []
        for i in range(self.napps):
            appid = i+1 #each application is identified by this (which will also be its jobid)
            if i < self.hf:
                #make a high-frequency app
                b = hf_block
                itera = hf_iter
                d = hf_comp
                prio =  hf_prio
            else:
                #make a low-frequency app
                b = lf_block
                itera = lf_iter
                d = lf_comp
                prio =  lf_prio
            #first we need to create a folder where ior will run, because otherwise tmio-generated files will collide
            myfolder = running_path+str(appid)+"/"
            subprocess.run(f"ssh root@{caller_node} rm -rf {myfolder}", shell=True)
            subprocess.run(f"ssh root@{caller_node} mkdir {myfolder}", shell=True)
            tmio_trace_path = myfolder+str(procs)+".jsonl"
            caller_trace_path = myfolder+"predictions_app"+str(i)+".csv"
            #now we craete a command to launch the predictor and the code that will communicate its results to the beegfs client. it has to be executed before IOR
            #this is only needed when using ftio
            if self.version == "iosets_ftio":
                cmd = "ssh root@"+caller_node+" timeout "+str(test_duration)+" mpirun -np 1 --allow-run-as-root -hostfile "+client_file+" "
                cmd += "-npernode 1 -x IOSETS_JOB_ID="+str(appid)+" "+caller+" "
                cmd += tmio_trace_path+" "+caller_trace_path+" "
                if self.error > 0:
                    cmd+= str(self.error) + " "
                cmd += "> "+self.log_path+"calleroutput_app"+str(i)+".txt & "
                caller_commands.append(cmd)
            else:
                #without ftio, they get the priority directly written to the file
                #(this is not relevant to no_iosets, but I left it in case my strategy to disable iosets is not working (then I should get the same results for iosets and no_iosets))
                subprocess.run(f"ssh root@{caller_node} mkdir /sys/kernel/config/iosets/", shell=True)
                subprocess.run(f"ssh root@{caller_node} mkdir /sys/kernel/config/iosets/{appid}", shell=True)
                subprocess.run(f"ssh root@{caller_node} sh /root/put_priority.sh {prio} /sys/kernel/config/iosets/{appid}/priority", shell=True)
            #NOW we create the IOR command
            com = "ssh root@"+caller_node+" timeout "+str(test_duration)+" mpirun --allow-run-as-root "
            com += "-np "+str(procs)+" -hostfile "+client_file+" "
            com+= "-npernode "+str(procs//nodes)+" -wdir "+myfolder+" -x IOSETS_JOB_ID="+str(appid)+" "
            com+= ior+" "+ior_general_parameters+" "
            com+= "-b "+b+" -o "+output_path+str(i)+"_ -i "+str(itera)+" -d "+str(d)+" "
            com+= "> "+self.log_path+"ioroutput_app"+str(i)+".txt & "
            commands.append(com)
        assert len(commands) == self.napps
        if self.version ==  "iosets_ftio":
            assert len(caller_commands) == self.napps
        print(f"starting applications... (I'll stop them after {test_duration} seconds)")
        #we start all the caller codes, then we sleep for a little bit, then we start all IOR instances
        for c in caller_commands:
            print(c)
            subprocess.run(c, shell=True)
        sleep(3)
        for c in commands:
            print(c)
            subprocess.run(c, shell=True)
        print("sleeping for "+str(test_duration+5)+"...")
        sleep(test_duration+5)
        print("will stop all processes")
        subprocess.run(f"ssh root@{caller_node} pkill -9 mpirun", shell=True)
        subprocess.run(f"ssh root@{caller_node} pkill -9 ior", shell=True)
        subprocess.run(f"ssh root@{caller_node} pkill -9 python3", shell=True)
        subprocess.run(f"ssh root@{caller_node} pkill -9 call_predictor", shell=True)
        sleep(3)
        #copy the output
        print("will copy the experiment results back")
        subprocess.run(f"ssh root@{caller_node} sh /root/cat_priority.sh {self.napps} /root/priorities_in_the_end.txt", shell=True)
        subprocess.run(f"scp root@{caller_node}:~/priorities_in_the_end.txt {self.log_path}", shell=True)
        subprocess.run(f"ssh root@{caller_node} chmod -R 777 {running_path}", shell=True)
        subprocess.run(f"ssh root@{caller_node} tar czf results.tgz {running_path}", shell=True)
        subprocess.run(f"scp root@{caller_node}:~/results.tgz {self.log_path}", shell=True)
        subprocess.run(f"ssh root@{caller_node} rm -rf {running_path}", shell=True)
        subprocess.run(f"ssh root@{caller_node} rm -rf /mnt/beegfs/*", shell=True)
        sleep(5)
        print("done with the test.")
