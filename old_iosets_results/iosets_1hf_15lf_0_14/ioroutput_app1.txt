############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 05:03:22 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705118602.021037
TestID              : 0
StartTime           : Sat Jan 13 05:03:22 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705118962.102866
Starting IO phase at 1705118962.102881
Ending IO phase at 1705119008.117271
write     55.60      57.83      0.136009    327680     1024.00    0.286704   44.27      22.26      46.04      0   
Ending compute phase at 1705119368.155074
Starting IO phase at 1705119368.155088
Ending IO phase at 1705119403.020568
write     73.92      74.10      0.052717    327680     1024.00    0.071841   34.55      17.76      34.63      1   
Ending compute phase at 1705119763.214607
Starting IO phase at 1705119763.214621
Ending IO phase at 1705119792.721609
write     87.88      87.89      0.071893    327680     1024.00    0.067668   29.13      7.07       29.13      2   
Ending compute phase at 1705120153.886167
Starting IO phase at 1705120153.886182
Ending IO phase at 1705120209.634393
write     45.26      46.08      0.168566    327680     1024.00    1.01       55.55      4.91       56.57      3   
Ending compute phase at 1705120572.319405
Starting IO phase at 1705120572.319425
Ending IO phase at 1705120625.060336
write     46.54      48.74      0.149875    327680     1024.00    3.08       52.52      5.38       55.01      4   
Ending compute phase at 1705120989.668463
Starting IO phase at 1705120989.668479
Ending IO phase at 1705121026.465461
write     62.57      70.13      0.100868    327680     1024.00    5.13       36.50      12.74      40.92      5   
Ending compute phase at 1705121390.836820
Starting IO phase at 1705121390.836839
Ending IO phase at 1705121453.670348
write     38.36      40.89      0.194159    327680     1024.00    4.42       62.60      1.00       66.74      6   
Ending compute phase at 1705121814.055593
Starting IO phase at 1705121814.055617
Ending IO phase at 1705121870.747810
write     45.25      45.36      0.137038    327680     1024.00    0.249345   56.44      22.79      56.57      7   
Ending compute phase at 1705122249.608338
Starting IO phase at 1705122249.608358
Ending IO phase at 1705122291.872774
write     42.28      60.46      0.130888    327680     1024.00    18.77      42.34      18.56      60.55      8   
