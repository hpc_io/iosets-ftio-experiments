############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 05:03:22 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_13_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705118602.932418
TestID              : 0
StartTime           : Sat Jan 13 05:03:23 2024
Path                : /mnt/beegfs/iosets_13_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_13_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705118967.998693
Starting IO phase at 1705118967.998710
Ending IO phase at 1705119266.272528
write     8.45       8.55       0.930586    327680     1024.00    7.02       299.51     2.13       302.78     0   
Ending compute phase at 1705119640.584372
Starting IO phase at 1705119640.584388
Ending IO phase at 1705119692.600056
write     38.84      49.51      0.150094    327680     1024.00    14.21      51.71      3.68       65.91      1   
Ending compute phase at 1705120067.349443
Starting IO phase at 1705120067.349464
Ending IO phase at 1705120125.844588
write     35.19      44.02      0.181713    327680     1024.00    14.51      58.16      8.94       72.75      2   
Ending compute phase at 1705120489.287588
Starting IO phase at 1705120489.287606
Ending IO phase at 1705120546.152107
write     42.74      45.21      0.172845    327680     1024.00    3.43       56.62      8.29       59.90      3   
Ending compute phase at 1705120908.245213
Starting IO phase at 1705120908.245231
Ending IO phase at 1705120967.384326
write     42.18      43.54      0.181283    327680     1024.00    2.05       58.79      11.24      60.69      4   
Ending compute phase at 1705121333.932708
Starting IO phase at 1705121333.932726
Ending IO phase at 1705121394.676039
write     38.31      42.31      0.186065    327680     1024.00    6.35       60.51      4.41       66.82      5   
Ending compute phase at 1705121754.867878
Starting IO phase at 1705121754.867892
Ending IO phase at 1705121811.336819
write     45.58      45.59      0.168118    327680     1024.00    0.150245   56.16      2.71       56.16      6   
Ending compute phase at 1705122177.088514
Starting IO phase at 1705122177.088531
Ending IO phase at 1705122252.379878
write     31.77      34.12      0.225625    327680     1024.00    5.68       75.02      16.53      80.57      7   
