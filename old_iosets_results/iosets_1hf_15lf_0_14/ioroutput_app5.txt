############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 05:03:22 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705118602.804422
TestID              : 0
StartTime           : Sat Jan 13 05:03:23 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705118964.248662
Starting IO phase at 1705118964.248683
Ending IO phase at 1705119075.943212
write     22.69      22.92      0.348361    327680     1024.00    2.23       111.69     2.43       112.81     0   
Ending compute phase at 1705119440.167385
Starting IO phase at 1705119440.167403
Ending IO phase at 1705119491.011192
write     46.75      50.53      0.141784    327680     1024.00    5.17       50.66      5.27       54.76      1   
Ending compute phase at 1705119851.249330
Starting IO phase at 1705119851.249345
Ending IO phase at 1705119904.661100
write     48.20      48.22      0.159025    327680     1024.00    0.660763   53.10      2.87       53.11      2   
Ending compute phase at 1705120264.890809
Starting IO phase at 1705120264.890827
Ending IO phase at 1705120322.164269
write     44.82      44.93      0.176891    327680     1024.00    0.171562   56.98      11.55      57.11      3   
Ending compute phase at 1705120682.371436
Starting IO phase at 1705120682.371450
Ending IO phase at 1705120738.779778
write     45.60      45.62      0.167207    327680     1024.00    0.062876   56.12      2.63       56.14      4   
Ending compute phase at 1705121102.536633
Starting IO phase at 1705121102.536650
Ending IO phase at 1705121162.876699
write     40.26      42.65      0.183914    327680     1024.00    3.91       60.02      1.94       63.58      5   
Ending compute phase at 1705121523.086642
Starting IO phase at 1705121523.086657
Ending IO phase at 1705121573.153688
write     51.46      51.46      0.147006    327680     1024.00    0.047932   49.75      2.71       49.75      6   
Ending compute phase at 1705121934.981429
Starting IO phase at 1705121934.981445
Ending IO phase at 1705122000.850508
write     38.03      39.00      0.203664    327680     1024.00    2.35       65.64      8.29       67.32      7   
Ending compute phase at 1705122367.158318
Starting IO phase at 1705122367.158334
