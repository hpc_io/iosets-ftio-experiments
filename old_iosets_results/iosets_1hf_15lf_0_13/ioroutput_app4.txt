############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 07:11:18 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_4_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705126278.748430
TestID              : 0
StartTime           : Sat Jan 13 07:11:18 2024
Path                : /mnt/beegfs/iosets_4_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_4_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705126650.692180
Starting IO phase at 1705126650.692196
Ending IO phase at 1705126787.031818
write     17.29      18.67      0.423422    327680     1024.00    13.64      137.10     9.31       148.03     0   
Ending compute phase at 1705127173.436636
Starting IO phase at 1705127173.436657
Ending IO phase at 1705127255.298692
write     23.75      31.42      0.253704    327680     1024.00    26.33      81.46      20.54      107.79     1   
Ending compute phase at 1705127626.866341
Starting IO phase at 1705127626.866360
Ending IO phase at 1705127713.760884
write     26.13      29.60      0.270251    327680     1024.00    11.99      86.48      0.399098   97.97      2   
Ending compute phase at 1705128084.366459
Starting IO phase at 1705128084.366479
Ending IO phase at 1705128158.208431
write     30.47      34.80      0.214722    327680     1024.00    10.65      73.57      4.86       84.03      3   
Ending compute phase at 1705128523.340333
Starting IO phase at 1705128523.340351
Ending IO phase at 1705128576.676468
write     44.13      48.08      0.164465    327680     1024.00    5.94       53.24      1.31       58.01      4   
Ending compute phase at 1705128951.037683
Starting IO phase at 1705128951.037699
Ending IO phase at 1705129005.259974
write     37.59      47.45      0.137634    327680     1024.00    14.26      53.95      9.91       68.10      5   
Ending compute phase at 1705129375.475530
Starting IO phase at 1705129375.475545
Ending IO phase at 1705129416.691236
write     49.99      62.54      0.116489    327680     1024.00    10.65      40.93      5.25       51.21      6   
Ending compute phase at 1705129776.825349
Starting IO phase at 1705129776.825363
Ending IO phase at 1705129810.765739
write     76.03      76.04      0.099628    327680     1024.00    0.069050   33.66      3.07       33.67      7   
