############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 00:47:42 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_11_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705103262.233809
TestID              : 0
StartTime           : Sat Jan 13 00:47:42 2024
Path                : /mnt/beegfs/iosets_11_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_11_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705103856.911089
Starting IO phase at 1705103856.911111
Ending IO phase at 1705103944.023844
write     7.98       29.18      0.271302    327680     1024.00    234.22     87.74      3.82       320.94     0   
Ending compute phase at 1705104322.334241
Starting IO phase at 1705104322.334260
Ending IO phase at 1705104384.323980
write     32.10      41.56      0.191202    327680     1024.00    18.28      61.60      7.49       79.74      1   
Ending compute phase at 1705104751.557829
Starting IO phase at 1705104751.557847
Ending IO phase at 1705104820.228315
write     33.94      37.43      0.202158    327680     1024.00    7.09       68.40      11.35      75.43      2   
Ending compute phase at 1705105187.281372
Starting IO phase at 1705105187.281392
Ending IO phase at 1705105256.296010
write     33.87      37.25      0.195796    327680     1024.00    6.95       68.72      6.07       75.59      3   
Ending compute phase at 1705105619.799361
Starting IO phase at 1705105619.799375
Ending IO phase at 1705105684.628196
write     37.72      39.64      0.132496    327680     1024.00    4.15       64.59      22.19      67.87      4   
Ending compute phase at 1705106047.868838
Starting IO phase at 1705106047.868857
Ending IO phase at 1705106116.148468
write     36.13      37.74      0.211066    327680     1024.00    3.03       67.83      9.89       70.85      5   
Ending compute phase at 1705106481.408995
Starting IO phase at 1705106481.409014
Ending IO phase at 1705106545.882301
write     36.97      39.97      0.191552    327680     1024.00    5.71       64.04      11.32      69.24      6   
Ending compute phase at 1705106921.685951
Starting IO phase at 1705106921.685966
Ending IO phase at 1705106998.731323
write     27.73      33.40      0.238071    327680     1024.00    22.49      76.65      2.28       92.33      7   
