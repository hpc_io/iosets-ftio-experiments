############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 00:47:42 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_5_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705103262.116410
TestID              : 0
StartTime           : Sat Jan 13 00:47:42 2024
Path                : /mnt/beegfs/iosets_5_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_5_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705103708.226306
Starting IO phase at 1705103708.226326
Ending IO phase at 1705103767.361657
write     17.70      43.29      0.184393    327680     1024.00    85.65      59.13      5.20       144.63     0   
Ending compute phase at 1705104141.700381
Starting IO phase at 1705104141.700401
Ending IO phase at 1705104206.211063
write     32.63      39.84      0.162289    327680     1024.00    14.23      64.26      12.33      78.44      1   
Ending compute phase at 1705104568.088251
Starting IO phase at 1705104568.088266
Ending IO phase at 1705104634.572144
write     37.76      38.72      0.203212    327680     1024.00    4.54       66.11      4.76       67.79      2   
Ending compute phase at 1705104996.542556
Starting IO phase at 1705104996.542572
Ending IO phase at 1705105064.251912
write     36.97      37.99      0.197116    327680     1024.00    1.94       67.38      4.39       69.24      3   
Ending compute phase at 1705105427.122548
Starting IO phase at 1705105427.122564
Ending IO phase at 1705105493.068412
write     37.45      38.97      0.205277    327680     1024.00    3.00       65.69      4.73       68.35      4   
Ending compute phase at 1705105856.103911
Starting IO phase at 1705105856.103931
Ending IO phase at 1705105924.228008
write     36.14      37.69      0.210592    327680     1024.00    3.10       67.93      2.06       70.83      5   
Ending compute phase at 1705106284.419819
Starting IO phase at 1705106284.419834
Ending IO phase at 1705106344.224590
write     43.03      43.04      0.183950    327680     1024.00    0.170234   59.48      2.00       59.49      6   
Ending compute phase at 1705106715.165685
Starting IO phase at 1705106715.165700
Ending IO phase at 1705106790.088428
write     29.99      33.83      0.230549    327680     1024.00    11.08      75.66      1.61       85.36      7   
