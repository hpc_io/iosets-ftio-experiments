############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 00:47:41 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705103261.988417
TestID              : 0
StartTime           : Sat Jan 13 00:47:42 2024
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705103625.294602
Starting IO phase at 1705103625.294623
Ending IO phase at 1705103913.880710
write     8.79       8.88       0.900228    327680     1024.00    202.40     288.25     3.60       291.32     0   
Ending compute phase at 1705104292.304042
Starting IO phase at 1705104292.304057
Ending IO phase at 1705104354.057701
write     32.07      41.56      0.187331    327680     1024.00    18.48      61.59      9.79       79.84      1   
Ending compute phase at 1705104721.671048
Starting IO phase at 1705104721.671067
Ending IO phase at 1705104791.500492
write     33.23      36.77      0.198212    327680     1024.00    7.53       69.62      9.80       77.05      2   
Ending compute phase at 1705105157.842915
Starting IO phase at 1705105157.842933
Ending IO phase at 1705105224.692261
write     35.22      38.49      0.201992    327680     1024.00    6.37       66.51      7.39       72.69      3   
Ending compute phase at 1705105585.938958
Starting IO phase at 1705105585.938977
Ending IO phase at 1705105650.510455
write     39.22      39.84      0.195429    327680     1024.00    1.23       64.26      4.33       65.28      4   
Ending compute phase at 1705106013.152113
Starting IO phase at 1705106013.152130
Ending IO phase at 1705106081.190122
write     36.38      37.97      0.209841    327680     1024.00    2.73       67.43      24.16      70.37      5   
Ending compute phase at 1705106441.485469
Starting IO phase at 1705106441.485487
Ending IO phase at 1705106508.892853
write     38.14      38.32      0.208190    327680     1024.00    0.086913   66.81      2.57       67.12      6   
Ending compute phase at 1705106883.461258
Starting IO phase at 1705106883.461282
Ending IO phase at 1705106963.572432
write     27.19      32.09      0.247770    327680     1024.00    14.40      79.78      9.27       94.14      7   
