############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sat Jan 13 00:47:40 2024
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_1_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1705103260.542939
TestID              : 0
StartTime           : Sat Jan 13 00:47:40 2024
Path                : /mnt/beegfs/iosets_1_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_1_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1705103620.554448
Starting IO phase at 1705103620.554462
Ending IO phase at 1705103672.915311
write     48.90      49.61      0.128328    327680     1024.00    0.073962   51.60      27.30      52.35      0   
Ending compute phase at 1705104032.945499
Starting IO phase at 1705104032.945512
Ending IO phase at 1705104088.379948
write     46.34      46.35      0.139636    327680     1024.00    0.096916   55.23      10.55      55.24      1   
Ending compute phase at 1705104453.400602
Starting IO phase at 1705104453.400620
Ending IO phase at 1705104510.951831
write     41.21      44.68      0.178491    327680     1024.00    5.46       57.30      8.33       62.12      2   
Ending compute phase at 1705104875.198360
Starting IO phase at 1705104875.198375
Ending IO phase at 1705104928.030897
write     45.20      48.72      0.163483    327680     1024.00    4.15       52.54      2.63       56.64      3   
Ending compute phase at 1705105290.042250
Starting IO phase at 1705105290.042270
Ending IO phase at 1705105356.406569
write     37.76      38.76      0.186712    327680     1024.00    2.06       66.04      6.30       67.79      4   
Ending compute phase at 1705105720.031289
Starting IO phase at 1705105720.031308
Ending IO phase at 1705105785.046813
write     37.59      39.57      0.195730    327680     1024.00    3.88       64.70      2.07       68.10      5   
Ending compute phase at 1705106146.820189
Starting IO phase at 1705106146.820206
Ending IO phase at 1705106216.593488
write     36.07      36.88      0.193695    327680     1024.00    1.74       69.41      8.84       70.98      6   
Ending compute phase at 1705106579.103580
Starting IO phase at 1705106579.103597
Ending IO phase at 1705106651.149866
write     34.55      35.67      0.223452    327680     1024.00    2.50       71.76      6.17       74.09      7   
Ending compute phase at 1705107018.050651
Starting IO phase at 1705107018.050671
