############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 07:56:13 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_3_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696139773.061311
TestID              : 0
StartTime           : Sun Oct  1 07:56:13 2023
Path                : /mnt/beegfs/iosets_3_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_3_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696140133.127042
Starting IO phase at 1696140133.127056
Ending IO phase at 1696140249.775555
write     21.94      21.95      0.305754    327680     1024.00    0.076838   116.62     54.73      116.68     0   
Ending compute phase at 1696140616.896995
Starting IO phase at 1696140616.897012
Ending IO phase at 1696140734.642610
write     20.52      21.76      0.361402    327680     1024.00    8.00       117.65     2.62       124.76     1   
Ending compute phase at 1696141097.961489
Starting IO phase at 1696141097.961508
Ending IO phase at 1696141158.644312
write     40.18      42.32      0.183797    327680     1024.00    3.71       60.49      3.03       63.71      2   
Ending compute phase at 1696141518.880307
Starting IO phase at 1696141518.880320
Ending IO phase at 1696141541.729603
write     113.83     113.86     0.069938    327680     1024.00    0.055508   22.48      0.103536   22.49      3   
Ending compute phase at 1696141901.966991
Starting IO phase at 1696141901.967005
Ending IO phase at 1696141925.517392
write     110.50     110.52     0.044823    327680     1024.00    0.069289   23.16      8.82       23.17      4   
Ending compute phase at 1696142285.691513
Starting IO phase at 1696142285.691527
Ending IO phase at 1696142310.797380
write     103.38     103.40     0.074438    327680     1024.00    0.049978   24.76      0.938628   24.76      5   
Ending compute phase at 1696142673.683629
Starting IO phase at 1696142673.683643
Ending IO phase at 1696142700.806110
write     86.80      93.16      0.083796    327680     1024.00    2.68       27.48      6.34       29.49      6   
Ending compute phase at 1696143060.991503
Starting IO phase at 1696143060.991516
Ending IO phase at 1696143112.252597
write     50.30      50.30      0.156470    327680     1024.00    0.245043   50.89      0.824321   50.90      7   
Ending compute phase at 1696143473.882948
Starting IO phase at 1696143473.882961
Ending IO phase at 1696143529.093239
write     45.45      46.61      0.136772    327680     1024.00    1.40       54.93      11.16      56.33      8   
