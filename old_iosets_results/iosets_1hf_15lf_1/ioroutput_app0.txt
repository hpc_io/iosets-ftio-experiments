############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 07:56:12 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 16M -o /mnt/beegfs/iosets_0_ -i 200 -d 18000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696139772.832889
TestID              : 0
StartTime           : Sun Oct  1 07:56:12 2023
Path                : /mnt/beegfs/iosets_0_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_0_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 200
xfersize            : 1 MiB
blocksize           : 16 MiB
aggregate filesize  : 128 MiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696139790.848245
Starting IO phase at 1696139790.848264
Ending IO phase at 1696139793.013664
write     60.87      61.08      0.130799    16384      1024.00    0.045955   2.10       0.012751   2.10       0   
Ending compute phase at 1696139811.274408
Starting IO phase at 1696139811.274419
Ending IO phase at 1696139813.393382
write     62.10      62.26      0.127058    16384      1024.00    0.062581   2.06       0.023713   2.06       1   
Ending compute phase at 1696139831.643366
Starting IO phase at 1696139831.643377
Ending IO phase at 1696139833.729289
write     63.93      64.30      0.123692    16384      1024.00    0.067193   1.99       0.017663   2.00       2   
Ending compute phase at 1696139851.954939
Starting IO phase at 1696139851.954950
Ending IO phase at 1696139854.009429
write     65.37      65.70      0.121048    16384      1024.00    0.051010   1.95       0.016396   1.96       3   
Ending compute phase at 1696139872.248842
Starting IO phase at 1696139872.248853
Ending IO phase at 1696139874.369475
write     62.40      62.63      0.127021    16384      1024.00    0.064056   2.04       0.040603   2.05       4   
Ending compute phase at 1696139892.591105
Starting IO phase at 1696139892.591119
Ending IO phase at 1696139894.657470
write     64.45      64.94      0.122465    16384      1024.00    0.076561   1.97       0.021538   1.99       5   
Ending compute phase at 1696139912.854025
Starting IO phase at 1696139912.854036
Ending IO phase at 1696139914.925556
write     64.32      64.73      0.122860    16384      1024.00    0.064412   1.98       0.020305   1.99       6   
Ending compute phase at 1696139933.168677
Starting IO phase at 1696139933.168689
Ending IO phase at 1696139935.317484
write     61.24      61.69      0.129671    16384      1024.00    0.053406   2.07       0.020170   2.09       7   
Ending compute phase at 1696139953.515019
Starting IO phase at 1696139953.515031
Ending IO phase at 1696139955.477491
write     66.94      67.14      0.110210    16384      1024.00    0.062254   1.91       0.143711   1.91       8   
Ending compute phase at 1696139973.707356
Starting IO phase at 1696139973.707367
Ending IO phase at 1696139975.697604
write     64.54      64.72      0.122890    16384      1024.00    0.076136   1.98       0.023666   1.98       9   
Ending compute phase at 1696139993.947281
Starting IO phase at 1696139993.947292
Ending IO phase at 1696139996.029452
write     63.59      63.77      0.124002    16384      1024.00    0.066124   2.01       0.023725   2.01       10  
Ending compute phase at 1696140014.326856
Starting IO phase at 1696140014.326867
Ending IO phase at 1696140016.505539
write     60.00      60.82      0.130698    16384      1024.00    0.043383   2.10       0.017680   2.13       11  
Ending compute phase at 1696140034.731287
Starting IO phase at 1696140034.731299
Ending IO phase at 1696140036.793466
write     65.38      65.82      0.120811    16384      1024.00    0.073356   1.94       0.019822   1.96       12  
Ending compute phase at 1696140055.011433
Starting IO phase at 1696140055.011444
Ending IO phase at 1696140057.141562
write     61.84      62.15      0.127986    16384      1024.00    0.053085   2.06       0.016879   2.07       13  
Ending compute phase at 1696140075.374505
Starting IO phase at 1696140075.374516
Ending IO phase at 1696140077.437517
write     63.93      64.37      0.123554    16384      1024.00    0.052831   1.99       0.020718   2.00       14  
Ending compute phase at 1696140095.666524
Starting IO phase at 1696140095.666535
Ending IO phase at 1696140097.777487
write     63.56      63.99      0.124290    16384      1024.00    0.053968   2.00       0.020932   2.01       15  
Ending compute phase at 1696140116.038887
Starting IO phase at 1696140116.038898
Ending IO phase at 1696140118.204446
write     63.43      64.00      0.124271    16384      1024.00    0.045804   2.00       0.024872   2.02       16  
Ending compute phase at 1696140164.615734
Starting IO phase at 1696140164.615750
Ending IO phase at 1696140172.980480
write     6.77       15.60      0.437131    16384      1024.00    11.91      8.21       2.16       18.92      17  
Ending compute phase at 1696140194.496123
Starting IO phase at 1696140194.496142
Ending IO phase at 1696140200.761102
write     13.20      20.24      0.372487    16384      1024.00    3.97       6.32       0.613120   9.70       18  
Ending compute phase at 1696140235.719178
Starting IO phase at 1696140235.719197
Ending IO phase at 1696140249.748018
write     4.14       9.13       0.218560    16384      1024.00    24.94      14.03      10.53      30.91      19  
Ending compute phase at 1696140271.519847
Starting IO phase at 1696140271.519866
Ending IO phase at 1696140283.031467
write     8.41       11.91      0.474739    16384      1024.00    4.43       10.75      4.66       15.23      20  
Ending compute phase at 1696140306.208114
Starting IO phase at 1696140306.208136
Ending IO phase at 1696140321.209548
write     6.52       8.35       0.564310    16384      1024.00    8.25       15.33      7.58       19.64      21  
Ending compute phase at 1696140345.582995
Starting IO phase at 1696140345.583012
Ending IO phase at 1696140364.105524
write     5.17       6.92       0.612677    16384      1024.00    13.95      18.51      12.75      24.77      22  
Ending compute phase at 1696140390.264234
Starting IO phase at 1696140390.264250
Ending IO phase at 1696140405.724956
write     5.45       8.28       0.506368    16384      1024.00    10.73      15.46      8.69       23.47      23  
Ending compute phase at 1696140428.859078
Starting IO phase at 1696140428.859095
Ending IO phase at 1696140444.109993
write     6.33       8.23       0.581792    16384      1024.00    14.30      15.55      7.75       20.22      24  
Ending compute phase at 1696140465.472526
Starting IO phase at 1696140465.472546
Ending IO phase at 1696140474.125351
write     10.88      15.00      0.450555    16384      1024.00    3.74       8.53       1.38       11.77      25  
Ending compute phase at 1696140496.220251
Starting IO phase at 1696140496.220269
Ending IO phase at 1696140506.041430
write     9.35       13.65      0.362277    16384      1024.00    4.46       9.37       3.98       13.68      26  
Ending compute phase at 1696140528.204637
Starting IO phase at 1696140528.204654
Ending IO phase at 1696140540.917564
write     7.70       10.65      0.459380    16384      1024.00    5.41       12.01      6.21       16.62      27  
Ending compute phase at 1696140563.368515
Starting IO phase at 1696140563.368533
Ending IO phase at 1696140573.373464
write     8.97       13.01      0.490260    16384      1024.00    5.07       9.84       3.40       14.27      28  
Ending compute phase at 1696140594.736719
Starting IO phase at 1696140594.736737
Ending IO phase at 1696140600.773333
write     14.01      22.25      0.359359    16384      1024.00    3.76       5.75       0.505337   9.14       29  
Ending compute phase at 1696140623.588963
Starting IO phase at 1696140623.588982
Ending IO phase at 1696140634.273485
write     8.39       11.82      0.304841    16384      1024.00    9.53       10.83      5.74       15.26      30  
Ending compute phase at 1696140656.704642
Starting IO phase at 1696140656.704663
Ending IO phase at 1696140665.537138
write     9.83       14.81      0.484359    16384      1024.00    5.45       8.64       1.85       13.02      31  
Ending compute phase at 1696140687.096842
Starting IO phase at 1696140687.096860
Ending IO phase at 1696140700.929478
write     7.46       9.88       0.475115    16384      1024.00    4.75       12.96      7.38       17.17      32  
Ending compute phase at 1696140723.316876
Starting IO phase at 1696140723.316891
Ending IO phase at 1696140735.373740
write     7.91       11.03      0.463892    16384      1024.00    11.61      11.60      6.20       16.18      33  
Ending compute phase at 1696140757.484669
Starting IO phase at 1696140757.484685
Ending IO phase at 1696140770.232778
write     7.67       9.88       0.454449    16384      1024.00    11.25      12.95      5.45       16.69      34  
Ending compute phase at 1696140794.301121
Starting IO phase at 1696140794.301141
Ending IO phase at 1696140810.073637
write     5.93       8.14       0.504494    16384      1024.00    13.92      15.73      9.46       21.59      35  
Ending compute phase at 1696140835.634414
Starting IO phase at 1696140835.634434
Ending IO phase at 1696140847.861369
write     6.55       9.89       0.489481    16384      1024.00    15.18      12.95      4.35       19.53      36  
Ending compute phase at 1696140877.145233
Starting IO phase at 1696140877.145253
Ending IO phase at 1696140892.689203
write     4.80       8.15       0.487233    16384      1024.00    18.97      15.71      9.15       26.68      37  
Ending compute phase at 1696140914.610338
Starting IO phase at 1696140914.610354
Ending IO phase at 1696140924.157565
write     9.63       13.96      0.278243    16384      1024.00    4.27       9.17       5.94       13.30      38  
Ending compute phase at 1696140943.918815
Starting IO phase at 1696140943.918836
Ending IO phase at 1696140949.197477
write     18.94      25.48      0.154364    16384      1024.00    1.88       5.02       2.71       6.76       39  
Ending compute phase at 1696140971.484949
Starting IO phase at 1696140971.484969
Ending IO phase at 1696140984.880333
write     7.30       9.58       0.436413    16384      1024.00    6.07       13.37      6.39       17.54      40  
Ending compute phase at 1696141010.285285
Starting IO phase at 1696141010.285299
Ending IO phase at 1696141024.363286
write     6.00       9.12       0.445034    16384      1024.00    14.40      14.04      8.61       21.32      41  
Ending compute phase at 1696141045.965486
Starting IO phase at 1696141045.965500
Ending IO phase at 1696141057.801377
write     8.42       10.87      0.380265    16384      1024.00    4.62       11.77      6.55       15.20      42  
Ending compute phase at 1696141079.929381
Starting IO phase at 1696141079.929397
Ending IO phase at 1696141091.089652
write     8.52       11.53      0.493527    16384      1024.00    8.48       11.10      3.22       15.02      43  
Ending compute phase at 1696141111.458008
Starting IO phase at 1696141111.458027
Ending IO phase at 1696141120.913486
write     11.14      13.85      0.250823    16384      1024.00    3.41       9.24       5.86       11.49      44  
Ending compute phase at 1696141147.469421
Starting IO phase at 1696141147.469436
Ending IO phase at 1696141156.842125
write     7.24       13.75      0.339800    16384      1024.00    14.63      9.31       4.79       17.67      45  
Ending compute phase at 1696141178.621684
Starting IO phase at 1696141178.621706
Ending IO phase at 1696141192.221368
write     7.47       9.93       0.453250    16384      1024.00    4.63       12.89      6.26       17.12      46  
Ending compute phase at 1696141212.342073
Starting IO phase at 1696141212.342092
Ending IO phase at 1696141215.937406
write     23.43      36.34      0.158657    16384      1024.00    2.31       3.52       0.984881   5.46       47  
Ending compute phase at 1696141235.841444
Starting IO phase at 1696141235.841461
Ending IO phase at 1696141239.421432
write     24.62      37.72      0.203116    16384      1024.00    2.17       3.39       0.584429   5.20       48  
Ending compute phase at 1696141258.950672
Starting IO phase at 1696141258.950692
Ending IO phase at 1696141266.259776
write     14.89      18.30      0.195499    16384      1024.00    4.51       6.99       4.10       8.59       49  
Ending compute phase at 1696141287.398180
Starting IO phase at 1696141287.398198
Ending IO phase at 1696141298.655575
write     9.07       11.44      0.273411    16384      1024.00    3.69       11.19      6.82       14.12      50  
Ending compute phase at 1696141318.642984
Starting IO phase at 1696141318.642998
Ending IO phase at 1696141328.085563
write     11.44      13.62      0.413253    16384      1024.00    8.40       9.40       5.84       11.19      51  
Ending compute phase at 1696141346.303010
Starting IO phase at 1696141346.303022
Ending IO phase at 1696141347.283244
write     138.22     140.31     0.040528    16384      1024.00    0.044195   0.912242   0.272646   0.926048   52  
Ending compute phase at 1696141365.502625
Starting IO phase at 1696141365.502637
Ending IO phase at 1696141374.605512
write     14.13      14.14      0.120442    16384      1024.00    0.761848   9.05       7.13       9.06       53  
Ending compute phase at 1696141392.775146
Starting IO phase at 1696141392.775157
Ending IO phase at 1696141393.863439
write     117.23     117.87     0.067870    16384      1024.00    0.057723   1.09       0.012339   1.09       54  
Ending compute phase at 1696141413.913949
Starting IO phase at 1696141413.913963
Ending IO phase at 1696141421.369584
write     13.88      17.42      0.312962    16384      1024.00    2.38       7.35       3.96       9.22       55  
Ending compute phase at 1696141439.546229
Starting IO phase at 1696141439.546242
Ending IO phase at 1696141442.185538
write     49.62      49.71      0.160224    16384      1024.00    0.249585   2.57       0.314857   2.58       56  
Ending compute phase at 1696141460.399793
Starting IO phase at 1696141460.399807
Ending IO phase at 1696141466.381557
write     21.72      21.77      0.135605    16384      1024.00    0.051544   5.88       3.72       5.89       57  
Ending compute phase at 1696141484.592900
Starting IO phase at 1696141484.592915
Ending IO phase at 1696141487.245676
write     49.55      50.49      0.155919    16384      1024.00    0.079922   2.54       0.294700   2.58       58  
Ending compute phase at 1696141505.446888
Starting IO phase at 1696141505.446899
Ending IO phase at 1696141506.945521
write     88.71      89.06      0.089824    16384      1024.00    0.057988   1.44       0.017039   1.44       59  
Ending compute phase at 1696141525.142836
Starting IO phase at 1696141525.142848
Ending IO phase at 1696141533.077691
write     16.24      16.25      0.237588    16384      1024.00    0.051247   7.88       4.08       7.88       60  
Ending compute phase at 1696141551.318774
Starting IO phase at 1696141551.318786
Ending IO phase at 1696141560.517497
write     14.02      14.03      0.279333    16384      1024.00    0.082547   9.12       4.65       9.13       61  
Ending compute phase at 1696141578.735385
Starting IO phase at 1696141578.735399
Ending IO phase at 1696141584.045515
write     24.43      24.50      0.135736    16384      1024.00    0.047138   5.22       3.06       5.24       62  
Ending compute phase at 1696141602.258034
Starting IO phase at 1696141602.258074
Ending IO phase at 1696141607.445572
write     24.89      25.00      0.132287    16384      1024.00    0.052461   5.12       3.01       5.14       63  
Ending compute phase at 1696141625.738801
Starting IO phase at 1696141625.738814
Ending IO phase at 1696141633.813514
write     15.95      15.96      0.133881    16384      1024.00    0.108409   8.02       5.88       8.02       64  
Ending compute phase at 1696141652.018914
Starting IO phase at 1696141652.018927
Ending IO phase at 1696141660.689535
write     14.92      14.93      0.269310    16384      1024.00    0.052960   8.57       4.74       8.58       65  
Ending compute phase at 1696141680.738352
Starting IO phase at 1696141680.738371
Ending IO phase at 1696141687.189291
write     15.50      20.03      0.399380    16384      1024.00    2.25       6.39       1.69       8.26       66  
Ending compute phase at 1696141705.709001
Starting IO phase at 1696141705.709019
Ending IO phase at 1696141709.011678
write     35.50      35.98      0.205566    16384      1024.00    0.314811   3.56       0.309492   3.61       67  
Ending compute phase at 1696141728.400314
Starting IO phase at 1696141728.400333
Ending IO phase at 1696141731.725499
write     29.50      40.07      0.199529    16384      1024.00    1.34       3.19       1.15       4.34       68  
Ending compute phase at 1696141750.522385
Starting IO phase at 1696141750.522400
Ending IO phase at 1696141754.469515
write     29.13      33.46      0.062545    16384      1024.00    0.717126   3.83       2.83       4.39       69  
Ending compute phase at 1696141772.714752
Starting IO phase at 1696141772.714763
Ending IO phase at 1696141773.825832
write     122.24     122.86     0.046146    16384      1024.00    0.088014   1.04       0.304407   1.05       70  
Ending compute phase at 1696141792.031135
Starting IO phase at 1696141792.031148
Ending IO phase at 1696141798.813515
write     19.24      19.27      0.170806    16384      1024.00    0.052336   6.64       3.92       6.65       71  
Ending compute phase at 1696141817.070869
Starting IO phase at 1696141817.070881
Ending IO phase at 1696141823.273555
write     20.90      20.93      0.146118    16384      1024.00    0.056294   6.12       3.78       6.12       72  
Ending compute phase at 1696141841.518400
Starting IO phase at 1696141841.518414
Ending IO phase at 1696141847.205535
write     22.75      22.77      0.155929    16384      1024.00    2.51       5.62       3.13       5.63       73  
Ending compute phase at 1696141865.370751
Starting IO phase at 1696141865.370762
Ending IO phase at 1696141866.593627
write     111.94     112.48     0.066845    16384      1024.00    0.072573   1.14       0.069111   1.14       74  
Ending compute phase at 1696141884.871567
Starting IO phase at 1696141884.871579
Ending IO phase at 1696141890.501535
write     23.23      23.26      0.141386    16384      1024.00    0.048733   5.50       3.24       5.51       75  
Ending compute phase at 1696141908.691917
Starting IO phase at 1696141908.691929
Ending IO phase at 1696141915.549505
write     18.96      18.98      0.159984    16384      1024.00    0.106199   6.74       4.18       6.75       76  
Ending compute phase at 1696141933.735127
Starting IO phase at 1696141933.735141
Ending IO phase at 1696141942.289503
write     15.17      15.18      0.292615    16384      1024.00    4.69       8.43       3.75       8.44       77  
Ending compute phase at 1696141960.519114
Starting IO phase at 1696141960.519128
Ending IO phase at 1696141966.053549
write     23.50      23.55      0.174645    16384      1024.00    2.80       5.43       2.65       5.45       78  
Ending compute phase at 1696141984.312535
Starting IO phase at 1696141984.312549
Ending IO phase at 1696141990.577535
write     20.63      20.67      0.333587    16384      1024.00    0.048568   6.19       3.77       6.21       79  
Ending compute phase at 1696142008.767315
Starting IO phase at 1696142008.767328
Ending IO phase at 1696142011.869560
write     41.31      41.39      0.193300    16384      1024.00    0.120334   3.09       0.527252   3.10       80  
Ending compute phase at 1696142030.131996
Starting IO phase at 1696142030.132008
Ending IO phase at 1696142033.017748
write     45.44      45.70      0.166818    16384      1024.00    0.076821   2.80       0.143007   2.82       81  
Ending compute phase at 1696142051.243482
Starting IO phase at 1696142051.243494
Ending IO phase at 1696142053.593529
write     56.83      56.98      0.136475    16384      1024.00    0.230043   2.25       0.086529   2.25       82  
Ending compute phase at 1696142072.093800
Starting IO phase at 1696142072.093814
Ending IO phase at 1696142081.365544
write     13.51      13.53      0.282752    16384      1024.00    0.287878   9.46       5.13       9.48       83  
Ending compute phase at 1696142101.667422
Starting IO phase at 1696142101.667438
Ending IO phase at 1696142108.501487
write     14.55      18.73      0.245809    16384      1024.00    5.94       6.83       3.62       8.80       84  
Ending compute phase at 1696142128.935279
Starting IO phase at 1696142128.935300
Ending IO phase at 1696142135.157501
write     15.16      21.66      0.221889    16384      1024.00    2.77       5.91       2.60       8.44       85  
Ending compute phase at 1696142153.887067
Starting IO phase at 1696142153.887087
Ending IO phase at 1696142159.041522
write     22.77      24.68      0.307333    16384      1024.00    0.642621   5.19       2.10       5.62       86  
Ending compute phase at 1696142180.077922
Starting IO phase at 1696142180.077943
Ending IO phase at 1696142183.314554
write     21.46      40.42      0.190383    16384      1024.00    3.54       3.17       1.32       5.96       87  
Ending compute phase at 1696142202.399580
Starting IO phase at 1696142202.399594
Ending IO phase at 1696142206.073508
write     28.44      35.30      0.185161    16384      1024.00    0.913317   3.63       0.801258   4.50       88  
Ending compute phase at 1696142226.368656
Starting IO phase at 1696142226.368670
Ending IO phase at 1696142230.001485
write     22.71      36.56      0.218809    16384      1024.00    3.05       3.50       0.815149   5.64       89  
Ending compute phase at 1696142248.139545
Starting IO phase at 1696142248.139558
Ending IO phase at 1696142252.753528
write     28.41      28.48      0.190495    16384      1024.00    0.040107   4.49       2.12       4.51       90  
Ending compute phase at 1696142270.915136
Starting IO phase at 1696142270.915150
Ending IO phase at 1696142276.257506
write     24.54      24.63      0.189778    16384      1024.00    3.04       5.20       2.89       5.22       91  
Ending compute phase at 1696142294.438849
Starting IO phase at 1696142294.438862
Ending IO phase at 1696142303.913519
write     13.67      13.68      0.358364    16384      1024.00    5.74       9.36       6.15       9.36       92  
Ending compute phase at 1696142322.098365
Starting IO phase at 1696142322.098379
Ending IO phase at 1696142331.389593
write     13.82      13.83      0.291453    16384      1024.00    4.69       9.25       5.16       9.26       93  
Ending compute phase at 1696142349.575602
Starting IO phase at 1696142349.575616
Ending IO phase at 1696142354.849523
write     24.60      24.68      0.136926    16384      1024.00    0.066517   5.19       3.01       5.20       94  
Ending compute phase at 1696142373.078664
Starting IO phase at 1696142373.078678
Ending IO phase at 1696142381.185516
write     15.94      15.95      0.145426    16384      1024.00    2.33       8.03       5.70       8.03       95  
Ending compute phase at 1696142399.383078
Starting IO phase at 1696142399.383092
Ending IO phase at 1696142405.205535
write     22.21      22.24      0.148879    16384      1024.00    2.39       5.75       3.38       5.76       96  
Ending compute phase at 1696142423.466895
Starting IO phase at 1696142423.466908
Ending IO phase at 1696142434.157559
write     12.04      12.05      0.443665    16384      1024.00    6.02       10.63      7.25       10.63      97  
Ending compute phase at 1696142452.422485
Starting IO phase at 1696142452.422498
Ending IO phase at 1696142456.417540
write     33.15      33.19      0.190697    16384      1024.00    0.058439   3.86       1.30       3.86       98  
Ending compute phase at 1696142474.640629
Starting IO phase at 1696142474.640642
Ending IO phase at 1696142476.553559
write     68.53      69.62      0.114906    16384      1024.00    0.154866   1.84       0.160779   1.87       99  
Ending compute phase at 1696142494.763472
Starting IO phase at 1696142494.763484
Ending IO phase at 1696142500.081511
write     24.42      24.54      0.273399    16384      1024.00    4.78       5.22       0.862305   5.24       100 
Ending compute phase at 1696142518.295204
Starting IO phase at 1696142518.295219
Ending IO phase at 1696142525.697547
write     17.49      17.51      0.265831    16384      1024.00    0.072581   7.31       4.70       7.32       101 
Ending compute phase at 1696142544.907833
Starting IO phase at 1696142544.907847
Ending IO phase at 1696142550.037565
write     20.88      25.14      0.236791    16384      1024.00    1.92       5.09       1.30       6.13       102 
Ending compute phase at 1696142569.755869
Starting IO phase at 1696142569.755883
Ending IO phase at 1696142575.345503
write     18.36      23.44      0.220834    16384      1024.00    2.66       5.46       1.93       6.97       103 
Ending compute phase at 1696142594.112095
Starting IO phase at 1696142594.112109
Ending IO phase at 1696142599.849471
write     20.59      22.62      0.179218    16384      1024.00    0.886087   5.66       2.78       6.22       104 
Ending compute phase at 1696142620.460188
Starting IO phase at 1696142620.460202
Ending IO phase at 1696142625.854458
write     16.54      24.09      0.272777    16384      1024.00    3.19       5.31       0.950713   7.74       105 
Ending compute phase at 1696142646.284427
Starting IO phase at 1696142646.284442
Ending IO phase at 1696142651.765509
write     17.03      23.74      0.157995    16384      1024.00    4.65       5.39       2.87       7.52       106 
Ending compute phase at 1696142669.934766
Starting IO phase at 1696142669.934778
Ending IO phase at 1696142675.571484
write     22.84      22.87      0.135851    16384      1024.00    0.066387   5.60       3.42       5.60       107 
Ending compute phase at 1696142693.807022
Starting IO phase at 1696142693.807035
Ending IO phase at 1696142699.634940
write     22.02      23.24      0.188066    16384      1024.00    3.01       5.51       3.45       5.81       108 
Ending compute phase at 1696142717.919015
Starting IO phase at 1696142717.919029
Ending IO phase at 1696142725.583389
write     16.75      16.77      0.309979    16384      1024.00    0.057980   7.63       5.15       7.64       109 
Ending compute phase at 1696142743.840126
Starting IO phase at 1696142743.840140
Ending IO phase at 1696142750.737504
write     18.62      18.66      0.187315    16384      1024.00    0.055502   6.86       3.84       6.87       110 
Ending compute phase at 1696142768.963905
Starting IO phase at 1696142768.963918
Ending IO phase at 1696142775.729553
write     19.17      19.21      0.232065    16384      1024.00    0.060624   6.66       3.78       6.68       111 
Ending compute phase at 1696142793.887338
Starting IO phase at 1696142793.887350
Ending IO phase at 1696142801.177475
write     17.72      17.73      0.255395    16384      1024.00    0.128236   7.22       3.34       7.22       112 
Ending compute phase at 1696142819.366932
Starting IO phase at 1696142819.366945
Ending IO phase at 1696142827.889535
write     15.19      15.20      0.205009    16384      1024.00    0.053146   8.42       5.14       8.42       113 
Ending compute phase at 1696142846.075482
Starting IO phase at 1696142846.075494
Ending IO phase at 1696142852.081508
write     21.66      21.70      0.180361    16384      1024.00    0.078757   5.90       3.58       5.91       114 
Ending compute phase at 1696142870.283195
Starting IO phase at 1696142870.283208
Ending IO phase at 1696142880.701550
write     12.32      12.32      0.316643    16384      1024.00    0.061632   10.39      7.17       10.39      115 
Ending compute phase at 1696142898.911576
Starting IO phase at 1696142898.911588
Ending IO phase at 1696142905.797488
write     18.80      18.83      0.267045    16384      1024.00    0.061521   6.80       4.24       6.81       116 
Ending compute phase at 1696142924.027355
Starting IO phase at 1696142924.027367
Ending IO phase at 1696142933.905514
write     13.05      13.06      0.296923    16384      1024.00    0.126369   9.80       5.69       9.81       117 
Ending compute phase at 1696142952.114970
Starting IO phase at 1696142952.114982
Ending IO phase at 1696142961.305557
write     14.00      14.02      0.235350    16384      1024.00    4.46       9.13       5.37       9.14       118 
Ending compute phase at 1696142979.535341
Starting IO phase at 1696142979.535353
Ending IO phase at 1696142991.577517
write     10.70      10.70      0.434463    16384      1024.00    0.071867   11.96      6.92       11.96      119 
Ending compute phase at 1696143009.740303
Starting IO phase at 1696143009.740318
Ending IO phase at 1696143018.513830
write     14.72      14.75      0.276697    16384      1024.00    0.033115   8.68       5.31       8.70       120 
Ending compute phase at 1696143038.543671
Starting IO phase at 1696143038.543686
Ending IO phase at 1696143045.241524
write     15.08      19.37      0.213983    16384      1024.00    2.10       6.61       3.19       8.49       121 
Ending compute phase at 1696143065.189050
Starting IO phase at 1696143065.189066
Ending IO phase at 1696143073.040434
write     13.33      16.42      0.303293    16384      1024.00    3.27       7.80       3.50       9.60       122 
Ending compute phase at 1696143094.025498
Starting IO phase at 1696143094.025511
Ending IO phase at 1696143105.129499
write     9.25       11.58      0.244920    16384      1024.00    8.33       11.05      7.14       13.84      123 
Ending compute phase at 1696143125.245304
Starting IO phase at 1696143125.245322
Ending IO phase at 1696143134.337493
write     11.71      14.23      0.308755    16384      1024.00    2.50       8.99       4.05       10.93      124 
Ending compute phase at 1696143154.916719
Starting IO phase at 1696143154.916737
Ending IO phase at 1696143163.601508
write     11.72      14.95      0.305044    16384      1024.00    2.87       8.56       3.98       10.92      125 
Ending compute phase at 1696143184.428991
Starting IO phase at 1696143184.429007
Ending IO phase at 1696143194.909545
write     9.81       12.49      0.403786    16384      1024.00    9.08       10.25      3.97       13.04      126 
Ending compute phase at 1696143216.540873
Starting IO phase at 1696143216.540891
Ending IO phase at 1696143223.773482
write     12.10      17.90      0.348922    16384      1024.00    4.21       7.15       1.90       10.58      127 
Ending compute phase at 1696143242.249543
Starting IO phase at 1696143242.249555
Ending IO phase at 1696143246.781527
write     26.97      28.74      0.173178    16384      1024.00    0.312113   4.45       1.68       4.75       128 
Ending compute phase at 1696143267.465685
Starting IO phase at 1696143267.465699
Ending IO phase at 1696143275.257464
write     12.54      18.15      0.291981    16384      1024.00    3.10       7.05       3.02       10.21      129 
Ending compute phase at 1696143296.161331
Starting IO phase at 1696143296.161345
Ending IO phase at 1696143303.065453
write     13.48      18.75      0.258713    16384      1024.00    3.33       6.83       2.69       9.50       130 
Ending compute phase at 1696143324.037410
Starting IO phase at 1696143324.037425
Ending IO phase at 1696143330.933568
write     13.31      18.69      0.329022    16384      1024.00    4.65       6.85       2.41       9.62       131 
Ending compute phase at 1696143351.769331
Starting IO phase at 1696143351.769346
Ending IO phase at 1696143358.341542
write     13.94      19.63      0.259965    16384      1024.00    6.82       6.52       2.36       9.18       132 
Ending compute phase at 1696143379.269713
Starting IO phase at 1696143379.269728
Ending IO phase at 1696143386.550279
write     12.93      17.73      0.230403    16384      1024.00    7.34       7.22       3.53       9.90       133 
Ending compute phase at 1696143407.686422
Starting IO phase at 1696143407.686437
Ending IO phase at 1696143417.195579
write     10.29      13.48      0.330391    16384      1024.00    8.21       9.50       4.94       12.44      134 
Ending compute phase at 1696143435.485946
Starting IO phase at 1696143435.485964
Ending IO phase at 1696143440.965434
write     23.60      23.85      0.335418    16384      1024.00    0.523249   5.37       0.921252   5.42       135 
Ending compute phase at 1696143461.611413
Starting IO phase at 1696143461.611430
Ending IO phase at 1696143468.149446
write     14.46      19.94      0.227826    16384      1024.00    2.46       6.42       2.77       8.85       136 
Ending compute phase at 1696143489.101720
Starting IO phase at 1696143489.101750
Ending IO phase at 1696143496.761476
write     12.40      16.91      0.276794    16384      1024.00    7.18       7.57       3.14       10.32      137 
Ending compute phase at 1696143517.581749
Starting IO phase at 1696143517.581766
Ending IO phase at 1696143525.273621
write     12.48      16.81      0.295036    16384      1024.00    3.42       7.61       3.30       10.26      138 
Ending compute phase at 1696143546.200892
Starting IO phase at 1696143546.200909
Ending IO phase at 1696143551.073453
write     16.95      26.48      0.265989    16384      1024.00    3.31       4.83       0.579009   7.55       139 
Ending compute phase at 1696143571.706521
Starting IO phase at 1696143571.706538
