############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 18:41:23 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_10_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696178483.372512
TestID              : 0
StartTime           : Sun Oct  1 18:41:23 2023
Path                : /mnt/beegfs/iosets_10_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_10_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696179029.551475
Starting IO phase at 1696179029.551496
Ending IO phase at 1696179081.531959
write     10.78      11.41      0.159730    327680     1024.00    186.93     224.33     2.00       237.43     0   
Ending compute phase at 1696179445.300179
Starting IO phase at 1696179445.300204
Ending IO phase at 1696179498.648265
write     45.23      48.33      0.160288    327680     1024.00    4.34       52.97      2.26       56.60      1   
Ending compute phase at 1696179872.013683
Starting IO phase at 1696179872.013704
Ending IO phase at 1696179926.195928
write     38.20      47.62      0.162789    327680     1024.00    13.25      53.76      5.62       67.02      2   
Ending compute phase at 1696180297.567258
Starting IO phase at 1696180297.567282
Ending IO phase at 1696180360.940174
write     34.45      40.52      0.184595    327680     1024.00    11.16      63.18      4.11       74.31      3   
Ending compute phase at 1696180722.645526
Starting IO phase at 1696180722.645547
Ending IO phase at 1696180781.424116
write     42.54      43.65      0.182644    327680     1024.00    1.55       58.65      3.09       60.17      4   
Ending compute phase at 1696181143.343341
Starting IO phase at 1696181143.343363
Ending IO phase at 1696181206.644285
write     39.47      40.58      0.193673    327680     1024.00    2.05       63.09      7.72       64.86      5   
Ending compute phase at 1696181571.400901
Starting IO phase at 1696181571.400922
Ending IO phase at 1696181637.583889
write     36.36      38.93      0.205479    327680     1024.00    6.37       65.75      11.61      70.41      6   
Ending compute phase at 1696181999.071445
Starting IO phase at 1696181999.071469
Ending IO phase at 1696182062.729037
write     39.56      40.39      0.191459    327680     1024.00    1.45       63.38      4.53       64.71      7   
