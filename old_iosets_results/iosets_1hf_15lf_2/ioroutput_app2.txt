############
Settings: 
- Test 1
- Calc 0
- Binary file: 0
- Samples: 5
############
IOR-3.4.0+dev: MPI Coordinated Test of Parallel I/O
Began               : Sun Oct  1 18:41:21 2023
Command line        : /root/ior_schedclient_tmio/src/ior -a MPIIO -F -t 1M -w -e -b 320M -o /mnt/beegfs/iosets_2_ -i 10 -d 360000000
Machine             : Linux grisou-9.nancy.grid5000.fr
Starting application at 1696178481.800741
TestID              : 0
StartTime           : Sun Oct  1 18:41:21 2023
Path                : /mnt/beegfs/iosets_2_.00000000
FS                  : 484.9 GiB   Used FS: 0.0%   Inodes: 0.0 Mi   Used Inodes: -nan%

Options: 
api                 : MPIIO
apiVersion          : (3.1)
test filename       : /mnt/beegfs/iosets_2_
access              : file-per-process
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 8
clients per node    : 8
repetitions         : 10
xfersize            : 1 MiB
blocksize           : 320 MiB
aggregate filesize  : 2.50 GiB

Results: 

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
Ending compute phase at 1696178841.813562
Starting IO phase at 1696178841.813575
Ending IO phase at 1696178890.933459
write     52.13      52.21      0.070057    327680     1024.00    0.062306   49.03      26.68      49.11      0   
Ending compute phase at 1696179251.015032
Starting IO phase at 1696179251.015045
Ending IO phase at 1696179298.363857
write     54.41      54.43      0.111279    327680     1024.00    0.078592   47.04      11.44      47.05      1   
Ending compute phase at 1696179658.658853
Starting IO phase at 1696179658.658869
Ending IO phase at 1696179712.403520
write     47.96      47.97      0.130738    327680     1024.00    0.074290   53.37      11.53      53.38      2   
Ending compute phase at 1696180072.596250
Starting IO phase at 1696180072.596268
Ending IO phase at 1696180129.785449
write     44.99      45.00      0.169306    327680     1024.00    0.113157   56.89      6.96       56.90      3   
Ending compute phase at 1696180499.823927
Starting IO phase at 1696180499.823952
Ending IO phase at 1696180550.044650
write     42.80      47.43      0.152725    327680     1024.00    9.94       53.97      4.13       59.81      4   
Ending compute phase at 1696180912.263035
Starting IO phase at 1696180912.263059
Ending IO phase at 1696180973.859600
write     40.42      41.84      0.181451    327680     1024.00    2.34       61.18      8.10       63.33      5   
Ending compute phase at 1696181334.138926
Starting IO phase at 1696181334.138942
Ending IO phase at 1696181383.772065
write     51.73      51.75      0.147184    327680     1024.00    0.069732   49.47      5.04       49.49      6   
Ending compute phase at 1696181747.264635
Starting IO phase at 1696181747.264658
Ending IO phase at 1696181810.752378
write     38.44      40.85      0.195290    327680     1024.00    3.88       62.67      8.90       66.60      7   
Ending compute phase at 1696182170.924196
Starting IO phase at 1696182170.924209
Ending IO phase at 1696182231.212575
write     42.66      42.67      0.166591    327680     1024.00    0.161327   60.00      6.69       60.01      8   
