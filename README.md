# IOSets+FTIO experiments

This repository contains the results of the experiments of IOSets with FTIO, the code used to parse them and compute the metrics, and some of the code used to run them. More details can be found in the technical report.

## Parsing the results

The `parse_iosets_experiments.py` script can be used to parse the results. It receives as argument a folder containing multiple experiments (one sub-folder per experiment). In this case, it's the `results` folder (see the next section for details). It generates multiple output files (which are available in this repository, so no need to re-run the script):

- `results.csv` contains the per-execution metrics: max stretch, geometric mean of stretch, I/O slowdown, utilization. Each execution is identified by a scheduler, numbers of high- and low-frequency apps and a repetition (the repetition number itself means nothing).

- `detailedresults.csv` contains the metrics computed for each application of each execution: stretch and I/O slowdown. Each application is identified by a number starting at 0 (id < number of high-frequency means it's a high-frequency app, otherwise it's a low-frequency one). 

- `ftio_results.csv` is only for the experiments with FTIO (scheduler is "iosetsftio"). For each execution and each application (identified as above), it lists all the data obtained from FTIO (period and confidence) and what priority was computed from that. Naturally, the high-frequency applications will have more lines in this file because they have more I/O phases (so FTIO will be called more times).

- In the `results` folder, each sub-folder (which corresponds to one execution) results in a `_phases.csv` file that lists the timestamps of beginning and end of cpu and I/O phases (one iteration, meaning a cpu phase and an I/O phase, per line). All timestamps are relative to the beginning of the first phase by any application.

## Plotting and comparing them

The `plotter.py` scripts generates the plots from the FTIO paper, using plotly. There is also a jupyter notebook, `compare_schedulers`, which calculates the numbers mentioned in the paper.

## The `results` folder

Each experiment has a sub-folder, named as `[scheduler]_[number of high-frequency apps]_[number of low-frequency apps]_[artificially injected error]_[repetition]` (some of them do not have the error part because they were executed before we decided to run experiments with error... in that case their error is 0). 

Inside an experiment's folder, we can find:

- The output generated by IOR for each of the applications (the `ioroutput_[application id].txt` files). The high-frequency will be the first applications to be generated (for example, with `1hf_15lf`, "app0" is the high-frequency one).

- For the experiments with FTIO, the `calleroutput_[application id].txt` files give period and confidence obtained from FTIO during the execution, followed by the computed priority which was given to BeeGFS.

- The `priorities_in_the_end.txt` file that contains the contents of the `/sys/kernel/config/iosets/[app id]/priority` files, which were used by the BeeGFS implementation as both the set id and priority for Set-10 (only relevant when scheduler is "iosets" or "iosetsftio", of course). In the experiments with FTIO (scheduler is "iosetsftio"), this priority may have changed multiple times during the execution, but still this file only shows the contents of the files after all the applications finished their execution. Please notice: in this file, applications are identified starting at 1, not 0. That means application 1 corresponds to `ioroutput_app0.txt` and so on.

- `results.tgz` contains all the traces generated by TMIO (in .jsonl files) during the application executions. They are also identified starting with application 1 (not 0). In the experiments with FTIO, an additional .csv file is present per application, containing the full output of FTIO during the execution of the application (note that it is in fact NOT a .csv file).

## The `code` folder

This folder contains code used to run the experiments:

- `call_predictor` is a simple C code that launches FTIO in prediction mode, watching the trace created by TMIO for a given application. Then, `call_predictor` obtains all output from FTIO, parses it to get period and confidence, computes a priority according to Set-10, and writes it to the priority file, from which it is taken by the BeeGFS client. Note that there are some hardcoded parameters to be set in the beginning of the .c file.

- `modify_ftio.txt` logs whatever was changed to FTIO to make `call_predictor` work. 

- `ior_schedclient_tmio.tgz` is the IOR benchmark with two important modifications:
    - It is integrated with TMIO to generate the trace file during its execution. Data is added to the trace file at the end of each write phase.
    - It writes timestamps of beginning and end of I/O phases (it was only implemented for write phases) to the standard output, so the metrics can be computed.
    - It also includes some code to talk to an I/O scheduler through the network, but that was NOT used for the experiments in this repository.

- `run_concior_test.py` is the script that runs a test execution. It includes many hardcoded parameters in the beginning of the file.

- `run_iosets_ftio_exps.py` is the script that runs a list of experiments that is read from a file (the file name is hardcoded in the script). Each line of the file contains the parameters to give to `run_concior_test.py`. The point of this script is that it keeps track of was already executed (so we can continue later if it is interrupted), and that it runs the list of experiments in random order.

- `iosets_tests.txt` is an example of a file containing experiments that is read by `run_iosets_ftio_exps.py`.

