import subprocess
import sys
from parse_iosets_exp_folder import parse_folder
from numpy import mean, median

def get_stats(a_list, unit):
    if len(a_list) == 0:
        return "empty"
    mini = min(a_list)
    maxi = max(a_list)
    meani = mean(a_list)
    mediani = median(a_list)
    num = len(a_list)
    stri = "min "+str(mini)+" max "+str(maxi)+" mean "+str(meani)+" median "+str(mediani)+" "+unit+" (measured from "+str(num)+" values)"
    return stri


#hardcoded parameters
t_start = 400 #the timeframe where we will measure things
t_end = 3200
hf_iter = 200
lf_iter = 10
hf_phase_MB = 8*16  #size in MB of each I/O phase (for the whole app)
lf_phase_MB = 8*320
#expected_perf = 90 #in MB/s #IMPORTANT to get this value, we run this script and see the performance observed for exclusive phases, then we adjust this and run again. It's important to have a good estimation of the expected performance
expected_perf = 75 #in MB/s #IMPORTANT to get this value, we run this script and see the performance observed for exclusive phases, then we adjust this and run again. It's important to have a good estimation of the expected performance
timewall = 3805 #after this many seconds, the apps will be killed
base_folder = sys.argv[1]

if base_folder[-1] != '/':
    base_folder+= "/"

#get all folders inside the experiment folder (there will be one folder per test)
folders = subprocess.getoutput("ls "+base_folder+" | grep hf |grep lf").split("\n")
to_remove = [folder for folder in folders if ".csv" in folder]
for folder in to_remove:
    folders.remove(folder)
#print(folders)

hf_set10_initial_perf = []
start_times=[]
cpu_time={}
arq = open("results.csv", "w")
arq.write("scheduler;hf_apps;lf_apps;error;rep;max_stretch;gmean_stretch;utilization;io_slowdown\n")
complete = open("detailedresults.csv", "w")
complete.write("scheduler;hf_apps;lf_apps;rep;app;stretch;io_slowdown\n")
ftio_file = open("ftio_results.csv", "w")
ftio_file.write("scheduler;hf_apps;lf_apps;error;rep;app;prediction;period;conf;given_priority\n")
for folder in folders:
    print(folder)
    print("===========================================================================")
    this_folder = folder
    if this_folder[-1] == '/':
        this_folder = this_folder[:len(this_folder)-1]
    assert this_folder[-1] != '/'
    parsed_foldername = this_folder.split('_')
    if len(parsed_foldername) == 5:
        error = int(parsed_foldername[3])
    else:
        error = 0
    phases_file = open(base_folder+this_folder+"_phases.csv", "w")
    phases_file.write("app_id,start_exec,end_exec,start_io,end_io\n")
    stri,all_stretch,all_io_slowdown = parse_folder(base_folder+folder, t_start, t_end, hf_iter, lf_iter, hf_phase_MB, lf_phase_MB, expected_perf, timewall, phases_file, hf_set10_initial_perf, start_times, cpu_time, ftio_file, error)
    assert len(all_stretch) == len(all_io_slowdown)
    parsed = stri.split(';')
    for i in range(len(all_stretch)):
        complete.write(f"{parsed[0]};{parsed[1]};{parsed[2]};{parsed[3]};{i};{all_stretch[i]};{all_io_slowdown[i]}\n")
    arq.write(stri)
    phases_file.close()
arq.close()
complete.close()
ftio_file.close()

stri = get_stats(hf_set10_initial_perf, "MB/s")
print(f"performance for set 0 phases using set-10 scheduler before the first I/O of any set 1 apps: {stri}")
print("====================")
first = min(start_times)
start_times = [t - first for t in start_times]
stri = get_stats(start_times, "")
print(f"variability in the start time of the applications: {stri}")
stri = get_stats(cpu_time[0], "s")
print(f"variability in compute phase duration for set 0: {stri}")
stri = get_stats(cpu_time[1], "s")
print(f"variability in compute phase duration for set 1: {stri}")


