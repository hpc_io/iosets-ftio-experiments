import subprocess
from numpy import mean
from scipy.stats import gmean

debug=False

class Phase:
    def __init__(self, phase):
        self.start = phase[0]
        self.duration = phase[1]
        self.ior_bw = None
        self.ior_duration = None
        self.MB = None
        self.bw = None

    def add_data_amount(self, amount):
        self.MB = amount
        self.bw = amount / self.duration

    def add_ior_perf(self, line):
        parsed = line.strip().split()
        self.ior_bw = float(parsed[1]) #in MB/s
        self.ior_duration = float(parsed[-2]) #in seconds

    def __str__(self):
        if self.bw != None:
            return "("+str(self.start)+", "+str(self.start+self.duration)+", "+str(self.bw)+")"
        else:
            return  "("+str(self.start)+", "+str(self.start+self.duration)+")"

    def outside_timeframe(self, t_start, t_end):
        if (self.start + self.duration) <= t_start:
            if debug:
                print(f"completely before: self.start+self.duration = {self.start+self.duration}, t_start={t_start}")
            #the phase happens completely before the time frame
            return True
        else:
            if self.start >= t_end and debug:
                print(f"completely after: self.start = {self.start}, t_end = {t_end}")
            return (self.start >= t_end) #this is true if the phase starts after the time frame

    def inside_timeframe(self, t_start, t_end):
        return self.start >= t_start and (self.start + self.duration) <= t_end

    def adapt_timeframe(self, t_start, t_end):
        end = self.start + self.duration
        if self.start < t_start:
            assert end > t_start
            self.duration -= t_start - self.start
            self.start = t_start
        end = self.start + self.duration #we recompute because the same phase could follow in both if cases
        if end > t_end:
            assert self.start >= t_start
            self.duration -= end - t_end
        assert self.inside_timeframe(t_start, t_end)
        #we recalculate the amount of bytes written in this phase
        if self.MB != None: #so only relevant for I/O phases
            self.MB = self.bw*self.duration

    @staticmethod
    def get_str(phases):
        ret = "["
        for elem in phases:
            if ret != "[":
                ret +=", "
            ret += str(elem)
        ret+= "]"
        return ret

class AppOutput:
    def __init__(self, filename, id, timewall, setid, io_phase_amount, hf_iter, lf_iter):
        self.id = id
        self.setid = setid
        self.stretch = None
        self.bw = None
        self.io_phase_amount = io_phase_amount
        arq = open(filename, "r")
        contents = arq.readlines()
        arq.close()
        self.compute_phases = []
        self.io_phases = []
        self.waiting_phases = []
        next_is_perf = False
        computing=False
        writing = False
        waiting = False
        self.end = None
        for line in contents:
            if line == "" or line == "\n": #skip blank line
                continue
            if "Starting application at " in line:
                self.start = AppOutput.get_timestamp(line)
                current_start = self.start #the start of the application is the start of the first compute phase
                if(debug):
                    print(f"application started at {self.start}")
                computing = True
            elif "nodes" in line and ":" in line and not("Inodes" in line):
                self.nodes = AppOutput.get_ior_header_field(line)
            elif "tasks" in line and ":" in line and not ("ordering inter" in line):
                self.procs = AppOutput.get_ior_header_field(line)
            elif "repetitions" in line and ":" in line:
                self.iterations = AppOutput.get_ior_header_field(line)
                assert self.iterations == hf_iter or self.iterations == lf_iter
            elif "aggregate filesize" in line and ":" in line:
                parsed= line.split()
                self.data_per_phase = parsed[-2]+parsed[-1]
            elif "Ending compute phase at " in line:
                assert computing
                phase, current_start = AppOutput.get_phase(line, current_start)
                self.compute_phases.append(Phase(phase))
                if debug:
                    print(f"Completed a compute phase {self.compute_phases[-1]}")
                computing = False
                waiting = True
            elif "Starting IO phase at " in line:
                assert waiting
                phase, current_start = AppOutput.get_phase(line, current_start)
                self.waiting_phases.append(Phase(phase))
                if debug:
                    print(f"Completed a waiting phase {self.waiting_phases[-1]}")
                waiting = False
                writing = True
            elif "Ending IO phase at " in line:
                assert writing
                phase, current_start = AppOutput.get_phase(line, current_start)
                self.io_phases.append(Phase(phase))
                self.io_phases[-1].add_data_amount(self.io_phase_amount)
                if debug:
                    print(f"Completed an io phase {self.io_phases[-1]}")
                next_is_perf = True
                writing = False
                computing = True
            elif next_is_perf:
                next_is_perf = False
                assert "write" in line
                assert len(self.io_phases) > 0
                self.io_phases[-1].add_ior_perf(line)
            elif "Ending application at " in line:
                computing = False
                waiting = False
                writing = False
                self.end = AppOutput.get_timestamp(line)
        if self.end == None:
            #the app did not finish normally, it was killed (it actually happens a lot because we stop
            #a little after the desired timeframe, so we don't waste cluster time)
            self.end = self.start + timewall #it is not super precise, but we don't care because we will not be calculating metrics at this point
            if debug:
                print(f"The application did not finish normally, setting its end to {self.end} ({self.start} + {timewall})")
            new_phase = Phase([current_start, self.end - current_start])
            if computing:
                self.compute_phases.append(new_phase)
                if debug:
                    print(f"Completed a compute phase {self.compute_phases[-1]}")
            elif waiting:
                self.waiting_phases.append(new_phase)
                if debug:
                    print(f"Completed a waiting phase {self.waiting_phases[-1]}")
            else:
                assert writing
                #we actually cannot estimate this interrupted I/O phase properly because we do not have the ior result of MB/s. So we have to drop it
                self.end = current_start #we pretend the application stopped earlier, at the beginning of the current phase
                #if we try to measure metrics past this timestamp, we will stop in error anyway
                if debug:
                    print(f"I was writing, so I'll ignore this phase. My end is now {self.end}")
        assert len(self.io_phases) == len(self.compute_phases) or abs(len(self.io_phases)-len(self.compute_phases)) == 1


    def print_app(self):
        print(f"Application {self.id}, {self.procs} processes in {self.nodes} nodes, {self.iterations} iterations writing {self.data_per_phase} per iteration")
        print(f"\tstart: {self.start}")
        computing = [l.duration for l in self.compute_phases]
        print("\tcompute phases (total "+str(sum(computing))+" seconds): "+Phase.get_str(self.compute_phases))
        waiting = [l.duration for l in self.waiting_phases]
        print("\twaiting time (total "+str(sum(waiting))+" seconds): "+Phase.get_str(self.waiting_phases))
        io = [p.duration for p in self.io_phases]
        print("\tIO time (total "+str(sum(io))+" seconds): "+Phase.get_str(self.io_phases))
        print(f"\tend: {self.end}\n")

    def cut_timeframe(self, t_start, t_end):
        if self.end < t_end:
            print(f"app {self.id}")
            print(f"t_end = {t_end}, self.end = {self.end}")
            print(f"{len(self.compute_phases)} compute phases, the last one {self.compute_phases[-1]}")
            print(f"{len(self.waiting_phases)} waiting phases, the last one {self.waiting_phases[-1]}")
            print(f"{len(self.io_phases)} IO phases, the last one {self.io_phases[-1]}")
        assert self.end >= t_end
        if debug:
            print("========================================")
            print(f"app {self.id}")
            print(f"t_start = {t_start}, t_end = {t_end}, self.start = {self.start}, self.end = {self.end}")
            print("Calling cut_timeframe_in_list for compute phases")
            print(f"{len(self.compute_phases)} compute phases, the last one {self.compute_phases[-1]}")
        AppOutput.cut_timeframe_in_list(self.compute_phases, t_start, t_end)
        if debug:
            print("Calling cut_timeframe_in_list for waiting phases")
            print(f"{len(self.waiting_phases)} waiting phases, the last one {self.waiting_phases[-1]}")
        AppOutput.cut_timeframe_in_list(self.waiting_phases, t_start, t_end)
        if debug:
            print("Calling cut_timeframe_in_list for io phases")
            print(f"{len(self.io_phases)} IO phases, the last one {self.io_phases[-1]}")
        AppOutput.cut_timeframe_in_list(self.io_phases, t_start, t_end)
        assert len(self.compute_phases) > 0
        assert len(self.io_phases) > 0

    def calculate_stretch(self, timeframe, expected_perf, hf_iter, lf_iter):
        """
        we assume cut_timeframe has been called beforehand
        """
        #get useful compute time inside data frame
        tcomp = sum([phase.duration for phase in self.compute_phases])
        #get useful amount of data inside data frame
        MB = sum([phase.MB for phase in self.io_phases])
        #calculate how long that should have taken if the application was by itself
        tio = MB / expected_perf
        self.stretch = timeframe / (tcomp + tio) #we take the actual compute time instead of what it should have been because otherwise any variation in compute time (which could happen in practice even if our compute phases are just sleep calls, but are no fault of the scheduler) will count as slow I/O
        actual_io_time = sum([phase.duration for phase in self.io_phases])
        self.bw = MB/actual_io_time
        self.io_slowdown = (timeframe - tcomp)/tio #see comment in the stretch computation above (about tcomp)

    def effective_cpu(self):
        """
        we assume cut_timeframe has been called beforehand
        """
        #get useful compute time inside data frame
        tcomp = sum([phase.duration for phase in self.compute_phases])
        return tcomp


    def write_phases_file(self, arq, first_start):
        stri = ""
        for i in range(min(len(self.io_phases), len(self.compute_phases))):
            c_start = self.compute_phases[i].start-first_start
            c_end = c_start + self.compute_phases[i].duration
            io_start = self.io_phases[i].start - first_start
            io_end = io_start + self.io_phases[i].duration
            stri += f"{self.id},{c_start},{c_end},{io_start},{io_end}\n"
        arq.write(stri)

    def add_phase_cpu_time(self, cpu_time):
        if not self.setid in cpu_time:
            cpu_time[self.setid] = []
        for phase in self.compute_phases[:len(self.compute_phases)-1]: #we don't count the last because we may have just a piece of a phase
            cpu_time[self.setid].append(phase.duration)

    def add_initial_phase_perf(self, perf_list, deadline):
        for phase in self.io_phases:
            if phase.start + phase.duration > deadline:  #we only take the phases that happen before the beginning of the first I/O phase by a low-frequency app
                break
            assert phase.start < deadline and phase.start + phase.duration < deadline
            perf_list.append(phase.bw)

    @staticmethod
    def cut_timeframe_in_list(phases, t_start, t_end):
        to_remove = []
        for phase in phases:
            if phase.outside_timeframe(t_start, t_end):
                if debug:
                    print(f"Removing {phase} because it is out of the timeframe")
                to_remove.append(phase)
            else:
                if debug:
                    print(f"Adapting {phase} because it could be partially out of the timeframe")
                phase.adapt_timeframe(t_start, t_end)
        for phase in to_remove:
            phases.remove(phase)

    @staticmethod
    def get_ior_header_field(line):
        return float(line.split()[-1])

    @staticmethod
    def get_timestamp(line):
        return float(line.split()[-1])

    @staticmethod
    def get_phase(line, current_start):
        end = AppOutput.get_timestamp(line)
        assert end > current_start
        return [current_start, end-current_start], end


class PredictorOutput:
    def __init__(self, filename):
        #read file
        arq = open(filename, "r")
        contents = arq.readlines()
        arq.close()
        #parse it
        self.results = []
        got_prediction = False
        current_result = {}
        for line in contents:
            parsed = line.strip().split()
            if "IOSETS_JOB_ID" in line:
                self.appid = int(parsed[-1])
            elif len(parsed) == 2:
                assert not "period" in current_result
                current_result["period"] = float(parsed[0])
                current_result["conf"] = float(parsed[1])
                got_prediction = True
            elif "Priority = " in line:
                if not got_prediction:
                    current_result["period"] = None
                    current_result["conf"] = None
                got_prediction=False
                current_result["priority"] = float(parsed[2])
                self.results.append(current_result)
                current_result = {}


    def write_to_file(self, ftio_file, hf_apps, lf_apps, rep, error=0):
        i = 0
        for result in self.results:
            if result["period"] == None:
                this_period = ""
                this_conf= ""
            else:
                this_period = str(result["period"])
                this_conf = str(result["conf"])
            ftio_file.write(f"iosetsftio;{hf_apps};{lf_apps};")
            ftio_file.write(f"{error};")
            ftio_file.write(f"{rep};{self.appid-1};{i};{this_period};{this_conf};{result['priority']}\n")
            i += 1




def parse_folder(folder, t_start, t_end, hf_iter, lf_iter, hf_phase_MB, lf_phase_MB, expected_perf, timewall, phases_file, set10_perf, start_times, cpu_time, ftio_file, error=0):
    #get list of files from folder (the ior output ones)
    files = subprocess.getoutput("ls "+folder+"/ioroutput*").split()
    assert len(files) > 0

    #get test information from folder name
    if folder[-1] == '/':
        folder = folder[:len(folder)-1]
    assert folder[-1] != '/'
    only_folder = folder.split('/')[-1]
    #print(only_folder)
    assert "hf" in only_folder and "lf" in only_folder
    parsed_folder = only_folder.strip().split('_')
    sched = parsed_folder[0]
    assert sched in ["noiosets", "iosets", "iosetsftio"]
    hf = int(parsed_folder[1].split('h')[0])
    lf = int(parsed_folder[2].split('l')[0])
    if len(parsed_folder) == 5:
        rep = int(parsed_folder[4])
    else:
        assert len(parsed_folder) == 4
        rep = int(parsed_folder[3])

    #get prediction files from folder (if relevant), read them and write their info to the ftio_results.txt file
    if sched == "iosetsftio":
        predictor_files = subprocess.getoutput("ls "+folder+"/calleroutput*").split()
        assert len(predictor_files) > 0
        for filename in predictor_files:
            this_app = PredictorOutput(filename)
            this_app.write_to_file(ftio_file, hf, lf, rep, error)

    #read files and find the start of the test
    apps = []
    first_start = None
    for filename in files:
        print(filename)
        appint = int(filename.split('.')[0].split('p')[-1])
        app = str(appint)
        if appint < hf:
            setid = 0
            phase_amount = hf_phase_MB
        else:
            setid = 1
            phase_amount = lf_phase_MB
        apps.append(AppOutput(filename, app, timewall, setid, phase_amount, hf_iter, lf_iter))
        if first_start == None or first_start > apps[-1].start:
            first_start = apps[-1].start
    #now first start is the start of our test
    t_start += first_start
    t_end += first_start

#    for a in apps:
 #       a.print_app()
    these_start = [(a.start-first_start) for a in apps]
    start_times.append(max(these_start))

    #print("------------------")
    #print(f"timeframe from {t_start} to {t_end}")
    #now cut in the time frame we'll use for measuring and calculate stretch
    max_stretch = {}
    max_stretch[0] = -100
    max_stretch[1] = -100
    all_stretch= []
    all_io_slowdown = []
    bw = {}
    bw[0] = []
    bw[1] = []
    #find the timestamp of the beginning of the first io phase by a low-frequency app
    start_lf_phases = []
    for app in apps:
        if app.setid == 1:
            starts = [phase.start for phase in app.io_phases]
            if len(starts) > 0:
                start_lf_phases.append(min(starts))
    if len(start_lf_phases) > 0:
        first_io_lf = min(start_lf_phases)
    else:
        first_io_lf = None #there are no low-frequency apps in this test
    e_cpu = 0
    for a in apps:
        a.write_phases_file(phases_file, first_start)
        a.add_phase_cpu_time(cpu_time)
        if sched ==  "iosets" and a.setid == 0 and first_io_lf != None:
            a.add_initial_phase_perf(set10_perf, first_io_lf)
        a.cut_timeframe(t_start, t_end)
        a.calculate_stretch(t_end - t_start, expected_perf, hf_iter, lf_iter)
        if a.stretch > max_stretch[a.setid]:
            max_stretch[a.setid] = a.stretch
        all_stretch.append(a.stretch)
        all_io_slowdown.append(a.io_slowdown)
#        a.print_app()
        bw[a.setid].append(a.bw)
        e_cpu += a.effective_cpu()
    mean_bw = {}
    mean_bw[0] = mean(bw[0])
    mean_bw[1] = mean(bw[1])

#    print("------------------")
 #   print(f"{max_stretch[0]}, {max_stretch[1]}")

    #meanstretch = mean(all_stretch)
    gmeanstretch = gmean(all_stretch)
    maxstretch = max(all_stretch)
    io_slowdown = gmean(all_io_slowdown)
    utilization = e_cpu / (len(apps)*(t_end-t_start))
    #stri = f"{sched};{hf};{lf};{rep};{max_stretch[0]};{max_stretch[1]};{meanstretch};{gmeanstretch};{maxstretch};{utilization};{mean_bw[0]};{mean_bw[1]}\n"
    stri = f"{sched};{hf};{lf};"
    stri += f"{error};"
    stri += f"{rep};{maxstretch};{gmeanstretch};{utilization};{io_slowdown}\n"
    return stri,all_stretch,all_io_slowdown


