#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/mman.h>
#include <math.h>

#define PRIORITY_OF_UNKNOWN_PERIOD 1.0/50.0 //TODO we'll have to decide what is the priority an unknown app should have (while we could never identify its period).
#define PRIORITY_MULTIPLIER 10000  //TODO it has to be big enough so all different priorities will still be different once treated as integers
char *predictor_path = "/root/ftio/predictor.py";
int param_nb = 3;
bool verbose= true;
int art_error = 0;


double previous_period = -1;


void verify(bool cond, char* msg) {
    if(!cond) {
//        fprintf(stderr, "%s\n", msg);
        perror(msg);
        exit(EXIT_FAILURE);
    }
}


void usage(char *cmd) {
    fprintf(stderr, "BEWARE: IT'S REALLY IMPORTANT THAT THIS IS LAUNCHED ***BEFORE*** THE APPLICATION\n");
    fprintf(stderr, "Usage: %s [path to trace file] [path to log file] [artificial error (optional)]\n", cmd);
    fprintf(stderr, "trace file is the one written by tmio\n");
    fprintf(stderr, "log file is the one I'll write with all results I get from the predictor. If it exists, it will be overwritten.\n");
    fprintf(stderr, "IMPORTANT: make sure the environment variable IOSETS_JOB_ID exists and contains an unique job id given to the application (corresponding to the application from which the trace file is coming). This job id MUST be > 0.\n");
    fprintf(stderr, "artificial error can be included in all predictions. In this case, please provide an integer between 0 and 100 (a percentage)\n");
    exit(EXIT_FAILURE);
}

double get_random_period() {
    int value = rand() % 4; //0, 1, 2, or 3
    value = pow(10,value); //1, 10, 100, or 1000
    return (double) value;
}

void put_into_shared_space(char *shared_space, double period, float conf) {
    memcpy(shared_space, &period, sizeof(double));
    memcpy(shared_space + sizeof(double), &conf, sizeof(float));
}

bool spell_short(char *str) {
    return (str[0] == 'S') && (str[1] == 'H') && (str[2] == 'O') && (str[3] == 'R') && (str[4] == 'T') && (str[5] == ' ');
}

/* look for a part of the string that start with the word SHORT followed by a space,
 * return the pointer to the beginning of that line (to the 'S')
 * return NULL if not found
 */
char *grep_short(char *buffer, int size) {
    char *ret = buffer;
    int len = size;
    while((len >= 11) && ret != NULL && !spell_short(ret)) {
        ret += 1;
        len -= 1;
    }
    if (len < 11 || ret == NULL) return NULL;
    else return ret; //found it
}

float get_iosets_priority(double period, float conf, int jobid);

float get_unknown_iosets_priority(int jobid) {
    if (previous_period > 0) return get_iosets_priority(previous_period, 1.0, jobid); //with conf = 1.0, get_iosets_priority is sure to give a result
    //we have no idea of this app's period, we'll have to give it its own set and hope for the best
    float ret = jobid/ 10.0;
    while (ret >= 0.01) ret = ret/10.0;
    ret += PRIORITY_OF_UNKNOWN_PERIOD; //in practice this will be the priority we'll have, the very small contribution of the jobid has the goal of putting applications into separate sets
    return ret;
}

float get_iosets_priority(double period, float conf, int jobid) {
    if (conf < 0.5 || period < 0) return get_unknown_iosets_priority(jobid);
    previous_period = period;
    float ret =  (float)(1.0/pow(10,round(log10(period))));
    assert(ret > 0);
    return ret;
}

void write_to_priority_file(FILE *prio_file, float priority) {
    fprintf(prio_file, "%d", (int)(priority*PRIORITY_MULTIPLIER));
    fflush(prio_file);
}

double impose_artificial_error(double period) {
    if (art_error > 0) {
        double new_period;
        int direction = rand() % 2;
        if (direction == 0) {
            new_period = period - period*art_error/100.0;
            if (period < 0) period = 1;
        } else {
            new_period = period + period*art_error/100.0;
        }
        return new_period;
    } else {
        return period;
    }
}

int main (int argc, char *argv[]) {
    srand(time(NULL));
    //check arguments
    if (argc < param_nb || argc > param_nb + 1) {
        usage(argv[0]);
    }
    if (argc == param_nb + 1) {
        art_error = atoi(argv[param_nb]);
        if (art_error < 0 || art_error > 100)
            usage(argv[0]);
        printf("Warning: I'll induce an error of %d percent on all FTIO predictions\n", art_error);
    }

    //launch the predictor, with its standard output directed to a pipe I'll read
    int pipefd[2];
    int ret = pipe(pipefd);
    verify(ret == 0, "Could not open pipe");
    int pid = fork();
    if (pid == 0) {
        //this is the child
        close(pipefd[0]); //close the reading side
        if (verbose) printf("I will launch the predictor on file %s\n", argv[1]);
        dup2(pipefd[1], STDOUT_FILENO); //redirect stdout to pipe's writing side
        //prepare arguments and run
        char *args[] = {"python3", predictor_path, argv[1], NULL};
        execvp(args[0], args);
        perror("Could not launch predictor!");
    } else {
        //this is the father
        close(pipefd[1]); //close the writing side
        //find the application's job id
        char *var = getenv("IOSETS_JOB_ID");
        verify(var != NULL, "Could not read IOSETS_JOB_ID environment variable");
        int jobid = atoi(var);
        verify(jobid > 0, "Invalid job id! Please notice it must be greater than 0");
        printf("I think I'm working for the application of IOSETS_JOB_ID = %d\n", jobid);
        //create the folder where I'll create the priority file
        char prio_filename[255];
        sprintf(prio_filename, "/sys/kernel/config/iosets/%s", var);
        mkdir(prio_filename, 0777); //we'll NOT test its output because if it exists it's not a big deal, the priority file will be overwritten
        //create the file where I'll write the priority for this application
        prio_filename[0] = '\0';
        sprintf(prio_filename, "/sys/kernel/config/iosets/%s/priority", var);
        if(verbose) printf("I'll open/create the priority file %s!\n", prio_filename);
        FILE *prio_file = fopen(prio_filename, "w");
        verify(prio_file != NULL, "Could not open/create priority file");

        float prio = get_unknown_iosets_priority(jobid);
        printf("Priority = %f (%d)\n", prio, (int)(prio*PRIORITY_MULTIPLIER));
        if(prio_file != NULL)
            write_to_priority_file(prio_file, prio);
        //create the file where I'll write all results
        if (verbose) printf("Now I'll create the log file\n");
        FILE *outfile = fopen(argv[2], "w");
        verify(outfile != NULL, "Could not create log file");
        //an infinite loop, this program has to be killed once the application finished...
        if(verbose) printf("Now I'll wait for information from the predictor\n");
        while(true) {
            //get period and confidence from the predictor
            char buffer[256];
            int bytes_read = read(pipefd[0], buffer, 256);
            verify(bytes_read > 0, "Could not read from pipe (output of the predictor)");
            buffer[bytes_read] = '\0';
            char *line;
            line = grep_short(buffer, bytes_read);
            if (line) {
                double period;
                float conf;
                sscanf(line, "SHORT %lf [%f]\n", &period, &conf);
                if(verbose) printf("%lf %f\n", period, conf);
                //write it to the priority file
                period = impose_artificial_error(period);
                prio = get_iosets_priority(period,conf,jobid);
                if(verbose)
                    printf("Priority = %f (%d)\n", prio, (int)(prio*PRIORITY_MULTIPLIER));
        if(prio_file != NULL)
                write_to_priority_file(prio_file, prio);
            }
            //write it to the log file (which will have ALL the predictor output, not only the SHORT lines)
            int bytes_written = fwrite(buffer,1,bytes_read,outfile);
            verify(bytes_written == bytes_read, "Could not write to log file!");
            fflush(outfile);
        }
        //TODO this program is an infinite loop, we should catch the signal sent by ctrl+c to do all the required cleanup before the end of the program
        fclose(outfile);
        close(pipefd[0]);
        fclose(prio_file);
        int status;
        waitpid(pid,&status,0);
    }

    return EXIT_SUCCESS;
}
