from datetime import datetime
from random import randrange


from file_functions import get_contents,append_to_file
from run_concior_test import IOSetsExperiment

#see hardcoded experiment parameters in run_concior_test.py

#read the tests to run and what was already executed, only keep the tests to run
all_tests = get_contents("iosets_tests.txt")
done = get_contents("iosets_done.txt")
tests = [t for t in all_tests if not t in done]
if len(tests) == 0:
    print("No test to run!")
    exit()

while len(tests) > 0:
    #take one random test from this list
    i = randrange(0,len(tests))
    print(tests[i])
    #run it
    now = datetime.now()
    print(now)
    exp = IOSetsExperiment(tests[i])
    exp.run()
    print("Done...")
    now = datetime.now()
    print(now)
    #remove it from the list and add it to the done file
    append_to_file("iosets_done.txt", tests[i])
    tests.remove(tests[i])






















