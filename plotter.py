import argparse
import os
import plotly.express as px
import pandas as pd

inputCsv = ["results.csv"]

plot_settings = {}
plot_settings["margin"] = dict(l=10, r=10, t=10, b=10)
plot_settings["height"] = 440
plot_settings["width"] = 333
plot_settings["legend"] = legend = dict(
    yanchor="top", y=0.5, xanchor="right", x=0.99)
plot_settings["font"] = dict(size=20, color="black")

x_values=["Set-10 + clairv.", "Set-10 + FTIO", "Set-10 + error", "Original"]

def change_scheduler_column(data):
 #   return data
    data = data.query('(scheduler == "iosetsftio" and error == 50) or error == 0')
    #data["scheduler"] = data["scheduler"] + " " + data["error"].astype(str)
    data.loc[data["scheduler"] == "iosets", "scheduler"] = "Set-10 + clairv."
    data.loc[(data["scheduler"] == "iosetsftio") & (data["error"] == 0), "scheduler"] = "Set-10 + FTIO"
    data.loc[(data["scheduler"] == "iosetsftio") & (data["error"] > 0), "scheduler"] = "Set-10 + error"
    data.loc[data["scheduler"] == "noiosets", "scheduler"] = "Original"
    return data

def read_input():
    all_data = []
    for filename in inputCsv:
        this_data = pd.read_csv(filename, sep=";")
        if not "error" in this_data.columns:
            this_data["error"] = 0
        all_data.append(this_data)
    return change_scheduler_column(pd.concat(all_data))

def format_plot(fig, colorbars=0):
    fig.update_layout(
    plot_bgcolor='white',
        legend=dict(
        bgcolor="rgba(255,255,255,.99)",
        bordercolor="Black",
        borderwidth=1,
    ),
   #     font = {"family": "Courier New, monospace", "size": 24, "color": "black"},
    )

    fig.update_xaxes(
        ticks='outside',
        # tickwidth=1,
      #  ticklen=10,
        showgrid=True,
        # gridwidth=1,
        mirror=True,
        showline=True,
        linecolor='black',
        gridcolor='lightgrey',
        tickangle=45
#        minor_ticks='outside',
    #    minor=dict(ticklen=2)
    )
    fig.update_yaxes(
        ticks='outside',
        # tickwidth=1,
#        ticklen=10,
        showgrid=True,
        # gridwidth=1,
        mirror=True,
        showline=True,
        linecolor='black',
        gridcolor='lightgrey'#,
 #       minor_ticks='outside',
  #      minor=dict(ticklen=2)
    )


def max_stretch(data, output_dir):
    fig = px.box(data, x="scheduler", y="max_stretch")
    fig.update_layout(yaxis_title="Max stretch",
                      xaxis_title="")
    fig.update_xaxes(categoryorder="array", categoryarray=x_values)
    fig.update_yaxes(range=[1, 2], row=1, col=1, title_standoff = 15)
    format_plot(fig)
    fig.update_layout(**plot_settings)
    fig.write_image(f"{output_dir}/new_max_stretch.png")

def gmean_stretch(data, output_dir):
    fig = px.box(data, x="scheduler", y="gmean_stretch")
    fig.update_layout(yaxis_title="Stretch",
                      xaxis_title="")
    fig.update_xaxes(categoryorder="array", categoryarray=x_values)
    fig.update_yaxes(range=[1, 1.6], row=1, col=1, title_standoff = 15)
    format_plot(fig)
    fig.update_layout(**plot_settings)
    fig.write_image(f"{output_dir}/new_gmean_stretch.png")

def utilization(data, output_dir):
    fig = px.box(data, x="scheduler", y="utilization")
    fig.update_layout(yaxis_title="Utilization",
                      xaxis_title="")
    fig.update_xaxes(categoryorder="array", categoryarray=x_values)
    fig.update_yaxes(range=[0.6, 0.9], row=1, col=1, dtick=0.05, title_standoff = 15)
    format_plot(fig)
    fig.update_layout(**plot_settings)
    # fig.update_layout(hight=440)
    fig.write_image(f"{output_dir}/new_utilization.png")

def io_slowdown(data, output_dir):
    fig = px.box(data, x="scheduler", y="io_slowdown")
    fig.update_layout(yaxis_title="I/O slowdown",
                      xaxis_title="")
    fig.update_xaxes(categoryorder="array", categoryarray=x_values)
    fig.update_yaxes(range=[1, 8], row=1, col=1, title_standoff = 25)
    format_plot(fig)
    fig.update_layout(**plot_settings)
#    fig.update_layout(width=346)
    fig.write_image(f"{output_dir}/new_io_slowdown.png")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate plots from IOSets+FTIO experiment")
    parser.add_argument('-o', '--outputDir', type=str,
                        required=True, help="Directory where the plots will be written")
    args = parser.parse_args()
    if not os.path.exists(args.outputDir):
        os.makedirs(args.outputDir)

    data = read_input()
    max_stretch(data, args.outputDir)
    gmean_stretch(data, args.outputDir)
    utilization(data, args.outputDir)
    io_slowdown(data, args.outputDir)
